# Copyright (C) 2020, Inria
# GRAPHDECO research group, https://team.inria.fr/graphdeco
# All rights reserved.
# 
# This software is free for non-commercial, research and evaluation use 
# under the terms of the LICENSE.md file.
# 
# For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr


set(PROJECT_PAGE "facade_repetitionsPage")
set(PROJECT_LINK "https://gitlab.inria.fr/sibr/projects/facades-repetitions/facade_repetitions")
set(PROJECT_DESCRIPTION "Exploiting Repetitions for IBR of Facades (paper reference :http://www-sop.inria.fr/reves/Basilic/2018/RBDD18/)")
set(PROJECT_TYPE "OURS")