/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */



#include <fstream>


#include <projects/facade_repetitions/renderer/ULRRepetitionsView.hpp>
#include <core/graphics/Shader.hpp>
#include <core/view/UIShortcuts.hpp>
#include "core/renderer/TexturedMeshRenderer.hpp"
#include "core/view/InteractiveCameraHandler.hpp"
#include "core/view/MultiViewManager.hpp"
#include "core/view/SceneDebugView.hpp"
#include "projects/facade_repetitions/renderer/RepetitionsScene.hpp"

#define PROGRAM_NAME "sibr_ulr_repetitions_app"
using namespace sibr;


const char* usage = ""
	"Usage: " PROGRAM_NAME " --path <dataset-path>" "\n"
	"\t\t--rendering-size <W> <H> Resolution rendering (default to 1280x800)" "\n"
	"\t\t--new Load a new SIBR dataset (can also be use in preprocess)" "\n"
	;

int main( int ac, char** av )
{

	CommandLineArgs::parseMainArgs(ac, av);
	RepetitionsArgs args;

	Window		window(PROGRAM_NAME, args);

	uint rw = args.rendering_size.get()[0];
	uint rh = args.rendering_size.get()[1];
	
	
	// Full scene.
	sibr::BasicIBRScene::Ptr scene;
	if (args.newLayout) {
		scene.reset(new sibr::BasicIBRScene(args, true));
	}
	else {
		scene.reset(new sibr::RepetitionsScene(args.dataset_path));
	}
	
	if (rw == 0 || rh == 0) {
		rw = 1280;
		rh = 800;
	}
	// The input RTs will be init when the proxy is loaded, not before (empty proxy).
	MultiViewManager viewManager(window);

	ULRRepetitionsView::Ptr	ulrView(new ULRRepetitionsView(scene, rw, rh));
	

	// The interactive camera can use a raycaster for additional interactions.
	std::shared_ptr<sibr::Raycaster> raycaster = std::make_shared<sibr::Raycaster>();
	raycaster->init();
	raycaster->addMesh(scene->proxies()->proxy());

	// Setup the  interactive camera.
	sibr::InteractiveCameraHandler::Ptr generalCamera(new InteractiveCameraHandler());
	sibr::InputCamera startCamera = *scene->cameras()->inputCameras()[0];
	const Viewport sceneViewport(0, 0, float(rw), float(rh));
	generalCamera->setup(scene->cameras()->inputCameras(), sceneViewport, raycaster);
	generalCamera->setupInterpolationPath(scene->cameras()->inputCameras());

	// Add the view and camera to the manager.
	viewManager.addIBRSubView("Repetitions", ulrView, { rw, rh });
	viewManager.addCameraForView("Repetitions", generalCamera);

	// Create and add a debug view.
	const std::shared_ptr<sibr::SceneDebugView>	topView(new SceneDebugView(scene, generalCamera, args));
	viewManager.addSubView("Top view", topView);
	
	while (window.isOpened())
	{
		sibr::Input::poll();
		window.makeContextCurrent();

		if (sibr::Input::global().key().isPressed(sibr::Key::Escape))
			window.close();

		viewManager.onUpdate(sibr::Input::global());
		
		window.viewport().bind();
		viewManager.onRender(window);
		window.swapBuffer();
	}

	return EXIT_SUCCESS;
	

}
