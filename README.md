# Exploiting Repetitions for IBR of Facades              {#facade_repetitionsPage}

This project is the reference implementation of [Rodriguez et al. 18] *Exploiting Repetitions for Image-Based Rendering of Facades*, (http://www-sop.inria.fr/reves/Basilic/2018/RBDD18/).

If you use the code, we would greatly appreciate it if you could cite the corresponding paper:

```
@Article{RBDD18,
  author       = "Rodriguez, Simon and Bousseau, Adrien and Durand, Fr\'edo and Drettakis, George",
  title        = "Exploiting Repetitions for Image-Based Rendering of Facades",
  journal      = "Computer Graphics Forum (Proceedings of the Eurographics Symposium on Rendering)",
  number       = "4",
  volume       = "37",
  year         = "2018",
  keywords     = "Image-based rendering, reconstruction, repetitions",
  url          = "http://www-sop.inria.fr/reves/Basilic/2018/RBDD18"
}
```

and of course the actual *sibr* system:

```
@misc{sibr2020,
   author       = "Bonopera, Sebastien and Hedman, Peter and Esnault, Jerome and Prakash, Siddhant and Rodriguez, Simon and Thonat, Theo and Benadel, Mehdi and Chaurasia, Gaurav and Philip, Julien and Drettakis, George",
   title        = "sibr: A System for Image Based Rendering",
   year         = "2020",
   url          = "https://gitlab.inria.fr/sibr/sibr_core"
}
```

--- 
## Authors

Simon Rodriguez, Adrien Bousseau, Frédo Durand, George Drettakis.

---

## How to use

### Binary distribution

The easiest way to use *SIBR* is to download the binary distribution. All steps described below, including all preprocessing for your datasets will work using this code.

Download the precompiled distribution from the page: https://sibr.gitlabpages.inria.fr/download.html (Facade Repetitions 140Gb); unzip the file and rename the directory "install". 


### Checkout & SIBR build

Clone the core SIBR repository and add the current repository as a project.

For this use the following commands:

```bash
## through HTTPS
git clone https://gitlab.inria.fr/sibr/sibr_core.git
## through SSH
git clone git@gitlab.inria.fr:sibr/sibr_core.git
```

Then go to *src/projects* and clone the current project:

```bash
## through HTTPS
git clone https://gitlab.inria.fr/sibr/projects/facades-repetitions/facade_repetitions.git
## through SSH
git clone git@gitlab.inria.fr:sibr/projects/facades-repetitions/facade_repetitions.git
```

Enable projects in Cmake (`BUILD_IBR_FACADE_REPETITIONS`) before running the configuration (see [Configuring](https://sibr.gitlabpages.inria.fr/docs/develop/index.html#sibr_configure_cmake)), then INSTALL everything (see [Compiling](https://sibr.gitlabpages.inria.fr/docs/develop/index.html#sibr_compile)).

You can then clone the `facade_repetitions_external` repository (650MB) from https://gitlab.inria.fr/sibr/projects/facades-repetitions/facade_repetitions_external in a separate directory. It will contains 

- the additional required binaries.
- Pix-2-Pix classifications weights: unzip `pix2pix-weights/model-data.7z` and rename to `model-400000.data-00000-of-00001`
- KIPPI distribution: unzip the `kippi/kippi-redist.7z` archive.


### Required softwares

- Python 3.
- ImageMagick.
- Tensorflow (1.0.1) installed. I used a self-contained Anaconda environment, but had to use it from the Window command-line, and not Cygwin.
- OpenMVG modified version compiled and installed (available in the repo above).

### Paths to update in the scripts
The scripts are located into `preprocess/scripts/`.

In `pipeline-1.py`:
- `SIBR_INSTALL_PATH`, ex: `D:/sirodrig/code/sibr/install/bin/`
- `OPENMVG_OUT_PATH`, ex: `D:/sirodrig/code/openMVG/out/`
- `IMAGE_MAGICK_BIN`, ex: `\"C:/Program Files/ImageMagick-6.9.2-Q16/convert.exe\"` (has to be escaped)

In `pipeline-2.py`:
- `SIBR_INSTALL_PATH`, ex: `D:/sirodrig/code/sibr/install/bin/`

In `pipeline-3.py`:
- `SIBR_INSTALL_PATH`, ex: `D:/sirodrig/code/sibr/install/bin/`

In `SfM_SequentialPipeline.py` and `SfM_FilteredSequentialPipeline.py`:
- `OPENMVG_OUT_PATH`, ex: `D:/sirodrig/code/openMVG/out`



## How to preprocess the dataset

Run all commands from the root of the `facade_repetitions` repository.

> To process datasets in the new SIBR scene layout, add `--new` as an argument to each preprocess executable in all scripts.

### Scene input layout

Input data is a set of 3 to 5 photographs of a building facade, put in an `images` directory.

### Windows detection

1.) Prepare data for tensorflow. Note that images should be in `path/to/scene_dir/images`

    python scripts/pix2pix-process.py --input_dir path/to/scene_dir/

This will output a command to copy and execute in an environment where tensorflow is available, looking like:

    python scripts/pix2pix-classif.py --mode test --checkpoint pix2pix-weights/ --input_dir path/to/images/directory/rectified/ --output_dir path/to/images/pix2pix/


2.) Execute the above command. Make sure the `pix2pix-weights/` directory  exists and contains the model, checkpoint, options for tensorflow.


3.) Tensorflow output post process.

    python scripts/pix2pix-process.py --input_dir path/to/scene_dir --post_process


### Windows extraction and scene reconstruction

4.) Then select the images, corresponding labels (black images) and matrices that you want to use, and put them in a directory with the same hierarchy.
The directory tree should be

    - scene_dir/
        - images/
            scene-XXXX.jpg
        - labels/
        - matrices/


5.) Full scene reconstruction and plane fitting (output to `plane_data.txt`)

    python scripts/pipeline-1.py C:/path/to/scene_dir --full


6.) Crops selection and extraction. The plane selection interface will load the automatic plane by default, you can then redefine it if needed.  
Usage: Select three points. Maj+click to select a point. Press F to visualize the plane. New points can be selected to correct mistakes. In all cases, press H to export the platonic cameras initial estimation. Platonic scene calibration and reconstruction will then take place.

    python scripts/pipeline-1.py C:/path/to/scene_dir --crops


7.) RealityCapture interlude.
Launch RC, "File > Open". Set the type to "Bundle file". Open the bundle located in `scene_dir/PMVS/RC/`. 
Perform mesh reconstruction only, colorize and export the mesh in the same directory as `mesh.ply`.


### Platonic element processing

8.) Scene first generation, selection of the fronto-parallel view.

    python scripts/pipeline-2.py C:/path/to/scene_dir --select


9.) Launch `Kinetic-Partition.exe`, load the image present in the `scene_dir/segmentation` directory. Set the following settings: `S=0.55`, `Log-e=4.0`, `D=0.6`, allow for conditional propagation. 
Run the partitioning and save the result in the same directory. (The next utility will look for any `XXXX_graph.txt` file in the segmentation directory.)


10.) Mesh refinement and layers extraction, generation of the final stitched views.
  
    python scripts/pipeline-2.py C:/path/to/scene_dir --refine


11.) Mask generation and final scene generation. The resulting scene will be in `scene_dir/IBR_fullScene_eq/`. It can be visualized with `FacadeRepetitionsApp (see below)`.
  
    python scripts/pipeline-2.py C:/path/to/scene_dir --final


### Scene merging

12.) To merge multiple scenes, use `pipeline-3.py` after processing the two scenes. The merged scene will be output in the `scene_dir1/IBR_mergeScene_eq` directory.
  
    python scripts/pipeline-3.py C:/path/to/scene_dir1 C:/path/to/scene_dir2


### Final dataset layout
	
	- masks/				window bounding box masks
	- masks_auto/			generated binary masks
	- pmvs/					contains the scene geometry
	- *.jpg					input images
	- bundle.out			camera calibration
	- clipping_planes.txt 	camera clipping planes
	- envmap.jpg			existing envmap for reflection synthesis
	- list_images.txt 		camera image list
	- plane.ply				extracted facade plane
	- plane_data.txt		points defining the plane
	- proxy_rc.ply			reference scene geometry for comparison
	- proxy_seg.ply			reference scene geoemtry segmented for comparison


## Running
  
Run (from `sibr/install/bin`):

 	./FacadeRepetitionsApp.exe --path path/to/IBR_scene_eq/ --width 1280 --height 800

An example dataset is in the auxiliary repository, in `datasets/ucl1.7z`. The final datasets can be found here: https://repo-sam.inria.fr/fungraph/facade-repetitions/datasets/

