/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#ifndef SIBR_REPETITIONS_MEDIAN_BILATERAL_FILTER_H
#define SIBR_REPETITIONS_MEDIAN_BILATERAL_FILTER_H



#include <core/assets/InputCamera.hpp>

/** MedianBilateralFilter was designed for depthmap processing and 
* thus ony handles CV_32F matrices and use the 3rd channel as depth.
* //TODO SR: generalize.
*/

class MedianBilateralFilter {
public:

	MedianBilateralFilter(const cv::Mat & image, const cv::Mat & guide, const cv::Mat & mask);

	void median(const float sigmac, const int halfSize = 16);

	void cross(const float sigmac, const float delta, const float sigmads);

	cv::Mat getResult(){ return _finalResult; }

private:

	cv::Mat _image;
	cv::Mat _guide;
	cv::Mat _mask;
	cv::Mat _finalResult;
	

};

#endif // SIBR_REPETITIONS_MEDIAN_BILATERAL_FILTER_H