/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#include "MedianBilateralFilter.hpp"

MedianBilateralFilter::MedianBilateralFilter(const cv::Mat & image, const cv::Mat & guide, const cv::Mat & mask)
{
	_image = image.clone();
	_guide = guide.clone();
	_mask = mask.clone();
	 _finalResult = _image.clone();
}

void MedianBilateralFilter::median(const float sigmac, const int halfSize)
{

	struct WeightDepth {
		float weight;
		float depth;

		WeightDepth(const float w, const float d) {
			weight = w;
			depth = d;
		}

		bool operator<(const WeightDepth & rhs) const {
			return depth < rhs.depth;
		}
	};

	const float sigmae = 2.0f * halfSize;
	
	_finalResult = _image.clone();
	const  int wI = (int)_image.cols;
	const  int hI = (int)_image.rows;


	
	for( int y = 0; y < hI; ++y){
		for ( int x = 0; x < wI; ++x) {
			std::vector<WeightDepth> samples;
			std::vector<cv::Vec3f> colors;
			colors.push_back(_guide.at<cv::Vec3f>(y, x));
			float depth = _image.at<cv::Vec3f>(y, x)[2];
			if (_mask.at<uchar>(y, x) < 128) {
				continue;
			}

			for (int dy = -halfSize; dy <= halfSize; ++dy){
				for (int dx = -halfSize; dx <= halfSize; ++dx){
					const int nx = x + dx;
					const int ny = y + dy;
					if (nx >= wI || ny >= hI || nx < 0 || ny < 0) {
						continue;
					}
					if (_mask.at<uchar>(ny, nx) < 128) {
						continue;
					}

					float colorDiff = 0.0f;
					int colorId = 0;

					cv::Vec3f nColor = _guide.at<cv::Vec3f>(ny, nx);
					float nDepth = _image.at<cv::Vec3f>(ny, nx)[2];
					colorDiff = (float)cv::norm(nColor - colors[colorId], cv::NormTypes::NORM_L2SQR);
					++colorId;

					const float weight = std::exp(-colorDiff / sigmac - (dx*dx + dy*dy) / sigmae);
					samples.push_back(WeightDepth(weight, nDepth));
				}
			}
			if (samples.empty()) {
				continue;
			}
			std::sort(samples.begin(), samples.end());
			for (size_t j = 1; j < samples.size(); ++j) {
				samples[j].weight += samples[j - 1].weight;
			}
			float target = samples[samples.size() - 1].weight / 2.0f;
			size_t best_fit = 0;
			for (size_t j = 0; j < samples.size(); ++j) {
				if (samples[j].weight > target)
					break;
				best_fit = j;
			}
			float finalDepth = samples[best_fit].depth;
			_finalResult.at<cv::Vec3f>(y, x)[2] = finalDepth;
		}
	}

	
}

void MedianBilateralFilter::cross(const float sigmac, const float deltaDepth, const float sigmads)
{
	_finalResult = _image.clone();
	const int wI = (int)_image.cols;
	const int hI = (int)_image.rows;

	const int windowSize = 8;
	const float sigmae = 2.0 * windowSize;

	const float sigmad = deltaDepth * deltaDepth * sigmads;

	cv::Mat discontinuityMap(hI, wI, CV_8UC1, cv::Scalar(0));
	for (int y = 0; y < hI; ++y) {
		for (int x = 0; x < wI; ++x) {
			
			if (_mask.at<uchar>(y, x) < 128) {
				continue;
			}

			//_guide.at<cv::Vec3f>(y, x));
			float depth = _image.at<cv::Vec3f>(y, x)[2];
			cv::Vec3f color = _guide.at<cv::Vec3f>(y, x);

			int discontinuities = 0;
			for (int dy = -windowSize; dy <= windowSize; ++dy) {
				for (int dx = -windowSize; dx <= windowSize; ++dx) {
					const int nx = x + dx;
					const int ny = y + dy;
					if (nx >= wI || ny >= hI || nx < 0 || ny < 0) {
						continue;
					}
					if (_mask.at<uchar>(ny, nx) < 128) {
						continue;
					}

					const float nDepth = _image.at<cv::Vec3f>(ny, nx)[2];
					if (abs(depth - nDepth) > deltaDepth) {
						++discontinuities;
					}
				}
			}

			

			if (discontinuities > 8) {
				discontinuityMap.at<uchar>(y, x) = (uchar)std::min(255, 255);
				float finalDepth = 0.0f;
				float denom = 0.0f;
				// We are on a split edge, bilateral smoothing.
				for (int dy = -windowSize; dy <= windowSize; ++dy) {
					for (int dx = -windowSize; dx <= windowSize; ++dx) {
						const int nx = x + dx;
						const int ny = y + dy;
						if (nx >= wI || ny >= hI || nx < 0 || ny < 0) {
							continue;
						}
						if (_mask.at<uchar>(ny, nx) < 128) {
							continue;
						}

				
						const cv::Vec3f nColor = _guide.at<cv::Vec3f>(ny, nx);
						const float nDepth = _image.at<cv::Vec3f>(ny, nx)[2];
						const float colorDiff = (float)cv::norm(nColor - color, cv::NormTypes::NORM_L2SQR);
		
						const float weight = std::exp(-colorDiff / sigmac - (dx*dx + dy*dy) / sigmae - 0.0f*std::pow(nDepth - depth, 2.0f) / sigmad);
						
						finalDepth += weight * nDepth;
						denom += weight;
					}
				}
				if (denom == 0.0f) {
					denom = 1.0f;
				}
				finalDepth /= denom;
				_finalResult.at<cv::Vec3f>(y, x)[2] = finalDepth;
			}
		}
	}


	/*cv::imshow("tr", discontinuityMap);
	cv::waitKey(0);
*/

}
