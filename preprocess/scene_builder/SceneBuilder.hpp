/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#ifndef SIBR_REPETITIONS_SCENEBUILDER_H
#define SIBR_REPETITIONS_SCENEBUILDER_H

#include <projects/facade_repetitions/renderer/FullScene.hpp>
#include <projects/facade_repetitions/renderer/ReprojectionScene.hpp>

#include <core/assets/InputCamera.hpp>


struct VirtualCamera {
	sibr::InputCamera camera;
	size_t id;
	size_t instance;
	bool full;
};

class SceneBuilder
{

public:

	SceneBuilder(const std::vector<std::shared_ptr<FullScene>> & fullScenes, const std::vector<std::shared_ptr<ReprojectionScene>> & instanceScenes);

	~SceneBuilder(void);

	void estimateSharedTransformations(const std::string & viewPlanesPath = "");

	void exportScene(const std::string & outputPath, const float holeScaling);

	void filterPlatonicMesh(const std::string & initialScenePath, const std::string & masksPath, const std::string & outputPath, const bool visu, const bool useSuperpixels);

	int getSelectedCamera();

private:

	struct InstanceInfos {
		std::shared_ptr<ReprojectionScene> scene;
		std::shared_ptr<FullScene> fullScene;
		sibr::Matrix4f estimatedTransformation;
		std::vector<sibr::Vector3f> intersections;
		std::vector<std::vector<size_t>> groups;
		std::vector<sibr::Vector3f> centers;
		std::vector<VirtualCamera> finalCameras;
		sibr::Mesh::Ptr cleanedProxy;
	};


	std::vector<sibr::Vector3f> computeRealIntersections(const ReprojectionScene::Ptr instanceScene);

	void estimateSharedTransformation(InstanceInfos & infos, const std::string & viewPlanesPath);

	void mergeInstances(InstanceInfos & scene);

	void computeFinalCamerasForScene(InstanceInfos & instance);

	void generateFinalSceneMesh(const std::string & outputPath, const float holeScaling);

	

	static void triangulate(const Eigen::MatrixXd & V, const Eigen::MatrixXi & E, const Eigen::MatrixXd & H, const std::string flags, Eigen::MatrixXd & V2, Eigen::MatrixXi & F2);

	std::vector<InstanceInfos> _instanceScenes;
	
};

#endif // SIBR_REPETITIONS_SCENE_BUILDER_H
