/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#include <iostream>
#include "SceneBuilder.hpp"

#include <core/raycaster/CameraRaycaster.hpp>
#include <core/system/Utils.hpp>
#include <Eigen/Eigenvalues>

#include <projects/facade_repetitions/renderer/PlatonicMeshRefinement.hpp>




#define ANSI_DECLARATORS
#define REAL double
#define VOID int
#define NO_TIMER
extern "C" {
#include "triangle.h"
}


SceneBuilder::SceneBuilder(const std::vector<std::shared_ptr<FullScene>>& fullScenes, const std::vector<std::shared_ptr<ReprojectionScene>> & instanceScenes)
{
	// Technically, when handling multiple scenes, they should refer to the same fullScene. 
	// BUT the fullScene object will also refer to the estimation of the platonic cameras 
	// related to each instance. So in practice they will load different crops infos from disk,
	// and have to be kept distinct.
	for (auto scene : instanceScenes) {
		_instanceScenes.emplace_back();
		auto & currentInstance = _instanceScenes.back();
		currentInstance.scene = scene;
		currentInstance.fullScene = fullScenes[_instanceScenes.size() - 1];
		currentInstance.estimatedTransformation = sibr::Matrix4f::Zero();
		currentInstance.intersections = computeRealIntersections(scene);
	}
	
}

std::vector<sibr::Vector3f> SceneBuilder::computeRealIntersections(const ReprojectionScene::Ptr instanceScene){

	// Setup raycaster.
	std::shared_ptr<sibr::Raycaster> raycaster = std::shared_ptr<sibr::Raycaster>(new sibr::Raycaster());
	if (!raycaster->init()) {
		SIBR_ERR << " failed raycaster init in constructor " << std::endl;
		return std::vector<sibr::Vector3f>();
	}
	raycaster->addMesh(instanceScene->proxy());

	// for each real camera, get its intersection with the proxy.
	// To alleviate issues caused by window recess/shutters/..., we sample points on the border.
	// We know that the crop was extended around the initial detected window, by a 1.5 factor. 
	// This leaves a margin of around 0.25, and we sample in the middle of it.

	std::vector<sibr::Vector3f> estimatedIntersections;
	for (size_t cid = 0; cid < instanceScene->inputCameras().size(); ++cid) {
		auto & cam = *instanceScene->inputCameras()[cid];
		const int samplesPerSideCount = 4;
		const int leftMargin = (int)(0.2f*cam.w());
		const int upMargin = (int)(0.2f*cam.h());

		const int wStride = (cam.w() - 2 * leftMargin) / (samplesPerSideCount - 1);
		const int hStride = (cam.h() - 2 * upMargin) / (samplesPerSideCount - 1);

		std::vector<sibr::Vector3f> surfaceSamples;
		for (int i = 0; i < samplesPerSideCount; ++i) {
			for (int j = 0; j < samplesPerSideCount; ++j) {
				if (i == 0 || j == 0 || i == samplesPerSideCount - 1 || j == samplesPerSideCount - 1) {
					sibr::Vector2f pixel(leftMargin + i * wStride, upMargin + j * hStride);
					const sibr::Vector3f d = sibr::CameraRaycaster::computeRayDir(cam, pixel);
					sibr::RayHit hit = raycaster->intersect(sibr::Ray(cam.position(), d));
					if (hit.hitSomething()) {
						sibr::Vector3f intersection(cam.position() + hit.dist()*d);
						surfaceSamples.push_back(intersection);
					} 
				}
			}
		}
		
		sibr::Vector3f averagePoint(0.0f, 0.0f, 0.0f);
		sibr::Vector3f centerPoint(0.0f, 0.0f, 0.0f);
		for (auto & sample : surfaceSamples) {
			averagePoint += sample;
		}
		averagePoint /= (float)surfaceSamples.size();
		if (surfaceSamples.size() < 5) {
			std::cout << "Low number of samples." << std::endl;
		}
		const sibr::Vector3f d = cam.dir().normalized();
		sibr::RayHit hit = raycaster->intersect(sibr::Ray(cam.position(), d));
		if (hit.hitSomething()) {
			centerPoint = sibr::Vector3f(cam.position() + hit.dist()*d);
		}
		else {
			std::cerr << "Shouldn't happen here" << std::endl;
			assert(false);
		}
		// We need to find the intersection of the view direction d with the plane defined by the average 
		// intersection point and the vector between this point and the real intersection point.
		const sibr::Vector3f normal = (averagePoint - centerPoint).normalized();
		const float lambda = normal.dot(averagePoint - cam.position()) / normal.dot(d);
		const sibr::Vector3f averagedIntersection = cam.position() + lambda * d;
		estimatedIntersections.push_back(averagedIntersection);
	}

	return estimatedIntersections;
}

void SceneBuilder::estimateSharedTransformations(const std::string & viewPlanesPath) {
	for (auto & scene : _instanceScenes) {
		estimateSharedTransformation(scene, viewPlanesPath);
	}
}

void SceneBuilder::estimateSharedTransformation(InstanceInfos & instance, const std::string & viewPlanesPath) {

	std::vector<sibr::Matrix4f> transformations;
	std::vector<float> scalings;

	// Now we find for each pair estimated/real cameras, the transformation between the two.
	for (size_t i = 0; i < instance.fullScene->estimatedCameras.size(); ++i) {

		auto & realCamera = *instance.scene->inputCameras()[i];
		auto & estimatedCamera = instance.fullScene->estimatedCameras[i].camera;
		sibr::Vector3f & realIntersection = instance.intersections[i]; 
		sibr::Vector3f & estimatedIntersection = instance.fullScene->estimatedCameras[i].intersection;// always the same.

		sibr::Vector3f a = realIntersection - realCamera.position();
		sibr::Vector3f b = estimatedIntersection - estimatedCamera.position();
		scalings.push_back(b.norm() / a.norm());

		const float fovAdjustment = tan(estimatedCamera.fovy() * 0.5f) / tan(realCamera.fovy() * 0.5f);
		sibr::Matrix4f scale;
		scale.setIdentity();
		scale(0, 0) = scale(1, 1) = scale(2, 2) = scalings[i] * fovAdjustment;

		sibr::Matrix4f t1 = sibr::matFromTranslation((realCamera.position() - realIntersection).eval());
		sibr::Matrix4f t2 = sibr::matFromTranslation((estimatedIntersection - estimatedCamera.position()).eval());
		sibr::Matrix4f transf = t2 * estimatedCamera.view().inverse() * scale * realCamera.view() * t1;

		transformations.push_back(transf);
	}

	if (viewPlanesPath.size() > 0) {
		sibr::Vector3f p0 = instance.fullScene->facadePlane.p0;
		sibr::Vector3f p1 = instance.fullScene->facadePlane.p1;
		sibr::Vector3f p2 = instance.fullScene->facadePlane.p2;

		for (size_t i = 0; i < instance.fullScene->estimatedCameras.size(); ++i) {
			sibr::Matrix4f transf = transformations[i];
			sibr::Vector3f plan0 = (transf.inverse() * sibr::Vector4f(p0.x(), p0.y(), p0.z(), 1.0f)).block<3, 1>(0, 0);
			sibr::Vector3f plan1 = (transf.inverse() * sibr::Vector4f(p1.x(), p1.y(), p1.z(), 1.0f)).block<3, 1>(0, 0);
			sibr::Vector3f plan2 = (transf.inverse() * sibr::Vector4f(p2.x(), p2.y(), p2.z(), 1.0f)).block<3, 1>(0, 0);
			sibr::Vector3f plan3 = plan0 + (plan1 - plan0) + (plan2 - plan0);

			sibr::Mesh plane(false);
			std::vector<sibr::Vector3f> vertices;
			std::vector<sibr::Vector3u> indices;
			vertices.push_back(plan0);
			vertices.push_back(plan1);
			vertices.push_back(plan2);
			vertices.push_back(plan3);
			sibr::Vector3u f0(0, 1, 2);
			sibr::Vector3u f1(2, 1, 3);
			indices.push_back(f0);
			indices.push_back(f1);
			plane.vertices(vertices);
			plane.triangles(indices);
			plane.save(viewPlanesPath + "/plane-" + std::to_string(i) + ".ply", true);
		}
	}

	// Compute average transformation.
	sibr::Matrix4f averageTransformation;
	averageTransformation.setZero();
	for (size_t i = 0; i < instance.fullScene->estimatedCameras.size(); ++i) {
		// translate to (0,0,0).
		sibr::Matrix4f shiftedTransformation = sibr::matFromTranslation((-instance.fullScene->estimatedCameras[i].intersection).eval()) * transformations[i];
		averageTransformation += shiftedTransformation;
		// maybe avoid participation from transformations associated to an instance with only one view ?
		// or medium transf using frobenius norm ?
	}
	instance.estimatedTransformation = (1.0f / (float)instance.fullScene->estimatedCameras.size()) * averageTransformation;

	mergeInstances(instance);
}

void SceneBuilder::mergeInstances(InstanceInfos & instance) {
	// Transform proxy with avg transfo compute bounding box.
	sibr::Vector3f pos0 = instance.scene->proxy().vertices()[0];
	sibr::Vector3f posnn0 = (instance.estimatedTransformation * sibr::Vector4f(pos0[0], pos0[1], pos0[2], 1.0f)).xyz();
	sibr::Vector3f minBox(posnn0);
	sibr::Vector3f maxBox(posnn0);
	for (size_t pid = 1; pid < instance.scene->proxy().vertices().size(); ++pid) {
		sibr::Vector3f pos = instance.scene->proxy().vertices()[pid];
		sibr::Vector3f posnn = (instance.estimatedTransformation * sibr::Vector4f(pos[0], pos[1], pos[2], 1.0f)).xyz();
		minBox = minBox.cwiseMin(posnn);
		maxBox = maxBox.cwiseMax(posnn);
	}
	const float sideBox = (maxBox - minBox).norm() / sqrt(3.0f);

	// For each window instance, find the average center.
	instance.centers = std::vector<sibr::Vector3f>(instance.fullScene->windowsCount, sibr::Vector3f(0.0f, 0.0f, 0.0f));
	for (size_t wi = 0; wi < instance.fullScene->windowsCount; ++wi) {
		size_t count = 0;
		for (size_t ci = 0; ci < instance.fullScene->estimatedCameras.size(); ++ci) {
			// translate to (0,0,0).
			if (instance.fullScene->windowsIds[ci].first == wi) {
				instance.centers[wi] += instance.fullScene->estimatedCameras[ci].intersection;
				++count;
			}
		}
		if (count > 0) {
			instance.centers[wi] /= (float)count;
		}
	}

	// Check if some instances overlap.
	std::vector<bool>doneCenters(instance.centers.size(), false);
	
	for (size_t wi = 0; wi < instance.fullScene->windowsCount; ++wi) {
		if (!doneCenters[wi]) {
			instance.groups.emplace_back();
			instance.groups.back().push_back(wi);
			bool addedOne = true;
			while (addedOne) {
				addedOne = false;
				for (size_t ti = 0; ti < instance.groups.back().size(); ++ti) {
					for (size_t si = instance.groups.back()[ti] + 1; si < instance.fullScene->windowsCount; ++si) {
						if (!doneCenters[si] && (instance.centers[instance.groups.back()[ti]] - instance.centers[si]).norm() < sideBox*0.1f) {
							instance.groups.back().push_back(si);
							doneCenters[si] = true;
							addedOne = true;
						}
					}
				}
			}
			doneCenters[wi] = true;
		}
	}

	for (auto & group : instance.groups) {
		sibr::Vector3f groupCenter(0.0f, 0.0f, 0.0f);
		for (auto & id : group) {
			groupCenter += instance.centers[id];
		}
		groupCenter /= (float)group.size();
		for (auto & id : group) {
			instance.centers[id] = groupCenter;
		}
	}
}

void SceneBuilder::computeFinalCamerasForScene(InstanceInfos & instance) {
	std::vector<VirtualCamera> finalCameras;
	

	for (auto group : instance.groups) {
		const size_t instanceId = group[0];

		sibr::Matrix4f transf = sibr::matFromTranslation(instance.centers[instanceId]) * instance.estimatedTransformation;

		for (size_t cid = 0; cid < instance.scene->inputCameras().size(); ++cid) {
			sibr::InputCamera cam(*instance.scene->inputCameras()[cid]);

			sibr::Vector4f oldPos = sibr::Vector4f(cam.position().x(), cam.position().y(), cam.position().z(), 1.0f);
			sibr::Vector4f oldUp = sibr::Vector4f(cam.up().x(), cam.up().y(), cam.up().z(), 0.0f);
			sibr::Vector4f oldAt = oldPos + sibr::Vector4f(cam.dir().x(), cam.dir().y(), cam.dir().z(), 0.0f);

			sibr::Vector3f eye = (transf * oldPos).xyz();
			sibr::Vector3f at = (transf * oldAt).xyz();
			sibr::Vector3f up = (transf * oldUp).xyz();
			
			cam.setLookAt(eye, at, up);

			VirtualCamera vcam;
			vcam.camera = cam;
			vcam.id = cid;
			vcam.instance = instanceId;
			vcam.full = false;
			finalCameras.push_back(vcam);
		}
	}

	instance.finalCameras = finalCameras;
}

void SceneBuilder::exportScene(const std::string & outputPath, const float holeScaling) {

	sibr::makeDirectory(outputPath);
	sibr::makeDirectory(outputPath + "/pmvs/");
	sibr::makeDirectory(outputPath + "/pmvs/models/");
	sibr::makeDirectory(outputPath + "/masks/");
	sibr::makeDirectory(outputPath + "/masks_auto/");

	//  Compute all cameras.
	const auto & fullScene = _instanceScenes[0].fullScene; // See remark in constructor about the multiplicity of fullScenes.
	std::vector<VirtualCamera> sharedFinalCameras;
	for (size_t fcid = 0; fcid < fullScene->cameras.size(); ++fcid) {
		VirtualCamera vcam;
		vcam.camera = *fullScene->cameras[fcid];
		vcam.id = fcid;
		vcam.instance = 0;
		vcam.full = true;
		sharedFinalCameras.push_back(vcam);
	}
	size_t totalCamerasCount = sharedFinalCameras.size();
	for (auto & scene : _instanceScenes) {
		computeFinalCamerasForScene(scene);
		totalCamerasCount += scene.finalCameras.size();
	}
	

	// Export bundle file, list_images file and copy and rename the images.
	std::ofstream file, fileListImages;
	file.open(outputPath + "/bundle.out", std::ios::out);
	fileListImages.open(outputPath + "/list_images.txt", std::ios::out);
	if (!file.is_open() || !fileListImages.is_open()) {
		SIBR_ERR << "Unable to write cameras to file" << std::endl;
		return;
	}

	file << "# Bundle file v0.3\n";
	file << totalCamerasCount << " 0\n";


	// Create a black image for full scene masks.
	sibr::ImageL8 blackImg(fullScene->images[0].w(), fullScene->images[0].h(), 0);

	size_t cid = 0;
	for (; cid < sharedFinalCameras.size(); ++cid) {
		auto & cam = sharedFinalCameras[cid].camera;
		// SR: make sure focal is not incoherent with fov
		file << cam.toBundleString();
		const std::string name = sibr::imageIdToString((int)cid);
		fileListImages << name << ".jpg" << " " << cam.w() << " " << cam.h() << std::endl;
		fullScene->images[sharedFinalCameras[cid].id].save(outputPath + "/" + name + ".jpg", false);
		blackImg.save(outputPath + "/masks/" + name + "_mask.png", false);
		blackImg.save(outputPath + "/masks_auto/" + name + "_mask.png", false);	
	}

	for (const auto & instance : _instanceScenes) {
		
		for (const auto & vcam : instance.finalCameras) {
			file << vcam.camera.toBundleString(); // SR: make sure focal/fov ok.
			const std::string name = sibr::imageIdToString((int)cid);
			fileListImages << name << ".jpg" << " " << vcam.camera.w() << " " << vcam.camera.h() << std::endl;
			
			//instance.scene->images()[vcam.id].save(outputPath + "/" + name + ".jpg", false);
			// Instaed of using the scene image, copy from the layers directory.
			sibr::ImageRGB imageInstance;
			imageInstance.load(instance.scene->path() + "/../layers/pass4/instance-" + std::to_string((int)vcam.instance) + "/" + sibr::imageIdToString((int)vcam.id) + ".jpg", false);
			imageInstance.save(outputPath + "/" + name + ".jpg", false);

			instance.scene->masks()[vcam.id].save(outputPath + "/masks/" + name + "_mask.png", false);

			sibr::ImageL8 reflecMask;
			reflecMask.load(instance.scene->path() + "/../masks_auto/" + sibr::imageIdToString((int)vcam.id) + "_mask.png", false);
			reflecMask.save(outputPath + "/masks_auto/" + name + "_mask.png", false);
		
			++cid;
		}
	}
	file.close();
	fileListImages.close();

	for (auto & instance : _instanceScenes) {
		instance.cleanedProxy = PlatonicMeshRefinement::cleanMesh(instance.scene->proxy(), instance.scene->inputCameras(), 2.0f);
		if (!instance.cleanedProxy->hasNormals()) {
			instance.cleanedProxy->generateNormals();
		}
	}

	generateFinalSceneMesh(outputPath, holeScaling);
}

void SceneBuilder::generateFinalSceneMesh(const std::string & outputPath, const float holeScaling) {

	// Mesh export
	sibr::Mesh outPlane;
	sibr::Mesh::Vertices vertices;
	sibr::Mesh::Colors colors;
	sibr::Mesh::Normals normals;
	sibr::Mesh::Triangles indices;

	const auto & fullScene = _instanceScenes[0].fullScene;
	// Start with the plane.
	const float planeScaling = 50.0f;
	const sibr::Vector3f p0 = fullScene->facadePlane.p0;
	const sibr::Vector3f p1 = fullScene->facadePlane.p1;
	const sibr::Vector3f p2 = fullScene->facadePlane.p2;
	const sibr::Vector3f pN = fullScene->facadePlane.pNormal.normalized();
	const sibr::Vector3f p3 = p0 + (p1 - p0) + (p2 - p0);
	const sibr::Vector3f planeCentroid = 0.25f * (p0 + p1 + p2 + p2);
	vertices.push_back(p0 + (p0 - planeCentroid) * planeScaling); normals.push_back(pN); colors.emplace_back(0.5f, 0.5f, 0.5f);
	vertices.push_back(p1 + (p1 - planeCentroid) * planeScaling); normals.push_back(pN); colors.emplace_back(0.5f, 0.5f, 0.5f);
	vertices.push_back(p2 + (p2 - planeCentroid) * planeScaling); normals.push_back(pN); colors.emplace_back(0.5f, 0.5f, 0.5f);
	vertices.push_back(p3 + (p3 - planeCentroid) * planeScaling); normals.push_back(pN); colors.emplace_back(0.5f, 0.5f, 0.5f);

	indices.emplace_back(0, 1, 2);
	indices.emplace_back(2, 1, 3);
	outPlane.vertices(vertices);
	outPlane.colors(colors);
	outPlane.normals(normals);
	outPlane.triangles(indices);
	// Export the plane alone.
	outPlane.save(outputPath + "/plane.ply", true);
	
	size_t totalGroupsSize = 0;
	for (const auto & instance : _instanceScenes) {
		totalGroupsSize += instance.groups.size();
	}
	// Plane triangulation:
	// Input polygon
	Eigen::MatrixXd V;
	Eigen::MatrixXi E;
	Eigen::MatrixXd H;
	V.resize(4 + 4 * totalGroupsSize, 2);
	E.resize(4 + 4 * totalGroupsSize, 2);
	H.resize(1 + totalGroupsSize, 2);

	// Build a plane base.
	sibr::Vector3f u1 = (p0 - planeCentroid).normalized();
	sibr::Vector3f u2 = pN.cross(u1).normalized();
	sibr::Vector3f outside = p0 + (p0 - planeCentroid) * planeScaling * 2.0f;

	// Register plane for triangulation.
	for (int i = 0; i < 4; ++i) {
		V(i, 0) = vertices[i].dot(u1);
		V(i, 1) = vertices[i].dot(u2);
	}
	E.row(0) = Eigen::Vector2i(0, 1);
	E.row(1) = Eigen::Vector2i(1, 3);
	E.row(2) = Eigen::Vector2i(3, 2);
	E.row(3) = Eigen::Vector2i(2, 0);
	H(0, 0) = outside.dot(u1);
	H(0, 1) = outside.dot(u2);

	// Need to cut holes in the plane for each instance of each platonic mesh to insert.
	size_t gid = 0;
	//std::vector<sibr::Vector3f> debugVertices;
	//std::vector<sibr::Vector3u> debugTriangles;
	
	for (const auto & instance : _instanceScenes) {
		std::vector<sibr::Vector3f> holeCorners(4);

		// Compute the axis of the rectangle for this platonic element.
		sibr::Vector3f avgAxis0(0.0f, 0.0f, 0.0f);
		sibr::Vector3f avgAxis1(0.0f, 0.0f, 0.0f);

		for (auto & group : instance.groups) {
			std::vector<sibr::Vector3f> averageIntersections(4, sibr::Vector3f(0.0f, 0.0f, 0.0f));
			size_t count = 0;

			for (size_t ci = 0; ci < instance.fullScene->estimatedCameras.size(); ++ci) {
				if (instance.fullScene->windowsIds[ci].first != group[0]) {
					continue;
				}
				auto trap = instance.fullScene->trapezoids[ci];
				auto & fcam = *instance.fullScene->cameras[instance.fullScene->estimatedCameras[ci].source];
				sibr::Vector3f c0 = fcam.position();
				std::vector<sibr::Vector2f> pixels;
				pixels.push_back(trap.p0.xy()); pixels.push_back(trap.p1.xy());
				pixels.push_back(trap.p2.xy()); pixels.push_back(trap.p3.xy());

				for (size_t pid = 0; pid < 4; ++pid) {

					const sibr::Vector3f cDir = sibr::CameraRaycaster::computeRayDir(fcam, pixels[pid]);
					float orientation = cDir.dot(pN);
					// Check for parallelism
					if (orientation == 0.0) {
						SIBR_WRG << "Ignoring a camera as parallel to the facade plane.\n";
						continue;
					}
					// Intersect camera center ray and plane.
					float abscisse = (p0 - c0).dot(pN) / orientation;
					averageIntersections[pid] += (c0 + abscisse * cDir);
				}
				++count;
			}
			if (count > 0) {
				for (size_t i = 0; i < 4; ++i) {
					averageIntersections[i] *= (1.0f / (float)count);
				}
			}

			// Compute two axis.
			avgAxis0 += (averageIntersections[1] - averageIntersections[0]).normalized();
			avgAxis1 += (averageIntersections[3] - averageIntersections[0]).normalized();
		}
		avgAxis0 /= (float)(instance.groups.size());
		avgAxis1 /= (float)(instance.groups.size());

		{
			// Project this mesh onto the plane.
			Eigen::MatrixXd projectedPoints(instance.cleanedProxy->vertices().size(), 2);
			for (size_t ii = 0; ii < instance.cleanedProxy->vertices().size(); ++ii) {
				const sibr::Vector3f pos = instance.cleanedProxy->vertices()[ii];
				const sibr::Vector3f posnn = (instance.estimatedTransformation * sibr::Vector4f(pos[0], pos[1], pos[2], 1.0f)).xyz();
				projectedPoints(ii, 0) = posnn.dot(u1);
				projectedPoints(ii, 1) = posnn.dot(u2);

			}
			Eigen::VectorXd mean = projectedPoints.colwise().mean();

			sibr::Vector2f axis0(avgAxis0.dot(u1), avgAxis0.dot(u2));
			sibr::Vector2f axis1(avgAxis1.dot(u1), avgAxis1.dot(u2));

			const sibr::Vector2f centroidPlane = sibr::Vector2f(0.0, 0.0);// mean.transpose().cast<float>();
			const sibr::Vector2f pointZero = sibr::Vector2f(projectedPoints.row(0).cast<float>()) - centroidPlane;
			sibr::Vector2f miniLocal(pointZero.dot(axis0), pointZero.dot(axis1));
			sibr::Vector2f maxiLocal(miniLocal);
			for (int ii = 1; ii < projectedPoints.rows(); ++ii) {
				const sibr::Vector2f pp = sibr::Vector2f(projectedPoints.row(ii).cast<float>()) - centroidPlane;
				const sibr::Vector2f dp(pp.dot(axis0), pp.dot(axis1));
				miniLocal = miniLocal.cwiseMin(dp);
				maxiLocal = maxiLocal.cwiseMax(dp);
			}

			std::vector<sibr::Vector2f> localCorners(4);
			localCorners[0] = miniLocal[0] * axis0 + miniLocal[1] * axis1;
			localCorners[3] = miniLocal[0] * axis0 + maxiLocal[1] * axis1;
			localCorners[2] = maxiLocal[0] * axis0 + maxiLocal[1] * axis1;
			localCorners[1] = maxiLocal[0] * axis0 + miniLocal[1] * axis1;


			for (int i = 0; i < 4; ++i) {
				const sibr::Vector2f localCorner = (localCorners[i] - centroidPlane) * holeScaling + centroidPlane;
				// Move from plane 2D space to world space.
				holeCorners[i] = localCorner[0] * u1 + localCorner[1] * u2;
			}
		}

		// Now populate the mesh to triangulate by repeting the rectangle at the correct positions.
		for (auto group : instance.groups) {
			const size_t instanceId = group[0];
			sibr::Vector3f interCentroid(0.0f, 0.0f, 0.0f);
			for (int vid = 0; vid < 4; ++vid) {
				const sibr::Vector3f shiftedHoleCorner = holeCorners[vid] + instance.centers[instanceId];
				V(4 + gid * 4 + vid, 0) = shiftedHoleCorner.dot(u1);
				V(4 + gid * 4 + vid, 1) = shiftedHoleCorner.dot(u2);
				interCentroid += shiftedHoleCorner;
			}
			interCentroid /= 4.0f;
			E.row(4 + gid * 4 + 0) = Eigen::Vector2i(4 + (int)gid * 4 + 0, 4 + (int)gid * 4 + 1);
			E.row(4 + gid * 4 + 1) = Eigen::Vector2i(4 + (int)gid * 4 + 1, 4 + (int)gid * 4 + 2);
			E.row(4 + gid * 4 + 2) = Eigen::Vector2i(4 + (int)gid * 4 + 2, 4 + (int)gid * 4 + 3);
			E.row(4 + gid * 4 + 3) = Eigen::Vector2i(4 + (int)gid * 4 + 3, 4 + (int)gid * 4 + 0);
			H(1 + gid, 0) = interCentroid.dot(u1);
			H(1 + gid, 1) = interCentroid.dot(u2);
			++gid;
		}
	}
	
	// Triangulated interior
	Eigen::MatrixXd V2;
	Eigen::MatrixXi F2;

	// Triangulate the interior
	SceneBuilder::triangulate(V, E, H, "a0.1q", V2, F2);

	sibr::Mesh outProxy;
	vertices.clear();
	normals.clear();
	colors.clear();
	indices.clear();

	for (size_t i = 0; i < (size_t)V2.rows(); ++i) {
		vertices.emplace_back(((float)V2(i, 0) * u1 + (float)V2(i, 1) * u2 + planeCentroid.dot(pN)*pN).eval());
		normals.push_back(pN);
		colors.emplace_back(0.5f, 0.5f, 0.5f);
	}
	for (size_t i = 0; i < (size_t)F2.rows(); ++i) {
		indices.emplace_back(F2(i, 0), F2(i, 1), F2(i, 2));
	}
	//sibr::Mesh::Vertices tempVertices;
	////sibr::Mesh::Normals tempNormals;
	////sibr::Mesh::Colors tempColors;
	//sibr::Mesh::Triangles tempIndices;
	//for (size_t i = 0; i < (size_t)V2.rows(); ++i) {
	//	tempVertices.emplace_back(((float)V2(i, 0) * u1 + (float)V2(i, 1) * u2 + planeCentroid.dot(pN)*pN).eval());
	//	//tempNormals.push_back(pN);
	//	//tempColors.emplace_back(0.5f, 0.5f, 0.5f);
	//}
	//for (size_t i = 0; i < (size_t)F2.rows(); ++i) {
	//	tempIndices.emplace_back(F2(i, 0), F2(i, 1), F2(i, 2));
	//}

	//// Select the biggest connected component.
	//sibr::Mesh tempMesh(false);
	//tempMesh.vertices(tempVertices);
	//tempMesh.triangles(tempIndices);
	//auto comps = tempMesh.removeDisconnectedComponents();
	//int idComp = 0;
	//size_t valComp = comps[0].size();
	//for(size_t iComp = 1; iComp < comps.size(); ++iComp) {
	//	if(comps[iComp].size() > valComp) {
	//		idComp = iComp;
	//		valComp = comps[iComp].size();
	//	}
	//}

	//// Copy remaining vertices.
	//for(int vid : comps[idComp]) {
	//	vertices.emplace_back(tempVertices[vid]);
	//	normals.push_back(pN);
	//	colors.emplace_back(0.5f, 0.5f, 0.5f);
	//}
	//// Copy remaining triangles.
	//for(sibr::Vector3u tri : tempIndices) {
	//	bool shouldKeep = false;
	//	for (int vid : comps[idComp]) {
	//		if(vid == tri[0] || vid == tri[1] || vid == tri[2]) {
	//			indices.emplace_back(tri);
	//			break;
	//		}
	//	}
	//}


	sibr::Vector3u shift((unsigned int)vertices.size(), (unsigned int)vertices.size(), (unsigned int)vertices.size());

	for (const auto & instance : _instanceScenes) {
		// Duplicate instances of each instance mesh.
		const unsigned int vCount = (unsigned int)(instance.cleanedProxy->vertices().size());
		sibr::Vector3u deltaShift(vCount, vCount, vCount);

		for (auto group : instance.groups) {
			const size_t instanceId = group[0];
			// TODO: is usage of center really legitimate here.
			// Nope, this is just the shift along the facade I guess. Or should we compensate?
			sibr::Matrix4f transf = sibr::matFromTranslation(instance.centers[instanceId]) * instance.estimatedTransformation;

			for (size_t pid = 0; pid < instance.cleanedProxy->vertices().size(); ++pid) {
				sibr::Vector3f pos = instance.cleanedProxy->vertices()[pid];
				sibr::Vector4f posnn = transf * sibr::Vector4f(pos[0], pos[1], pos[2], 1.0f);
				vertices.push_back(posnn.xyz());
				if (instance.cleanedProxy->hasColors()) {
					colors.push_back(instance.cleanedProxy->colors()[pid]);
				}
				if (instance.cleanedProxy->hasNormals()) {
					normals.push_back(instance.cleanedProxy->normals()[pid]);
				}
			}

			for (size_t tid = 0; tid < instance.cleanedProxy->triangles().size(); ++tid) {
				indices.push_back(instance.cleanedProxy->triangles()[tid] + shift);
			}
			shift += deltaShift;
		}
	}
	
	
	

	outProxy.vertices(vertices);
	outProxy.colors(colors);
	outProxy.normals(normals);
	outProxy.triangles(indices);
	outProxy.save(outputPath + "/pmvs/models/pmvs_recon.ply", true);

}

int SceneBuilder::getSelectedCamera()
{	// In this case we assume only one instance scene to process.
	assert(_instanceScenes.size() == 1);
	const auto & infos = _instanceScenes[0];
	const auto cleanedProxy = PlatonicMeshRefinement::cleanMesh(infos.scene->proxy(), infos.scene->inputCameras(), 1.1f);
	PlatonicMeshRefinement meshRefine(cleanedProxy, infos.fullScene, infos.scene);
	// Frame for the mesh.
	const sibr::Vector3f normal = infos.fullScene->facadePlane.pNormal;
	const sibr::Vector3f platonicNormal = (infos.estimatedTransformation.transpose() * sibr::Vector4f(normal[0], normal[1], normal[2], 0.0f)).xyz().normalized();
	// Estimate fronto parallel camera.
	int camId = -1;
	meshRefine.estimateFrontoParallelCamera(infos.scene->inputCameras(), platonicNormal, true, camId, 1);
	return camId;
}

void SceneBuilder::filterPlatonicMesh(const std::string & initialScenePath, const std::string & masksPath, const std::string & outputPath, const bool visu, const bool useSuperpixels)
{	// In this case we assume only one instance scene to process.
	assert(_instanceScenes.size() == 1);
	const auto & infos = _instanceScenes[0];
	// Make it quite large, to be sure not to introduce too many superpixels with missing depths.
	// In fact we could even not clean it.
	const auto cleanedProxy = PlatonicMeshRefinement::cleanMesh(infos.scene->proxy(), infos.scene->inputCameras(), 2.0f);
	PlatonicMeshRefinement meshRefine(cleanedProxy, infos.fullScene, infos.scene);
	meshRefine.computeFrontoParallelInfos(infos.estimatedTransformation, initialScenePath, true, 1, outputPath);
	if (visu) {
		meshRefine.computePhotoConsistencyVisualisation(10, initialScenePath, masksPath, outputPath);
	}
	if (useSuperpixels) {
		meshRefine.refineUsingSuperpixels(infos.estimatedTransformation, outputPath);
	}
}

SceneBuilder::~SceneBuilder(void)
{

}

// TODO SR: export in a utility class.
void SceneBuilder::triangulate(
	const Eigen::MatrixXd& V,
	const Eigen::MatrixXi& E,
	const Eigen::MatrixXd& H,
	const std::string flags,
	Eigen::MatrixXd& V2,
	Eigen::MatrixXi& F2)
{
	using namespace std;
	using namespace Eigen;

	// Prepare the flags
	string full_flags = flags + "pzB";

	// Prepare the input struct
	triangulateio in;

	assert(V.cols() == 2);

	in.numberofpoints = (int)V.rows();
	in.pointlist = (double*)calloc(V.rows() * 2, sizeof(double));
	for (unsigned i = 0; i<V.rows(); ++i)
		for (unsigned j = 0; j<2; ++j)
			in.pointlist[i * 2 + j] = V(i, j);

	in.numberofpointattributes = 0;
	in.pointmarkerlist = (int*)calloc(V.rows(), sizeof(int));
	for (unsigned i = 0; i<V.rows(); ++i)
		in.pointmarkerlist[i] = 1;

	in.trianglelist = NULL;
	in.numberoftriangles = 0;
	in.numberofcorners = 0;
	in.numberoftriangleattributes = 0;
	in.triangleattributelist = NULL;

	in.numberofsegments = (int)E.rows();
	in.segmentlist = (int*)calloc(E.rows() * 2, sizeof(int));
	for (unsigned i = 0; i<E.rows(); ++i)
		for (unsigned j = 0; j<2; ++j)
			in.segmentlist[i * 2 + j] = E(i, j);
	in.segmentmarkerlist = (int*)calloc(E.rows(), sizeof(int));
	for (unsigned i = 0; i<E.rows(); ++i)
		in.segmentmarkerlist[i] = 1;

	in.numberofholes = (int)H.rows();
	in.holelist = (double*)calloc(H.rows() * 2, sizeof(double));
	for (unsigned i = 0; i<H.rows(); ++i)
		for (unsigned j = 0; j<2; ++j)
			in.holelist[i * 2 + j] = H(i, j);
	in.numberofregions = 0;

	// Prepare the output struct
	triangulateio out;

	out.pointlist = NULL;
	out.trianglelist = NULL;
	out.segmentlist = NULL;

	// Call triangle
	::triangulate(const_cast<char*>(full_flags.c_str()), &in, &out, 0);

	// Return the mesh
	V2.resize(out.numberofpoints, 2);
	for (unsigned i = 0; i<V2.rows(); ++i)
		for (unsigned j = 0; j<2; ++j)
			V2(i, j) = out.pointlist[i * 2 + j];

	F2.resize(out.numberoftriangles, 3);
	for (unsigned i = 0; i<F2.rows(); ++i)
		for (unsigned j = 0; j<3; ++j)
			F2(i, j) = out.trianglelist[i * 3 + j];

	// Cleanup in
	free(in.pointlist);
	free(in.pointmarkerlist);
	free(in.segmentlist);
	free(in.segmentmarkerlist);
	free(in.holelist);

	// Cleanup out
	free(out.pointlist);
	free(out.trianglelist);
	free(out.segmentlist);
}

