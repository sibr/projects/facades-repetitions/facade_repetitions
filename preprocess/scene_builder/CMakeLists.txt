# Copyright (C) 2020, Inria
# GRAPHDECO research group, https://team.inria.fr/graphdeco
# All rights reserved.
# 
# This software is free for non-commercial, research and evaluation use 
# under the terms of the LICENSE.md file.
# 
# For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr


project(SIBR_repetitions_scene_builder)

file(GLOB SOURCES "*.cpp" "*.c" "*.h" "*.hpp")
source_group("Source Files" FILES ${SOURCES})

file(GLOB SHADERS "shaders/*.frag" "shaders/*.vert" "shaders/*.geom")
source_group("Source Files\\shaders" FILES ${SHADERS})

file(GLOB SOURCES "*.cpp" "*.c" "*.h" "*.hpp" "shaders/*.frag" "shaders/*.vert" "shaders/*.geom")

include_directories(${Boost_INCLUDE_DIRS} .)

add_executable(${PROJECT_NAME} ${SOURCES})
target_link_libraries(${PROJECT_NAME}
	${Boost_LIBRARIES}
	${ASSIMP_LIBRARIES}
	${GLEW_LIBRARIES}
	${OPENGL_LIBRARIES}
    ${OpenCV_LIBRARIES}		
	sibr_system
	sibr_view
	sibr_assets
	sibr_raycaster
	sibr_graphics
	sibr_facade_repetitions
	sibr_ulr
)

set_target_properties(${PROJECT_NAME} PROPERTIES FOLDER "projects/facade_repetitions/preprocess")


## High level macro to install in an homogen way all our ibr targets
include(install_runtime)
ibr_install_target(${PROJECT_NAME}
    INSTALL_PDB                         ## mean install also MSVC IDE *.pdb file (DEST according to target type)
	SHADERS 	${SHADERS}
	RSC_FOLDER 	"facade_repetitions"
    STANDALONE  ${INSTALL_STANDALONE}   ## mean call install_runtime with bundle dependencies resolution
    COMPONENT   ${PROJECT_NAME}_install ## will create custom target to install only this project
)
