/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */



#include "SceneBuilder.hpp"

#include <projects/facade_repetitions/renderer/ReprojectionScene.hpp>
#include <projects/facade_repetitions/renderer/FullScene.hpp>

#include <core/graphics/Texture.hpp>
#include <core/system/Utils.hpp>
#include <core/raycaster/CameraRaycaster.hpp>
#include <core/assets/Resources.hpp>
#include <core/graphics/Utils.hpp>
#include <core/graphics/GUI.hpp>

#include <Eigen/Sparse>
#include <Eigen/Dense>
#include <iostream>
#include <boost/filesystem.hpp>
#include "core/graphics/Shader.hpp"
#include "core/graphics/Input.hpp"


#define PROGRAM_NAME "SIBR_repetitions_scene_builder"
using namespace sibr;


const char* usage = ""
"Usage: " PROGRAM_NAME " " "         " "\n"
;


void mergeScenes(const std::string & path1, const std::string & path2, const std::string & pathDest) {
	std::shared_ptr<FullScene> fullScene1 = std::make_shared<FullScene>(path1, false);
	std::shared_ptr<ReprojectionScene> scene1 = std::make_shared<ReprojectionScene>(path1 + "/IBR_data_OPENMVG-CMPMVS/");
	std::shared_ptr<FullScene> fullScene2 = std::make_shared<FullScene>(path2, false);
	std::shared_ptr<ReprojectionScene> scene2 = std::make_shared<ReprojectionScene>(path2 + "/IBR_data_OPENMVG-CMPMVS/");

	// -- SCENE GENERATION
	SceneBuilder builder({ fullScene1, fullScene2 }, { scene1, scene2 });
	builder.estimateSharedTransformations();
	builder.exportScene(pathDest, 0.8f);

}

struct RepetitionsSceneBuilderArgs {
	Arg<bool> merge = { "merge", "perform merging" };
	Arg<std::string> path1 = { "path1", "", "first dataset path" };
	Arg<std::string> path2 = { "path2", "", "second dataset path" };
	Arg<std::string> dest = { "dest", "", "output path" };
	Arg<std::string> path = { "path", "", "single dataset apth" };
	Arg<float> scaling = { "scaling", 0.80f, "scaling of trapezoids" };
	Arg<int> iter = { "iter", 10, "iterations count" };
	Arg<int> down = { "down", 1, "downscaling" };
	Arg<bool> refine = { "refine", "perform refinement" };
	Arg<bool> id = { "id" , "perform ID processing"};
	Arg<bool> visuGen = { "visu-gen", "generate visualization data" };
	Arg<bool> useB = { "b", "use bundle file" };
};



int main(int ac, char** av) {

	if (ac < 2) {
		return EXIT_FAILURE;
	}

	CommandLineArgs::parseMainArgs(ac, av);
	RepetitionsSceneBuilderArgs args;

	if (args.merge) {
		mergeScenes(args.path1, args.path2, args.dest);
		return EXIT_SUCCESS;
	}


	const std::string rootPath = args.path;

	

	const float trapezoidScaling = args.scaling;  
	const bool preprocessRefine = args.refine;
	const bool preprocessId = args.id;
	bool visuProcess = args.visuGen;
	const int iterationCount = args.iter;
	
	const int downscaling = args.down;
	const bool useB = args.useB;

	const std::string ibrPath = rootPath + "/IBR_data_OPENMVG-CMPMVS/";
	const std::string bundlerFile = ibrPath + "/bundle.out";
	const std::string listFile = ibrPath + "/list_images.txt";
	const std::string outputPath = rootPath + "/mesh_cleaning/";
	const std::string ibrOutputPath = rootPath + "/IBR_fullScene/";

	sibr::makeDirectory(outputPath);

	// -- FULL SCENE
	std::shared_ptr<FullScene> fullScene = std::shared_ptr<FullScene>(new FullScene(rootPath, useB));

	// -- INSTANCE SCENE
	std::shared_ptr<ReprojectionScene> scene = std::shared_ptr<ReprojectionScene>(new ReprojectionScene(ibrPath));

	// -- SCENE GENERATION
	SceneBuilder builder({ fullScene }, { scene });
	builder.estimateSharedTransformations();
	if(preprocessId) {
		return builder.getSelectedCamera();
	}
	if (preprocessRefine) {
		builder.filterPlatonicMesh(rootPath + "/IBR_data_OPENMVG-CMPMVS/", rootPath + "/masks_auto/", outputPath, visuProcess, true);
	}  else {
		builder.exportScene(ibrOutputPath, trapezoidScaling);
	}
	
	

	
	return EXIT_SUCCESS;
}
