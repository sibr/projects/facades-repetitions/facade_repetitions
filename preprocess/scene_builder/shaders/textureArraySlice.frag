/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#version 420

layout(location = 0) out vec3 out_color;

in vec2 vertex_coord;

uniform int slice;
layout(binding = 0) uniform sampler2DArray array;


float interpolate(float val, float y0, float x0, float y1, float x1) {
		return (val - x0)*(y1 - y0) / (x1 - x0) + y0;
}
	//
float base(float val) {
		if (val <= -0.75) return 0;
		else if (val <= -0.25) return interpolate(val, 0.0, -0.75, 1.0, -0.25);
		else if (val <= 0.25) return 1.0;
		else if (val <= 0.75) return interpolate(val, 1.0, 0.25, 0.0, 0.75);
		else return 0.0;
}
float red(float gray) {
		return base(gray - 0.5);
}
	//
float green(float gray) {
		return base(gray);
}
	//
float blue(float gray) {
		return base(gray + 0.5);
}

vec3 getJetColorFromProba(float proba)
{
		// val must be [-1;+1] for jet color estimation
		float val = 2.0 * proba - 1.0;
		//
		float redColor = red(val);
		float greenColor = green(val);
		float blueColor = blue(val);

		//
		return vec3(redColor, greenColor, blueColor);
}

float getProbaFromLinearColor(vec3 color) {
		return (color.x + color.y + color.z) / 3.0;
}

void main(void) {
	float proba = getProbaFromLinearColor(texture(array, vec3(vertex_coord, slice)).rgb);
	out_color = getJetColorFromProba(proba);
}
