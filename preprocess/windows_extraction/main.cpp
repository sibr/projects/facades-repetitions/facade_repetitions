/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */



#include <fstream>
#include <iostream>
#include <boost/filesystem.hpp>
#include <core/graphics/Image.hpp>
#include <opencv2/core/core.hpp>
#include <core/system/Utils.hpp>

#include "SelectionInterface.hpp"
#include "WindowsExtractor.hpp"
#include "core/system/CommandLineArgs.hpp"

#define WINDOW_LABEL 1

#define PROGRAM_NAME "sibr_ulr_repetitions_windows_extraction"

using namespace sibr;


const char* usage = ""
"Usage: " PROGRAM_NAME " --path <dataset-path>" "\n"
;

static void	loadRectificationinfos(const std::string & path, RectificationInfos & infos) {
	// Load the file.
	std::ifstream file(path);
	SIBR_ASSERT(file.is_open());

	int wo, ho, wr, hr;
	file >> wo; file >> ho; file >> wr; file >> hr;

	Eigen::Matrix3f H; H.setZero();

	float temp;
	for (int i = 0; i < 3; ++i) {
		for (int j = 0; j < 3; ++j) {
			file >> temp;
			H(i, j) = temp;
		}
	}
	// Fill infos.
	infos.H = H;
	infos.wo = wo; infos.ho = ho;
	infos.wr = wr; infos.hr = hr;
	file.close();
}

static void binarizeLabels(sibr::ImageL8 & labelsImage){
	// Convert labels to black and white.
	for(unsigned int i = 0; i < labelsImage.w(); ++i){
		for(unsigned int j = 0; j < labelsImage.h(); ++j){
			auto& label = labelsImage(i,j)[0];
			if(label == WINDOW_LABEL){
				sibr::ImageL8::Pixel pixel;
				pixel[0] = 255;
				labelsImage(i,j) = pixel;
			}
		}
	}
}


struct RepetitionsWindowsExtraction {
	RequiredArg<std::string> path = {"path", "dataset root path"};
	Arg<float> scaling = { "scaling", 1.5f, "window boxes scaling" };
	Arg<int> threshold = { "th", 80, "mask threshold" };
	Arg<int> smooth = { "smooth", 11, "mask smoothing" };
	Arg<float> scalingW = { "sw", 0.0f, "horizontal scaling bias" };
	Arg<float> scalingH = { "sh", 0.0f, "vertical scaling bias" };
	Arg<bool> debug = { "debug", "debug mode" };
};

int main( int ac, char** av )
{
	if(ac < 2){ std::cout << usage << std::endl; return -10; }
	//Arguments
	CommandLineArgs::parseMainArgs(ac, av);
	RepetitionsWindowsExtraction args;

	// Parameters
	std::string				rootPath = args.path;
	const float		rectangleScaling = args.scaling; 
	const int			   threshold = args.threshold; 
	int			   smoothing = args.smooth;
	if (smoothing % 2 == 0) {
		++smoothing;
	}
	const bool debug = args.debug;
	const float scalingWidth = args.scalingW;
	const float scalingHeight = args.scalingH;


	// Paths
	std::string rectifiedPath( rootPath + "/rectified/");//"_out.jpg"
	std::string labelsPath( rootPath + "/labels/");		// "-outputs.png"
	std::string matricesPath( rootPath + "/matrices/"); // "_params.txt"
	std::string imagesPath( rootPath + "/images/");		// ".JPG"
	std::string globalMasksPath( rootPath + "/images-masks/");		// ".JPG"
	std::string outputPath( rootPath + "/");
	
	// Extensions
	std::string rectifiedSuffix("_out.jpg");
	std::string labelsSuffix("-outputs.png");
	std::string matricesSuffix("_params.txt");
	std::string imagesSuffix(".JPG");

	// Create output dirs if needed.
	sibr::makeDirectory(outputPath);
	sibr::makeDirectory(globalMasksPath);

	std::string outputPathDebug( rootPath + "/debug/");
#ifdef DEBUG
	sibr::makeDirectory(outputPathDebug);
#endif
	
	// Load image names.
	std::vector<std::string> images;
	boost::filesystem::directory_iterator  endDir;
	for(boost::filesystem::directory_iterator dir(imagesPath) ; dir != endDir; ++dir){
		if(boost::filesystem::is_regular_file(dir->status()) ){
			images.push_back(dir->path().stem().string());
		}
	}

	WindowsExtractor extractor(rectangleScaling, sibr::Vector2f(scalingWidth, scalingHeight),threshold, smoothing, debug);

	std::ofstream cropsList;
	cropsList.open(outputPath + "/crop_list.txt"); 
	std::ofstream trapList;
	trapList.open(outputPath + "/trapezoid_list.txt");

	for(auto& imageName : images){

		// Load initial image, labels and rectification infos.
		sibr::ImageRGB initialImage;
		initialImage.load(imagesPath + imageName + imagesSuffix);

		sibr::ImageL8 labelsImage;
		labelsImage.load(labelsPath + imageName + labelsSuffix);
		binarizeLabels(labelsImage);

		RectificationInfos infos;
		loadRectificationinfos(matricesPath + imageName + matricesSuffix, infos);
		infos.wi = initialImage.w();
		infos.hi = initialImage.h();

		extractor.extract(labelsImage, infos);

		// Ask user to select areas to keep.
		SelectionInterface intf("Areas selection");
		auto bboxes = extractor.boundingBoxes();
		intf.display(initialImage, bboxes);
		auto indices = intf.getSelectedIndices();
		
		// Export the corresponding crops in initial images.
		int count = 0;
		cropsList << (imageName+imagesSuffix) << std::endl;
		trapList << (imageName + imagesSuffix) << std::endl;
		for(int k = 0; k < bboxes.size();++k){
			if(indices.count(k) > 0){
				Trapezoid trap = extractor.trapezoids()[k];
				trapList << "t " << trap.p0.xy().transpose() << " " << trap.p1.xy().transpose() << " " << trap.p2.xy().transpose() << " " << trap.p3.xy().transpose() << std::endl;
				cropsList << "c " << bboxes[k].x << " " << bboxes[k].y << " " << bboxes[k].width << " " << bboxes[k].height << std::endl;
				extractor.exportMask(k, initialImage.w(), initialImage.h(), globalMasksPath + imageName + "_mask_" + std::to_string(count) + imagesSuffix); 
				++count;
			}
		}

#ifdef DEBUG
		const std::string debugExportPath = outputPathDebug + "/" + imageName;
		extractor.exportAnnotatedInputImage(initialImage, debugExportPath, true, true);
		extractor.exportCrops(initialImage, debugExportPath, indices);
		extractor.exportRectifiedCrops(initialImage,debugExportPath, infos,indices);
#endif
		
	}
	cropsList.close();
	trapList.close();

	return EXIT_SUCCESS;
}

