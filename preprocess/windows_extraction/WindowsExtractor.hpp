/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#ifndef __SIBR_REPETITION_WINDOWS_EXTRACTOR_HPP__
#define __SIBR_REPETITION_WINDOWS_EXTRACTOR_HPP__

#include <string>
#include <set>
#include <core/graphics/Image.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/core/core.hpp>

struct RectificationInfos {
	Eigen::Matrix3f H;
	// Size of the input image (potentially scaled down before rectification);
	int wo;
	int ho;
	// Size of the rectified image (before resizing to PIX2PIX_OUTPUT_SIZE x PIX2PIX_OUTPUT_SIZE);
	int wr;
	int hr;
	// Size of the input image (full-res)
	int wi;
	int hi;
};

struct Trapezoid {
	sibr::Vector3f p0;
	sibr::Vector3f p1;
	sibr::Vector3f p2;
	sibr::Vector3f p3;
};

class WindowsExtractor {

public:

	WindowsExtractor(float rectangleScaling, const sibr::Vector2f & axisScaling, int threshold, int blurRadius, bool debug);

	void extract(const sibr::ImageL8 & labelsImage, const RectificationInfos & infos);

	// Exports
	void exportAnnotatedInputImage(const sibr::ImageRGB & inputImage, const std::string rootPath, bool showBoundingBoxes, bool showTrapezoids);
	void exportCrops(const sibr::ImageRGB & inputImage, const std::string rootPath, const std::set<int> & indices);
	void exportRectifiedCrops(const sibr::ImageRGB & inputImage, const std::string rootPath, const RectificationInfos & infos, const std::set<int> & indices);
	void exportMask(int k, uint w, uint h, const std::string & path);
	// Getters
	std::vector<cv::Rect> boundingBoxes(){ return _boundingBoxes; }
	std::vector<cv::Rect> initialRectangles(){ return _initialRectangles; }
	std::vector<Trapezoid> trapezoids() { return _reprojectedTrapezoids; }

private:

	void extractRectanglesFromLabels(const sibr::ImageL8 & labelsImage, std::vector<cv::Rect> & boxes);

	void reprojectRectangles( const RectificationInfos & infos, const std::vector<cv::Rect> & boxes);
	
	sibr::Vector3f project(float x, float y, const Eigen::Matrix3f & H, const sibr::Vector3f preScaling, const sibr::Vector3f postScaling);

	static sibr::ImageRGB::Pixel readInterpolated(const sibr::ImageRGB & image, float x, float y);
	
	Trapezoid reprojectTrapezoid(const cv::Rect & rect, const RectificationInfos & infos);

	void exportGlobalMasks(const sibr::ImageRGB & inputImage, const std::string path);
	
	sibr::Vector2f _finalScaling;
	int _blurRadius;
	float _minimumFraction;
	int _threshold;
	float _rectangleScaling; 
	float _rectangleShift;
	bool _visualiseLabels;

	std::vector<cv::Rect> _initialRectangles;
	std::vector<Trapezoid> _reprojectedTrapezoids;
	std::vector<Trapezoid> _unscaledReprojTrapezoids;
	std::vector<cv::Rect> _boundingBoxes;
	 
};
#endif