/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#include "SelectionInterface.hpp"

#define LINEWIDTH 4


SelectionInterface::SelectionInterface(const std::string & name, float scaling){
	_interfaceName = name;
	_scaling = scaling;
	_selectionActive = false;
	cv::namedWindow(_interfaceName, cv::WINDOW_GUI_EXPANDED | cv::WINDOW_KEEPRATIO | cv::WINDOW_NORMAL);
	cv::setMouseCallback(_interfaceName, staticCallBackFunction, this);
}

void SelectionInterface::display(const sibr::ImageRGB & image, const std::vector<cv::Rect> & rects){
	// Build initial image by displaying all rectangles as red over the image.
	cv::Mat fullSizeImage = image.toOpenCVBGR().clone();
	
	cv::resize(fullSizeImage, _initialImage, cv::Size(), _scaling, _scaling);
	for(auto& rect: rects){
		cv::Rect rectScaled = cv::Rect((int)(rect.x * _scaling), (int)(rect.y * _scaling), (int)(rect.width * _scaling), (int)(rect.height * _scaling));
		_rectsScaled.push_back(rectScaled);
	}
	updateRectangles();
	resetImage();
	refreshScreen();

	cv::waitKey(0);
	cv::destroyWindow(_interfaceName);
}


void SelectionInterface::updateRectangles(){
	for(unsigned int i = 0; i < _rectsScaled.size(); ++i){
		bool selected = _indicesToKeep.count(i) > 0;
		cv::rectangle(_initialImage, _rectsScaled[i], selected ? cv::Scalar(0,255,0) : cv::Scalar(0,0,255), LINEWIDTH);
	}
}

void SelectionInterface::staticCallBackFunction(int event, int x, int y, int flags, void* userdata){
	SelectionInterface * intf = static_cast<SelectionInterface *>(userdata);
	if(event == cv::EVENT_LBUTTONDOWN){
		intf->beginSelection(cv::Point(x,y));
	} else if(event == cv::EVENT_LBUTTONUP) {
		intf->endSelection(cv::Point(x,y));
	} else if(intf->selectionActive() && event == cv::EVENT_MOUSEMOVE){
		intf->updateSelection(cv::Point(x,y));
	}
}

void SelectionInterface::beginSelection(const cv::Point & point){
	_firstPoint = point;
	_selectionActive = true;
}

void SelectionInterface::endSelection(const cv::Point & point){
	cv::Rect area = cv::Rect(_firstPoint, point);

	_selectionActive = false;

	// Compute intersections.
	for(unsigned int i = 0; i < _rectsScaled.size(); ++i){
		auto& rect = _rectsScaled[i];
		bool intersects = (rect & area).area() > 0;
		if(intersects){
			_indicesToKeep.insert(i);
		}
	}

	// Update rectangles.
	updateRectangles();
	resetImage();
	refreshScreen();
}

void SelectionInterface::updateSelection(const cv::Point & point){
	cv::Rect currentArea = cv::Rect(_firstPoint, point);
	_currentImage = _initialImage.clone();
	cv::rectangle(_currentImage, currentArea, cv::Scalar(255,255,255), LINEWIDTH);
	refreshScreen();
}

void SelectionInterface::blitImage(){
	_initialImage = _currentImage.clone();
}

void SelectionInterface::resetImage(){
	_currentImage = _initialImage.clone();
}

std::set<int> SelectionInterface::getSelectedIndices(){
	return _indicesToKeep;
}


void SelectionInterface::refreshScreen()
{
	cv::imshow(_interfaceName, _currentImage);
}