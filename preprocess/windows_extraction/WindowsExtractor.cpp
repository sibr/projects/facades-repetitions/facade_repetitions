/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#include "WindowsExtractor.hpp"

#define PIX2PIX_OUTPUT_SIZE 256

WindowsExtractor::WindowsExtractor(float rectangleScaling, const sibr::Vector2f & axisScaling, int threshold, int blurRadius, bool debug){
	
	_blurRadius = blurRadius;
	_minimumFraction = 0.0025f;
	_threshold = threshold;
	_rectangleScaling = rectangleScaling; 
	_rectangleShift =  (rectangleScaling - 1.0f)*0.5f;
	_visualiseLabels = debug;
	_finalScaling = axisScaling;
}

void WindowsExtractor::extract(const sibr::ImageL8 & labelsImage, const RectificationInfos & infos){

	// Extract rectangles from labels image.
	std::vector<cv::Rect> boxes;
	extractRectanglesFromLabels(labelsImage, boxes);

	// Obtain the projected rectangles in the input image.
	// Result stored in 	_reprojectedTrapezoids.
	reprojectRectangles(infos, boxes);
	
	_boundingBoxes.clear();
	_boundingBoxes.reserve(_reprojectedTrapezoids.size());
	// Convert trapezoid to bboxes.
	for(int j = 0; j < _reprojectedTrapezoids.size(); ++j){
		auto& trapeze = _reprojectedTrapezoids[j];
		// Compute bounding box.
		sibr::Vector3f pmin = trapeze.p0.cwiseMin(trapeze.p1).cwiseMin(trapeze.p2).cwiseMin(trapeze.p3).cwiseMax(0.0f);
		sibr::Vector3f pmax = trapeze.p0.cwiseMax(trapeze.p1).cwiseMax(trapeze.p2).cwiseMax(trapeze.p3).cwiseMin(sibr::Vector3f((float)infos.wi, (float)infos.hi, 0.0f));
			
		// Make the crop a multiple of 4 (for IBR half-images).
		int fWidth = (int)pmax(0) - (int)pmin(0); 
		fWidth += (fWidth % 4 == 0) ? 0 : 4 - fWidth % 4;
		int fHeight = (int)pmax(1) - (int)pmin(1); 
		fHeight += (fHeight % 4 == 0) ? 0 : 4 - fHeight % 4;
		// Clamp the box to the picture size, still respecting the multiplicity condition.
		while(fWidth + (int)pmin(0) >= (int)infos.wi){
			fWidth -= 4;
		}
		while(fHeight + (int)pmin(1) >= (int)infos.hi){
			fHeight -= 4;
		}

		// Build the bounding box.
		sibr::Vector2i finalShift = (_finalScaling.cwiseProduct(sibr::Vector2f(fWidth, fHeight))).cast<int>();
		cv::Point cornerLow = cv::Point((int)pmin(0) - finalShift[0], (int)pmin(1) - finalShift[1]);
		cv::Point cornerHigh = cv::Point((int)pmin(0) + fWidth + finalShift[0], (int)pmin(1) + fHeight + finalShift[1]);
		cv::Rect bbox = cv::Rect(cornerLow, cornerHigh);
		_boundingBoxes.push_back(bbox);
	}
}


void WindowsExtractor::exportAnnotatedInputImage(const sibr::ImageRGB & inputImage, const std::string rootPath, bool showBoundingBoxes, bool showTrapezoids){
	cv::Mat image = inputImage.toOpenCVBGR().clone();
	if(showBoundingBoxes){
		for(auto& box : _boundingBoxes){
			cv::rectangle(image, box, cv::Scalar(0,0,255),6);
		}
	}
	if(showTrapezoids){
		for(auto& trapeze : _unscaledReprojTrapezoids){
			cv::Point p0((int)trapeze.p0[0], (int)trapeze.p0[1]);
			cv::Point p1((int)trapeze.p1[0], (int)trapeze.p1[1]);
			cv::Point p2((int)trapeze.p2[0], (int)trapeze.p2[1]);
			cv::Point p3((int)trapeze.p3[0], (int)trapeze.p3[1]);
			cv::line(image, p0, p1, cv::Scalar(255,0,0), 6);
			cv::line(image, p1, p2, cv::Scalar(255,0,0), 6);
			cv::line(image, p2, p3, cv::Scalar(255,0,0), 6);
			cv::line(image, p3, p0, cv::Scalar(255,0,0), 6);
		}
	}
	cv::imwrite(rootPath + "_input_rects.png", image);
}

void WindowsExtractor::exportCrops(const sibr::ImageRGB & inputImage, const std::string rootPath, const std::set<int> & indices){
	int count = 0;
	cv::Mat image = inputImage.toOpenCVBGR();
	for(unsigned int k = 0; k < _boundingBoxes.size(); ++k){
		if(indices.count(k) > 0){
			auto& box = _boundingBoxes[k];
			cv::Mat crop = image(box);
			cv::imwrite(rootPath + "_crop_" + std::to_string(count) + ".png", crop);
			
			// Also export the mask.
			/*auto& trapeze = _unscaledReprojTrapezoids[k];
			std::vector<cv::Point> trapezoidPoints;
			trapezoidPoints.push_back(cv::Point((int)trapeze.p0[0], (int)trapeze.p0[1]));
			trapezoidPoints.push_back(cv::Point((int)trapeze.p1[0], (int)trapeze.p1[1]));
			trapezoidPoints.push_back(cv::Point((int)trapeze.p2[0], (int)trapeze.p2[1]));
			trapezoidPoints.push_back(cv::Point((int)trapeze.p3[0], (int)trapeze.p3[1]));

			cv::Mat mask(crop.rows, crop.cols, CV_8UC1);
			mask = 0;
			cv::fillConvexPoly(mask, trapezoidPoints, cv::Scalar(255));
			cv::imwrite(rootPath + "_crop_" + std::to_string(count) + "_mask.png", mask);
			*/
			++count;
		}
	}
}

void WindowsExtractor::exportRectifiedCrops(const sibr::ImageRGB & inputImage, const std::string rootPath, const RectificationInfos & infos, const std::set<int> & indices){
	int count = 0;
	sibr::Vector3f scalingInit((float)infos.wi/(float)infos.wo,	    (float)infos.hi/(float)infos.ho, 1.0f);
	sibr::Vector3f scalingRect((float)infos.wr/PIX2PIX_OUTPUT_SIZE, (float)infos.hr/PIX2PIX_OUTPUT_SIZE, 1.0f);

	for(unsigned int k = 0; k < _initialRectangles.size(); ++k){
		if(indices.count(k) > 0){
			auto& rect = _initialRectangles[k];
			// Create image for rectified crop.
			sibr::ImageRGB recCrop((uint)(rect.width * scalingRect(0)), (uint)(rect.height * scalingRect(1)));
			// Fill each pixel of it by reprojecting back and reading in the initial image.
			for(unsigned int i = 0; i < recCrop.h(); ++i){
				for(unsigned int j = 0; j < recCrop.w(); ++j){
					// Position  in global rectified image.
					sibr::Vector3f p = project(rect.x * scalingRect(0) + j, rect.y * scalingRect(1) + i, infos.H, sibr::Vector3f(1.0f,1.0f,1.0f), scalingInit);
				
					if(p(0) < 0 || p(1) < 0 || p(0) > inputImage.w()-1 || p(1) > inputImage.h()-1){
						continue;
					}
					// Read in the initial image.
					sibr::ImageRGB::Pixel rgb = readInterpolated(inputImage, p(0), p(1));
					recCrop(j, i) = rgb;
				}
			}
			// Save crop.
			recCrop.save(rootPath + "_rectified_crop_" + std::to_string(count) + ".png", false);
			++count;
		}
	}
}

void WindowsExtractor::exportGlobalMasks(const sibr::ImageRGB & inputImage, const std::string path){
	cv::Mat mask(inputImage.h(), inputImage.w(), CV_8UC1);// = inputImage.toOpenCVBGR().clone();
	mask = 0;

	for(auto& trapeze : _unscaledReprojTrapezoids){
		std::vector<cv::Point> trapezoidPoints;
		trapezoidPoints.push_back(cv::Point((int)trapeze.p0[0], (int)trapeze.p0[1]));
		trapezoidPoints.push_back(cv::Point((int)trapeze.p1[0], (int)trapeze.p1[1]));
		trapezoidPoints.push_back(cv::Point((int)trapeze.p2[0], (int)trapeze.p2[1]));
		trapezoidPoints.push_back(cv::Point((int)trapeze.p3[0], (int)trapeze.p3[1]));
		cv::fillConvexPoly(mask, trapezoidPoints, cv::Scalar(255));
	}
	cv::imwrite(path, mask);
}

void WindowsExtractor::exportMask(int k, uint w, uint h, const std::string & path){
	cv::Mat mask(h, w, CV_8UC1);// = inputImage.toOpenCVBGR().clone();
	mask = 0;

	auto& trapeze = _unscaledReprojTrapezoids[k];
	std::vector<cv::Point> trapezoidPoints;
	trapezoidPoints.push_back(cv::Point((int)trapeze.p0[0], (int)trapeze.p0[1]));
	trapezoidPoints.push_back(cv::Point((int)trapeze.p1[0], (int)trapeze.p1[1]));
	trapezoidPoints.push_back(cv::Point((int)trapeze.p2[0], (int)trapeze.p2[1]));
	trapezoidPoints.push_back(cv::Point((int)trapeze.p3[0], (int)trapeze.p3[1]));
	cv::fillConvexPoly(mask, trapezoidPoints, cv::Scalar(255));
	
	cv::imwrite(path, mask);
}

void WindowsExtractor::extractRectanglesFromLabels(const sibr::ImageL8 & labelsImage, std::vector<cv::Rect> & boxes){
	const float minimumArea = _minimumFraction * (float)labelsImage.w() * (float)labelsImage.h();

	cv::Size imageSize = cv::Size(labelsImage.w(),labelsImage.h());
	cv::Mat downScaled, downScaled2,  blurred2(imageSize, CV_8U), blurred(imageSize, CV_8U), binary;

	/*cv::Size imageSizeHalf = cv::Size(labelsImage.w()/2,labelsImage.h()/2);
	cv::pyrDown(labelsImage.toOpenCV(), downScaled, imageSizeHalf);
	cv::pyrUp(downScaled, blurred, imageSize);*/
	cv::GaussianBlur(labelsImage.toOpenCV(), blurred, cv::Size(_blurRadius, _blurRadius), 0);
	cv::threshold(blurred, binary, _threshold, 255, cv::THRESH_BINARY);
	if (_visualiseLabels) {
		cv::imshow("bin", binary);
		cv::waitKey(0);
	}
	std::vector<std::vector<cv::Point>> contours;
	cv::findContours(binary, contours, cv::RETR_EXTERNAL, cv::CHAIN_APPROX_SIMPLE);
	
	// Filter boxes too small.
	for(auto& contour : contours){
		cv::Rect box = cv::boundingRect(contour);
		if(box.area() > minimumArea){
			boxes.push_back(box);
		}
	}
}


Trapezoid WindowsExtractor::reprojectTrapezoid(const cv::Rect & rect, const RectificationInfos & infos){
	// Scaling from pix2pix output to final image.
	sibr::Vector3f scalingInit((float)infos.wi/(float)infos.wo,	    (float)infos.hi/(float)infos.ho, 1.0f);
	sibr::Vector3f scalingRect((float)infos.wr/PIX2PIX_OUTPUT_SIZE, (float)infos.hr/PIX2PIX_OUTPUT_SIZE, 1.0f);

	// Compute the 4 points.
	// (after projection we won't necessarily have a rectangle, so we need the 4 points.)
	sibr::Vector3f p0 = project((float)rect.x, (float)rect.y, infos.H, scalingRect, scalingInit);
	sibr::Vector3f p1 = project((float)rect.x, (float)rect.y + (float)rect.height, infos.H, scalingRect, scalingInit);
	sibr::Vector3f p2 = project((float)rect.x + (float)rect.width, (float)rect.y + (float)rect.height, infos.H, scalingRect, scalingInit);
	sibr::Vector3f p3 = project((float)rect.x + (float)rect.width, (float)rect.y, infos.H, scalingRect, scalingInit);
			
	Trapezoid trp;
	trp.p0 = p0; trp.p1 = p1;
	trp.p2 = p2; trp.p3 = p3;
	return trp;
}

void WindowsExtractor::reprojectRectangles( const RectificationInfos & infos, const std::vector<cv::Rect> & boxes){
	_initialRectangles.clear();
	_reprojectedTrapezoids.clear();
	_unscaledReprojTrapezoids.clear();
	
	for(auto& box : boxes){
		// First estimate unscalaed trapezoid.
		Trapezoid unscaledTrp = reprojectTrapezoid(box, infos);

		// Scale box to the correct aspect ratio.
		cv::Rect rect;
		rect.width = (int)floor(_rectangleScaling * box.width);
		rect.height = (int)floor(_rectangleScaling * box.height);
		rect.x = box.x - (int)floor(box.width * _rectangleShift);
		rect.y = box.y - (int)floor(box.height * _rectangleShift);

		Trapezoid scaledTrp = reprojectTrapezoid(rect, infos);

		// Check that they are all inside the initial image, else discard the current box. We don't want to keep a half-cut-window.
		if(scaledTrp.p0(0) < 0 || scaledTrp.p2(0) > infos.wi - 1 || scaledTrp.p0(1) < 0 || scaledTrp.p2(1) > infos.hi){
			//continue;
			// No, keep it.
		}
		
		_unscaledReprojTrapezoids.push_back(unscaledTrp);
		_reprojectedTrapezoids.push_back(scaledTrp);
		_initialRectangles.push_back(rect);
	}
}

sibr::Vector3f WindowsExtractor::project(float x, float y, const Eigen::Matrix3f & H, const sibr::Vector3f preScaling, const sibr::Vector3f postScaling){
	sibr::Vector3f p0(x,y,1.0f);
	p0 = p0.cwiseProduct(preScaling);
	p0 = H * p0;
	p0 /= p0(2);
	p0 = p0.cwiseProduct(postScaling);
	return p0;
}
	
sibr::ImageRGB::Pixel WindowsExtractor::readInterpolated(const sibr::ImageRGB & image, float x, float y){
	int lowx = (int)std::floor(x);
	int lowy = (int)std::floor(y);

	int highx = lowx+1;
	int highy = lowy+1;
	float xfactor = x - (float)lowx;
	float yfactor = y - (float)lowy;

	auto pll = image(lowx, lowy).cast<float>();
	auto phl = image(highx, lowy).cast<float>();
	auto plh = image(lowx, highy).cast<float>();
	auto phh = image(highx, highy).cast<float>();
	
	auto pfinal = (1.0f - xfactor ) * ((1.0f - yfactor) * pll + yfactor * plh) + xfactor * ((1.0f - yfactor) * phl + yfactor * phh);
	return pfinal.cast<uint8>();

}
