/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#ifndef __SIBR_REPETITION_SELECTION_INTERFACE_HPP__
#define __SIBR_REPETITION_SELECTION_INTERFACE_HPP__

#include <set>
#include <string>
#include <core/graphics/Image.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/core/core.hpp>

class SelectionInterface {

public:

	SelectionInterface(const std::string & name, float scaling = 0.25);

	void display(const sibr::ImageRGB & image, const std::vector<cv::Rect> & rects);

	/// Selection callback functions.
	static void staticCallBackFunction(int event, int x, int y, int flags, void* userdata);

	void beginSelection(const cv::Point & point);
	void endSelection(const cv::Point & point);
	void updateSelection(const cv::Point & point);

	// Getters
	std::set<int> getSelectedIndices();
	bool selectionActive(){ return _selectionActive; }

private:

	void updateRectangles();

	void refreshScreen();

	void blitImage();
	void resetImage();

	std::string _interfaceName;
	float _scaling;

	cv::Mat _initialImage;
	cv::Mat _currentImage;

	// Rectangles.
	std::vector<cv::Rect> _rectsScaled;
	std::set<int> _indicesToKeep;
	
	// Selection data.
	cv::Point _firstPoint;
	bool _selectionActive; 
	

};
#endif