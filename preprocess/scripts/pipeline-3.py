# Copyright (C) 2020, Inria
# GRAPHDECO research group, https://team.inria.fr/graphdeco
# All rights reserved.
# 
# This software is free for non-commercial, research and evaluation use 
# under the terms of the LICENSE.md file.
# 
# For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr


#!/usr/bin/env python
#! -*- encoding: utf-8 -*-
import subprocess
import shutil
import os
import re
import sys
import struct
import imghdr
from tempfile import mkstemp
from shutil import move
from os import remove, close
from utils.commands import getProcess
from utils.paths import getBinariesPath

SIBR_INSTALL_PATH = getBinariesPath()

if len(sys.argv) < 3:
    print("python scripts/pipeline-3.py C:/path/to/scene_dir1 C:/path/to/scene_dir2")
    exit()

ROOTPATH1 = sys.argv[1]
ROOTPATH2 = sys.argv[2]

IBR_FULL_SCENE_PATH = ROOTPATH1 + "/IBR_mergeScene/"
IBR_FULL_SCENE_EQ_PATH = ROOTPATH1 + "/IBR_mergeScene_eq/"

STITCH_PATH = ROOTPATH1 + "/stitch/"

SIBR_SCENE_BUILDER_BIN = getProcess("SIBR_repetitions_scene_builder", SIBR_INSTALL_PATH)
SIBR_LAYERS_COMPOSITION_BIN = getProcess("SIBR_repetitions_layers_compositing", SIBR_INSTALL_PATH)

# Two scenes case
pro = subprocess.Popen([SIBR_SCENE_BUILDER_BIN, "--merge", "--path1", ROOTPATH1, "--path2", ROOTPATH2, "--dest", IBR_FULL_SCENE_PATH], cwd=SIBR_INSTALL_PATH)
pro.wait()

shutil.copyfile(ROOTPATH1 + "/plane_data.txt", IBR_FULL_SCENE_PATH+"/plane_data.txt")
# MRF segmentation and stitching.
if os.path.exists(IBR_FULL_SCENE_EQ_PATH):
	shutil.rmtree(IBR_FULL_SCENE_EQ_PATH)

shutil.copytree(IBR_FULL_SCENE_PATH, IBR_FULL_SCENE_EQ_PATH)


pro = subprocess.Popen([SIBR_LAYERS_COMPOSITION_BIN, "--path", IBR_FULL_SCENE_PATH,"--width", "2500"], cwd=SIBR_INSTALL_PATH)
pro.wait()

for file_name in os.listdir( STITCH_PATH ):
	if os.path.isfile(os.path.join(STITCH_PATH, file_name)):
		m = re.search("(.+?).jpg", file_name)
		if m:
			num_str = m.group(1)
			img_id = int(num_str)
			file_jpg = "%08d.jpg" % img_id
			new_file_name = IBR_FULL_SCENE_EQ_PATH + file_jpg            
			shutil.copyfile( os.path.join(STITCH_PATH, file_name), new_file_name)