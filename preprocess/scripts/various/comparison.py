# Copyright (C) 2020, Inria
# GRAPHDECO research group, https://team.inria.fr/graphdeco
# All rights reserved.
# 
# This software is free for non-commercial, research and evaluation use 
# under the terms of the LICENSE.md file.
# 
# For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr


#!/usr/bin/env python
#! -*- encoding: utf-8 -*-
import subprocess
import sys
import os
import shutil
import re

OPENMVG_OUT_PATH = "D:/sirodrig/code/openMVG/out"
OPENMVG_SFM_SCRIPT = OPENMVG_OUT_PATH + "/software/SfM/SfM_SequentialPipeline.py"
OPENMVG_SFM_CONVERT_BIN = OPENMVG_OUT_PATH + "/Windows-AMD64-Release/Release/openMVG_main_openMVG2PMVS.exe"

ROOTPATH = sys.argv[1]
#IMAGES_DIR = ROOTPATH + '/images/'

FULL_SFM_DIR = ROOTPATH + "/full/"
PMVS_OUT_PATH = FULL_SFM_DIR + "/PMVS/"
PMVS_OUT_IMAGES_PATH = PMVS_OUT_PATH + "/visualize/"
PMVS_RC_PATH = FULL_SFM_DIR + "/RC/"

#print("Running SFM on whole images.\n")

#export = subprocess.Popen(["python", OPENMVG_SFM_SCRIPT, IMAGES_DIR, FULL_SFM_DIR])
#export.wait()

export = subprocess.Popen( [OPENMVG_SFM_CONVERT_BIN, "-i", FULL_SFM_DIR + "/reconstruction_sequential/sfm_data.bin", "-o", FULL_SFM_DIR])
export.wait()



# Copy files for reality capture.
if not os.path.exists(PMVS_RC_PATH):
    os.makedirs( PMVS_RC_PATH )
shutil.copyfile( PMVS_OUT_PATH + "/bundle.rd.out", PMVS_RC_PATH + "/bundle.out" )

for file_name in os.listdir( PMVS_OUT_IMAGES_PATH  ):
    if os.path.isfile(os.path.join(PMVS_OUT_IMAGES_PATH, file_name)):
        m = re.search("(.+?).jpg", file_name)
        if m:
            num_str = m.group(1)
            img_id = int(num_str)
            file_jpg = "%05d.jpg" % img_id
            new_file_name = PMVS_RC_PATH + file_jpg            
            shutil.copyfile( os.path.join(PMVS_OUT_IMAGES_PATH, file_name), new_file_name)