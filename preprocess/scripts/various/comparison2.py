# Copyright (C) 2020, Inria
# GRAPHDECO research group, https://team.inria.fr/graphdeco
# All rights reserved.
# 
# This software is free for non-commercial, research and evaluation use 
# under the terms of the LICENSE.md file.
# 
# For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr


#!/usr/bin/env python
#! -*- encoding: utf-8 -*-
import subprocess
import shutil
import os
import re
import sys
import struct
import imghdr
from tempfile import mkstemp
from shutil import move
from os import remove, close
from utils.commands import getProcess
from utils.paths import getBinariesPath

SIBR_INSTALL_PATH = getBinariesPath()
SIBR_MESH_TRANSFORM_BIN = getProcess("SIBR_rc_mesh_transformer", SIBR_INSTALL_PATH)
ROOTPATH = sys.argv[1]

IBR_PATH = ROOTPATH + "/full/IBR_data_OPENMVG-CMPMVS/"
IBR_PLY_PATH = IBR_PATH + "/pmvs/models/"
IBR_LIST_PATH = IBR_PATH + "/list_images.txt"

PMVS_RC_PATH = ROOTPATH + "/full/RC/"
def replace(file_path, pattern, subst):
	#Create temp file
	fh, abs_path = mkstemp()
	with open(abs_path,'w') as new_file:
		with open(file_path) as old_file:
			for line in old_file:
				new_file.write(line.replace(pattern, subst))
	close(fh)
	#Remove original file
	remove(file_path)
	#Move new file
	move(abs_path, file_path)   


def get_image_size(fname):
	'''Determine the image type of fhandle and return its size.
	from draco'''
	with open(fname, 'rb') as fhandle:
		head = fhandle.read(24)
		if len(head) != 24:
			return
		if imghdr.what(fname) == 'png':
			check = struct.unpack('>i', head[4:8])[0]
			if check != 0x0d0a1a0a:
				return
			width, height = struct.unpack('>ii', head[16:24])
		elif imghdr.what(fname) == 'gif':
			width, height = struct.unpack('<HH', head[6:10])
		elif imghdr.what(fname) == 'jpeg':
			try:
				fhandle.seek(0) # Read 0xff next
				size = 2
				ftype = 0
				while not 0xc0 <= ftype <= 0xcf:
					fhandle.seek(size, 1)
					byte = fhandle.read(1)
					while ord(byte) == 0xff:
						byte = fhandle.read(1)
					ftype = ord(byte)
					size = struct.unpack('>H', fhandle.read(2))[0] - 2
				# We are at a SOFn block
				fhandle.seek(1, 1)  # Skip `precision' byte.
				height, width = struct.unpack('>HH', fhandle.read(4))
			except Exception: #IGNORE:W0703
				return
		else:
			return
		return width, height


if not os.path.exists(IBR_PATH):
	os.makedirs( IBR_PATH )
	os.makedirs( IBR_PLY_PATH )

#shutil.copyfile( ,  )
subprocess.call([SIBR_MESH_TRANSFORM_BIN, "-path", PMVS_RC_PATH + "/mesh.ply", "-out", IBR_PLY_PATH+"/pmvs_recon.ply"])
	#Generate bundler file

shutil.copyfile( PMVS_RC_PATH + "/bundle.out", IBR_PATH + "/bundle.out" )
	
	#create file listing
file_listImgs = open(IBR_LIST_PATH, 'w')
for file_name in os.listdir( PMVS_RC_PATH  ):
	if os.path.isfile(os.path.join(PMVS_RC_PATH, file_name)):
		m = re.search("(.+?).jpg", file_name)
		if m:
			num_str = m.group(1)
			img_id = int(num_str)
			file_jpg = "%08d.jpg" % img_id
			new_file_name = IBR_PATH + file_jpg            
			shutil.copyfile( os.path.join(PMVS_RC_PATH, file_name), new_file_name)
			imgsize = get_image_size(new_file_name)                                    
			print(file_jpg, imgsize[0], imgsize[1], file=file_listImgs )            
									
file_listImgs.close()
