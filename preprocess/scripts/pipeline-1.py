# Copyright (C) 2020, Inria
# GRAPHDECO research group, https://team.inria.fr/graphdeco
# All rights reserved.
# 
# This software is free for non-commercial, research and evaluation use 
# under the terms of the LICENSE.md file.
# 
# For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr


#!/usr/bin/env python
#! -*- encoding: utf-8 -*-
import subprocess
import shutil
import os
import re
import sys
from tempfile import mkstemp
from shutil import move
from os import remove, close
from utils.commands import getProcess
from utils.paths import getBinariesPath

SIBR_INSTALL_PATH = getBinariesPath()
OPENMVG_OUT_PATH = "D:/sirodrig/code/openMVG/out/"
IMAGE_MAGICK_BIN = '\"C:/Program Files/ImageMagick-6.9.2-Q16/convert.exe\" '

SCALE = 1.75
FORCE_RC_BUNDLE = False
FORCE_INIT_FOCAL = False
FORCE_FOCAL_VALUE = 3600.0
WINDOW_THRESH = 80 #50 80
WINDOW_SMOOTH = 11 #21 11
WINDOWS_EXPAND_WIDTH = 0.0
WINDOWS_EXPAND_HEIGHT = 0.0

if len(sys.argv) < 3:
    print("python scripts/pipeline-1.py C:/path/to/scene_dir --STEP")
    exit()

ROOTPATH = sys.argv[1]
FIRST_PASS = (sys.argv[2] == "--full")
SECOND_PASS = (sys.argv[2] == "--crops")

SIBR_AUTO_PLANE_EXTRACTION_BIN = getProcess("SIBR_repetitions_plane_fitting", SIBR_INSTALL_PATH)
SIBR_WINDOWS_EXTRACTION_BIN = getProcess("SIBR_repetitions_windows_extraction", SIBR_INSTALL_PATH)
SIBR_CAMERAS_ESTIMATION_BIN = getProcess("SIBR_repetitions_cameras_estimation", SIBR_INSTALL_PATH)
OPENMVG_SFM_CONVERT_BIN = OPENMVG_OUT_PATH + "/Windows-AMD64-Release/Release/openMVG_main_openMVG2PMVS.exe"
OPENMVG_SFM_SCRIPT = "scripts/openmvg/SfM_SequentialPipeline.py"
OPENMVG_SFM_SCRIPT_FILTERED = "scripts/openmvg/SfM_FilteredSequentialPipeline.py"

IMAGES_DIR = ROOTPATH + '/images/'
MASKS_DIR = ROOTPATH + '/masks/'
CROP_LIST_PATH = ROOTPATH + '/crop_list.txt'
CROPS_DIR = ROOTPATH + '/crops/'
FULL_SFM_DIR = ROOTPATH + "/full/"
FULL_SFM_OUTPUT_JSON = ROOTPATH + "/full/reconstruction_sequential/sfm_data.json"
ALIGNED_CAM_PATH = ROOTPATH + "/aligned_cams.txt"
PMVS_OUT_PATH = ROOTPATH + "/PMVS/"
PMVS_OUT_IMAGES_PATH = ROOTPATH + "/PMVS/visualize/"
PMVS_RC_PATH = PMVS_OUT_PATH + "/RC/"

FULL_PMVS_OUT_PATH = FULL_SFM_DIR + "/PMVS/"
FULL_PMVS_OUT_IMAGES_PATH = FULL_PMVS_OUT_PATH + "/visualize/"
FULL_PMVS_RC_PATH = FULL_PMVS_OUT_PATH + "/RC/"



def replace(file_path, pattern, subst):
    #Create temp file
    fh, abs_path = mkstemp()
    with open(abs_path,'w') as new_file:
        with open(file_path) as old_file:
            for line in old_file:
                new_file.write(line.replace(pattern, subst))
    close(fh)
    #Remove original file
    remove(file_path)
    #Move new file
    move(abs_path, file_path)   



if FIRST_PASS:
    if not os.path.exists(FULL_SFM_DIR):
        print("Running SFM on whole images.\n")
        fullSfmArgs = ["python", OPENMVG_SFM_SCRIPT, OPENMVG_OUT_PATH, IMAGES_DIR, FULL_SFM_DIR]
        if FORCE_INIT_FOCAL:
            fullSfmArgs += [ str(FORCE_FOCAL_VALUE) ]
        subprocess.call(fullSfmArgs)

        export = subprocess.Popen( [OPENMVG_SFM_CONVERT_BIN, "-i", FULL_SFM_DIR + "/reconstruction_sequential/sfm_data.bin", "-o", FULL_SFM_DIR])
        export.wait()

        # Copy files for reality capture for fullScene comparison.
        if not os.path.exists(FULL_PMVS_RC_PATH):
            os.makedirs( FULL_PMVS_RC_PATH )
        shutil.copyfile( FULL_PMVS_OUT_PATH + "/bundle.rd.out", FULL_PMVS_RC_PATH + "/bundle.out" )

        for file_name in os.listdir( FULL_PMVS_OUT_IMAGES_PATH  ):
            if os.path.isfile(os.path.join(FULL_PMVS_OUT_IMAGES_PATH, file_name)):
                m = re.search("(.+?).jpg", file_name)
                if m:
                    num_str = m.group(1)
                    img_id = int(num_str)
                    file_jpg = "%05d.jpg" % img_id
                    new_file_name = FULL_PMVS_RC_PATH + file_jpg            
                    shutil.copyfile( os.path.join(FULL_PMVS_OUT_IMAGES_PATH, file_name), new_file_name)
        # Auto plane fitting.
        exp = subprocess.Popen([SIBR_AUTO_PLANE_EXTRACTION_BIN, FULL_SFM_OUTPUT_JSON], cwd=SIBR_INSTALL_PATH)
    	exp.wait()
    else:
        print("Using existing reconstruction")


if SECOND_PASS:

    print("Extracting crops from Pix2Pix output.\n")
    #default parameters: --th 80 (lower means larger areas labeled as windows), --smooth 11 (higher is blurrier)
    exp = subprocess.Popen([SIBR_WINDOWS_EXTRACTION_BIN, "--path", ROOTPATH, "--scale", str(SCALE), "--th", str(WINDOW_THRESH), "--smooth", str(WINDOW_SMOOTH), "--sh", str(WINDOWS_EXPAND_HEIGHT), "--sw", str(WINDOWS_EXPAND_WIDTH)], cwd=SIBR_INSTALL_PATH)
    exp.wait()
    print("Generating crops images.\n")
    if not os.path.exists(CROPS_DIR):
        os.makedirs( CROPS_DIR )
    if not os.path.exists(MASKS_DIR):
        os.makedirs( MASKS_DIR )
    nothingExtracted=True
    with open(CROP_LIST_PATH) as listing:
        for line in listing:
            if line[0] not in ['c', '\n', '\t', '\r']:
                image_name = line[:-1]
                sub_index = 0
            elif line[0] == 'c':
                nothingExtracted=False
                comps = line.split()
                x = comps[1]
                y = comps[2]
                w = comps[3]
                h = comps[4]
                base_name = image_name[:image_name.rfind('.')]
                extension = image_name[image_name.rfind('.'):]
                new_image_name = base_name + "_crop_" + str(sub_index) + extension
                com = IMAGE_MAGICK_BIN + ' ' + os.path.join(IMAGES_DIR, image_name)
                com += ' -auto-orient +repage -crop ' + str(w) + 'x' + str(h) + '+' + str(x) + '+' + str(y) + ' +repage '
                com += ' ' + os.path.join(CROPS_DIR, new_image_name)
                subprocess.call(com)
                #also generate the mask
                new_image_name = base_name + "_crop_" + str(sub_index) + "_mask" + ".png"
                com = IMAGE_MAGICK_BIN + ' ' + os.path.join(ROOTPATH + "/images-masks", base_name + "_mask_" + str(sub_index) + ".JPG")
                com += ' -auto-orient +repage -crop ' + str(w) + 'x' + str(h) + '+' + str(x) + '+' + str(y) + ' +repage '
                com += ' ' + os.path.join(MASKS_DIR, new_image_name) 
                subprocess.call(com)
                sub_index += 1

    if nothingExtracted:
        print("No crops found, aborting.\n")
        exit()

    
    print("Please export aligned cameras.\n")
    exportCamsArgs = [SIBR_CAMERAS_ESTIMATION_BIN, "--c", CROP_LIST_PATH, "--o", ROOTPATH]
    if FORCE_RC_BUNDLE:
        exportCamsArgs += ["--s", FULL_SFM_DIR+"/reconstruction_sequential/bundle.out"]
    else:
        exportCamsArgs += ["--s", FULL_SFM_DIR+"/reconstruction_sequential/sfm_data.json"]
    exp = subprocess.Popen(exportCamsArgs, cwd=SIBR_INSTALL_PATH)
    exp.wait()
    print(" ".join(exportCamsArgs))
    if not os.path.exists(ALIGNED_CAM_PATH):
        print("No initial cameras found, aborting.\n")
        exit()

    print("Performing filtered OpenMVG SFM.\n")

    failed = []
    output_dir = ROOTPATH + "/sfm"

    cropSfmArgs =  ["python", OPENMVG_SFM_SCRIPT_FILTERED, OPENMVG_OUT_PATH, CROPS_DIR, ALIGNED_CAM_PATH, output_dir]
    if FORCE_INIT_FOCAL:
        cropSfmArgs += [ str(FORCE_FOCAL_VALUE) ]

    exp = subprocess.Popen(cropSfmArgs)
    exp.wait()

    export = subprocess.Popen( [OPENMVG_SFM_CONVERT_BIN, "-i", output_dir + "/reconstruction_sequential/sfm_data.bin", "-o", ROOTPATH])
    export.wait()
    if os.path.exists(ROOTPATH + "/prematchingSiftGPU.cams"):
        os.remove(ROOTPATH + "/prematchingSiftGPU.cams")
        os.remove(ROOTPATH + "/prematchingSiftGPU.imgs")
        os.remove(ROOTPATH + "/prematchingSiftGPU.out")

    # Copy files for reality capture.
    if not os.path.exists(PMVS_RC_PATH):
        os.makedirs( PMVS_RC_PATH )
    shutil.copyfile( PMVS_OUT_PATH + "/bundle.rd.out", PMVS_RC_PATH + "/bundle.out" )

    for file_name in os.listdir( PMVS_OUT_IMAGES_PATH  ):
        if os.path.isfile(os.path.join(PMVS_OUT_IMAGES_PATH, file_name)):
            m = re.search("(.+?).jpg", file_name)
            if m:
                num_str = m.group(1)
                img_id = int(num_str)
                file_jpg = "%05d.jpg" % img_id
                new_file_name = PMVS_RC_PATH + file_jpg            
                shutil.copyfile( os.path.join(PMVS_OUT_IMAGES_PATH, file_name), new_file_name)

