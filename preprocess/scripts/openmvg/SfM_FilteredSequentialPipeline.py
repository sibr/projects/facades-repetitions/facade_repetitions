# Copyright (C) 2020, Inria
# GRAPHDECO research group, https://team.inria.fr/graphdeco
# All rights reserved.
# 
# This software is free for non-commercial, research and evaluation use 
# under the terms of the LICENSE.md file.
# 
# For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr


#!/usr/bin/env python
#! -*- encoding: utf-8 -*-

# Python implementation of the bash script written by Romuald Perrot
# Created by @vins31
# Modified by Pierre Moulon
#
# this script is for easy use of OpenMVG
#
# usage : python openmvg.py image_dir output_dir
#
# image_dir is the input directory where images are located
# output_dir is where the project must be saved
#
# if output_dir is not present script will create it
#


import os
import subprocess
import sys

if len(sys.argv) < 5:
    print ("Usage %s openmvg_out_path image_dir aligned_cams_path output_dir" % sys.argv[0])
    sys.exit(1)


OPENMVG_OUT_PATH = sys.argv[1] #"D:/sirodrig/code/openMVG/out/"
input_dir = sys.argv[2]
aligned_cam_path = sys.argv[3]
output_dir = sys.argv[4]

# Indicate the openMVG binary directory
OPENMVG_SFM_BIN = OPENMVG_OUT_PATH + "/Windows-AMD64-Release/Release"
CAMERA_SENSOR_WIDTH_DIRECTORY = OPENMVG_OUT_PATH + "/../src/openMVG/exif/sensor_width_database"
matches_dir = os.path.join(output_dir, "matches")
reconstruction_dir = os.path.join(output_dir, "reconstruction_sequential")
camera_file_params = os.path.join(CAMERA_SENSOR_WIDTH_DIRECTORY, "sensor_width_camera_database.txt")

print ("Using input dir  : ", input_dir)
print ("      output_dir : ", output_dir)
# Create the ouput/matches folder if not present
if not os.path.exists(output_dir):
  os.mkdir(output_dir)
if not os.path.exists(matches_dir):
  os.mkdir(matches_dir)

#Handle forced focal
force_focal = False
focal_value = ''
if len(sys.argv) > 5:
    force_focal = True
    focal_value = sys.argv[5]
    print("Using focal value : ", focal_value)

print ("1. Intrinsics analysis")
argsIntrinsics = [os.path.join(OPENMVG_SFM_BIN, "openMVG_main_SfMInit_ImageListing"),  "-i", input_dir, "-o", matches_dir]
if force_focal:
    argsIntrinsics += ["-f", focal_value]
else:
    argsIntrinsics += ["-d", camera_file_params]

pIntrisics = subprocess.Popen( argsIntrinsics)
pIntrisics.wait()

print ("2. Compute features")
pFeatures = subprocess.Popen( [os.path.join(OPENMVG_SFM_BIN, "openMVG_main_ComputeFeatures"),  "-i", matches_dir+"/sfm_data.json", "-o", matches_dir, "-m", "SIFT", "-p", "ULTRA"] )
pFeatures.wait()

print ("3. Compute matches")
pMatches = subprocess.Popen( [os.path.join(OPENMVG_SFM_BIN, "openMVG_main_ComputeMatches"),  "-i", matches_dir+"/sfm_data.json", "-d", "0.5", "-b", aligned_cam_path , "-o", matches_dir, "-m", "0", "-n", "BRUTEFORCEL2"] )
pMatches.wait()

# Create the reconstruction if not present
if not os.path.exists(reconstruction_dir):
    os.mkdir(reconstruction_dir)

print ("4. Do Sequential/Incremental reconstruction")
pRecons = subprocess.Popen( [os.path.join(OPENMVG_SFM_BIN, "openMVG_main_IncrementalSfM"),  "-i", matches_dir+"/sfm_data.json", "-m", matches_dir, "-f", "ADJUST_FOCAL_LENGTH", "-o", reconstruction_dir] )
pRecons.wait()

#print ("5. Colorize Structure")
#pRecons = subprocess.Popen( [os.path.join(OPENMVG_SFM_BIN, "openMVG_main_ComputeSfM_DataColor"),  "-i", reconstruction_dir+"/sfm_data.bin", "-o", os.path.join(reconstruction_dir,"colorized.ply")] )
#pRecons.wait()

# optional, compute final valid structure from the known camera poses
print ("6. Structure from Known Poses (robust triangulation)")
pRecons = subprocess.Popen( [os.path.join(OPENMVG_SFM_BIN, "openMVG_main_ComputeStructureFromKnownPoses"),  "-i", reconstruction_dir+"/sfm_data.bin", "-m", matches_dir, "-f", os.path.join(matches_dir, "matches.f.bin"), "-o", os.path.join(reconstruction_dir,"robust.bin")] )
pRecons.wait()

#pRecons = subprocess.Popen( [os.path.join(OPENMVG_SFM_BIN, "openMVG_main_ComputeSfM_DataColor"),  "-i", reconstruction_dir+"/robust.bin", "-o", os.path.join(reconstruction_dir,"robust_colorized.ply")] )
#pRecons.wait()

#print ("6. Structure from Known Poses (robust triangulation)")
#pRecons = subprocess.Popen( [os.path.join(OPENMVG_SFM_BIN, "openMVG_main_ComputeStructureFromKnownPoses"),  "-i", matches_dir+"/sfm_data.json", "-m", matches_dir, "-f", os.path.join(matches_dir, "matches.f.bin"),  "-b", "1", "-o", os.path.join(reconstruction_dir,"robust.bin")] )
#pRecons.wait()


