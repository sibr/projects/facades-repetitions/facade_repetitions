# Copyright (C) 2020, Inria
# GRAPHDECO research group, https://team.inria.fr/graphdeco
# All rights reserved.
# 
# This software is free for non-commercial, research and evaluation use 
# under the terms of the LICENSE.md file.
# 
# For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr


#!/usr/bin/env python
#! -*- encoding: utf-8 -*-
import numpy as np
from scipy import misc
import argparse
import os
import math
import glob
import shutil
import subprocess

parser = argparse.ArgumentParser()
parser.add_argument("--input_dir", required=True, help="path to folder containing images")
parser.add_argument('--post_process', action='store_true', default=False)
parser.add_argument('--no_rectif', action='store_true', default=False)
a = parser.parse_args()



INPUT_DIR_PATH = a.input_dir + "/images/"
if not os.path.exists(INPUT_DIR_PATH):
    raise Exception("input_dir does not exist")

OUTPUT_DIR_PATH = a.input_dir + "/rectified/"

if not os.path.exists(OUTPUT_DIR_PATH):
    os.makedirs(OUTPUT_DIR_PATH)

MATRICES_PATH = a.input_dir + "/matrices/"
if not os.path.exists(MATRICES_PATH):
    os.makedirs(MATRICES_PATH)

LABELS_PATH = a.input_dir + "/labels/"
if not os.path.exists(LABELS_PATH):
    os.makedirs(LABELS_PATH)

LABELS_DEBUG_PATH = a.input_dir + "/labels-debug/"
if not os.path.exists(LABELS_DEBUG_PATH):
    os.makedirs(LABELS_DEBUG_PATH)

RECTIFIER_PATH = "binaries/repdemo.exe"
CONVERT_PATH = "C:/Program Files/ImageMagick-6.9.2-Q16/convert.exe"

PIX2PIX_OUTPUT_PATH = a.input_dir + "/pix2pix/"



def get_name(path):
    name, _ = os.path.splitext(os.path.basename(path))
    return name


if not a.post_process:
    input_paths = glob.glob(os.path.join(INPUT_DIR_PATH, "*.jpg"))
    if len(input_paths) == 0:
        raise Exception("input_dir contains no image files")

    for input_path in input_paths:
        file_name = get_name(input_path) ;
        output_path = os.path.join(OUTPUT_DIR_PATH, file_name + ".jpg")
        output_matrix_path_temp = os.path.join(OUTPUT_DIR_PATH, file_name + "_params.txt")
        output_matrix_path = os.path.join(MATRICES_PATH, file_name + "_params.txt")
        output_path_temp = os.path.join(OUTPUT_DIR_PATH, get_name(input_path) + "_out.jpg")
        output_path_sift = os.path.join(OUTPUT_DIR_PATH, file_name + ".sift")
        if not a.no_rectif:
            #Copy image to output dir
            shutil.copy(input_path, output_path)
            subprocess.call([CONVERT_PATH, output_path,  "-resize", "x1000", output_path])
            # perform rectification
            subprocess.call([RECTIFIER_PATH, output_path])
            #remove initial image
            if not os.path.exists(output_path_temp):
                print(file_name + " failed.")
                continue
            os.remove(output_path)
            os.remove(output_path_sift)
            shutil.copy(output_path_temp, output_path)
            shutil.copy(output_matrix_path_temp, output_matrix_path)
            os.remove(output_path_temp)
            os.remove(output_matrix_path_temp)

        #resize rectified to 256x256
        #remove sift
        subprocess.call([CONVERT_PATH, output_path, "-resize", "256x256!", output_path])
        subprocess.call([CONVERT_PATH, output_path, "-gravity", "west", "-background", "black", "-extent", "512x256" , output_path])
    print("python "+"scripts/pix2pix-classif.py"+" --mode "+"test"+" --checkpoint "+"pix2pix-weights/"+" --input_dir " + OUTPUT_DIR_PATH + " --output_dir " + PIX2PIX_OUTPUT_PATH + "\n")
       

    #subprocess.call(["python","pix2pix-classif.py","--mode","test","--checkpoint","pix2pix-weights/","--input_dir", OUTPUT_DIR_PATH, "--output_dir", PIX2PIX_OUTPUT_PATH])

else:

    labels_input_paths = glob.glob(os.path.join(PIX2PIX_OUTPUT_PATH, "images/*-outputs.png"))
    if len(labels_input_paths) == 0:
        raise Exception("input_dir contains no image files")

    colormap = np.array([[0, 0, 170], [0, 0, 255], [0, 85, 255], [0, 170, 255], [0, 255, 255], [85, 255, 170], [170, 255, 85], [255, 255, 0], [255, 170, 0], [255, 85, 0], [255, 0, 0], [170, 0, 0]], dtype=np.uint8)

    for label_input_path in labels_input_paths:
        output_path = os.path.join(LABELS_PATH, get_name(label_input_path) + ".png")
        shutil.copy(label_input_path, output_path)

        output_debug_path = os.path.join(LABELS_DEBUG_PATH,get_name(label_input_path) + ".png")
        image = misc.imread(label_input_path)
        new_image = np.zeros([image.shape[0], image.shape[1], 3], dtype = np.uint8)
        for c in range(colormap.shape[0]):
            color = colormap[c]
            idx = image == c
            new_image[idx,0] = color[0]
            new_image[idx,1] = color[1]
            new_image[idx,2] = color[2]
        misc.imsave(output_debug_path, new_image)