# Copyright (C) 2020, Inria
# GRAPHDECO research group, https://team.inria.fr/graphdeco
# All rights reserved.
# 
# This software is free for non-commercial, research and evaluation use 
# under the terms of the LICENSE.md file.
# 
# For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr


#!/usr/bin/env python
#! -*- encoding: utf-8 -*-
import subprocess
import shutil
import os
import re
import sys
import struct
import imghdr
from tempfile import mkstemp
from shutil import move
from os import remove, close
from utils.commands import getProcess
from utils.paths import getBinariesPath

SIBR_INSTALL_PATH = getBinariesPath()


if len(sys.argv) < 3:
    print("python scripts/pipeline-2.py C:/path/to/scene_dir --STEP")
	exit()

ROOTPATH = sys.argv[1]
FIRST_PASS = False
SECOND_PASS = False
THIRD_PASS = False

if sys.argv[2] == "--select":
	FIRST_PASS = True
elif sys.argv[2] == "--refine"
	SECOND_PASS = True
elif sys.argv[2] == "--final":
	THIRD_PASS = True
	

SIBR_MESH_TRANSFORM_BIN = getProcess("SIBR_rc_mesh_transformer", SIBR_INSTALL_PATH)
SIBR_SCENE_BUILDER_BIN = getProcess("SIBR_repetitions_scene_builder", SIBR_INSTALL_PATH)
SIBR_LAYERS_EXTRACTION_BIN = getProcess("SIBR_repetitions_layers_extraction", SIBR_INSTALL_PATH)
SIBR_LAYERS_COMPOSITION_BIN = getProcess("SIBR_repetitions_layers_compositing", SIBR_INSTALL_PATH)

IBR_PATH = ROOTPATH + "/IBR_data_OPENMVG-CMPMVS/"
IBR_BCKP_PATH = ROOTPATH + "/IBR_original_OPENMVG-CMPMVS/"
IBR_BCKP_MESH_PATH = ROOTPATH + "/IBR_original_noisy_OPENMVG-CMPMVS/"
IBR_FULL_SCENE_PATH = ROOTPATH + "/IBR_fullScene/"
IBR_FULL_SCENE_EQ_PATH = ROOTPATH + "/IBR_fullScene_eq/"
IBR_PLY_PATH = IBR_PATH + "/pmvs/models/"
IBR_LIST_PATH = IBR_PATH + "/list_images.txt"
IBR_HALF_IMAGES_PATH  = IBR_PATH + "/half_size/"
IBR_SUPERPIXELS_PATH = IBR_PATH + "/superpixels/"
IBR_MASKS_PATH = IBR_PATH + "/masks/"
ROOT_MASKS_PATH = ROOTPATH + "/masks/"

IBR_REFLECTIONS_MASKS_PATH = IBR_FULL_SCENE_PATH + "/masks_auto/"
ROOT_REFLECTION_MASKS_PATH = ROOTPATH + "/masks_auto/"

LAYERS_PATH = ROOTPATH + "/layers/pass1/"
PMVS_RC_PATH = ROOTPATH + "/PMVS/RC/"
STITCH_PATH = ROOTPATH + "/stitch/"
SEGMENTATION_PATH = ROOTPATH + "/segmentation/"

def replace(file_path, pattern, subst):
	#Create temp file
	fh, abs_path = mkstemp()
	with open(abs_path,'w') as new_file:
		with open(file_path) as old_file:
			for line in old_file:
				new_file.write(line.replace(pattern, subst))
	close(fh)
	#Remove original file
	remove(file_path)
	#Move new file
	move(abs_path, file_path)   


def get_image_size(fname):
	'''Determine the image type of fhandle and return its size.
	from draco'''
	with open(fname, 'rb') as fhandle:
		head = fhandle.read(24)
		if len(head) != 24:
			return
		if imghdr.what(fname) == 'png':
			check = struct.unpack('>i', head[4:8])[0]
			if check != 0x0d0a1a0a:
				return
			width, height = struct.unpack('>ii', head[16:24])
		elif imghdr.what(fname) == 'gif':
			width, height = struct.unpack('<HH', head[6:10])
		elif imghdr.what(fname) == 'jpeg':
			try:
				fhandle.seek(0) # Read 0xff next
				size = 2
				ftype = 0
				while not 0xc0 <= ftype <= 0xcf:
					fhandle.seek(size, 1)
					byte = fhandle.read(1)
					while ord(byte) == 0xff:
						byte = fhandle.read(1)
					ftype = ord(byte)
					size = struct.unpack('>H', fhandle.read(2))[0] - 2
				# We are at a SOFn block
				fhandle.seek(1, 1)  # Skip `precision' byte.
				height, width = struct.unpack('>HH', fhandle.read(4))
			except Exception: #IGNORE:W0703
				return
		else:
			return
		return width, height

if FIRST_PASS:

	if not os.path.exists(IBR_PATH):

		os.makedirs( IBR_PATH )
		os.makedirs( IBR_PLY_PATH )
		os.makedirs( IBR_MASKS_PATH )

		# create new IBR_data directory
		# later we might want to be able to force this
		subprocess.call([SIBR_MESH_TRANSFORM_BIN, "--path", PMVS_RC_PATH + "/mesh.ply", "--out", IBR_PLY_PATH+"/pmvs_recon.ply"])
		shutil.copyfile(PMVS_RC_PATH + "/mesh.ply", IBR_PLY_PATH+"/pmvs_recon_no_rotation.ply")
		
		#Generate bundler file
		shutil.copyfile( PMVS_RC_PATH + "/bundle.out", IBR_PATH + "/bundle.out" )
			
		#create file listing
		file_listImgs = open(IBR_LIST_PATH, 'w')
		for file_name in os.listdir( PMVS_RC_PATH  ):
			if os.path.isfile(os.path.join(PMVS_RC_PATH, file_name)):
				m = re.search("(.+?).jpg", file_name)
				if m:
					num_str = m.group(1)
					img_id = int(num_str)
					file_jpg = "%08d.jpg" % img_id
					new_file_name = IBR_PATH + file_jpg            
					shutil.copyfile( os.path.join(PMVS_RC_PATH, file_name), new_file_name)
					#shutil.copyfile( new_file_name, IBR_HALF_IMAGES_PATH+file_jpg )
					imgsize = get_image_size(new_file_name)                                    
					print(file_jpg, imgsize[0], imgsize[1], file=file_listImgs )            
											
		file_listImgs.close()

		img_id = 0
		#need to reverse copying if missing images
		for file_name in os.listdir( ROOT_MASKS_PATH  ):
			if os.path.isfile(os.path.join(ROOT_MASKS_PATH, file_name)):
				m = re.search("(.+?).png", file_name)
				if m:
					file_jpg = "%08d_mask.png" % img_id
					new_file_name = IBR_MASKS_PATH + file_jpg            
					shutil.copyfile( os.path.join(ROOT_MASKS_PATH, file_name), new_file_name)
					img_id += 1


	pro = subprocess.Popen([SIBR_SCENE_BUILDER_BIN, "--path", ROOTPATH, "--id"], cwd=SIBR_INSTALL_PATH)
	selected_cam_id = pro.wait()
	print("Put the KIPPI graph in the segmentation subdirectory")
	os.makedirs( SEGMENTATION_PATH )
	ref_file_jpg = "%08d.jpg" % selected_cam_id
	shutil.copyfile( os.path.join(IBR_PATH, ref_file_jpg), os.path.join(SEGMENTATION_PATH, ref_file_jpg))
	exit()


elif SECOND_PASS:

	#find and copy the graph file to the segmentation root dir.
	for file_name in os.listdir( SEGMENTATION_PATH  ):
		candidate_path = os.path.join(SEGMENTATION_PATH, file_name)
		if os.path.isdir(candidate_path):
			for file_name1 in os.listdir( candidate_path  ):
				if file_name1[-10:] == '_graph.txt':
					shutil.copyfile(os.path.join(candidate_path, file_name1), os.path.join(SEGMENTATION_PATH, file_name1))

	
	pro = subprocess.Popen([SIBR_SCENE_BUILDER_BIN, "--path", ROOTPATH, "--refine"], cwd=SIBR_INSTALL_PATH)
	pro.wait()

	#backup:
	if os.path.exists(IBR_BCKP_MESH_PATH):
		print("Backup directory already exists, not copying.")
	else:
		shutil.copytree(IBR_PATH, IBR_BCKP_MESH_PATH)
	

	# then copy clean mesh to IBR path.
	shutil.copy(os.path.join(ROOTPATH, "mesh_cleaning/out_mesh_regions_proj-smooth.ply"),  os.path.join(IBR_PLY_PATH , "pmvs_recon.ply"))
	

	#and create copy for layers replacement.
	#backup:
	#shutil.copytree(IBR_PATH, IBR_BCKP_PATH)


	# Extract first series of layers.
	pro = subprocess.Popen([SIBR_LAYERS_EXTRACTION_BIN, "--path", ROOTPATH], cwd=SIBR_INSTALL_PATH)
	pro.wait()

	# Extract second series of layers.
	#pro = subprocess.Popen([SIBR_LAYERS_EXTRACTION_BIN, "--path", ROOTPATH, "--second-pass"], cwd=SIBR_INSTALL_PATH)
	#pro.wait()
	#pro = subprocess.Popen([SIBR_LAYERS_EXTRACTION_BIN, "--path", ROOTPATH, "--second-pass", "--combi"], cwd=SIBR_INSTALL_PATH)
	#pro.wait()

	# Generate per-instance views.
	pro = subprocess.Popen([SIBR_LAYERS_EXTRACTION_BIN, "--path", ROOTPATH, "--test-reproj"], cwd=SIBR_INSTALL_PATH)
	pro.wait()
elif THIRD_PASS:

	# Extract masks.
	pro = subprocess.Popen([SIBR_LAYERS_EXTRACTION_BIN, "--path", ROOTPATH, "--masks", "--sigma", "0.25", "--ratio", "3.5"], cwd=SIBR_INSTALL_PATH)
	pro.wait()
	
	# one scene case
	pro = subprocess.Popen([SIBR_SCENE_BUILDER_BIN, "--path", ROOTPATH], cwd=SIBR_INSTALL_PATH)
	pro.wait()

	shutil.copyfile(ROOTPATH + "/plane_data.txt", IBR_FULL_SCENE_PATH+"/plane_data.txt")
	# MRF segmentation and stitching.
	IBR_FULL_SCENE_PATH = ROOTPATH + "/IBR_fullScene/"
	IBR_FULL_SCENE_EQ_PATH = ROOTPATH + "/IBR_fullScene_eq/"
	if os.path.exists(IBR_FULL_SCENE_EQ_PATH):
		shutil.rmtree(IBR_FULL_SCENE_EQ_PATH)

	shutil.copytree(IBR_FULL_SCENE_PATH, IBR_FULL_SCENE_EQ_PATH)
	

	# Copy images from output stitch folder to fullScene_eq
	pro = subprocess.Popen([SIBR_LAYERS_COMPOSITION_BIN, "--path", IBR_FULL_SCENE_PATH,"--width", "2500"], cwd=SIBR_INSTALL_PATH)
	pro.wait()

	for file_name in os.listdir( STITCH_PATH ):
		if os.path.isfile(os.path.join(STITCH_PATH, file_name)):
			m = re.search("(.+?).jpg", file_name)
			if m:
				num_str = m.group(1)
				img_id = int(num_str)
				file_jpg = "%08d.jpg" % img_id
				new_file_name = IBR_FULL_SCENE_EQ_PATH + file_jpg            
				shutil.copyfile( os.path.join(STITCH_PATH, file_name), new_file_name)
