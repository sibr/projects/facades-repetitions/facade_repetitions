/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#include <iostream>
#include <core/graphics/Image.hpp>
#include "MaskGenerator.hpp"


#define LARGE_PENALTY 1000.0

MaskGenerator::MaskGenerator(const sibr::ImageRGB & image, const sibr::ImageRGB & background, const sibr::ImageL8 & mask)
{
	_image = image.clone();
	_background = background.clone();
	_mask = mask.clone();
	_w = image.w();
	_h = image.h();
	_wExtMargin = uint(0.01*_w);
	_hExtMargin = uint(0.01*_h);
	_wIntMargin = uint(0.35*_w);
	_hIntMargin = uint(0.35*_h);
	using namespace std::placeholders;

	std::shared_ptr< FuncItoF > unaryLabelOnly_ptr;
	std::shared_ptr< Func2ItoF > unaryFull_ptr;
	std::shared_ptr< Func2ItoF > pairwiseLabelsOnly_ptr;
	std::shared_ptr< Func4ItoF > pairwiseFull_ptr;

	unaryLabelOnly_ptr = std::shared_ptr< FuncItoF >();	
	pairwiseLabelsOnly_ptr = std::shared_ptr< Func2ItoF >();
	unaryFull_ptr = std::shared_ptr<Func2ItoF >(new Func2ItoF(std::bind(&MaskGenerator::unaryCost, this, _1, _2)));
	pairwiseFull_ptr = std::shared_ptr< Func4ItoF >(new Func4ItoF(std::bind(&MaskGenerator::binaryCost, this, _1, _2, _3, _4)));

	// Fill the neighbours map, this is a grid where each pixel is linked to its four neighbours.
	_neighboursMap.resize(_w*_h);
	for (uint y = 0; y < _h; ++y) {
		for (uint x = 0; x < _w; ++x) {
			const int currentIndex = pixelToArray(x, y);
			std::vector<int> neighbours;
			if (x > 0) {
				neighbours.push_back(pixelToArray(x - 1, y));
			}
			if (y > 0) {
				neighbours.push_back(pixelToArray(x, y - 1));
			}
			if (x < _w-1) {
				neighbours.push_back(pixelToArray(x + 1, y));
			}
			if (y < _h-1) {
				neighbours.push_back(pixelToArray(x, y + 1));
			}
			_neighboursMap[currentIndex] = neighbours;
		}
	}
	// labels for the optimisation
	std::vector<int> labels = { 1,2 };
		
	_mrfSolver = std::shared_ptr<sibr::MRFSolver>(new sibr::MRFSolver(labels, &_neighboursMap, 1,
		unaryLabelOnly_ptr,
		unaryFull_ptr,
		pairwiseLabelsOnly_ptr,
		pairwiseFull_ptr
	));

		
}

double MaskGenerator::unaryCost(int p, int l) {
	const sibr::Vector2i pixel = arrayToPixel(p);
	// Exterior border.
	if ((pixel.x() < (int)_wExtMargin) || (pixel.x() > (int)_w - (int)_wExtMargin) || (pixel.y() < (int)_hExtMargin) || (pixel.y() > (int)_h - (int)_hExtMargin)) {
		return (l==1) ? 0.0 : LARGE_PENALTY;
	}
	// Inside border.
	/*if ((pixel.x() < _w/2 + _wIntMargin) && (pixel.x() > _w/2 - _wIntMargin) && (pixel.y() < _h/2 + _hIntMargin) && (pixel.y() > _h/2 - _hIntMargin)) {
		return (l == 2) ? 0.0 : LARGE_PENALTY;
	}*/
	// Inside mask.
	if (_mask(pixel.x(), pixel.y())[0] > 128) {
		return (l == 2) ? 0.0 : LARGE_PENALTY;
	}
	// Other cases: L2 distance between our image and the background.
	//std::cout << "U:" << dist << std::endl;
	sibr::Vector2d uv = pixel.cast<double>().cwiseQuotient(sibr::Vector2d(_w, _h));// 
	uv = uv*2.0 - sibr::Vector2d(1.0, 1.0); 
	// TODO SR: check modif didn't break anything.
	const double exposant = 15.0;
	const double lo = std::pow(std::abs(uv.x()), exposant) + std::pow(std::abs(uv.y()), exposant);
	const double dist = 5.0 * std::min(lo, 0.2);
	return dist * LARGE_PENALTY;
}

double MaskGenerator::binaryCost(int p1, int p2, int l1, int l2) {
	if (l1 == l2) { return 0.0; }

	const sibr::Vector2i pixel1 = arrayToPixel(p1);
	const sibr::Vector2i pixel2 = arrayToPixel(p2);
	const sibr::Vector3d c1f = _image(pixel1.x(), pixel1.y()).cast<double>() / 255.0;
	const sibr::Vector3d c2f = _image(pixel2.x(), pixel2.y()).cast<double>() / 255.0;
	const sibr::Vector3d c1b = _background(pixel1.x(), pixel1.y()).cast<double>() / 255.0;
	const sibr::Vector3d c2b = _background(pixel2.x(), pixel2.y()).cast<double>() / 255.0;
	const double distp1 = ( c1f - c1b ).norm();
	const double distp2 = ( c2f - c2b ).norm();

	const sibr::Vector3d c1fv = pixel1.y() == _h - 1 ? c1f : _image(pixel1.x(), pixel1.y()+1).cast<double>() / 255.0;
	const sibr::Vector3d c1fh = pixel1.x() == _w - 1 ? c1f : _image(pixel1.x()+1, pixel1.y() ).cast<double>() / 255.0;
	const sibr::Vector3d c1bv = pixel1.y() == _h - 1 ? c1b : _background(pixel1.x(), pixel1.y() + 1).cast<double>() / 255.0;
	const sibr::Vector3d c1bh = pixel1.x() == _w - 1 ? c1b : _background(pixel1.x() + 1, pixel1.y()).cast<double>() / 255.0;
	const sibr::Vector3d c2fv = pixel2.y() == _h - 1 ? c2f : _image(pixel2.x(), pixel2.y() + 1).cast<double>() / 255.0;
	const sibr::Vector3d c2fh = pixel2.x() == _w - 1 ? c2f : _image(pixel2.x() + 1, pixel2.y()).cast<double>() / 255.0;
	const sibr::Vector3d c2bv = pixel2.y() == _h - 1 ? c2b : _background(pixel2.x(), pixel2.y() + 1).cast<double>() / 255.0;
	const sibr::Vector3d c2bh = pixel2.x() == _w - 1 ? c2b : _background(pixel2.x() + 1, pixel2.y()).cast<double>() / 255.0;


	const double g1 = std::sqrt((c1fv - c1f - c1bv + c1b).squaredNorm() + (c1fh - c1f - c1bh + c1b).squaredNorm());
	const double g2 = std::sqrt((c2fv - c2f - c2bv + c2b).squaredNorm() + (c2fh - c2f - c2bh + c2b).squaredNorm());
	//std::cout << "B:" << distp1 + distp2 << std::endl;
	return (g1+g2);
}

void MaskGenerator::solve() {
	_mrfSolver->solveBinaryLabels();
}

const int MaskGenerator::pixelToArray(const int x, const int y) const {
	return y*_w + x;
}

const sibr::Vector2i MaskGenerator::arrayToPixel(const int i) const {
	return sibr::Vector2i(i%_w, i/_w);
}

sibr::ImageL8 MaskGenerator::getResult()
{
	std::vector<int> labellingSP = _mrfSolver->getLabels();
	
	sibr::ImageL8 result(_w,_h);
	sibr::ColorRGBA zeroP(0.0f,0.0f,0.0f,0.0f);
	sibr::ColorRGBA fullP(1.0f,1.0f,1.0f,1.0f);
	for (size_t id = 0; id < labellingSP.size(); ++id) {
		sibr::Vector2i pixel = arrayToPixel((int)id);
		result.color(pixel.x(), pixel.y(),labellingSP[id] > 1 ? fullP : zeroP);
		
	}
	return result;
}

sibr::ImageRGB MaskGenerator::getOverlayResult()
{
	sibr::ImageRGB result = _image.clone();
	std::vector<int> labellingSP = _mrfSolver->getLabels();

	for (size_t id = 0; id < labellingSP.size(); ++id) {
		sibr::Vector2i pixel = arrayToPixel((int)id);
		result(pixel) = result(pixel.x(), pixel.y())/(labellingSP[id] > 1 ? 1 : 2);
	}
	return result;
}

MaskGenerator::~MaskGenerator(void)
{

}

