/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#ifndef SIBR_REPETITIONS_MASKGENERATOR_H
#define SIBR_REPETITIONS_MASKGENERATOR_H

#include <core/imgproc/MRFSolver.h>

class MaskGenerator
{

public:

	MaskGenerator(const sibr::ImageRGB & image, const sibr::ImageRGB & background, const sibr::ImageL8 & mask);

	~MaskGenerator(void);

	double unaryCost(int p, int l);

	double binaryCost(int p1, int p2, int l1, int l2);

	void solve();

	const int pixelToArray(const int x, const int y) const;

	const sibr::Vector2i arrayToPixel(const int i) const;

	sibr::ImageL8 getResult();
	sibr::ImageRGB getOverlayResult();
private:

	typedef std::function<double(int)>				FuncItoF;
	typedef std::function<double(int, int)>			Func2ItoF;
	typedef std::function<double(int, int, int, int)>	Func4ItoF;
	
	sibr::ImageRGB _image, _background;
	sibr::ImageL8 _mask;
	std::vector<std::vector<int>> _neighboursMap;
	std::shared_ptr<sibr::MRFSolver> _mrfSolver;
	uint _w, _h, _wExtMargin, _hExtMargin, _wIntMargin, _hIntMargin;
		
};				 

#endif // SIBR_REPETITIONS_MASKGENERATOR_H