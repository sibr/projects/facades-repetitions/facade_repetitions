/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */



#include <fstream>
#include <iostream>
#include <boost/filesystem.hpp>

#include <core/system/CommandLineArgs.hpp>
#include <core/system/Utils.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/photo/photo.hpp>
#include <projects/facade_repetitions/renderer/PoissonReconstruction.hpp>

#include <core/graphics/Window.hpp>
#include <core/graphics/Input.hpp>
#include <core/renderer/PoissonRenderer.hpp>
#include <projects/ulr/renderer/ULRV2Renderer.hpp>

#include "MaskGenerator.hpp"
#include "projects/facade_repetitions/renderer/RepetitionsScene.hpp"


#define PROGRAM_NAME "sibr_ulr_layers_compositing"
using namespace sibr;


const char* usage = ""
"Usage: " PROGRAM_NAME " --path <dataset-path>" "\n"
"\t\t--new Load a new SIBR dataset instead of a legacy one." "\n"
;




int main(int ac, char** av)
{
	Window		window(100, 100, PROGRAM_NAME);

	CommandLineArgs::parseMainArgs(ac, av);
	RepetitionsArgs args;
	// Full scene (use compatibility scene for now).
	BasicIBRScene::Ptr		scene;
	if(args.newLayout) {
		scene.reset(new BasicIBRScene(args, true));
	} else {
		scene.reset(new RepetitionsScene(args.dataset_path));
	}

	const std::string outputDir = args.dataset_path.get() + "../stitch/";
	sibr::makeDirectory(outputDir);
	
	scene->createRenderTargets();
	
	std::vector<sibr::InputCamera::Ptr> cameras = scene->cameras()->inputCameras();

	sibr::ULRV2Renderer::Ptr ulr = sibr::ULRV2Renderer::Ptr(new sibr::ULRV2Renderer(scene->cameras()->inputCameras(), 
		args.rendering_size.get()[0], args.rendering_size.get()[1], 90, "ulr_v2", "ulr_v2", false));
	ulr->setCulling(false);
	const float factor = 2.0f;
	for (auto & camPtr : cameras) {
		sibr::InputCamera& cam = *camPtr;
		cam.fovy(2.0f * std::atan(cam.h()*0.5f*factor / cam.focal()));
		cam.znear(0.1f*cam.znear());
		cam.zfar(5.0f*cam.zfar());
	}

	window.makeContextCurrent();
	size_t li = 0;
	std::vector<uint> imgs_ulr = { 0};
	const auto & camerasA = scene->cameras()->inputCameras();
	for (li = 1; li < camerasA.size(); ++li) {
		if (std::abs(int(camerasA[li]->w()) - int(camerasA[li - 1]->w())) > 200) {
			break;
		} 
		imgs_ulr.push_back((uint)li);
	}
	
	
	for (size_t i = 0; i < imgs_ulr.size(); ++i) {
		cameras.erase(cameras.begin());
	}
	sibr::Mesh::Ptr altMesh = nullptr;

	for (auto & cam : cameras) {
		sibr::Input::poll();

		window.viewport().bind();
		
		const uint destW = cam->w() * (uint)factor;
		const uint destH = cam->h() * (uint)factor;
		sibr::PoissonRenderer::Ptr poisson = sibr::PoissonRenderer::Ptr(new sibr::PoissonRenderer(destW, destH));

		sibr::RenderTargetRGB dest(destW, destH);
		sibr::RenderTargetRGBA::Ptr destPoisson = std::make_shared<sibr::RenderTargetRGBA>(destW, destH);
		glViewport(0, 0, destW, destH);
		dest.bind();

		//std::vector<uint> imgs_ulr = ULRRepetitionsView::chosen_cameras(cam, scene->inputCameras(), true, 50);

		ulr->process(
			/* input -- images chosen */ imgs_ulr,
			/* input -- camera position */ *cam,
			/* input -- scene */ scene,
			/* input -- alt mesh if available */ altMesh,
			/* input -- input RTs -- can be RGB or alpha */ scene->renderTargets()->inputImagesRT(),
			/* output */ dest);
		poisson->process(dest.handle(), destPoisson);
		sibr::ImageRGBA backgroundIm(destW, destH);
		destPoisson->readBack(backgroundIm);
		
		backgroundIm.save(outputDir + "/" + cam->name() + "_render.png", false);
	}
	window.close();

	#pragma omp barrier

	const std::string basePath = args.dataset_path;
	#pragma omp parallel for
	for (int cid = 0; cid < cameras.size(); ++cid) {
		auto cam = *cameras[cid];
		sibr::ImageRGB backgroundIm;
		backgroundIm.load(outputDir + "/" + cam.name() + "_render.png", false);
		sibr::ImageRGB foregroundIm;
		foregroundIm.load(basePath + "/" + cam.name() + ".jpg", false);
		
		sibr::ImageL8 foregroundMask;
		foregroundMask.load(basePath + "/masks/" + cam.name() + "_mask.png", true);
		for (size_t lid = 0; lid < 40; ++lid) {
			sibr::ImageL8 localMask = foregroundMask.clone();
			for (uint y = 1; y < foregroundMask.h()-1; ++y) {
				for (uint x = 1; x < foregroundMask.w()-1; ++x) {
					if (foregroundMask(x, y)[0] > 128) {
						continue;
					}
					bool foundInMaskNeighbour = false;
					if (localMask(x - 1, y)[0] > 128 || localMask(x + 1, y)[0] > 128
					 || localMask(x, y - 1)[0] > 128 || localMask(x, y + 1)[0] > 128
					 || localMask(x-1, y - 1)[0] > 128 || localMask(x-1, y + 1)[0] > 128
					 || localMask(x + 1, y - 1)[0] > 128 || localMask(x + 1, y + 1)[0] > 128){

						foregroundMask(x, y)[0] = 255;
					}
				}
			}
		}
		//foregroundMask.save(outputDir + "/" + cam.name() + "_in_mask.png", true);
		cv::Mat foreground, background;
		foregroundIm.toOpenCV().convertTo(foreground, CV_32F, 1.0 / 255.0);
		backgroundIm.toOpenCV().convertTo(background, CV_32F, 1.0 / 255.0);
		const uint cropW = foregroundIm.w();
		const uint cropH = foregroundIm.h();
		
		cv::Mat fullForeground = cv::Mat::zeros(background.rows, background.cols, CV_32FC3);
		foreground.copyTo(fullForeground(cv::Rect(cropW / 2, cropH / 2, cropW, cropH)));

		// Running the optimisation
		//  solver functions
		sibr::ImageRGB backgroundTightIm;
		backgroundTightIm.fromOpenCV(backgroundIm.toOpenCV()(cv::Rect(cropW / 2, cropH / 2, cropW, cropH)));
		//foregroundIm.save(outputDir + "/" + cam.name() + "_fore.png");
		//backgroundTightIm.save(outputDir + "/" + cam.name() + "_back.png");

		MaskGenerator mrfMask(foregroundIm, backgroundTightIm, foregroundMask);
		mrfMask.solve();
		sibr::ImageL8 maskSeg = mrfMask.getResult();
		//sibr::ImageRGB maskSeg = mrfMask.getOverlayResult();
		//maskSeg.save(outputDir + "/" + cam.name() + "_mask.png");
		//mrfMask.getOverlayResult().save(outputDir + "/" + cam.name() + "_over.png");
		// Compute mask.
		cv::Mat mask = cv::Mat::ones(background.rows, background.cols, CV_32FC1);
		cv::Mat submask;// cv::Mat::zeros(foreground.rows, foreground.cols, CV_32FC1);
		maskSeg.toOpenCV().convertTo(submask, CV_32FC1, 1.0f / 255.0f);
		submask = 1.0f - submask;
		submask.copyTo(mask(cv::Rect(cropW / 2, cropH / 2, cropW, cropH)));

		// Compute gradients in both directions, 3 channels each.
		cv::Mat gradientX(fullForeground.size(), CV_32FC3), gradientY(fullForeground.size(), CV_32FC3);

		// Compute gradient image, handling occlusions with the mask.
		for (int i = 0; i < fullForeground.rows; ++i) {
			for (int j = 0; j < fullForeground.cols; ++j) {
				// Check mask.
				if (mask.at<float>(i, j) > 0.5) {
					continue;
				}

				int ip = std::min(i + 1, fullForeground.rows - 1);
				int jp = std::min(j + 1, fullForeground.cols - 1);

				// Get the 3x3 neighborhood.
				cv::Vec3f c = fullForeground.at<cv::Vec3f>(i, j);
				cv::Vec3f d = mask.at<float>(ip, j) > 0.5 ? c : fullForeground.at<cv::Vec3f>(ip, j);
				cv::Vec3f r = mask.at<float>(i, jp) > 0.5 ? c : fullForeground.at<cv::Vec3f>(i, jp);

				cv::Vec3f dX = d - c;
				cv::Vec3f dY = r - c;
				gradientX.at<cv::Vec3f>(i, j) = dX;
				gradientY.at<cv::Vec3f>(i, j) = dY;
			}
		}
		
		PoissonReconstruction recons(gradientX, gradientY, mask, background);
		recons.solve();
		cv::Mat result = recons.result();
		cv::Mat cropResult = result(cv::Rect(cropW / 2, cropH / 2, cropW, cropH));
		sibr::ImageRGB finalImg;
		finalImg.fromOpenCV(cropResult);
		finalImg.save(outputDir + "/" + cam.name() + ".jpg");
		sibr::ImageRGB finalImFullg;
		finalImFullg.fromOpenCV(result);
		//finalImFullg.save(outputDir + "/" + cam.name() + "_full.png");
	}
	
	return EXIT_SUCCESS;
}
