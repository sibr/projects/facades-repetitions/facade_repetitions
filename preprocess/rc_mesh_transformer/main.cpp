/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */




#include <core/system/CommandLineArgs.hpp>
#include <core/graphics/Mesh.hpp>


#define PROGRAM_NAME "sibr_rc_transformer"

using namespace sibr;


const char* usage = ""
	"Usage: " PROGRAM_NAME " " "-path input/mesh/path.ply -out output/mesh/path.ply" "\n"
	;

struct RCTransformArgs {
	RequiredArg<std::string> path = { "path", "path to the input mesh" };
	RequiredArg<std::string> newpath = { "out", "path to the output mesh" };
};


int main( int ac, char** av )
{
	if(ac < 2){ std::cout << usage << std::endl; return 0; }

	sibr::CommandLineArgs::parseMainArgs(ac, av);
	RCTransformArgs args;
	
	// Parameters
	std::string				meshPath = args.path;
	std::string				newMeshPath = args.newpath;

	
	// For now, only support a 90� rotation around the X axis.
	// That's all I needed (SR). Could be extended into a general utility later.
	const float thetax = 90.0f / 180.0f * (float)M_PI;
	Eigen::Matrix3f transf;
	transf << 1.0f, 0.0f, 0.0f,
			 0.0f, cos(thetax), -sin(thetax),
			 0.0f, sin(thetax),  cos(thetax);


	sibr::Mesh mesh(false); // no need for the GL mesh setup.
	mesh.load(meshPath);

	// Vertices.
	sibr::Mesh::Vertices newVertices;
	auto & oldVertices = mesh.vertices();
	for(auto & vertex : oldVertices){
		newVertices.push_back(transf * vertex);
	}
	mesh.vertices(newVertices);

	// Transform normals too if they exist.
	if(mesh.hasNormals()){
		sibr::Mesh::Normals newNormals;
		auto & oldNormals = mesh.normals();
		for(auto & normal : oldNormals){
			// Can use transf directly as we only apply a rotation.
			newNormals.push_back(transf * normal);
		}
		mesh.normals(newNormals);
	}

	// Save to the output destination. Keep it readable by other software.
	mesh.save(newMeshPath, true);

	return EXIT_SUCCESS;
}

