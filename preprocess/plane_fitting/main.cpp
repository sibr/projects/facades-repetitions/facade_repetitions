/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#include <fstream>
#include <iostream>
#include <boost/filesystem.hpp>


#include <projects/facade_repetitions/renderer/KIPPIGraph.hpp>
#include <projects/facade_repetitions/renderer/OpenMVGSFM.hpp>


#include <core/graphics/Texture.hpp>


#include <core/system/Utils.hpp>
#include <core/raycaster/CameraRaycaster.hpp>
#include <random>
#include <omp.h>

#include <core/raycaster/PlaneEstimator.hpp>

#include <core/raycaster/CameraRaycaster.hpp>
#include <core/renderer/ColoredMeshRenderer.hpp>
#include <core/renderer/CopyRenderer.hpp>
#include <core/view/InteractiveCameraHandler.hpp>
#include <core/graphics/RenderUtility.hpp>
#include <core/graphics/Input.hpp>
#include "core/graphics/Window.hpp"


#include <Eigen/Sparse>
#include <Eigen/Dense>

#define PROGRAM_NAME "sibr_ulr_repetitions_test"
using namespace sibr;

const char* usage = ""
"Usage: " PROGRAM_NAME " --path <dataset-path>" "\n"
;



int main(int ac, char** av) {

	CommandLineArgs::parseMainArgs(ac, av);
	BasicDatasetArgs args;

	sibr::Window::Ptr _window(new Window(100, 100, "win"));
	
	sibr::OpenMVGSFM sfmData(args.dataset_path);

	PlaneEstimator PE(sfmData.points());
	const int nPlane = 2;
	const float delta = 0.01f;
	const int nTry = 100000;
	PE.computePlanes(nPlane, delta, nTry);
	std::cout << PE._planes[0] << std::endl;
	
	sibr::ColoredMeshRenderer renderer;
	sibr::CopyRenderer copyRenderer;
	_window->makeContextCurrent();
	_window->size(1200, 900);

	sibr::Mesh pointCloud;
	pointCloud.vertices(PE._Points);
	GLShader			_shader;
	GLParameter			_paramMVP;

	std::string vertShader =
		"#version 420\n"
		"uniform mat4 MVP;\n"
		"layout(location = 0) in vec3 in_vertex;\n"
		"layout(location = 1) in vec3 in_color;\n"
		"out vec3 vertColor;\n"
		"void main(void) {\n"
		"gl_Position = MVP * vec4(in_vertex, 1.0);\n"
		"vertColor = in_color;\n"
		"}";

	std::string fragShader =
		"#version 420\n"
		"out vec4 out_color;\n"
		"in vec3 vertColor;\n"
		"void main(void) {\n"
		"out_color = vec4(vertColor, 1.0);\n"
		"}\n";

	_shader.init("ColoredMesh",vertShader,fragShader);
	_paramMVP.init(_shader, "MVP");

	sibr::Mesh meshWithPlanes;
	sibr::Mesh::Vertices  meshWithPlaneV;
	sibr::Mesh::Triangles  meshWithPlaneT;
	sibr::Mesh::Colors  meshWithPlaneC;
	// Build the plane mesh
	int color_list[25][3] = {
		{ 255, 179, 0 },{ 128, 62, 117 },{ 166, 189, 215 } ,{ 193, 0, 32 },{ 0,128,255 },{ 0, 125, 52 },
		{ 246, 118, 142 },{ 0, 83, 138 },{ 255, 122, 92 } ,{ 0, 255, 0 },{ 255, 142, 0 },{ 179, 40, 81 },
		{ 244, 200, 0 },{ 127, 24, 13 },{ 147, 170, 0 } ,{ 89, 51, 21 },{ 241, 58, 19 },{ 35, 44, 22 },
		{ 83, 55, 122 },{ 255,0,128 },{ 128,255,0 } ,{ 128,0,255 },{ 206, 162, 98 },{ 128,128,128 },{ 255,255,255 }
	};

	int i = 0;
	float r1 = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
	float r2 = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
	float r3 = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
	sibr::Vector3f randDir(r1, r2, r3);
	float planeSize = 0.5;
	int numPointCircle = 30;
	int shift = 0;
	sibr::Vector3f bv1;
	sibr::Vector3f bv2;
	for (auto & p : PE._planes) {

		const int currentVsize = (int)meshWithPlaneV.size();
		bv1 = planeSize*p.xyz().cross(randDir).normalized();
		bv2 = planeSize*p.xyz().cross(bv1).normalized();

		sibr::Vector3f colorPlane(
			color_list[i % 25][0] / 255.0f,
			color_list[i % 25][1] / 255.0f,
			color_list[i % 25][2] / 255.0f
		);

		meshWithPlaneV.push_back(PE._covMeans[i].second);
		meshWithPlaneC.push_back(colorPlane);

		for (int ci = 1; ci <= numPointCircle; ci++) {

			meshWithPlaneV.push_back(PE._covMeans[i].second + cos(ci*2.0f*(float)M_PI / numPointCircle)*bv1 + sin(ci*2.0f*(float)M_PI / numPointCircle)*bv2);
			meshWithPlaneC.push_back(colorPlane);

			meshWithPlaneT.push_back(sibr::Vector3u(shift, ci + shift, (ci % numPointCircle) + 1 + shift));
			meshWithPlaneT.push_back(sibr::Vector3u(shift, (ci % numPointCircle) + 1 + shift, ci + shift));
		}
		shift += numPointCircle + 1;
		i++;
		if(i>0) {
			break;
		}
	}
	meshWithPlanes.vertices(meshWithPlaneV);
	meshWithPlanes.triangles(meshWithPlaneT);
	meshWithPlanes.colors(meshWithPlaneC);
	meshWithPlanes.save(args.dataset_path.get() + "/../../plane.ply", true);
	

	const float scaling = meshWithPlanes.getBoundingBox().diagonal().norm();
	std::ofstream planeFile(args.dataset_path.get() + "/../../../plane_data.txt");
	if (!planeFile.is_open()) {
		SIBR_ERR << "Unable to write plane to backup file" << std::endl;
		return EXIT_FAILURE;
	}


	sibr::Vector3f P0(PE._covMeans[0].second - bv1 - bv2);
	sibr::Vector3f P1(PE._covMeans[0].second + bv1 - bv2);
	sibr::Vector3f P2(PE._covMeans[0].second - bv1 + bv2);

	planeFile << P0[0] << " " << P0[1] << " " << P0[2] << "\n";
	planeFile << P1[0] << " " << P1[1] << " " << P1[2] << "\n";
	planeFile << P2[0] << " " << P2[1] << " " << P2[2] << "\n";
	planeFile.close();

	

	std::shared_ptr<sibr::Raycaster> raycaster(new sibr::Raycaster());
	if (!raycaster->init()) {
		std::cerr << " failed raycaster init in constructor " << std::endl;
	}
	else {
		raycaster->addMesh(meshWithPlanes);
	}

	sibr::InteractiveCameraHandler camera;
	//camera.setup(*getImgs(getInpaintImages().at(0))->getCamera(), _window->size().cast<unsigned int>(), _window->viewport(), raycaster);
	camera.setup(meshWithPlanes.getBoundingBox(), _window->viewport(), raycaster);
	auto timeLastFrame = std::chrono::steady_clock::now();

	CHECK_GL_ERROR;
	sibr::RenderTargetRGB targetPlane(1200, 900);

	bool exitMesh = false;
	while (_window->isOpened() && !exitMesh)
	{
		sibr::Input::poll();
		_window->makeContextCurrent();

		// Inputs.
		if (sibr::Input::global().key().isPressed(sibr::Key::Escape))
			_window->close();

		else if (sibr::Input::global().key().isReleased(sibr::Key::N)) {
			exitMesh = true;
		}


		float deltaTime = std::chrono::duration<float>(std::chrono::steady_clock::now() - timeLastFrame).count();
		timeLastFrame = std::chrono::steady_clock::now();
		camera.update(sibr::Input::global(), deltaTime);


		_window->viewport().bind();
		_window->viewport().clear(sibr::Vector3f(0.9f, 0.9f, 0.9f));
		//RENDER POINT CLOUD and PLANES
		glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);
		glEnable(GL_DEPTH_TEST);
		_shader.begin();
		_paramMVP.set(camera.getCamera().viewproj());
		pointCloud.render_points();
		meshWithPlanes.render(true);
		_shader.end();

		//RENDER CAM
		camera.onRender(_window->viewport());
		_window->swapBuffer();

	}

	return EXIT_SUCCESS;
}
