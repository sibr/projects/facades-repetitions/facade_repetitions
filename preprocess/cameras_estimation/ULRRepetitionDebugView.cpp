/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


// TODO: make shorter
#include "ULRRepetitionDebugView.hpp"

#include <core/assets/Resources.hpp>
#include <core/system/Vector.hpp>
#include <core/graphics/Texture.hpp>
#include <core/system/CommandLineArgs.hpp>
#include <chrono>
#include <iostream>
#include <fstream>
#include <projects/facade_repetitions/renderer/OpenMVGSFM.hpp>
#include <core/graphics/Input.hpp>

struct ULRRepetitionDebugViewArgs {
	sibr::RequiredArg<std::string> cropsPathV = { "c", "crops file" };
	sibr::RequiredArg<std::string> omvgSfmArgV = { "s", "sfm file" };
	sibr::RequiredArg<std::string> outputArgV = { "o", "output dir" };
	sibr::Arg<bool> bArg = { "b" , "load bundle file instead"};
};

ULRRepetitionDebugView::~ULRRepetitionDebugView( )
{
	_mesh.reset();
}

ULRRepetitionDebugView::ULRRepetitionDebugView( int ac, char** av) 
		
{
	sibr::CommandLineArgs::parseMainArgs(ac, av);
	ULRRepetitionDebugViewArgs args;
	std::string cropsArg = "--c";
	
	const std::string cropsPath = args.cropsPathV;
	const std::string omvgSfmPath = args.omvgSfmArgV;
	_outputPath = args.outputArgV.get();

	std::vector<sibr::Vector3f> points;

	const std::string bundleExt = omvgSfmPath.substr(omvgSfmPath.size() - 3, 3);
	if ((args.bArg) || bundleExt == "out") {
		const std::string listPath = omvgSfmPath + "/../list_images.txt";
		_inputCameras = CropCameraUtilities::load(omvgSfmPath, listPath, points);
	}
	else {
		sibr::OpenMVGSFM sfmData(omvgSfmPath);
		_inputCameras = sfmData.cameras();
		points = sfmData.points();
	}
	
	_mesh.reset();
	_mesh = std::make_shared<sibr::Mesh>();

	std::vector<sibr::Vector3f> vertices;
	std::vector<uint> indices;
	std::vector<sibr::Vector3f> colors;
	
	for(size_t id = 0; id < points.size(); ++id){
		vertices.push_back(points[id]);
		size_t index = id + 1;
		sibr::Vector3f colTemp = sibr::Vector3f(float((index/(256*256))%256),float((index/256)%256),float(index%256));
		colors.push_back((1.0f/255.0f)*colTemp);
	}
	
	_mesh->vertices(vertices);
	_mesh->colors(colors);
	_mesh->triangles(indices);
	
	auto crops = CropCameraUtilities::loadCropList(cropsPath);
	_cropCameras = CropCameraUtilities::estimateCameras(crops, _inputCameras);
	

	_planeMesh.reset();
	_planeMesh = std::make_shared<sibr::Mesh>();
	_camStubMesh = std::move(sibr::RenderUtility::createCameraStub());
	
	
	std::vector<sibr::Vector3f> verticesPlane;	verticesPlane.push_back(sibr::Vector3f(0.0f, 0.0f, 0.0f));			_pointMesh.vertices(verticesPlane);
	std::vector<uint> indicesPlane;				indicesPlane.push_back(0);indicesPlane.push_back(0);indicesPlane.push_back(0);	_pointMesh.triangles(indicesPlane);
	std::vector<sibr::Vector3f> colorsPlane;		colorsPlane.push_back(sibr::Vector3f(1.0f, 1.0f, 0.0f));				_pointMesh.colors(colorsPlane);
	

    _depthShader.init("Depth", 
			sibr::loadFile(sibr::getShadersDirectory("facade_repetitions") + "/point_cloud-pick.vert"), 
			sibr::loadFile(sibr::getShadersDirectory("facade_repetitions") + "/point_cloud-pick.frag"));

    _depthShader_proj.init(_depthShader,"MVP");
	_depthShader_useColor.init(_depthShader,"pick");


	_camStubShader.init("camstub", 
			sibr::loadFile(sibr::getShadersDirectory("facade_repetitions") + "/camstubskewed.vert"),
			sibr::loadFile(sibr::getShadersDirectory("facade_repetitions") + "/camstubskewed.frag"));
	_camStubShaderMVP.init(_camStubShader, "MVP");
	_camStubShaderColor.init(_camStubShader, "color");

	
	
	_performPicking = false;
	_pickCoordinates = sibr::Vector2i(0,0);
	_planePoints.resize(3); // 3 points for the plane
	_currentPointIndex = 0;
	_showPlane = false;
	_showAligned = false;

	glLineWidth(2.f);

	const std::string planeFilePath = _outputPath + "/plane_data.txt";
	if (sibr::fileExists(planeFilePath)) {
		std::ifstream planePointsFile(planeFilePath);
		if (planePointsFile.is_open()) {
			float x, y, z;
			planePointsFile >> x >> y >> z;
			_planePoints[0] = sibr::Vector3f(x, y, z);
			planePointsFile >> x >> y >> z;
			_planePoints[1] = sibr::Vector3f(x, y, z);
			planePointsFile >> x >> y >> z;
			_planePoints[2] = sibr::Vector3f(x, y, z);
			_showPlane = true;
			generatePlane();
			intersectCropCameras();
			estimateInstances();
		}
	}
}


void ULRRepetitionDebugView::onUpdate(sibr::Input& input ){
	if(		input.mouseButton().isReleased(sibr::Mouse::Left)
		&&  input.key().isActivated(sibr::Key::LeftShift) ){
		_performPicking = true;
		_pickCoordinates = input.mousePosition();
	}

	if(input.key().isReleased(sibr::Key::F)){
		_showPlane = true;
		generatePlane();
		intersectCropCameras();
		estimateInstances();
	}

	if(input.key().isReleased(sibr::Key::G)){
		_showAligned = !_showAligned;
	}
	
	if(input.key().isReleased(sibr::Key::H)){
		// Export plane to separate file for reloading.
		savePlaneToFile(_outputPath + "/plane_data.txt");
		saveCamerasBundle(_outputPath + "/aligned_cams.txt");
		saveInstances(_outputPath + "/initial_cameras.txt");
		std::cout << "Export finished." << std::endl;
		
	}

}

void ULRRepetitionDebugView::savePlaneToFile(const std::string & planeFilePath) {
	std::ofstream planeFile(planeFilePath);
	if (!planeFile.is_open()) {
		SIBR_ERR << "Unable to write plane to backup file" << std::endl;
		return;
	}
	planeFile << _planePoints[0][0] << " " << _planePoints[0][1] << " " << _planePoints[0][2] << "\n";
	planeFile << _planePoints[1][0] << " " << _planePoints[1][1] << " " << _planePoints[1][2] << "\n";
	planeFile << _planePoints[2][0] << " " << _planePoints[2][1] << " " << _planePoints[2][2] << "\n";
	planeFile.close();
}

void ULRRepetitionDebugView::saveCamerasBundle(const std::string & alignedCamsPath) {
	// Export aligned cameras pseudo-bundle file.
	std::ofstream file;
	file.open(alignedCamsPath, std::ios::out);
	if (!file.is_open()) {
		SIBR_ERR << "Unable to write cameras to file" << std::endl;
		return;
	}
	file << "# Bundle file v0.3\n";
	// We include the plane points for future use.
	if (_planePoints.size() < 3) {
		SIBR_WRG << "No plane to export, skipping." << std::endl;
	} else {
		file << _planePoints[0][0] << " " << _planePoints[0][1] << " " << _planePoints[0][2] << "\n";
		file << _planePoints[1][0] << " " << _planePoints[1][1] << " " << _planePoints[1][2] << "\n";
		file << _planePoints[2][0] << " " << _planePoints[2][1] << " " << _planePoints[2][2] << "\n";
	}
	// Similar to a bundle export.
	file << _alignedCameras.size() << " 0\n";
	for (auto& cam : _alignedCameras) {
		float focal = 0.5f * cam.crop[3] / (std::tan(0.5f * cam.camera.fovy()));
		file << focal << " " << "0" << " " << "0" << "\n";
		auto r = cam.camera.rotation().matrix();
		auto t = cam.camera.position();
		t = (-r.inverse() * t).eval();
		file << r(0, 0) << " " << r(1, 0) << " " << r(2, 0) << "\n"
			<< r(0, 1) << " " << r(1, 1) << " " << r(2, 1) << "\n"
			<< r(0, 2) << " " << r(1, 2) << " " << r(2, 2) << "\n";
		file << t[0] << " " << t[1] << " " << t[2] << "\n";
	}
	file.close();
}

void ULRRepetitionDebugView::estimateInstances() {
	// Instance identifications
	// TODO SR: test

	std::vector<std::vector<sibr::Vector3f>> reprojCenters(_cropCameras.size(), std::vector<sibr::Vector3f>(_cropCameras.size()));

	for (size_t i = 0; i < _cropCameras.size(); ++i) {
		for (size_t j = 0; j < _cropCameras.size(); ++j) {
			if (i == j) { continue; }
			reprojCenters[i][j] = _cropCameras[j].camera.project(_cropCameras[i].intersection);
		}
	}
	Eigen::MatrixXf values = Eigen::MatrixXf::Zero(_cropCameras.size(), _cropCameras.size());
	for (size_t i = 0; i < _cropCameras.size(); ++i) {
		for (size_t j = 0; j < _cropCameras.size(); ++j) {
			// Norm in screen space is in the center quarter.
			values(i, j) = (reprojCenters[i][j].block<2, 1>(0, 0).norm() < 0.5) ? 1.0f : 0.0f;
		}
	}

	// Remove non symmetric independency.
	Eigen::MatrixXf vt = values.transpose();
	Eigen::MatrixXf values1 = values.cwiseProduct(vt).eval();
	values1 = values1 - Eigen::MatrixXf::Identity(_cropCameras.size(), _cropCameras.size());

	// Label each camera by propagating labels.
	_labels.clear();
	_labels.resize(_cropCameras.size());
	// init
	size_t clustersCount = 1;
	_labels[0] = 0;

	for (size_t i = 1; i < _cropCameras.size(); ++i) {
		bool found = false;
		for (size_t j = 0; j < i; ++j) {
			if (values1(i, j) > 0.5f) {
				found = true;
				_labels[i] = _labels[j];
			}
		}
		if (!found) {
			_labels[i] = clustersCount;
			++clustersCount;
		}

	}
}

void ULRRepetitionDebugView::saveInstances(const std::string & instancesPath) {
	
	std::ofstream streamCams;
	streamCams.open(instancesPath);
	if (!streamCams.is_open()) {
		SIBR_ERR << "Unable to open export file." << std::endl;
		assert(false);
	}
	streamCams << "#window id, picture id" << std::endl;
	for (size_t i = 0; i < _labels.size(); ++i) {
		streamCams << _labels[i] << " " << _cropCameras[i].source << std::endl;
	}
	streamCams.close();

	
}

void ULRRepetitionDebugView::generatePlane(){
	std::vector<sibr::Vector3f> vertices;
	std::vector<uint> indices;
	std::vector<sibr::Vector3f> colors;
		
	for(size_t i = 0; i < 3; ++i){
		vertices.push_back(_planePoints[i]);
		colors.push_back(sibr::Vector3f(0.0f,0.0f,1.0f*float(i+1)/4.0f));
	}

	sibr::Vector3f fourthPoint = _planePoints[0] + (_planePoints[1] - _planePoints[0]) + (_planePoints[2] - _planePoints[0]);
	vertices.push_back(fourthPoint);
	colors.push_back(sibr::Vector3f(0.0f,0.0f,1.0f));
		
	indices.push_back(0); indices.push_back(1); indices.push_back(2);
	indices.push_back(1); indices.push_back(3); indices.push_back(2);

	_planeMesh->vertices(vertices);
	_planeMesh->colors(colors);
	_planeMesh->triangles(indices);

	

}

void ULRRepetitionDebugView::intersectCropCameras(){
	// Plane normal and point.
	sibr::Vector3f p0 = _planePoints[0];
	sibr::Vector3f pNormal = ( _planePoints[1] -  _planePoints[0]).normalized().cross(( _planePoints[2] -  _planePoints[0]).normalized()).normalized();

	for(auto& camCrop : _cropCameras){ 
		// Camera point and direction.
		auto& camera = camCrop.camera;
		sibr::Vector3f c0 = camera.position();
		sibr::Vector3f cDir = camera.dir().normalized();

		float orientation = cDir.dot(pNormal);
		// Check for parallelism
		if(orientation == 0.0) {
			SIBR_WRG << "Ignoring a camera (from image " << camCrop.source << ") as parallel to the facade plane.\n";
			continue;
		}
		// Intersect camera center ray and plane.
		float abscisse = (p0 - c0).dot(pNormal) / orientation;
		sibr::Vector3f intersectionPoint = c0 + abscisse * cDir;
		camCrop.intersection = intersectionPoint;
	}

	_alignedCameras.clear();

	for(auto& camCrop : _cropCameras){
		CropCamera alignedCam(camCrop);
		alignedCam.camera.translate(-alignedCam.intersection);
		_alignedCameras.push_back(alignedCam);
	}

}


void ULRRepetitionDebugView::onRender( sibr::RenderTargetRGB& dst, const sibr::Camera& eye ) {
	
	dst.clear();
    dst.bind();
	glViewport(0,0, dst.w(), dst.h());
	glClearColor(0.0f,0.0f,0.0f,0.0f);
	glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);

	if(_showAligned){
		for(auto& camCrop : _alignedCameras){
			renderCropCamera(camCrop, eye, sibr::Vector3f(01.0f, 0.0f, 1.0f));
		}
		renderPoint(sibr::Vector3f(0.0f,0.0f,0.0f),eye);
		dst.unbind();
		return;
	}
	
	sibr::Mesh* proxy = _mesh.get();


	_depthShader.begin();
	
	_depthShader_proj.set(eye.viewproj());
	_depthShader_useColor.set(_performPicking ? 1.0f : 0.0f);
	glPointSize(8.0f);
	glEnable(GL_DEPTH_TEST);
	proxy->render_points();
	glPointSize(2.0f);
	glDisable(GL_DEPTH_TEST);
	_depthShader.end();
	
	if(_performPicking){
		// Need to investigate if flushing is needed.
		glReadBuffer(GL_COLOR_ATTACHMENT0);
		GLubyte pixels[3];
		glReadPixels(_pickCoordinates[0], dst.h() - _pickCoordinates[1], 1, 1, GL_RGB, GL_UNSIGNED_BYTE, pixels);
		
		size_t selectedPointIndex = 256*256 * int(pixels[0]) + 256 * int(pixels[1]) + int(pixels[2]);
		if(selectedPointIndex > 0){
			std::cout << "Picked point " << _currentPointIndex << std::endl;
			sibr::Vector3f selectedPoint = proxy->vertices()[selectedPointIndex-1];
			_planePoints[_currentPointIndex] = selectedPoint;
			_currentPointIndex = (_currentPointIndex + 1) % 3;
			
		}
		_performPicking = false;
	}

	if(_showPlane){
		_depthShader.begin();
		_depthShader_proj.set(eye.viewproj());
		_depthShader_useColor.set(1.0f);
		_planeMesh->render(true, false);
		_depthShader.end();
	}

	const float scaling = 0.8f;
	for (size_t i = 0; i < _inputCameras.size(); ++i)
	{
		sibr::Matrix4f scale;
		scale << _inputCameras[i]->aspect()*scaling, 0.0f, 0.0f, 0.0f,
				  0.0f, scaling, 0.0f, 0.0f,
				  0.0f, 0.0f, scaling, 0.0f,
				  0.0f, 0.0f, 0.0f, 1.0f;
		_camStubShader.begin();
		_camStubShaderMVP.set(sibr::Matrix4f(eye.viewproj() * _inputCameras[i]->model() * scale));
		_camStubShaderColor.set(sibr::Vector3f(0.f, 1.f, 0.f));
		_camStubMesh.render(false, false, sibr::Mesh::LineRenderMode);
		_camStubShader.end();	
	}
	
	// Crops
	for(auto& camCrop : _cropCameras){
		
		renderCropCamera(camCrop, eye, sibr::Vector3f(1.0f,0.0f,0.0f));
		// If plane computed, display axis/plane intersection.
		if(_showPlane){
			renderPoint(camCrop.intersection, eye);
		}
	}
	
    dst.unbind();
	
}

void ULRRepetitionDebugView::renderCropCamera(const CropCamera& cam, const sibr::Camera& eye, const sibr::Vector3f& color){

			if( !_inputCameras[cam.source]->isActive() ) { return; }
			const float scaling = 0.8f;
			sibr::Matrix4f scaleCrop;
			scaleCrop << cam.camera.aspect() * cam.crop[3] / _inputCameras[cam.source]->h() * scaling, 0.0f, 0.0f, 0.0f,
							0.0f, cam.crop[3] / _inputCameras[cam.source]->h() * scaling, 0.0f, 0.0f,
							0.0f, 0.0f, scaling, 0.0f,
							0.0f, 0.0f, 0.0f, 1.0f;

			_camStubShader.begin();	
			_camStubShaderMVP.set(sibr::Matrix4f(eye.viewproj() * cam.camera.model() * scaleCrop ));
			_camStubShaderColor.set(color);
			_camStubMesh.render(false, false, sibr::Mesh::LineRenderMode);
			_camStubShader.end();
}

void ULRRepetitionDebugView::renderPoint(const sibr::Vector3f& position, const sibr::Camera& eye){
			sibr::Matrix4f translateIntersection;
			translateIntersection << 1.0f, 0.0f, 0.0f, position[0],
									 0.0f, 1.0f, 0.0f, position[1],
									 0.0f, 0.0f, 1.0f, position[2],
									 0.0f, 0.0f, 0.0f, 1.0f;
			_depthShader.begin();
			_depthShader_proj.set(sibr::Matrix4f(eye.viewproj() * translateIntersection));
			_depthShader_useColor.set(1.0f);
			glPointSize(10.0f);
			_pointMesh.render_points();
			glPointSize(2.0f);
			_depthShader.end();

}
