/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#ifndef _ULR_REPETITIONS_DEBUG_VIEW_H_
# define _ULR_REPETITIONS_DEBUG_VIEW_H_

# include <core/graphics/Mesh.hpp>
# include <core/graphics/Texture.hpp>
# include <core/graphics/Shader.hpp>
# include <projects/facade_repetitions/renderer/CropCamera.hpp>

namespace sibr {
	class Input;
}

class ULRRepetitionDebugView 
{
public:
	ULRRepetitionDebugView( int ac, char** av );
	~ULRRepetitionDebugView();

	virtual void onUpdate(sibr::Input& input);
	virtual void onRender( sibr::RenderTargetRGB& dst, const sibr::Camera& eye );


	const std::vector<sibr::InputCamera::Ptr> & cameras() { return _inputCameras; }
	

protected:

	std::shared_ptr<sibr::Mesh>	_mesh;
	std::shared_ptr<sibr::Mesh>	_planeMesh;

	sibr::GLShader    _depthShader;
	sibr::GLParameter _depthShader_proj;
	sibr::GLParameter _depthShader_useColor;
	sibr::GLParameter _depthShader_id;
	sibr::Mesh		  _camStubMesh;
	sibr::GLShader	  _camStubShader;
	sibr::GLParameter _camStubShaderMVP;
	sibr::GLParameter _camStubShaderColor;
	sibr::Mesh		  _pointMesh;
	
private:

	void savePlaneToFile(const std::string & planeFilePath);
	void saveInstances(const std::string & instancesPath);
	void saveCamerasBundle(const std::string & alignedCamsPath);

	void generatePlane();
	void intersectCropCameras();
	void estimateInstances();

	void renderCropCamera(const CropCamera& cam, const sibr::Camera& eye, const sibr::Vector3f& color);
	void renderPoint(const sibr::Vector3f& position, const sibr::Camera& eye);

	std::vector<sibr::InputCamera::Ptr> _inputCameras;
	std::vector<CropCamera> _cropCameras;
	std::vector<CropCamera> _alignedCameras;

	std::vector<sibr::Vector3f> _planePoints;
	sibr::Vector2i _pickCoordinates;
	std::vector<size_t> _labels;
	size_t _currentPointIndex;
	bool _showPlane;
	bool _showAligned;
	bool _performPicking;

	std::string _outputPath;
};

#endif // _ULR_REPETITIONS_DEBUG_VIEW_H_
