/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#ifndef __SIBR_VIEW_GENERALVIEW_HPP__
# define __SIBR_VIEW_GENERALVIEW_HPP__

# include <type_traits>
# include <chrono>

# include <core/graphics/Window.hpp>
# include <core/graphics/Viewport.hpp>
# include <core/assets/InputCamera.hpp>
# include <core/graphics/Input.hpp>
# include <core/graphics/Image.hpp>
# include <core/graphics/RenderUtility.hpp>
# include <core/view/RenderingMode.hpp>
# include <core/graphics/Shader.hpp>
# include <core/raycaster/Raycaster.hpp>
# include <core/graphics/Texture.hpp>
# include "ULRRepetitionDebugView.hpp"

namespace sibr
{
	

	class  GeneralView
	{

	public:
		

		GeneralView( Window& window );


		virtual void				onUpdate( sibr::Input& input );
		virtual void				onRender( sibr::Window& win );

		
		template <typename TIBRRenderer /*where TIBRRenderer inherites from IBRRenderer*/>
		std::shared_ptr<TIBRRenderer>									addRenderer( int ac, char** av ); 

		
	private:

		
		void		onUpdateRenderer( sibr::Input& input , float deltaTime);
		void		onUpdateOrbit( sibr::Input& input  );
		void		moveUsingMousePan( const sibr::Input& input, float deltaTime );
		void		moveCameraUsingWASD( const sibr::Input& input, float deltaTime, Camera& cam );

	
		void		updateOrbitParameters( const sibr::Input& input );

		

		// Behavior of the camera
		// The resulting camera itself should be in IBRScene but its behavior
		// is more like an UI-behavior that belong to this view
		std::chrono::steady_clock::time_point	_timeLastFrame;
		
	
		/////////////////////////////////////////////////////////////////
		// The following subview will be exported in their own classes //
	
		// IBR VIEW
		Viewport							_viewport;
		sibr::Camera						_viewCamera;

		std::shared_ptr<ULRRepetitionDebugView>			_clientView;

		
		sibr::GLShader							_quadShader;
		std::unique_ptr<sibr::RenderTargetRGB>		_destRT;


	};

	template <typename TIBRRenderer>
	std::shared_ptr<TIBRRenderer>	GeneralView::addRenderer(int ac, char** av ) {
		static_assert( std::is_base_of<ULRRepetitionDebugView, TIBRRenderer>::value,
			"Renderer class added to IBRView must be derived from 'ULRRepetitionDebugView'" );

		// IMPORTANT
		// If you have a compilation error at this point, that probably means your
		// Renderer cannot be constructed with an 'IBRView'(*this) as parameter.
		// Sorry but this is a requirement for future improvements
		// If your compilation error is about an 'abstract class' that cannot
		// be instantiated...well define every virtual functions that IBRClientView
		// requires !
		std::shared_ptr<TIBRRenderer> renderer( new TIBRRenderer(ac, av) );

		if( !_clientView ) {
			_clientView = renderer; // IBRView is the main host
		
			_viewCamera = *_clientView->cameras()[0];
		}
		
		return renderer; // if user need to control/adjust things
	}

	
	
} // namespace sibr

#endif // __SIBR_VIEW_GENERALVIEW_HPP__
