/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


# include "GeneralView.hpp"
# include "core/assets/Resources.hpp"
# include "core/view/UIShortcuts.hpp"
# include "core/assets/ImageListFile.hpp"

#include <Eigen/Geometry>

# define SIBR_INTERPOLATE_FRAMES 30
# define SIBR_ORBIT_INTERPOLATE_FRAMES 900
# define SIBR_ROTATION_DELTA 0.004f

# define IBRVIEW_CAMSPEED			1.f
# define IBRVIEW_SMOOTHCAM_POWER	0.1f
# define IBRVIEW_USESMOOTHCAM		true

namespace sibr
{

	GeneralView::GeneralView( Window& window ) :
		_clientView(nullptr)
	{

		UIShortcuts::global().add("a", "Loop through Render targets");
		UIShortcuts::global().add("c", "playback camera path");
		UIShortcuts::global().add("p", "Align user camera with the nearest input camera");
		UIShortcuts::global().add("n", "Increase IOD by 0.01"); 
		UIShortcuts::global().add("m", "Reduce IOD by 0.01");
		UIShortcuts::global().add("v", "Toggle view interp");
		UIShortcuts::global().add("t", "Toggle top view");


		UIShortcuts::global().add("[camera] j", "rotate camera -Y (look left)");
		UIShortcuts::global().add("[camera] l", "rotate camera +Y (look right)");
		UIShortcuts::global().add("[camera] i", "rotate camera +X (look up)");
		UIShortcuts::global().add("[camera] k", "rotate camera -X (look down)");
		UIShortcuts::global().add("[camera] u", "rotate camera +Z ");
		UIShortcuts::global().add("[camera] o", "rotate camera -Z ");
		UIShortcuts::global().add("[camera] w", "move camera -Z (move forward)");
		UIShortcuts::global().add("[camera] s", "move camera +Z (move backward)");
		UIShortcuts::global().add("[camera] a", "move camera -X (strafe left)");
		UIShortcuts::global().add("[camera] d", "move camera +X (strafe right)");
		UIShortcuts::global().add("[camera] q", "move camera -Y (move down)");
		UIShortcuts::global().add("[camera] e", "move camera +Y (move up)");

		sibr::UIShortcuts::global().add("b", "Toggle orbit auto path");
		sibr::UIShortcuts::global().add("alt + left click", "Selectorbit center on proxy");

		UIShortcuts::global().add("space", "Toggle snap correct");
		UIShortcuts::global().add("]", "Increase focal by 10");
		UIShortcuts::global().add("[", "Reduce focal by 10");
		UIShortcuts::global().add("-", "Decrease opacity of debug input images");
		UIShortcuts::global().add("=", "Increase opacity of debug input images");
		UIShortcuts::global().add("ctrl+c", "save camera path (enter filename in the prompt)");
		UIShortcuts::global().add("shift+c", "load camera path (enter filename in the prompt)");
		UIShortcuts::global().add("alt+c", "start recording camera path");

		UIShortcuts::global().list();

		_viewport = Viewport(&window.viewport(), 0.f, 0.f, 1.f, 1.f);
	
		_viewCamera = sibr::Camera();
		_viewCamera.aspect(_viewport.finalWidth()/_viewport.finalHeight());
	
		_timeLastFrame = std::chrono::steady_clock::now();

		_quadShader.init("Texture",
			sibr::loadFile(sibr::Resources::Instance()->getResourceFilePathName("texture.vp")), 
			sibr::loadFile(sibr::Resources::Instance()->getResourceFilePathName("texture.fp")));
		
	}



	void	GeneralView::onUpdate( sibr::Input& input )
	{	
		auto timeNow = std::chrono::steady_clock::now();
		float deltaTime = std::chrono::duration<float>(timeNow - _timeLastFrame).count();
		_timeLastFrame = std::chrono::steady_clock::now();

		
		onUpdateRenderer(input, deltaTime);
		

	}

	void	GeneralView::onRender( sibr::Window& win )
	{


		if ( _clientView  ){
			int w = (int)_viewport.finalWidth();
			int h = (int)_viewport.finalHeight();

			if (!_destRT)// || _destRT->w() != w || _destRT->h() != h)
				_destRT.reset( new RenderTargetRGB(w, h, SIBR_GPU_LINEAR_SAMPLING) );
			//std::cout << w << ", " << h << std::endl;
			_viewport.clear(sibr::Vector3f(0.0,1.0,1.0));
		
			_clientView->onRender(*_destRT, _viewCamera);
			_destRT->unbind();

			glDisable (GL_BLEND);
			glDisable(GL_DEPTH_TEST);
			
			_quadShader.begin();
			glActiveTexture(GL_TEXTURE0); glBindTexture(GL_TEXTURE_2D, _destRT->texture());
			RenderUtility::renderScreenQuad();
			_quadShader.end();
			
		} 
		else if ( !_clientView ) // assuming some other one is active
			SIBR_WRG << "no rendering mode set in IBRView (use IBRView::renderingMode(...))" << std::endl;
		
	}

	

	void			GeneralView::onUpdateRenderer( sibr::Input& input, float deltaTime )
	{
		
		moveCameraUsingWASD(input, deltaTime, _viewCamera);
		moveUsingMousePan(input, deltaTime);

		

		if (_clientView)
			_clientView->onUpdate(input);

	}

	

	void		GeneralView::moveCameraUsingWASD( const sibr::Input& input, float deltaTime, Camera& cam )
	{	


		float camSpeed = 2.f * deltaTime		* IBRVIEW_CAMSPEED;
		float camRotSpeed = 20.f * deltaTime	* IBRVIEW_CAMSPEED;

		Vector3f move(0, 0, 0);

		move.x() -= input.key().isActivated(Key::A) ? camSpeed : 0.f;
		move.x() += input.key().isActivated(Key::D) ? camSpeed : 0.f;
		move.z() -= input.key().isActivated(Key::W) ? camSpeed : 0.f;
		move.z() += input.key().isActivated(Key::S) ? camSpeed : 0.f;
		move.y() -= input.key().isActivated(Key::Q) ? camSpeed : 0.f;
		move.y() += input.key().isActivated(Key::E) ? camSpeed : 0.f;

		Vector3f pivot(0,0,0);

		pivot[1] += input.key().isActivated(Key::J) ? camRotSpeed : 0.f;
		pivot[1] -= input.key().isActivated(Key::L) ? camRotSpeed : 0.f;
		pivot[0] -= input.key().isActivated(Key::K) ? camRotSpeed : 0.f;
		pivot[0] += input.key().isActivated(Key::I) ? camRotSpeed : 0.f;
		pivot[2] -= input.key().isActivated(Key::O) ? camRotSpeed : 0.f;
		pivot[2] += input.key().isActivated(Key::U) ? camRotSpeed : 0.f;

		cam.translate(move, cam.transform());
		cam.rotate(pivot, cam.transform());
	}


	void		GeneralView::moveUsingMousePan( const sibr::Input& input, float deltaTime )
	{
		if (_viewport.contains((float)input.mousePosition().x(), (float)input.mousePosition().y()))
		{
			//float speed = 0.001f;//0.05f*deltaTime;
			float speed = 0.05f*deltaTime; 
			Vector3f move(
				input.mouseButton().isActivated(Mouse::Left)? input.mouseDeltaPosition().x()*speed : 0.f,
				input.mouseButton().isActivated(Mouse::Right)? input.mouseDeltaPosition().y()*speed : 0.f,
				input.mouseButton().isActivated(Mouse::Middle)? input.mouseDeltaPosition().y()*speed : 0.f
				);
			_viewCamera.translate(move, _viewCamera.transform());

			
		}
	}

} // namespace sibr
