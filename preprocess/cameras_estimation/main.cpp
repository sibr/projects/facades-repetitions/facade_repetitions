/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */



#include <fstream>
#include <core/system/Utils.hpp>


#include <core/assets/ImageListFile.hpp>
#include "core/graphics/Shader.hpp"

#include "GeneralView.hpp"


#define PROGRAM_NAME "sibr_ulr_repetitions_utility"
using namespace sibr;


const char* usage = ""
	"Usage: " PROGRAM_NAME " " "--c <crops file>  --s <sfm file>  --o <output dir>	         " "\n"
	;


int main( int ac, char** av )
{
	try
	{

		sibr::Window	window(800, 600, PROGRAM_NAME);
		

		GeneralView			generalView(window);
		/*IBRView			ibrView(window, scene);*/

		// TODO: move to new rendering system, discard the horrible GeneralView class.
		generalView.addRenderer<ULRRepetitionDebugView>(ac, av);
		
		std::cout << "Usage: Select three points. Maj+click to select a point. Press F to visualize the plane. New points can be selected to correct mistakes. Press H to export result." << std::endl;
		while (window.isOpened())
		{
			sibr::Input::poll();
			window.makeContextCurrent();

			if (sibr::Input::global().key().isActivated(sibr::Key::Escape))
				window.close();

			generalView.onUpdate(sibr::Input::global());


			window.viewport().bind();
			generalView.onRender(window);

			window.swapBuffer();
		}
	}
	catch (std::exception& e)
	{
		SIBR_ERR << e.what() << std::endl;
		return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;
}
