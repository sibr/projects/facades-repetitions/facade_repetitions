/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#include "LayersExtraction.hpp"

#include <projects/facade_repetitions/renderer/GuidedFiltering.hpp>
#include <projects/facade_repetitions/renderer/PoissonReconstruction.hpp>
#include <projects/facade_repetitions/renderer/joint_bilateral_filter.hpp>

#include <core/system/Utils.hpp>
#include <iostream>

LayersExtraction::LayersExtraction(const std::string & sourcePath, const int cid)
{
	std::vector<std::string> imagesPaths;
	std::vector<std::string> windowMasksPaths;

	boost::filesystem::directory_iterator  endDir;
	for (boost::filesystem::directory_iterator dir(sourcePath); dir != endDir; ++dir) {
		if (boost::filesystem::is_regular_file(dir->status())) {
			const std::string fileName = dir->path().filename().string();
			if (fileName[0] == 'd') {
				imagesPaths.push_back(fileName);
				_ids.push_back(std::stoi(fileName.substr(1, fileName.length() - 1 - 4)));
			} else if (fileName[0] == 'm') {
				windowMasksPaths.push_back(fileName);
			}
		}
	}

	// Load CNN masks.
	for (auto& maskName : windowMasksPaths) {
		// Load window mask.
		sibr::ImageL8 wmask;
		wmask.load(sourcePath + maskName, false);
		cv::Mat wmaskF;
		wmask.toOpenCV().convertTo(wmaskF, CV_32FC1);
		_windowMasks.push_back(wmaskF / 255.0f);
	}

	// Load images and coverage masks.
	for (size_t lid = 0; lid < imagesPaths.size(); ++lid) {
		auto& imageName = imagesPaths[lid];

		sibr::ImageRGBA initialImage;
		initialImage.load(sourcePath + imageName, false);

		cv::Mat RGBA = initialImage.toOpenCVBGR();
		
		// Split into RGB and A
		cv::Mat RGB(RGBA.rows, RGBA.cols, CV_8UC3);
		cv::Mat ALPHA(RGBA.rows, RGBA.cols, CV_8UC1);
		cv::Mat out[] = { RGB, ALPHA };
		int from_to[] = { 0,0, 1,1, 2,2, 3,3 };
		cv::mixChannels(&RGBA, 1, out, 2, from_to, 4);

		// Convert RGB to floats in [0,1]
		cv::Mat RGBF, ALPHAF;
		RGB.convertTo(RGBF, CV_32FC3);
		ALPHA.convertTo(ALPHAF, CV_32FC1);
		RGBF = RGBF / 255.0f;
		ALPHAF = ALPHAF / 255.0f;

		if (_ids[lid] == cid) {
			_refImg = RGBF;
		}
		_masks.push_back(ALPHAF);
		_rgbImgs.push_back(RGBF);
	}

	

}

void LayersExtraction::computeGradients() {
	for (size_t imgId = 0; imgId < _rgbImgs.size(); imgId++ ) {
		
		auto & rgbImg = _rgbImgs[imgId];
		auto & mask = _masks[imgId];

		cv::Mat gradientX(rgbImg.size(), CV_32FC3), gradientY(rgbImg.size(), CV_32FC3);

		// Compute gradient image, handling occlusions with the mask.
		for (int i = 0; i < rgbImg.rows; ++i) {
			for (int j = 0; j < rgbImg.cols; ++j) {
				// Check mask.
				if (mask.at<float>(i, j) < 0.5) {
					continue;
				}

				int ip = std::min(i + 1, rgbImg.rows - 1);
				int jp = std::min(j + 1, rgbImg.cols - 1);

				// Get the 3x3 neighborhood.
				cv::Vec3f c = rgbImg.at<cv::Vec3f>(i, j);
				cv::Vec3f d = mask.at<float>(ip, j) < 0.5 ? c : rgbImg.at<cv::Vec3f>(ip, j);
				cv::Vec3f r = mask.at<float>(i, jp) < 0.5 ? c : rgbImg.at<cv::Vec3f>(i, jp);

				cv::Vec3f dX = d - c;
				cv::Vec3f dY = r - c;
				gradientX.at<cv::Vec3f>(i, j) = dX;
				gradientY.at<cv::Vec3f>(i, j) = dY;
			}
		}
		_gradientsX.push_back(gradientX);
		_gradientsY.push_back(gradientY);
	}
}

LayersExtraction::~LayersExtraction(void)
{
}

void LayersExtraction::saveMatRGB(const cv::Mat & mat, const std::string & filePath) {
	cv::Mat localMat = mat;
	cv::Mat matU;
	localMat.convertTo(matU, CV_8UC3, 255.0f);
	cv::imwrite(filePath, matU);
}

void LayersExtraction::saveMat(const cv::Mat & mat, const std::string & filePath, bool scale) {
	cv::Mat localMat = mat;
	if (scale) {
		double mini, maxi;
		cv::minMaxLoc(mat, &mini, &maxi);
		float scale = (float)std::max(std::abs(mini), std::abs(maxi));
		localMat = mat / scale;
	}
	cv::Mat matU;
	localMat.convertTo(matU, CV_8U, 255.0f);
	cv::imwrite(filePath, matU);
}


void LayersExtraction::computeMedian(const std::vector<cv::Mat> & imgs, cv::Mat & out, bool interp) {
	if (imgs.size() == 0) { return; }
	out = cv::Mat(imgs[0].rows, imgs[0].cols, CV_32FC1);
	const int rows = imgs[0].rows;
	const int cols = imgs[0].cols;
	for (int i = 0; i < rows; ++i) {
		for (int j = 0; j < cols; ++j) {
			std::vector<float> values;
			for (size_t k = 0; k < _masks.size(); ++k) {
				auto & mask = _masks[k];
				auto & img = imgs[k];
			
				// STANDARD MEDIAN
				// Skip if masked.
				if (mask.at<float>(i, j) < 0.5) {
					continue;
				}
				values.push_back(img.at<float>(i, j));
				
				
			}

			if (values.size() > 0) {
				// Sort the values, the median will be in the middle.
				std::sort(values.begin(), values.end());
				if (interp && values.size() % 2 == 0) {
					out.at<float>(i, j) = 0.5f * (values[values.size() / 2 - 1] + values[values.size() / 2]);
				}
				else {
					out.at<float>(i, j) = values[values.size() / 2];
				}
			}
		}
	}

}


void LayersExtraction::computeMedianTri(const std::vector<cv::Mat> & imgs, cv::Mat & out, const METRIC_FOR_TRIPLE & type, bool interp) {
	if (imgs.size() == 0) { return; }
	out = cv::Mat(imgs[0].rows, imgs[0].cols, CV_32FC3);
	const int rows = imgs[0].rows;
	const int cols = imgs[0].cols;
	
	for (int i = 0; i < rows; ++i) {
		for (int j = 0; j < cols; ++j) {

			std::vector<TripleWithMetric> values;

			for (size_t k = 0; k < _masks.size(); ++k) {
				auto & mask = _masks[k];
				auto & img = imgs[k];
				
				// STANDARD MEDIAN
				// Skip if masked.
				if (mask.at<float>(i, j) < 0.5) {
					continue;
				}
				TripleWithMetric pix(img.at<cv::Vec3f>(i, j), type);
				values.push_back(pix);
				
			}

			if (values.size() > 0) {
				// Sort the values, the median will be in the middle.
				std::sort(values.begin(), values.end());

				cv::Vec3f finalCol;
				if (interp && values.size() % 2 == 0) {
					finalCol = 0.5f * (values[values.size() / 2 - 1].abc + values[values.size() / 2].abc);
				}
				else {
					finalCol = values[values.size() / 2].abc;
				}
				out.at<cv::Vec3f>(i, j) = finalCol;
			}
		}
	}

}

cv::Mat LayersExtraction::threeChannelsMask(const cv::Mat & mask) {
	std::vector<cv::Mat> maskChannels;
	maskChannels.push_back(mask);
	maskChannels.push_back(mask);
	maskChannels.push_back(mask);
	cv::Mat finalMask(mask.rows, mask.cols, CV_32FC3);
	cv::merge(&maskChannels[0], 3, finalMask);
	return finalMask;
}

void LayersExtraction::extraction(const bool useWindowMask, const bool secondPass) {
	

	cv::Mat finalImg, finalGradientX, finalGradientY, finalRGBImg, finalLocalMask, finalWindowMask;
	// Median fo the gradient values for each pixel, taking only values not masked.
	computeMedianTri(_gradientsX, finalGradientX, MAGNITUDE);
	computeMedianTri(_gradientsY, finalGradientY, MAGNITUDE);
	computeMedianTri(_rgbImgs, finalRGBImg, LUMINANCE);
	finalImg =  _refImg;
	computeMedian(_masks, finalLocalMask, false);
	computeMedian(_windowMasks, finalWindowMask, false);

	// Dilation of the mask.
	cv::Mat finalMask = !useWindowMask ? cv::Mat(finalWindowMask.rows, finalWindowMask.cols, CV_32FC1, cv::Scalar(1.0)) : finalWindowMask;

	if (!secondPass) {


		const auto computeHue = [](const cv::Vec3f & col) {
			float min = std::min(std::min(col[0], col[1]), col[2]);
			float max = std::max(std::max(col[0], col[1]), col[2]);

			if (min == max) {
				return 0.0f;
			}

			float hue = 0.0f;
			if (max == col[0]) {
				hue = ((col[1] - col[2]) / (max - min));

			} else if (max == col[1]) {
				hue = 2.0f + (col[2] - col[0]) / (max - min);

			} else {
				hue = 4.0f + (col[0] - col[1]) / (max - min);
			}

			hue = hue * 60;
			if (hue < 0) hue = hue + 360;
			return (hue / 360.0f);
		};

		const auto hueDist = [](const float h1, const float h2) {
			return std::min(std::abs(h1 - h2), 1.0f - std::abs(h1 - h2));
		};

		// what should the final mask look like.
		// The window is in it.
		// Every area where there is high variance should be too.
		for (int y = 0; y < finalImg.cols; ++y) {
			for (int x = 0; x < finalImg.rows; ++x) {
				std::vector<float> samples;
				//sibr::Vector3f avgColor(0.0f, 0.0f, 0.0f);
				float avgHue = 0.0;
				for (size_t rid = 0; rid < _rgbImgs.size(); ++rid) {
					if (_masks[rid].at<float>(x, y) < 0.5f) { continue; }

					const auto & rgbimg = _rgbImgs[rid];
					const auto & col = rgbimg.at<cv::Vec3f>(x, y);
					samples.emplace_back(computeHue(col));
					avgHue += samples.back();
				}
				avgHue /= samples.size();

				/*float var = 0.0;
				for(size_t cid = 0; cid < samples.size(); ++cid) {
					var += std::pow(hueDist(samples[cid]*2.0,avgHue*2.0),2);
				}
				var /= (samples.size() - 1);
				if(var > 0.1) {
					finalMask.at<float>(x, y) = 1.0;
				}*/
				const float refHue = computeHue(finalImg.at<cv::Vec3f>(x, y));
				const float distance = hueDist(refHue, avgHue);
				if (distance > 0.02) {
					finalMask.at<float>(x, y) = 1.0;
				}
			}
		}


		
		int dilationSize = 2;
		const cv::Mat kernel = cv::getStructuringElement(cv::MORPH_ELLIPSE,
			cv::Size(2 * dilationSize + 1, 2 * dilationSize + 1),
			cv::Point(dilationSize, dilationSize));
		//
		//cv::morphologyEx(finalMask, finalMask, cv::MORPH_OPEN, kernel);
		dilationSize = 2;
		const cv::Mat kernel2 = cv::getStructuringElement(cv::MORPH_ELLIPSE,
			cv::Size(2 * dilationSize + 1, 2 * dilationSize + 1),
			cv::Point(dilationSize, dilationSize));
		cv::dilate(finalMask, finalMask, kernel2, cv::Point(-1, -1), 1);

	}
	const int marginS = 5;
	for (int i = 0; i < finalMask.rows; ++i) {
		for (int j = 0; j < finalMask.cols; ++j) {
			// Add a thin border in case the mask is all white.
			if (i < marginS || j < marginS || j > finalMask.cols - marginS/* || i > finalMask.rows - marginS*/) {
				finalMask.at<float>(i, j) = 0.0f;
			}
		}
	}
	
	//cv::imshow("finalMask", finalMask);
	//cv::waitKey(0);
		
	
	// Comp. for Poisson.
	finalMask = 1.0f - finalMask;

	// Median Poisson reconstruction.
	PoissonReconstruction recons(finalGradientX, finalGradientY, finalMask, finalImg);
	//cv::imshow("tr", finalGradientX.mul(finalGradientX));
	//cv::waitKey(0);

	recons.solve();
	_finalImg = recons.result();
	_finalGradientX = finalGradientX.clone();
	_finalGradientY = finalGradientY.clone();
}

void LayersExtraction::exportResults(int cid, const std::string & outputPath, const bool secondPass, const float threshold){
	sibr::makeDirectory(outputPath);
	sibr::makeDirectory(outputPath + "../masks/");
	const bool useWindowMask = !secondPass;

	// Export the reconstructed base.
	saveMatRGB(_finalImg, outputPath + "../00000" + (cid > 99 ? "" : "0") + (cid > 9 ? "" : "0") + std::to_string(cid) + ".jpg");
	
	// Try substracting the reconstructed image from the intial rgb images.
	//cv::Mat avgDiff(_finalImg.rows, _finalImg.cols, CV_32FC3);

	for (size_t k = 0; k < _rgbImgs.size(); ++k) {

		// Compute delta.
		auto & rgbImg = _rgbImgs[k];
		cv::Mat finalVar = rgbImg - _finalImg;

		// Masking occlusions.
		const cv::Mat mask = threeChannelsMask(_masks[k]);
		cv::multiply(finalVar, mask, finalVar, 1.0, CV_32F);

		// Additional masking by Pix2Pix result.
		if (useWindowMask) {
			const cv::Mat windowMask = threeChannelsMask(_windowMasks[k]);
			cv::multiply(finalVar, windowMask, finalVar, 1.0, CV_32F);
		}

		// Take absolute val, save it for previz.
		saveMat(cv::abs(finalVar), outputPath + std::to_string(_ids[k]) + "-" + std::to_string(cid) + "-abs.png", false);
		
		// Compute delta.
		cv::Mat finalVarX = _gradientsX[k] - _finalGradientX;
		cv::Mat finalVarY = _gradientsY[k] - _finalGradientY;
		// Masking.
		cv::multiply(finalVarX, mask, finalVarX, 1.0, CV_32F);
		cv::multiply(finalVarY, mask, finalVarY, 1.0, CV_32F);

		if(!secondPass && _ids[k] != cid) {
			continue;
		}
		sibr::ImageRGB32F finalVarXImg, finalVarYImg;
		finalVarXImg.fromOpenCV(finalVarX);
		sibr::convertRGB32FtoRGBA(finalVarXImg).save(outputPath + std::to_string(_ids[k]) + "-" + std::to_string(cid) + "-gradX.png");
		finalVarYImg.fromOpenCV(finalVarY);
		sibr::convertRGB32FtoRGBA(finalVarYImg).save(outputPath + std::to_string(_ids[k]) + "-" + std::to_string(cid) + "-gradY.png");

		/*cv::FileStorage fs2(outputPath + std::to_string(_ids[k]) + "-" + std::to_string(cid) + "-gradX.xml", cv::FileStorage::WRITE);
		fs2 << "gradX" << finalVarX;
		cv::FileStorage fs4(outputPath + std::to_string(_ids[k]) + "-" + std::to_string(cid) + "-gradY.xml", cv::FileStorage::WRITE);
		fs4 << "gradY" << finalVarY;*/
	}

	
	// Save raw base result.
	//if (secondPass) {

		sibr::ImageRGB32F finalGradXImg, finalGradYImg;
		finalGradXImg.fromOpenCV(_finalGradientX);
		sibr::convertRGB32FtoRGBA(finalGradXImg).save(outputPath + "../00000" + (cid > 99 ? "" : "0") + (cid > 9 ? "" : "0") + std::to_string(cid) + "-gradX.png");
		finalGradYImg.fromOpenCV(_finalGradientY);
		sibr::convertRGB32FtoRGBA(finalGradYImg).save(outputPath + "../00000" + (cid > 99 ? "" : "0") + (cid > 9 ? "" : "0") + std::to_string(cid) + "-gradY.png");
		/*
		cv::FileStorage fs(outputPath + "../00000" + (cid > 99 ? "" : "0") + (cid > 9 ? "" : "0") + std::to_string(cid) + "-gradX.xml", cv::FileStorage::WRITE);
		fs << "gradX" << _finalGradientX;
		cv::FileStorage fs3(outputPath + "../00000" + (cid > 99 ? "" : "0") + (cid > 9 ? "" : "0") + std::to_string(cid) + "-gradY.xml", cv::FileStorage::WRITE);
		fs3 << "gradY" << _finalGradientY;*/
	//}

}

