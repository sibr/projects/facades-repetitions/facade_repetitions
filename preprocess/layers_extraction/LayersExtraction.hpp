/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#ifndef LAYERSEXTRACTION_H
#define LAYERSEXTRACTION_H

#include <core/graphics/Image.hpp>


class LayersExtraction
{

private:
	
	enum METRIC_FOR_TRIPLE {
		LUMINANCE, MAGNITUDE
	};

	struct TripleWithMetric {
		cv::Vec3f abc;
		float m;

		TripleWithMetric() {
			abc = cv::Vec3f(0.0f, 0.0f, 0.0f);
			m = 0.0f;
		}

		TripleWithMetric(const cv::Vec3f & xyz, const METRIC_FOR_TRIPLE & type) {
			abc = xyz;
			if (type == LUMINANCE) {
				m = 0.299f * xyz(0) + 0.587f * xyz(1) + 0.114f * xyz(2);
			}
			else {
				//m = xyz(0)*xyz(0) + xyz(1)*xyz(1) + xyz(2)*xyz(2);
				//m = (xyz(0) + xyz(1) + xyz(2))/3.0f;
				m = 0.299f * xyz(0) + 0.587f * xyz(1) + 0.114f * xyz(2);
			}
		}

		bool operator < (const TripleWithMetric& trp) const {
			return (m < trp.m);
		}
	};


	
	static cv::Mat threeChannelsMask(const cv::Mat & mask);
	
	void computeMedian(const std::vector<cv::Mat> & imgs, cv::Mat & out, bool interp = true);
	void computeMedianTri(const std::vector<cv::Mat> & imgs, cv::Mat & out, const METRIC_FOR_TRIPLE & type, bool interp = true);

public:

	LayersExtraction(const std::string & sourcePath, const int cid);

	void computeGradients();
	void extraction(const bool useWindowMask, const bool secondPass);
	void exportResults(int cid, const std::string & outputPath, const bool secondPass, const float threshold);
	
	static void saveMat(const cv::Mat & mat, const std::string & filePath, bool scale = false);
	static void saveMatRGB(const cv::Mat & mat, const std::string & filePath);

	~LayersExtraction(void);

private:

	std::vector<int> _ids;
	std::vector<cv::Mat> _gradientsX; // 3 components gradient. 32FC3
	std::vector<cv::Mat> _gradientsY; // 3 components gradient. 32FC3
	std::vector<cv::Mat> _masks; // 1 component mask. 32FC1
	std::vector<cv::Mat> _rgbImgs; // 3 components image. 32FC3
	std::vector<cv::Mat> _windowMasks; // 1 component mask. 32FC1
	cv::Mat _refImg; // 3 components image. 32FC3
	cv::Mat _finalImg;
	cv::Mat _finalGradientX;
	cv::Mat _finalGradientY;
};

#endif // LAYERSEXTRACTION_H