/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */



#include "LayersExtraction.hpp"
#include <projects/facade_repetitions/renderer/OpenMVGSFM.hpp>
#include <projects/facade_repetitions/renderer/ViewReprojection.hpp>
#include <projects/facade_repetitions/renderer/CropCamera.hpp>
#include <projects/facade_repetitions/renderer/KIPPIGraph.hpp>
#include <projects/facade_repetitions/renderer/PoissonReconstruction.hpp>

#include <core/system/CommandLineArgs.hpp>
#include <core/system/Utils.hpp>
#include <core/raycaster/CameraRaycaster.hpp>

#include <opencv2/core/core.hpp>
#include <Eigen/Sparse>

#include <iostream>
#include <map>

#define PROGRAM_NAME "sibr_ulr_layers_extraction"
using namespace sibr;


const char* usage = ""
"Usage: " PROGRAM_NAME " --path <dataset-path>" "\n"
;


void generateReflectionmask(const std::string & rootPath, const std::string & scenePath, const float sigmaScale, const float maxRatioAccepted) {

	const std::string outputMaskPath = rootPath + "/masks_auto/";
	sibr::makeDirectory(outputMaskPath);
	
	// Load initial scene w/ refined proxy.
	auto scene = std::make_shared<ReprojectionScene>(scenePath);

	// Setup raycaster.
	auto raycaster = std::make_shared<sibr::Raycaster>();
	if (!raycaster->init()) {
		SIBR_ERR << " failed raycaster init in constructor " << std::endl;
		return;
	}
	sibr::Mesh::Ptr doubleMesh = scene->proxy().clone();
	doubleMesh->merge(scene->proxy().invertedFacesMesh());
	raycaster->addMesh(*doubleMesh);



	// Load graph.
	auto files = sibr::listFiles(rootPath + "/segmentation/", false, false, { "txt" });
	std::string graphFile;
	unsigned int camId = 0;
	for (const auto & file : files) {
		if (file.substr(file.size() - 10) == "_graph.txt") {
			graphFile = file;
			camId = std::stoi(file.substr(0, file.size() - 10));
			break;
		}
	}

	const sibr::InputCamera frontoCam = *scene->inputCameras()[camId];
	KIPPIGraph graph(rootPath + "/segmentation/" + graphFile);

	struct ReflexionRecord {
		double value = 0.0;
		size_t count = 0;
	};

	std::vector<ReflexionRecord> votes(graph.superPixels().size());

	// For each view in the delta directory.
	auto deltaFiles = sibr::listFiles(rootPath + "/layers/pass1/deltas/", false, false, { "png" });
	for (const auto & deltaFile : deltaFiles) {
		size_t d1pos = deltaFile.find_first_not_of("0123456789");
		size_t d2pos = deltaFile.find_first_not_of("0123456789", d1pos + 1);
		// Detect unwanted file.
		if (d1pos == std::string::npos || d2pos == std::string::npos) {
			continue;
		}

		size_t imageId = std::stoi(deltaFile.substr(0, d1pos));
		size_t viewId = std::stoi(deltaFile.substr(d1pos + 1, d2pos - (d1pos + 1)));

		// Load delta layer.
		sibr::ImageRGB layer;
		layer.load(rootPath + "/layers/pass1/deltas/" + deltaFile);

		// We need to raycast from viewCam to the mesh, then project to frontoCam.
		sibr::InputCamera viewCam(*scene->inputCameras()[viewId]);

		sibr::Vector3f dx, dy, upLeftOffset;
		sibr::CameraRaycaster::computePixelDerivatives(viewCam, dx, dy, upLeftOffset);

		// For each pixel of the camera's image
		for (uint py = 0; py < viewCam.h(); ++py) {
			for (uint px = 0; px < viewCam.w(); ++px) {

				const sibr::Vector3ub deltaColor = layer(px, py);
				// Early exit if not in mask.
				if (deltaColor.isNull()) {
					continue;
				}
				uint gx = px;
				uint gy = py;
				bool reprojected = viewId == camId;
				if (!reprojected) {
					// Reproject to find the pixel in the graph fronto view.
					const sibr::Vector3f worldPos = ((float)px + 0.5f)*dx + ((float)py + 0.5f)*dy + upLeftOffset;
					// Cast a ray
					sibr::Vector3f dir = worldPos - viewCam.position();
					sibr::RayHit hit = raycaster->intersect(sibr::Ray(viewCam.position(), dir));
					if (hit.hitSomething()) {
						// Get the intersection point in world space.
						const sibr::Vector3f intersection(viewCam.position() + hit.dist()*dir.normalized());
						//// Check visibility from fronto camera.
						sibr::Vector3f dir1 = frontoCam.position() - intersection;
						sibr::RayHit hitViz = raycaster->intersect(sibr::Ray(intersection, dir1), 0.01f);
						// If we hit the mesh again, the intersection is occluded for the input camera.
						if (hitViz.hitSomething()) {
							continue;
						}
						// Reproject in current processed camera screen space.
						sibr::Vector3f projInter = frontoCam.projectScreen(intersection);
						// Check that we are inside frontoCam frustum.
						if (projInter.x() < 0 || projInter.x() >= frontoCam.w() ||
							projInter.y() < 0 || projInter.y() >= frontoCam.h()) {
							continue;
						}
						reprojected = true;
						gx = (uint)projInter.x();
						gy = (uint)projInter.y();
					}
				}

				// if not reprojected in the image, don't vote.
				if (!reprojected) {
					continue;
				}

				//const double val = (deltaColor.cast<float>().dot(sibr::Vector3f(0.2126f, 0.7152f, 0.0722f)))/255.0f;
				const double val = (deltaColor.cast<float>().dot(sibr::Vector3f(1.0, 1.0, 1.0))) / 3.0f / 255.0f;
				// We now know the pixel into which we reprojected into the fronto parallel view.
				// Now to find the superpixel.
				for (size_t spid = 0; spid < graph.superPixels().size(); ++spid) {
					const auto & sp = graph.superPixels()[spid];
					if (!sp.bbox.contains(sibr::Vector2i(gx, gy))) {
						continue;
					}
					if (graph.isInside(sp, gx, gy)) {
						// Vote for this superpixel.
						votes[spid].count += 1;
						votes[spid].value += val;
						break;
					}
				}
			}
		}
	}
	// Zero superpixels out of the mask.
	
	for (size_t sid = 0; sid < graph.superPixels().size(); ++sid) {
		size_t allCount = 0;
		size_t outCount = 0;
		const auto & sp = graph.superPixels()[sid];
		const sibr::Vector2i bmin = sp.bbox.min();
		const sibr::Vector2i bmax = sp.bbox.max();
		for (int y = bmin[1]; y <= bmax[1]; ++y) {
			for (int x = bmin[0]; x <= bmax[0]; ++x) {
				if (x < 0 || x >= (int)graph.w() || y < 0 || y >= (int)graph.h()) {
					continue;
				}

				if (graph.isInside(sp, x, y)) {
					++allCount;
					if (scene->masks()[camId](x, y).x() < 128) {
						++outCount;
					}
				}
			}
		}
		// Remove outsiders
		if (outCount > allCount / 4) {
			votes[sid].value = 0.0;
			votes[sid].count = 0;
		}
	}
	
	

	double maxVote = 0.0;
	size_t maxCount = 0;
	double maxRatio = 0.0;

	double avgVote = 0.0;
	double avgCount = 0.0;
	double avgRatio = 0.0;

	double varVote = 0.0;
	double varCount = 0.0;
	double varRatio = 0.0;

	size_t denom = 0;

	for (const auto & vote : votes) {
		if (vote.count > 0) {
			maxVote = std::max(maxVote, vote.value);
			maxCount = std::max(maxCount, vote.count);
			maxRatio = std::max(maxRatio, vote.value / double(vote.count));

			avgVote += vote.value;
			avgCount += vote.count;
			avgRatio += vote.value / double(vote.count);

			++denom;
		}
	}

	avgVote = avgVote / maxVote;
	avgCount = avgCount / maxCount;
	avgRatio = avgRatio / maxRatio;

	avgVote /= denom;
	avgCount /= denom;
	avgRatio /= denom;

	for (const auto & vote : votes) {
		if (vote.count > 0) {
			varVote += std::pow(vote.value / maxVote - avgVote, 2);
			varCount += std::pow(vote.count / maxCount - avgCount, 2);
			varRatio += std::pow((vote.value / double(vote.count)) / maxRatio - avgRatio, 2);
		}
	}
	varVote /= (denom - 1);
	varCount /= (denom - 1);
	varRatio /= (denom - 1);

	/*
	std::cout << avgVote << ", " << avgCount << ", " << avgRatio << std::endl;
	std::cout << varVote << ", " << varCount << ", " << varRatio << std::endl;
	*/
	//DrawingUtils draw;
	sibr::ImageRGB imgDebug4(graph.w(), graph.h(), sibr::Vector3ub(0, 0, 0));
	/*sibr::ImageRGB imgDebug2(graph.w(), graph.h(), sibr::Vector3ub(0, 0, 0));
	sibr::ImageRGB imgDebug3(graph.w(), graph.h(), sibr::Vector3ub(0, 0, 0));
	sibr::ImageRGB imgDebug1(graph.w(), graph.h(), sibr::Vector3ub(0, 0, 0));*/

	sibr::ImageL8 finalFrontoMask(graph.w(), graph.h(), 0);



	for (unsigned int sidd = 0; sidd < graph.superPixels().size(); ++sidd) {
		auto & sp = graph.superPixels()[sidd];
		const sibr::Vector2i bmin = sp.bbox.min();
		const sibr::Vector2i bmax = sp.bbox.max();
		const sibr::Vector2f size = (bmax - bmin).cwiseAbs().cast<float>();
		const bool ratioReject = (size.x() / size.y() > maxRatioAccepted || size.y() / size.x() > maxRatioAccepted);

		const double sval = votes[sidd].value / maxVote;
		const double scou = double(votes[sidd].count) / maxCount;
		const double tt = (votes[sidd].value / double(votes[sidd].count)) / maxRatio;



		const bool c1 = sval > (avgVote + sigmaScale * sqrt(varVote));
		const bool c2 = scou > (avgCount + sigmaScale * sqrt(varCount));
		const bool c3 = tt   > (avgRatio + sigmaScale * sqrt(varRatio));

		const sibr::Vector3ub col1((unsigned char)(sval * 255), c1 ? 255 : 0, 0);
		const sibr::Vector3ub col2((unsigned char)(scou * 255), c2 ? 255 : 0, 0);
		const sibr::Vector3ub col3((unsigned char)(tt * 255), c3 ? 255 : 0, 0);

		bool shouldKeep = c2 || c1;// || c3;// (int(c1) + int(c2) + int(c3)) > 1;
		if (shouldKeep) {
			size_t allCount = 0;
			size_t outCount = 0;
			for (int y = bmin[1]; y <= bmax[1]; ++y) {
				for (int x = bmin[0]; x <= bmax[0]; ++x) {
					if (x < 0 || x >= (int)graph.w() || y < 0 || y >= (int)graph.h()) {
						continue;
					}

					if (graph.isInside(sp, x, y)) {
						++allCount;
						if (scene->masks()[camId](x, y).x() < 128) {
							++outCount;
						}
					}
				}
			}
			// Remove outsiders
			if (outCount > allCount / 4) {
				shouldKeep = false;
			}
		}
		for (int y = bmin[1]; y <= bmax[1]; ++y) {
			for (int x = bmin[0]; x <= bmax[0]; ++x) {
				if (x < 0 || x >= (int)graph.w() || y < 0 || y >= (int)graph.h()) {
					continue;
				}

				if (shouldKeep && graph.isInside(sp, x, y) && !ratioReject) {
					finalFrontoMask(x, y).x() = 255;

				}
				if (graph.isInside(sp, x, y)) {
					imgDebug4(x, y) = sibr::Vector3ub(col1[0], col2[0], shouldKeep ? 255 : 0);
				}
				
			}
		}
	}
	finalFrontoMask.save(rootPath + "/layers/fronto-mask.png");
	/*imgDebug1.save(rootPath + "/segmentation/debug-ref1.png");
	imgDebug2.save(rootPath + "/segmentation/debug-ref2.png");
	imgDebug3.save(rootPath + "/segmentation/debug-ref3.png");*/
	imgDebug4.save(rootPath + "/segmentation/debug-ref4.png");
	//sibr::show(finalFrontoMask);
	#pragma omp parallel for
	for (int destCamId = 0; destCamId < scene->inputCameras().size(); ++destCamId) {
		sibr::InputCamera destCam(*scene->inputCameras()[destCamId]);

		sibr::ImageL8 destMask(destCam.w(), destCam.h(), 0);

		sibr::Vector3f dx, dy, upLeftOffset;
		sibr::CameraRaycaster::computePixelDerivatives(destCam, dx, dy, upLeftOffset);
		if (destCamId == camId) {
			destMask = finalFrontoMask.clone();
		} else {
			for (uint py = 0; py < destCam.h(); ++py) {
				for (uint px = 0; px < destCam.w(); ++px) {
					if (scene->masks()[destCamId](px, py).x() < 128) {
						continue;
					}


					sibr::Vector3f worldPos = ((float)px + 0.5f)*dx + ((float)py + 0.5f)*dy + upLeftOffset;
					sibr::Vector3f dir = worldPos - destCam.position();
					sibr::RayHit hit = raycaster->intersect(sibr::Ray(destCam.position(), dir));
					if (hit.hitSomething()) {
						// Get the intersection point in world space.
						sibr::Vector3f intersection(destCam.position() + hit.dist()*dir.normalized());
						// Check visibility from input camera.
						const sibr::Vector3f dir1 = frontoCam.position() - intersection;
						sibr::RayHit hitViz = raycaster->intersect(sibr::Ray(intersection, dir1), 0.001f);
						// If we hit the mesh again, the intersection is occluded for the input camera.
						if (hitViz.hitSomething()) {
							continue;
						}

						if (frontoCam.frustumTest(intersection)) {
							const sibr::Vector3f projInter = frontoCam.projectScreen(intersection);
							// HACK: ceil or floor to avoid discretization artifacts on side faces...
							const uint baseX = std::min(uint(std::ceil(projInter.x())), destCam.w() - 1);
							const uint baseY = std::min(uint(std::ceil(projInter.y())), destCam.h() - 1);
							destMask(px, py) = finalFrontoMask(baseX, baseY);
						}
					}
				}
			}
		}
		const std::string baseFilename = sibr::imageIdToString(destCamId) + "_mask.png";
		destMask.save(outputMaskPath + "/" + baseFilename);
	}
}

void generateInstanceViews(const std::string & rootPath, const std::string & scenePath, const std::vector<std::pair<unsigned, unsigned>> &
                           windowsIds, const unsigned instanceCount, const bool addReflection) {
	// Load just the cameras (lifted from ReprojectionScene w/o activeImageFile handling).
	auto inputCameras = sibr::InputCamera::load(scenePath);
	const auto bgPass = rootPath + "/layers/pass1/";
	const auto basePath = rootPath + "/layers/pass2/";
	const auto deltaPath = rootPath + "/layers/pass2/deltas/";
	const auto finalPath = rootPath + "/layers/pass3/";
	const auto deltaReflecPath = rootPath + "/layers/pass1/deltas/";
	sibr::makeDirectory(finalPath);

	#pragma omp parallel for
	for (int cid = 0; cid < (int)windowsIds.size(); ++cid) {
		const auto & cam = *inputCameras[cid];
		// load base of camera cid
		sibr::ImageRGBA baseXImg, baseYImg;
		baseXImg.load(basePath + sibr::imageIdToString(cid) + "-gradX.png");
		baseYImg.load(basePath + sibr::imageIdToString(cid) + "-gradY.png");
		const cv::Mat baseX = sibr::convertRGBAtoRGB32F(baseXImg).toOpenCV().clone();
		const cv::Mat baseY = sibr::convertRGBAtoRGB32F(baseYImg).toOpenCV().clone();
		

		sibr::ImageRGB baseImg;
		baseImg.load(bgPass + sibr::imageIdToString(cid) + ".jpg");
		cv::Mat imgTarget;
		baseImg.toOpenCVBGR().convertTo(imgTarget, CV_32FC3, 1.0f / 255.0f);

		// for each instance
		for (size_t iid = 0; iid < instanceCount; ++iid) {
			const auto instanceFinalPath = finalPath + "/instance-" + std::to_string(iid) + "/";
			sibr::makeDirectory(instanceFinalPath);

			// Find camera of instance iid that is the closest of cid
			unsigned int bestId = cid;
			float bestDistance = 0.0f;
			bool first = true;
			for (size_t ncid = 0; ncid < windowsIds.size(); ++ncid) {

				if (windowsIds[ncid].first != iid) {
					continue;
				}
				const auto & ncam = *inputCameras[ncid];
				// Compute distance.
				const float distance = (ncam.position() - cam.position()).norm();
				if (distance < bestDistance || first) {
					first = false;
					bestDistance = distance;
					bestId = (unsigned int)ncid;
				}
			}
			// reproject bestId delta into cid (should be file bestId-cid)
			sibr::ImageRGBA deltaMapXImg, deltaMapYImg;
			deltaMapXImg.load(deltaPath + std::to_string(bestId) + "-" + std::to_string(cid) + "-gradX.png");
			deltaMapYImg.load(deltaPath + std::to_string(bestId) + "-" + std::to_string(cid) + "-gradY.png");
			cv::Mat deltaMapX = sibr::convertRGBAtoRGB32F(deltaMapXImg).toOpenCV().clone();
			cv::Mat deltaMapY = sibr::convertRGBAtoRGB32F(deltaMapYImg).toOpenCV().clone();
			

			sibr::ImageRGBA deltaMapXRImg, deltaMapYRImg;
			deltaMapXRImg.load(deltaPath + std::to_string(cid) + "-" + std::to_string(cid) + "-gradX.png");
			deltaMapYRImg.load(deltaPath + std::to_string(cid) + "-" + std::to_string(cid) + "-gradY.png");
			cv::Mat deltaMapXR = sibr::convertRGBAtoRGB32F(deltaMapXRImg).toOpenCV().clone();
			cv::Mat deltaMapYR = sibr::convertRGBAtoRGB32F(deltaMapYRImg).toOpenCV().clone();
			

			const float thresholdSq = 5.0f*float(10e-6);
			const float borderSize = 10.0f;
			cv::Mat nullMask(deltaMapX.rows, deltaMapX.cols, CV_32FC1);
			cv::Mat nullMask3(deltaMapX.rows, deltaMapX.cols, CV_32FC3);
			for (int y = 0; y < nullMask.rows; ++y) {
				for (int x = 0; x < nullMask.cols; ++x) {
					if (y < borderSize || x < borderSize || y > nullMask.rows - borderSize || x > nullMask.cols - borderSize) {
						nullMask.at<float>(y, x) = 1.0f;
					} else {
						nullMask.at<float>(y, x) = 0.0f;
					}

					const cv::Vec3f colX = deltaMapX.at<cv::Vec3f>(y, x);
					const cv::Vec3f colY = deltaMapY.at<cv::Vec3f>(y, x);
					const cv::Vec3f norms = colX.mul(colX) + colY.mul(colY);

					if (cv::countNonZero(colX) == 0 && cv::countNonZero(colY) == 0) {
						nullMask3.at<cv::Vec3f>(y, x) = cv::Vec3f(1.0f, 1.0f, 1.0f);
					} /*else if (norms[0] < thresholdSq && norms[1] < thresholdSq && norms[2] < thresholdSq) {
					  //nullMask.at<float>(y, x) = 1.0f;
					  nullMask3.at<cv::Vec3f>(y, x) = cv::Vec3f(1.0f, 1.0f, 1.0f);
					  }*/ else {
					  //nullMask.at<float>(y, x) = 0.0f;
						nullMask3.at<cv::Vec3f>(y, x) = cv::Vec3f(0.0f, 0.0f, 0.0f);
					}
				}
			}
			cv::multiply(deltaMapXR, nullMask3, deltaMapXR);
			cv::multiply(deltaMapYR, nullMask3, deltaMapYR);


			if (addReflection) {
				sibr::ImageRGBA deltaMapXRRefImg, deltaMapYRRefImg;
				deltaMapXRRefImg.load(deltaReflecPath + std::to_string(cid) + "-" + std::to_string(cid) + "-gradX.png");
				deltaMapYRRefImg.load(deltaReflecPath + std::to_string(cid) + "-" + std::to_string(cid) + "-gradY.png");
				const cv::Mat deltaMapXRRef = sibr::convertRGBAtoRGB32F(deltaMapXRRefImg).toOpenCV().clone();
				const cv::Mat deltaMapYRRef = sibr::convertRGBAtoRGB32F(deltaMapYRRefImg).toOpenCV().clone();


				deltaMapXR += deltaMapXRRef;
				deltaMapYR += deltaMapYRRef;
			}

			//cv::multiply(deltaMapX, cv::Vec3f(1.0f, 1.0f, 1.0f) - nullMask3, deltaMapX);
			//cv::multiply(deltaMapY, cv::Vec3f(1.0f, 1.0f, 1.0f) - nullMask3, deltaMapY);
			const cv::Mat imageFinalX = baseX + deltaMapX + deltaMapXR;
			const cv::Mat imageFinalY = baseY + deltaMapY + deltaMapYR;



			PoissonReconstruction recons(imageFinalX, imageFinalY, nullMask, imgTarget);
			recons.solve();

			cv::Mat finalResult;
			recons.result().convertTo(finalResult, CV_8UC3, 255.0f);
			sibr::ImageRGB finalSave;
			finalSave.fromOpenCVBGR(finalResult);
			finalSave.save(instanceFinalPath + sibr::imageIdToString(cid) + ".jpg", false);
		}


	}
}


struct RepetitionsLayersExtractionArgs {
	RequiredArg<std::string> path = { "path", "path to dataset directory" };
	Arg<bool> secondPass = { "second-pass" , "perform second pass"};
	Arg<bool> reflectionMasks = { "masks", "use masks" };
	Arg<bool> recombination = { "combi", "perform recombination" };
	Arg<float> threshold1 = { "th1" , 0.15f, "see code and script for use"};
	Arg<float> threshold2 = { "th2" , 0.25f, "see code and script for use" };
	Arg<bool> useRef = { "use-ref",  "Add reflections"};
	Arg<float> sigma = { "sigma" , 0.28f, "see code and script for use" };
	Arg<float> ratio = { "ratio" , 3.5f, "see code and script for use" };
	Arg<bool> testReprojectionSimple = { "test-reproj", "test basic reprojection" };
};

int main(int ac, char** av)
{

	if (ac < 2) { std::cout << usage << std::endl; return -10; }

	CommandLineArgs::parseMainArgs(ac, av);
	RepetitionsLayersExtractionArgs args;

	//Arguments
	const std::string		rootPath = args.path;

	const float th1 = args.threshold1;
	const float th2 = args.threshold2;

	const int windowSize = 0;
	 //rootPath.at(rootPath.length() - 1) == '2' || rootPath.at(rootPath.length() - 2) == '2';
	const bool recombination = args.recombination;
	const bool secondPass = (args.secondPass) || recombination;

	const std::string passDir = secondPass ? "pass2" : "pass1";
	const std::string outputPath = rootPath + "/layers/" + passDir + "/";
	const std::string outputPathReproj(rootPath + "/reprojections/" + passDir + "/");
	sibr::makeDirectory(rootPath + "/reprojections/");
	sibr::makeDirectory(rootPath + "/layers/");
	sibr::makeDirectory(outputPath);
	sibr::makeDirectory(outputPathReproj);
	
	

	// Load images/windows associations.
	const std::string listingPath = rootPath + "/initial_cameras.txt";
	unsigned int instanceCount, maxView;
	std::vector<std::pair<unsigned int, unsigned int>> windowsIdsAll = CropCameraUtilities::loadWindowsCorrespondences(listingPath, instanceCount, maxView);
	++instanceCount;
	const std::string osfmPath = rootPath + "/sfm/reconstruction_sequential/sfm_data.json";
	const std::map<unsigned int, unsigned int> camerasShift = CropCameraUtilities::checkCorrespondances(osfmPath);
	
	std::vector<std::pair<unsigned int, unsigned int>> windowsIds;
	for (size_t k = 0; k < windowsIdsAll.size(); ++k) {
		if (camerasShift.count((unsigned int)k) > 0) {
			windowsIds.push_back(windowsIdsAll[k]);
		}
	}

	const std::string scenePath = rootPath + "/IBR_data_OPENMVG-CMPMVS/";
	

	if(recombination) {
		
		const bool addReflection = args.useRef;
		generateInstanceViews(rootPath, scenePath, windowsIds, instanceCount, addReflection);
		return EXIT_SUCCESS;
	}

	// Masks generation.
	if (args.reflectionMasks) {

		const float sigmaScale = args.sigma;

		const float ratio = args.ratio;
		generateReflectionmask(rootPath, scenePath, sigmaScale, ratio);

		return EXIT_SUCCESS;
	}

	
	if(args.testReprojectionSimple) {
		// For each instance, pick the closest view of this instance and reproject it in the other. fill holes with other instance image for this view.
		auto inputCameras = sibr::InputCamera::load(scenePath);
		const auto bgPass = rootPath + "/layers/pass1/";
		const auto finalPath1 = rootPath + "/layers/pass4-poisson/";
		const auto finalPath2 = rootPath + "/layers/pass4/";
		const auto finalPathMask = rootPath + "/layers/pass4-masks/";
		sibr::makeDirectory(finalPath1);
		sibr::makeDirectory(finalPath2);

		// Load initial scene w/ refined proxy.
		auto scene = std::make_shared<ReprojectionScene>(scenePath);

		// Setup raycaster.
		auto raycaster = std::make_shared<sibr::Raycaster>();
		if (!raycaster->init()) {
			SIBR_ERR << " failed raycaster init in constructor " << std::endl;
			return EXIT_SUCCESS;
		}
		sibr::Mesh::Ptr doubleMesh = scene->proxy().clone();
		doubleMesh->merge(scene->proxy().invertedFacesMesh());
		raycaster->addMesh(*doubleMesh);

		// for each camera
		#pragma omp parallel for
		for (int cid = 0; cid < (int)windowsIds.size(); ++cid) {
			const auto & destCam = *inputCameras[cid];
			// load base of camera cid
			sibr::ImageRGB baseImg;
			baseImg.load(bgPass + sibr::imageIdToString(cid) + ".jpg");

			sibr::ImageRGBA baseXImg, baseYImg;
			baseXImg.load(bgPass + sibr::imageIdToString(cid) + "-gradX.png");
			baseYImg.load(bgPass + sibr::imageIdToString(cid) + "-gradY.png");
			const sibr::ImageRGB32F baseX = sibr::convertRGBAtoRGB32F(baseXImg);
			const sibr::ImageRGB32F baseY = sibr::convertRGBAtoRGB32F(baseYImg);	

			
			
			// for each instance
			for (size_t iid = 0; iid < instanceCount; ++iid) {
				const auto instanceFinalPath1 = finalPath1 + "/instance-" + std::to_string(iid) + "/";
				sibr::makeDirectory(instanceFinalPath1);
				const auto instanceFinalPathMask = finalPathMask + "/instance-" + std::to_string(iid) + "/";
				sibr::makeDirectory(instanceFinalPathMask);
				const auto instanceFinalPath2 = finalPath2 + "/instance-" + std::to_string(iid) + "/";
				sibr::makeDirectory(instanceFinalPath2);

				// Find camera of instance iid that is the closest of cid
				unsigned int bestId = cid;
				float bestDistance = 0.0f;
				bool first = true;
				for (size_t ncid = 0; ncid < windowsIds.size(); ++ncid) {

					if (windowsIds[ncid].first != iid) {
						continue;
					}
					const auto & ncam = *inputCameras[ncid];
					// Compute distance.
					const float distance = (ncam.position() - destCam.position()).norm();
					if (distance < bestDistance || first) {
						first = false;
						bestDistance = distance;
						bestId = (unsigned int)ncid;
					}
				}

				const auto & sourceCam = *inputCameras[bestId];

				sibr::ImageRGBA layerXImg, layerYImg;
				layerXImg.load(bgPass + sibr::imageIdToString(bestId) + "-gradX.png");
				layerYImg.load(bgPass + sibr::imageIdToString(bestId) + "-gradY.png");
				const sibr::ImageRGB32F layerX = sibr::convertRGBAtoRGB32F(layerXImg);
				const sibr::ImageRGB32F layerY = sibr::convertRGBAtoRGB32F(layerYImg);
				sibr::ImageRGB32F destX = baseX.clone();
				sibr::ImageRGB32F destY = baseY.clone();

				// load corresponding diffuse image.
				sibr::ImageRGB layerImg;
				layerImg.load(bgPass + sibr::imageIdToString(bestId) + ".jpg");			

				// reproject layerImg into cid
				sibr::ImageRGB destImg = baseImg.clone();
				sibr::ImageL8 touchedMask(destImg.w(), destImg.h(), 0);
				sibr::Vector3f dx, dy, upLeftOffset;
				sibr::CameraRaycaster::computePixelDerivatives(destCam, dx, dy, upLeftOffset);
				if (cid != bestId) {
					for (uint py = 0; py < destCam.h(); ++py) {
						for (uint px = 0; px < destCam.w(); ++px) {
			
							const sibr::Vector3f worldPos = ((float)px + 0.5f)*dx + ((float)py + 0.5f)*dy + upLeftOffset;
							sibr::Vector3f dir = worldPos - destCam.position();
							sibr::RayHit hit = raycaster->intersect(sibr::Ray(destCam.position(), dir));
							if (hit.hitSomething()) {
								// Get the intersection point in world space.
								sibr::Vector3f intersection(destCam.position() + hit.dist()*dir.normalized());
								// Check visibility from input camera.
								const sibr::Vector3f dir1 = sourceCam.position() - intersection;
								sibr::RayHit hitViz = raycaster->intersect(sibr::Ray(intersection, dir1), 0.01f);
								// If we hit the mesh again, the intersection is occluded for the input camera.
								if (!hitViz.hitSomething() && sourceCam.frustumTest(intersection)) {
									const sibr::Vector3f projInter = sourceCam.projectScreen(intersection);

									destX(px, py) = layerX.bilinear(projInter.xy());
									destY(px, py) = layerY.bilinear(projInter.xy());

									destImg(px, py) = layerImg.bilinear(projInter.xy());
									touchedMask(px, py).x() = 255;
								}
							}

						}
					}
				}
				const std::string baseFilename = sibr::imageIdToString(cid) + ".jpg";
				destImg.save(instanceFinalPath1 + "/" + baseFilename, false);
				touchedMask.save(instanceFinalPathMask + "/" + baseFilename, false);
				cv::Mat finalImg;
				baseImg.toOpenCVBGR().convertTo(finalImg, CV_32FC3, 1.0f / 255.0f);

				const float borderSize = 5;
				cv::Mat borderMask(finalImg.rows, finalImg.cols, CV_32FC1, 0.0f);
				for (int y = 0; y < borderMask.rows; ++y) {
					for (int x = 0; x < borderMask.cols; ++x) {
						if (y < borderSize || x < borderSize || y > borderMask.rows - borderSize || x > borderMask.cols - borderSize) {
							borderMask.at<float>(y, x) = 1.0f;
						} 
					}
				}

				PoissonReconstruction recons(destX.toOpenCV(), destY.toOpenCV(), borderMask, finalImg);
				recons.solve();
				cv::Mat finalRGBImg;
				recons.result().convertTo(finalRGBImg, CV_8UC3, 255.0f);
				sibr::ImageRGB destImgXY;
				destImgXY.fromOpenCVBGR(finalRGBImg);
				destImgXY.save(instanceFinalPath2 + "/" + baseFilename, false);
				


			}
		}


		return EXIT_SUCCESS;
	}

	// Load scene and setup raycaster.
	ViewReprojection reprojector(scenePath, secondPass ? rootPath + "/layers/pass1/": "");

	// For each camera of the scene.
	#pragma omp parallel for
	for (int cid = 0; cid < reprojector.scene()->inputCameras().size(); ++cid) {
		// Get window id of the current camera.
		int wid = windowsIds[cid].first;
		
		// Get cameras that have the same id.
		std::vector<int> cids;
		for (unsigned int k = 0; k < windowsIds.size(); ++k) {
			if (secondPass || windowsIds[k].first == wid) {
				cids.push_back(k);
			}
		}
		
		// Reproject all corresponding images from the current camera position and save them.
		reprojector.reproject(cid, cids, outputPathReproj + "camera-" + std::to_string(cid) + "/");
	}

	#pragma omp barrier

	#pragma omp parallel for // there seems to be an issue with the parallelism, random black spots in some outputs.
	for (int cid = 0; cid < windowsIds.size(); ++cid) {
		const std::string sourcePath = rootPath + "/reprojections/" + passDir + "/camera-" + std::to_string(cid) + "/";
		const std::string destinationPath = outputPath + "/deltas/";

		LayersExtraction extractor(sourcePath, cid);
		extractor.computeGradients();
		extractor.extraction(!secondPass, secondPass);

		extractor.exportResults(cid, destinationPath, secondPass, th1);
		
	}
	
	return EXIT_SUCCESS;
}
