/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#include "DepthIterativeOptim.hpp"

#include <iostream>


#include <core/raycaster/CameraRaycaster.hpp>


DepthIterativeOptim::DepthIterativeOptim(const cv::Mat initialDepthMap, DepthMapEnergy * energy, const double minZ, const double maxZ)
{
	_energy = energy;
	_w = initialDepthMap.rows;
	_h = initialDepthMap.cols;
	_minZ = minZ;
	_maxZ = maxZ;
	
	
	_depthMap = new DepthMapEnergy::DepthData*[_w];
	for (int x = 0; x < _w; ++x) {
		_depthMap[x] = new DepthMapEnergy::DepthData[_h];
		for (int y = 0; y < _h; ++y) {
			const cv::Vec3d position = initialDepthMap.at<cv::Vec3d>(x, y);
			_depthMap[x][y] = Eigen::Vector3d(position[0], position[1], position[2]);
		}
	}
	_energy->registerDepthMap(_depthMap);

	_costMap = cv::Mat(_w, _h, CV_64FC1, std::numeric_limits<double>::max());
	for (int x = 0; x < _w; ++x) {
		for (int y = 0; y < _h; ++y) {
			_costMap.at<double>(x, y) = _energy->getCost(x, y, 0.0);
		}
	}	
}

DepthIterativeOptim::~DepthIterativeOptim(void)
{
	for (int x = 0; x < _w; ++x) {
		delete[] _depthMap[x];
		_depthMap[x] = NULL;
	}
	_depthMap = NULL;
}



void DepthIterativeOptim::solve(const int iteration_count, const int levels)
{
	const double deltaZ = (_maxZ - _minZ) / levels;
	for (size_t it = 0; it < iteration_count; ++it) {
		std::cout << "[Optim] Iteration " << (it+1) << "/" << iteration_count << "." << std::endl;
		
		
		for (int y = 0; y < _h; ++y) {
			for (int x = 0; x < _w; ++x) {
				// Test depth from all neighbours.
				const double currentCost = _costMap.at<double>(x, y);
				for (int dy = -1; dy <= 1; ++dy) {
					for (int dx = -1; dx <= 1; ++dx) {
						// Skip center.
						if(dx == 0 && dy == 0) {
							continue;
						}
						const int nx = x + dx;
						const int ny = y + dy;
						if (nx < 0 || ny < 0 || nx >= _w || ny >= _h) {
							continue;
						}
						// Test neighbours depths.
						const double newDepth = _depthMap[nx][ny][2];
						compareCosts(x, y, newDepth);
					}
				}
				// Test depths in a given interval.
				for (double nd = _minZ; nd <= _maxZ; nd += deltaZ) {
					compareCosts(x, y, nd);
				}
				
			}

		}

	}
}


void DepthIterativeOptim::compareCosts(const int x0, const int y0, const double newDepth)
{
	
	const double newCost = _energy->getCost(x0, y0, newDepth - _depthMap[x0][y0][2]);
	
	if (newCost < _costMap.at<double>(x0, y0)) {
		_depthMap[x0][y0][2] = newDepth;
		_costMap.at<double>(x0, y0) = newCost;
	}
 }




cv::Mat DepthIterativeOptim::getDepthMap() {
	cv::Mat finalDepthMap(_w, _h, CV_64FC3);
	for (int py = 0; py < _h; ++py) {
		for (int px = 0; px < _w; ++px) {
			const sibr::Vector3d pos = _depthMap[px][py];
			finalDepthMap.at<cv::Vec3d>(px, py) = cv::Vec3d(pos[0], pos[1], pos[2]);
		}
	}
	return finalDepthMap;
}
