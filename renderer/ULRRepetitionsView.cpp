/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#include "ULRRepetitionsView.hpp"
#include "Config.hpp"

#include <core/assets/Resources.hpp>
#include <core/system/Vector.hpp>
#include <core/graphics/Texture.hpp>
#include <core/graphics/GUI.hpp>
#include <map>


namespace sibr
{



	void initializeDepthRenderTargets(const std::vector<InputCamera::Ptr> & cams, const Mesh & mesh, bool facecull, std::vector<RenderTargetLum32F::Ptr> & rts, uint w, uint h)
	{
		

		rts.resize(cams.size());
		for(int i = 0; i < cams.size(); ++i) {
			rts[i] = std::make_shared< RenderTargetLum32F>(w, h);
		}

		GLShader depthShader;
		depthShader.init("DepthOnly",
			loadFile(Resources::Instance()->getResourceFilePathName("depthonly.vp")),
			loadFile(Resources::Instance()->getResourceFilePathName("depthonly.fp")));

		GLParameter proj;
		proj.init(depthShader, "proj");

		
		
		for (uint i = 0; i < cams.size(); i++) {
			
			rts[i]->bind();
			glViewport(0, 0, w, h);
			
			glEnable(GL_DEPTH_TEST);
			glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);
			glDepthMask(GL_TRUE);
			depthShader.begin();
			proj.set(cams[i]->viewproj());
			mesh.render(true, facecull);
			depthShader.end();

			rts[i]->unbind();
		}
	}


	ULRRepetitionsView::~ULRRepetitionsView()
	{
	}

	ULRRepetitionsView::ULRRepetitionsView(const BasicIBRScene::Ptr& ibrScene, uint w, uint h) :
		ViewBase(w, h)
	{
		_mode = APPROX;
		_allImages = true;
		_showinfos = true;
		_usePoisson = false;

		_numDistUlr = 100;
		std::cerr << "[ULR] setting number of images to blend " << _numDistUlr << "." << std::endl;

		const std::string basePath = ibrScene->data()->basePathName();

		auto cloneSmooth = ibrScene->proxies()->proxy().clone();
		cloneSmooth->generateSmoothNormals(4);
		ibrScene->proxies()->replaceProxy(cloneSmooth);

		_meshs.push_back(std::make_shared<Mesh>(true));
		_meshs.back()->load(basePath + "/pmvs/models/pmvs_recon.ply");
		_meshs.push_back(std::make_shared<Mesh>(true));
		_meshs.back()->load(basePath + "/plane.ply");

		_meshs.push_back(std::make_shared<Mesh>(true));
		const std::string rcMeshpath = basePath + "/proxy_rc.ply";
		if (fileExists(rcMeshpath))
		{
			_meshs.back()->load(rcMeshpath);
		}

		_meshs.push_back(std::make_shared<Mesh>(true));
		const std::string noisyMeshpath = basePath + "/proxy_seg.ply";
		if (fileExists(noisyMeshpath))
		{
			_meshs.back()->load(noisyMeshpath);
		}

		const std::string masksDirectory = basePath + "/masks_auto/";
		bool masksUse = directoryExists(masksDirectory);

		// Create TextureArray with input images.
		std::vector<ImageRGBA> scaledMaskedImages(ibrScene->images()->inputImages().size());
		for (size_t i = 0; i < ibrScene->images()->inputImages().size(); ++i)
		{
			if (!ibrScene->cameras()->inputCameras()[i]->isActive())
			{
				scaledMaskedImages[i] = ImageRGBA(w, h, ImageRGBA::Pixel(0, 0, 0, 0));
				continue;
			}
			const auto img = ibrScene->images()->inputImages()[i];
			ImageL8 mask(img->w(), img->h(), 0);
			if (masksUse)
			{
				mask.load(masksDirectory + imageIdToString((int)i) + "_mask.png", false);
			}

			cv::Mat imgMat = img->toOpenCV();
			cv::Mat maskMat = mask.toOpenCV();

			cv::Mat imgChannels[3];
			cv::split(imgMat, imgChannels);

			cv::Mat imageMaskedMat;
			std::vector<cv::Mat> channels = {imgChannels[0], imgChannels[1], imgChannels[2], maskMat};
			cv::merge(channels, imageMaskedMat);

			ImageRGBA imageMasked;
			imageMasked.fromOpenCV(imageMaskedMat);
			scaledMaskedImages[i] = imageMasked.resized(w, h, cv::InterpolationFlags::INTER_CUBIC);
			scaledMaskedImages[i].flipH();
		}
		_inputImages = std::make_shared<Texture2DArrayRGBA>();
		_inputImages->createFromImages(scaledMaskedImages, SIBR_GPU_LINEAR_SAMPLING);

		_inputDepths.resize(4);
		std::vector<RenderTargetLum32F::Ptr> depths;

		// Create TextureArray with depths from proxy.
		initializeDepthRenderTargets(ibrScene->cameras()->inputCameras(), ibrScene->proxies()->proxy(), false, depths, w, h);
		_inputDepths[0] = std::make_shared<Texture2DArrayLum32F>();
		_inputDepths[0]->createFromRTs(depths);
		
		// Create TextureArray with depths from plane.
		initializeDepthRenderTargets(ibrScene->cameras()->inputCameras(), *_meshs[1], false, depths, w, h);
		_inputDepths[1] = std::make_shared<Texture2DArrayLum32F>();
		_inputDepths[1]->createFromRTs(depths);
		depths.clear();

		// Create TextureArray with depths from RC mesh.
		_inputDepths[2] = std::make_shared<Texture2DArrayLum32F>();
		if (!_meshs[2]->vertices().empty())
		{
			initializeDepthRenderTargets(ibrScene->cameras()->inputCameras(), *_meshs[2], false, depths, w, h);
			_inputDepths[2]->createFromRTs(depths);
		}
		depths.clear();

		// Create TextureArray with depths from noisy full scene mesh.
		_inputDepths[3] = std::make_shared<Texture2DArrayLum32F>();
		if (!_meshs[3]->vertices().empty())
		{
			initializeDepthRenderTargets(ibrScene->cameras()->inputCameras(), *_meshs[3], false, depths, w, h);
			_inputDepths[3]->createFromRTs(depths);
		}
		depths.clear();

		_ulr.reset(new ULRRepetitionsRenderer(ibrScene, _numDistUlr));
		_poisson.reset(new PoissonRenderer(w, h));

		_ulrDest = std::make_shared<RenderTargetRGBA>(w, h);
		_poissonDest = std::make_shared<RenderTargetRGBA>(w, h);

		// We need to determine which images are full size input ones (listed at the beginning, constant sizes.
		size_t i = 0;
		for (i = 1; i < ibrScene->cameras()->inputCameras().size(); ++i)
		{
			if (std::abs(int(ibrScene->cameras()->inputCameras()[i]->w()) - int(ibrScene->cameras()->inputCameras()[i - 1]->w())) > 200)
			{
				break;
			}
		}
		_fullCamCount = i;
		CHECK_GL_ERROR;

		_doReflections = true;

		const std::string envmapPath = basePath + "/envmap.jpg";
		const std::string planeDataPath = basePath + "/plane_data.txt";
		_canDoReflections = fileExists(envmapPath) && fileExists(planeDataPath);

		if (_canDoReflections)
		{
			_reflections.reset(
				new RepetitionsReflectionRenderer(w, h));

			ImageRGB env;
			env.load(envmapPath, false);
			_envmap.reset(new Texture2DRGB(env, SIBR_GPU_LINEAR_SAMPLING));

			std::ifstream planeData(planeDataPath);

			float x, y, z;
			planeData >> x >> y >> z;
			Vector3f p0(x, y, z);
			planeData >> x >> y >> z;
			Vector3f p1(x, y, z);
			planeData >> x >> y >> z;
			Vector3f p2(x, y, z);
			// Plane normal and point.
			Vector3f pN = (p1 - p0).normalized().cross((p2 - p0).normalized()).normalized();
			Vector3f pU(0.0f, 0.0f, 0.0f);
			// use only full cam to compute up vector of the scene.
			for (int idi = 0; idi < _fullCamCount; ++idi)
			{
				pU += ibrScene->cameras()->inputCameras()[i]->up();
			}
			pU /= (float)_fullCamCount;
			// project the average up onto the plane.
			pU = pU - pU.dot(pN) * pN;
			pU = pU.normalized();
			_planeNormal = pN.normalized();

			const Vector3f refU(0.0f, 0.0f, 1.0f);
			float factor = 1.0f / (1.0f + pU.dot(refU));
			const Vector3f v = refU.cross(pU);
			Eigen::Matrix3f Vcross;
			Vcross << 0.0f, -v[2], v[1], v[2], 0.0f, -v[0], -v[1], v[0], 0.0f;
			Eigen::Matrix3f rUp = Eigen::Matrix3f::Identity() + Vcross + Vcross * Vcross * factor;
			_envmapMat = Matrix4f::Identity();
			_envmapMat.block<3, 3>(0, 0) = rUp.inverse();

			glEnable(GL_BLEND);
			glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
			glDisable(GL_BLEND);
		}
		_scene = ibrScene;
	}

	void ULRRepetitionsView::onRenderIBR(IRenderTarget& dst, const Camera& eye)
	{
		// Select (subset of) input images for ULR
		std::vector<uint> imgs_ulr = chosen_cameras(eye, !_allImages);
		Mesh::Ptr meh = nullptr;

		_ulr->process(imgs_ulr, (int)(_fullCamCount - 1), eye, *_scene, _meshs[_mode], _inputImages,
		              _inputDepths[_mode], *_ulrDest);

		// Poisson blending.
		// TODO SR: might want to be able to toggle it.
		if (_usePoisson)
		{
			_poisson->process(_ulrDest, _poissonDest);
			blit(*_poissonDest, dst);
		}
		else
		{
			blit(*_ulrDest, dst);
		}


		// need to render reflections using the mask.
		// over the existing rendertargets.
		// no occlusion or anything to handle.
		// either use the mesh normal by re-rendering it w/ early discard
		// or just use the plane normal.
		if (_canDoReflections && _doReflections)
		{
			_reflections->process(_ulrDest->texture(), eye, _scene->proxies()->proxy(), _envmap->handle(), _planeNormal,
			                      _envmapMat, dst);
		}


		// Display debug infos.
		if (_showinfos)
		{
			
			if (ImGui::Begin("Infos", nullptr))
			{
				ImGui::Text(
					"Comparisons:\nF5: w/ plane\nF6: w/ RC\nF7: w/ noisy\nF8: input/all images\nF9: mesh/rc\nF10: reflections\nF11: Poisson");
			}
			ImGui::End();
			if (ImGui::Begin("Setup", nullptr))
			{
				const std::string textMesh = "Mesh: " + std::string(
					_mode == APPROX ? "OURS" : (_mode == PLANE ? "PLANE" : (_mode == SEGFULL ? "FULLSEG" : "RC")));
				ImGui::Text(textMesh.c_str());
				const std::string imgMesh = "Images: " + std::string(_allImages ? "ALL" : "INPUT");
				ImGui::Text(imgMesh.c_str());
				const std::string reflec = "Reflections: " + std::string(_doReflections ? "ON" : "OFF");
				ImGui::Text(reflec.c_str());
				if (_canDoReflections)
				{
					const std::string fresn = "Intensity: " + std::to_string(_reflections->getFresnel());
					ImGui::Text(fresn.c_str());
				}
			}
			ImGui::End();
		}
	}

	void ULRRepetitionsView::onUpdate(Input& input)
	{
		if (input.key().isReleased(Key::F5))
		{
			_mode = _mode != PLANE ? PLANE : APPROX;
			_allImages = _mode == APPROX;
		}
		else if (input.key().isReleased(Key::F6))
		{
			_mode = _mode != RC ? RC : APPROX;
			_allImages = _mode == APPROX;
		}
		else if (input.key().isReleased(Key::F7))
		{
			_mode = _mode != SEGFULL ? SEGFULL : APPROX;
			_allImages = _mode == APPROX;
		}
		else if (input.key().isReleased(Key::F9))
		{
			_mode = _mode == APPROX ? RC : APPROX;
		}
		else if (input.key().isReleased(Key::F8))
		{
			_allImages = !_allImages;
		}
		else if (input.key().isReleased(Key::F10))
		{
			_doReflections = !_doReflections;
		}
		else if (input.key().isReleased(Key::F11))
		{
			_usePoisson = !_usePoisson;
		}
		else if (input.key().isReleased(Key::F))
		{
			_showinfos = !_showinfos;
		}
		else if (_canDoReflections)
		{
			if (input.key().isReleased(Key::F2))
			{
				_reflections->updateFresnel(0.05f);
			}
			else if (input.key().isReleased(Key::F1))
			{
				_reflections->updateFresnel(-0.05f);
			}
		}
	}

	std::vector<uint> ULRRepetitionsView::chosen_cameras(const Camera& eye, const bool onlyFull)
	{
		const auto& cams = _scene->cameras()->inputCameras();
		std::vector<uint> out;
		const int allowedBound = onlyFull ? (int)_fullCamCount : (int)cams.size();
		// sort angle / dist combined
		struct camAng
		{
			camAng()
			{
			}

			camAng(float a, float d, int i) : ang(a), dist(d), id(i)
			{
			}

			float ang, dist;
			int id;
			static bool compare(const camAng& a, const camAng& b) { return a.ang / a.dist > b.ang / b.dist; }
		};

		int total_size = _numDistUlr;

		std::vector<camAng> allAng;
		for (int id = 0; id < allowedBound; ++id)
		{
			const auto& cam = *cams[id];
			float angle = dot(cam.dir(), eye.dir());
			// reject back facing 
			if (angle > 0.001 && cam.isActive())
			{
				float dist = (cam.position() - eye.position()).norm();
				allAng.push_back(camAng(angle, dist, id));
			}
		}

		std::vector<bool> wasChosen(cams.size(), false);

		std::sort(allAng.begin(), allAng.end(), camAng::compare);
		for (int id = 0; id < std::min((int)allAng.size(), total_size); ++id)
		{
			out.push_back(allAng[id].id);
			wasChosen[allAng[id].id] = true;
		}

		if (!onlyFull)
		{
			for (int id = 0; id < (int)cams.size(); ++id)
			{
				if (!wasChosen[id] && out.size() < total_size && cams[id]->isActive())
				{
					out.push_back(id);
				}
			}
		}

		return out;
	}

} /*namespace sibr*/
