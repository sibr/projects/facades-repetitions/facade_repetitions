/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#include <iostream>

#include "MultiViewPMSEnergy.hpp"

PMSEnergy::PMSEnergy(sibr::BasicIBRScene::Ptr scene, const std::vector<cv::Mat> & masks,
	const double gamma, const double alpha, const int halfSize, 
	const sibr::InputCamera & cam, cv::Mat colorMap) : DepthMapEnergy() {

	_scene = scene;
	_masks = masks;
	_gamma = gamma;
	_frontoCam = cam;
	_alpha = alpha;
	_halfSize = halfSize;

	// Compute colormap gradient.
	cv::Mat src_gray, src_gray32, Sx, Sy;
	cv::cvtColor(colorMap, src_gray, cv::COLOR_BGR2GRAY);
	src_gray.convertTo(src_gray32, CV_32F, 1.0 / 255.0f);
	cv::Scharr(src_gray32, Sx, CV_64F, 1, 0);
	cv::Scharr(src_gray32, Sy, CV_64F, 0, 1);
	cv::magnitude(Sx, Sy, _magnitude);
	cv::phase(Sx, Sy, _orientation, false /*radians*/);

	colorMap.convertTo(_colorMap, CV_64FC3, 1.0 / 255.0);

	// Compute input views gradients.
	const auto &inputImages = scene->images()->inputImages();
	for (size_t i = 0; i < inputImages.size(); ++i) {
		_inputMagnitudes.emplace_back();
		_inputOrientations.emplace_back();
		cv::Mat isrc_gray, isrc_gray32, iSx, iSy;
		cv::Mat transInput, inputColorMap;
		cv::transpose(inputImages[i]->toOpenCV(), inputColorMap);
		cv::cvtColor(inputColorMap, isrc_gray, cv::COLOR_BGR2GRAY);
		isrc_gray.convertTo(isrc_gray32, CV_32F, 1.0 / 255.0f);
		cv::Scharr(isrc_gray32, iSx, CV_64F, 1, 0);
		cv::Scharr(isrc_gray32, iSy, CV_64F, 0, 1);
		cv::magnitude(iSx, iSy, _inputMagnitudes.back());
		cv::phase(iSx, iSy, _inputOrientations.back(), false /*radians*/);
	}
}

double PMSEnergy::getCost(int x, int y, const double shift) {
	double totalEnergy = 0.0;

	// Create Q
	const sibr::Vector3d worldPos = DepthMapEnergy::_depthMap[x][x];
	const sibr::Vector4d worldQ = sibr::Vector4d(worldPos[0], worldPos[1], worldPos[2] + shift, 1.0);
	// Reproject in all cameras.
	const sibr::Vector3f worldPosCenter = (_frontoCam.view().inverse() * worldQ.cast<float>()).eval().xyz();


	for (int yw = std::max(y - _halfSize, 0); yw < std::min(_colorMap.cols, y + _halfSize + 1); ++yw) {
		for (int xw = std::max(x - _halfSize, 0); xw < std::min(_colorMap.rows, x + _halfSize + 1); ++xw) {

			const double colorsNormL1 = cv::norm(_colorMap.at<cv::Vec3d>(x, y) - _colorMap.at<cv::Vec3d>(xw, yw), cv::NormTypes::NORM_L1);
			const double w_pq = std::exp(-colorsNormL1 / _gamma);

			// Create Q
			const sibr::Vector3d pos = DepthMapEnergy::_depthMap[xw][yw];
			const sibr::Vector4d q = sibr::Vector4d(pos[0], pos[1], pos[2] +  shift , 1.0);

			// Reproject in all cameras.
			const sibr::Vector3f worldPos = (_frontoCam.view().inverse() * q.cast<float>()).eval().xyz();

			float reprojColorDiff = 0.0f;
			float count = 0.0;

			const cv::Vec3d frontoColor = _colorMap.at<cv::Vec3d>(xw, yw);
			const double frontoMagnitude = _magnitude.at<double>(xw, yw);
			const double frontoPhase = _orientation.at<double>(xw, yw);

			double coherencyEnergy = 0.0;
			for (size_t id = 0; id < _scene->cameras()->inputCameras().size(); ++id) {
				const sibr::Vector3d reproj = _scene->cameras()->inputCameras()[id]->projectScreen(worldPos).cast<double>();
				const int ix = (int)reproj[0];
				const int iy = (int)reproj[1];

				const sibr::ImageRGB::Ptr imgId = _scene->images()->inputImages()[id];

				if (ix >= 0 && iy >= 0 && ix < (int)imgId->w() && iy < (int)imgId->h()) {

					const sibr::Vector3d color = imgId(ix, iy).cast<double>() / 255.0;
					const cv::Vec3d colorReproj = cv::Vec3d(color[0], color[1], color[2]);
					const double colorDeltaNormL1 = cv::norm(frontoColor - colorReproj, cv::NormTypes::NORM_L1);

					const double gradReproj = _inputMagnitudes[id].at<double>(ix, iy);
					const double gradientDeltaNormL1 = std::abs(frontoMagnitude - gradReproj);

					const double maskingFactor = (double)_masks[id].at<float>(ix, iy);
					const double localEnergy = (1.0 - _alpha)*colorDeltaNormL1 + _alpha*gradientDeltaNormL1;
					
					coherencyEnergy += maskingFactor * localEnergy;
					++count;
				}

			}
			
			reprojColorDiff = (float)coherencyEnergy / count;
			totalEnergy += w_pq * reprojColorDiff;
		}
	}
	return totalEnergy;
}


IterativeEnergy::IterativeEnergy(sibr::BasicIBRScene::Ptr scene, const std::vector<cv::Mat> & images, const std::vector<cv::Mat> & masks,
	const double gamma, const double alpha, const int halfSize,
	const sibr::InputCamera & cam, cv::Mat colorMap, std::shared_ptr<StructureTensor> tensor, const sibr::Vector3f & pNormal) : DepthMapEnergy() {

	_scene = scene;
	_images = images;
	_masks = masks;
	_gamma = gamma;
	_frontoCam = cam;
	_alpha = alpha;
	_halfSize = halfSize;
	_tensor = tensor;
	const float dott = cam.dir().dot(-pNormal.normalized());
	_depthShiftTan = sqrt(1.0f - pow(dott,2.0f)) / dott; 

	// Compute colormap gradient.
	cv::Mat src_gray, src_gray32, Sx, Sy;
	cv::cvtColor(colorMap, src_gray, cv::COLOR_BGR2GRAY);
	src_gray.convertTo(src_gray32, CV_32F, 1.0 / 255.0f);
	cv::Scharr(src_gray32, Sx, CV_64F, 1, 0);
	cv::Scharr(src_gray32, Sy, CV_64F, 0, 1);
	cv::magnitude(Sx, Sy, _magnitude);
	//cv::phase(Sx, Sy, _orientation, false /*radians*/);

	colorMap.convertTo(_colorMap, CV_64FC3, 1.0 / 255.0);

	// Compute input views gradients.
	for (size_t i = 0; i < scene->images()->inputImages().size(); ++i) {
		_inputMagnitudes.emplace_back();
		//_inputOrientations.emplace_back();
		cv::Mat isrc_gray, isrc_gray32, iSx, iSy;
		cv::Mat transInput, inputColorMap;
		cv::transpose(scene->images()->inputImages()[i]->toOpenCV(), inputColorMap);
		cv::cvtColor(inputColorMap, isrc_gray, cv::COLOR_BGR2GRAY);
		isrc_gray.convertTo(isrc_gray32, CV_32F, 1.0 / 255.0f);
		cv::Scharr(isrc_gray32, iSx, CV_64F, 1, 0);
		cv::Scharr(isrc_gray32, iSy, CV_64F, 0, 1);
		cv::magnitude(iSx, iSy, _inputMagnitudes.back());
		//cv::phase(iSx, iSy, _inputOrientations.back(), false /*radians*/);
	}
}


double IterativeEnergy::getCost(int x, int y, const double shift) {
	
	double consistencyTerm = 0.0;
	double smoothnessTerm = 0.0;
	double alignementTerm = 0.0;
	double maskingFactor = 0.0;
	double maskingCount = 0.0;
	// Photo consistency term.
	// -----------------------------
	for (int yw = std::max(y - 1, 0); yw < std::min(_colorMap.cols, y + 1 + 1); ++yw) {
		for (int xw = std::max(x - 1, 0); xw < std::min(_colorMap.rows, x + 1 + 1); ++xw) {

			float reprojColorDiff = 0.0f;
			float count = 0.0;
			const cv::Vec3d frontoColor = _colorMap.at<cv::Vec3d>(xw, yw);
			const double frontoMagnitude = _magnitude.at<double>(xw, yw);
			// Reproject in all cameras.
			const sibr::Vector3d viewPos = DepthMapEnergy::_depthMap[xw][yw];
			const sibr::Vector4d worldQ = sibr::Vector4d(viewPos[0], viewPos[1], viewPos[2] + shift, 1.0);
			const sibr::Vector3f worldPos = (_frontoCam.view().inverse() * worldQ.cast<float>()).eval().xyz();

			for (size_t id = 0; id < _scene->cameras()->inputCameras().size(); ++id) {
				const sibr::Vector3d reproj = _scene->cameras()->inputCameras()[id]->projectScreen(worldPos).cast<double>();
				const int ix = (int)reproj[0];
				const int iy = (int)reproj[1];


				if (ix >= 0 && iy >= 0 && ix < _images[id].rows && iy < _images[id].cols) {

					const cv::Vec3d colorReproj = _images[id].at<cv::Vec3d>(ix,iy);
					const double colorDeltaNormL1 = cv::norm(frontoColor - colorReproj, cv::NormTypes::NORM_L1);

					const double gradReproj = _inputMagnitudes[id].at<double>(ix, iy);
					const double gradientDeltaNormL1 = std::abs(frontoMagnitude - gradReproj);
					const double localEnergy = (1.0 - _alpha)*colorDeltaNormL1 + _alpha*gradientDeltaNormL1;

					const double localMask = _masks[id].at<float>(ix, iy);
					maskingFactor += localMask;
					reprojColorDiff += (float)localEnergy;
					++count;
					
					++maskingCount;
					
				}

			}
			reprojColorDiff /= count;
			consistencyTerm += reprojColorDiff;
		}
	}
	if (maskingCount == 0.0) {
		maskingCount = 1.0;
	}
	maskingFactor /= maskingCount;

	// Smoothness term.
	// -----------------------------
	// need to reproject. const double maskingFactor = (double)_masks[id].at<float>(x, y);
	const sibr::Vector3d viewPos = DepthMapEnergy::_depthMap[x][y];
	const sibr::Vector4d worldQ = sibr::Vector4d(viewPos[0], viewPos[1], viewPos[2] + shift, 1.0);
	const sibr::Vector3f worldPos = (_frontoCam.view().inverse() * worldQ.cast<float>()).eval().xyz();

	

	for (int yw = std::max(y - _halfSize, 0); yw < std::min(_colorMap.cols, y + _halfSize + 1); ++yw) {
		for (int xw = std::max(x - _halfSize, 0); xw < std::min(_colorMap.rows, x + _halfSize + 1); ++xw) {

			const double colorsNormL1 = cv::norm(_colorMap.at<cv::Vec3d>(x, y) - _colorMap.at<cv::Vec3d>(xw, yw), cv::NormTypes::NORM_L1);
			const double w_pq = std::exp(-colorsNormL1*colorsNormL1 / _gamma);
		
			const sibr::Vector3d posN = DepthMapEnergy::_depthMap[xw][yw];
			const sibr::Vector4d qN = sibr::Vector4d(posN[0], posN[1], posN[2], 1.0);
			const sibr::Vector3f worldPosN = (_frontoCam.view().inverse() * qN.cast<float>()).eval().xyz();
			
			const double positionDiff = (double)((worldPosN - worldPos).squaredNorm());
			
			smoothnessTerm += w_pq * positionDiff;
		}
	}

	// Alignement.
	// -----------------------------
	// Get general direction of surface in fronto image through Surface Tensor.
	const Eigen::Vector2d tangent = _tensor->getTangent(x, y).normalized();
	const double magnitude = _tensor->getMagnitude(x, y);
	const float step = 3.0;
	const int halfStepCount = 5;
	double accumAlign = 0.0;
	double denomAlign = 0.0;
	for (int dp = 1; dp <= halfStepCount; ++dp) {
		// Compute position along the edge.
		const float dx = (float)tangent[0] * dp * step;
		const float dy = (float)tangent[1] * dp * step;
		float fpx = x + dx;
		float fpy = y + dy;
		int px = (int)std::round(fpx);
		int py = (int)std::round(fpy);
		if (px < 0 || py < 0 || px >= _colorMap.rows || py >= _colorMap.cols || (px == x && py == y)) {
			continue;
		}
		// Compute position along the edge.
		float fnx = x - dx;
		float fny = y - dy;
		int nx = (int)std::round(fnx);
		int ny = (int)std::round(fny);
		if (nx < 0 || ny < 0 || nx >= _colorMap.rows || ny >= _colorMap.cols || (nx == x && ny == y)) {
			continue;
		}
		//Weight should be strong if they have the same surface direction.
		//const double weight1n = std::abs(tangent.dot(_tensor->getTangent(nx, ny)));
		//const double weight1p = std::abs(tangent.dot(_tensor->getTangent(px, py)));
		const double weight2n = std::exp(-std::abs(_tensor->getMagnitude(nx, ny) - magnitude));
		const double weight2p = std::exp(-std::abs(_tensor->getMagnitude(px, py) - magnitude));
		
		const sibr::Vector3d posN = DepthMapEnergy::_depthMap[nx][ny];
		//const sibr::Vector4d qN = sibr::Vector4d(posN[0], posN[1], posN[2], 1.0);
		//const sibr::Vector3f worldPosN = (_frontoCam.view().inverse() * qN.cast<float>()).eval().xyz();

		const sibr::Vector3d posP = DepthMapEnergy::_depthMap[px][py];
		//const sibr::Vector4d qP = sibr::Vector4d(posP[0], posP[1], posP[2], 1.0);
		//const sibr::Vector3f worldPosP = (_frontoCam.view().inverse() * qP.cast<float>()).eval().xyz();

		//const double positionDiff = (double)((worldPosN - worldPos).squaredNorm());
		//const Eigen::Vector3f vP = worldPosP - worldPos;
		//const Eigen::Vector3f vN = worldPosN - worldPos;
		/*const double energy = 1.0 - std::abs(vP.dot(vN) / (vP.norm() * vN.norm()));
		accumAlign += (weight2n * weight2p) * energy;
		denomAlign += weight2n * weight2p;*/
		// TODO correct alignment.
		//const double magShiftP = sqrt(std::pow(posP[0] - viewPos[0],2.0) + std::pow(posP[1] - viewPos[1], 2.0)) * _depthShiftTan;
		//const double magShiftN = -sqrt(std::pow(posN[0] - viewPos[0], 2.0) + std::pow(posN[1] - viewPos[1], 2.0)) * _depthShiftTan;
		const double norView = std::sqrt(std::pow(viewPos[0], 2.0) + std::pow(viewPos[1], 2.0));
		const double norN = std::sqrt(std::pow(posN[0], 2.0) + std::pow(posN[1], 2.0));
		const double norP = std::sqrt(std::pow(posP[0], 2.0) + std::pow(posP[1], 2.0));
		const double deltaN = (posN[2] - viewPos[2]) / (std::max)(0.001, norN - norView);
		const double deltaP = (posP[2] - viewPos[2]) / (std::max)(0.001, norP - norView);
		//const double energy = weight2n * std::pow(posN[2] - viewPos[2], 2.0) + weight2p * std::pow(posP[2] - viewPos[2], 2.0);
		const double energy = weight2n * std::abs(std::abs(deltaN) - std::abs(_depthShiftTan)) + weight2p * std::abs(std::abs(deltaP) - std::abs(_depthShiftTan));
		accumAlign += energy;
		denomAlign += weight2n + weight2p;
	}
	if (denomAlign == 0.0) {
		denomAlign = 1.0;
	}
	alignementTerm = accumAlign;// / denomAlign;
	const double magnitudeFactor = (std::max)((std::min)(magnitude, 1.0), 0.001);
	// Final
	// -----------------------------
	// average masking factor, 1 means in mask, so stronger smoothing, lower coherency.
//	std::cout << consistencyTerm << "," << smoothnessTerm * 100.0 * (1.0 + maskingFactor)  * (1.0 - magnitudeFactor) << "," << 10.0*magnitudeFactor*alignementTerm << std::endl;
	const double totalEnergy = consistencyTerm + smoothnessTerm * 100.0 * (1.0 + maskingFactor) * (1.0 - magnitudeFactor) + (std::min)(1000.0, 10.0*magnitudeFactor*alignementTerm);
	
	return totalEnergy;
}




PhotoconsistencyEnergy::PhotoconsistencyEnergy(sibr::BasicIBRScene::Ptr scene, const std::vector<cv::Mat> & images,
	const double alpha, const sibr::InputCamera & cam, cv::Mat colorMap) : DepthMapEnergy() {

	_inputCams = scene->cameras()->inputCameras();
	_images = images;
	_frontoCam = cam;
	_alpha = alpha;

	// Compute colormap gradient.
	cv::Mat src_gray, src_gray32, Sx, Sy;
	cv::cvtColor(colorMap, src_gray, cv::COLOR_BGR2GRAY);
	src_gray.convertTo(src_gray32, CV_32F, 1.0 / 255.0f);
	cv::Scharr(src_gray32, Sx, CV_64F, 1, 0);
	cv::Scharr(src_gray32, Sy, CV_64F, 0, 1);
	cv::magnitude(Sx, Sy, _magnitude);

	colorMap.convertTo(_colorMap, CV_64FC3, 1.0 / 255.0);

	// Compute input views gradients.
	for (size_t i = 0; i < scene->images()->inputImages().size(); ++i) {
		_inputMagnitudes.emplace_back();
		cv::Mat isrc_gray, isrc_gray32, iSx, iSy;
		cv::Mat transInput, inputColorMap;
		cv::transpose(scene->images()->inputImages()[i]->toOpenCV(), inputColorMap);
		cv::cvtColor(inputColorMap, isrc_gray, cv::COLOR_BGR2GRAY);
		isrc_gray.convertTo(isrc_gray32, CV_32F, 1.0 / 255.0f);
		cv::Scharr(isrc_gray32, iSx, CV_64F, 1, 0);
		cv::Scharr(isrc_gray32, iSy, CV_64F, 0, 1);
		cv::magnitude(iSx, iSy, _inputMagnitudes.back());
	}
}



/// Compute cost at source mesh depth.
double PhotoconsistencyEnergy::getCostReal(int x, int y, const std::vector<bool> & camIds) {
	return getCost(x, y, DepthMapEnergy::_depthMap[x][y][2], camIds);
}
/// Compute cost at arbitrary depth.
double PhotoconsistencyEnergy::getCostArbitrary(int x, int y, const double depth, const std::vector<bool> & camIds) {
	
	return getCost(x, y, depth, camIds);
}

/// Compute cost at shifted mesh depth.
double PhotoconsistencyEnergy::getCost(int x, int y, const double shift) {
	return getCost(x, y, DepthMapEnergy::_depthMap[x][y][2]+shift, {});
}

double PhotoconsistencyEnergy::getCost(int x, int y, const double refDepth, const std::vector<bool> & camIds) {

	double consistencyTerm = 0.0;
	// Photo consistency term.
	// -----------------------------
	const double shift = refDepth - DepthMapEnergy::_depthMap[x][y][2];
	for (int yw = std::max(y - 1, 0); yw < std::min(_colorMap.cols, y + 1 + 1); ++yw) {
		for (int xw = std::max(x - 1, 0); xw < std::min(_colorMap.rows, x + 1 + 1); ++xw) {

			float reprojColorDiff = 0.0f;
			float count = 0.0;
			const cv::Vec3d frontoColor = _colorMap.at<cv::Vec3d>(xw, yw);
			const double frontoMagnitude = _magnitude.at<double>(xw, yw);
			// Reproject in all cameras.
			const sibr::Vector3d viewPos = DepthMapEnergy::_depthMap[xw][yw];
			const sibr::Vector4d worldQ = sibr::Vector4d(viewPos[0], viewPos[1], refDepth/*viewPos[2]+shift*/, 1.0);
			const sibr::Vector3f worldPos = (_frontoCam.view().inverse() * worldQ.cast<float>()).eval().xyz();

			for (size_t id = 0; id < _inputCams.size(); ++id) {
				if (!camIds.empty() && !camIds[id]) {
					continue;
				}
				
				const sibr::Vector3d reproj = _inputCams[id]->projectScreen(worldPos).cast<double>();
				const int ix = (int)reproj[0];
				const int iy = (int)reproj[1];


				if (ix >= 0 && iy >= 0 && ix < _images[id].rows && iy < _images[id].cols) {

					const cv::Vec3d colorReproj = _images[id].at<cv::Vec3d>(ix, iy);
					const double colorDeltaNormL1 = cv::norm(frontoColor - colorReproj, cv::NormTypes::NORM_L1);

					const double gradReproj = _inputMagnitudes[id].at<double>(ix, iy);
					const double gradientDeltaNormL1 = std::abs(frontoMagnitude - gradReproj);
					const double localEnergy = (1.0 - _alpha)*colorDeltaNormL1 + _alpha*gradientDeltaNormL1;

					reprojColorDiff += (float)localEnergy;
					++count;

				}

			}
			reprojColorDiff /= count;
			consistencyTerm += reprojColorDiff;
		}
	}
	
	
	return consistencyTerm;
}