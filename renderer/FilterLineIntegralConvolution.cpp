/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#include "FilterLineIntegralConvolution.h"

#include <cmath>

#define STOP Eigen::Vector2d(-1,0)
#define SBOTTOM Eigen::Vector2d(1,0)
#define SLEFT Eigen::Vector2d(0,-1)
#define SRIGHT Eigen::Vector2d(0,1)

using namespace cv;

FilterLineIntegralConvolution::FilterLineIntegralConvolution()
{
}


std::vector<std::pair<double,double> > FilterLineIntegralConvolution::findNeighbour(Eigen::Vector2d ** field,int i, int j, int h, int w, int k)
{
    // The list of line neighbourds (p0,p1,p2..,pi) with i = k
	std::vector<std::pair<double,double> > neighbours;

    // The center of the stream line neighbour. The i,j directions in the image are fliped to match the x,y axis.
    double x = j;
    double y = i;

    // fx, fy are the internal pixel deplacement of the stream line. We start in the midle of a pixel (0.5,0.5)
    double fx = 0.5;
    double fy = 0.5;

    // Push the center into the results (y,x)
    neighbours.push_back(std::pair<double,double>(y,x));

    // Add neighbours (positive direction)
    for(int l = 0; l < k; l++)
    {
        // Take the last pixel added (current position)
        std::pair<double,double> pi = neighbours.back();

        // Check if we are inside the image bounds still.
        if((pi.first < h)&&(pi.first > 0.0)&&(pi.second < w)&&(pi.second > 0.0))
        {
            // Look for the vector in field(Pi-1)
            Eigen::Vector2d vpi = field[(int)pi.first][(int)pi.second];

            // Normalized
			Eigen::Vector2d vpiNorm = vpi;
			if (vpiNorm.norm() != 0) {
				vpiNorm.normalize();
			}

            // Vx and Vy are the directions of the next movement.
            // We stil need to decide if we do a step in the x axis or in the y axis.
			double vx = vpiNorm[0];
            double vy = vpiNorm[1];

            // Tx represents the relationship between the current position inside the pixel (fx)
            // and the magnitud of the deplacement in x (vx). The same for ty.
            double tx = 0;
            double ty = 0;

            // Check if at least one of them is != 0. If one of them is cero, doesn't mather
            // f will be infinite, and we will choose to move in the other direction.
            if((vx!=0)||(vy!=0))
            {
                // Compute fx and fy. It must be always a positive factor.
                if(vx>=0)
                    tx = (1-fx)/vx;
                else
                    tx = -fx/vx;
                if(vy>=0)
                    ty = (1-fy)/vy;
                else
                    ty = -fy/vy;

                // The smaller one is the direction we must follow.
                // Big vx means we want to move in x direction.
                // Big vy means we want to move in y direction.
                // Small (or 0) vx means an infinite tx, we want to move in the y direction.
                // Small (or 0) vy means an infinite ty, we want to move in the x direction.

                if(tx<ty)
                {
                    // qDebug() << "x step";
                    // Go to the right
                    if(vx>0)
                    {
                       x+=1;
                       fy+=vy*tx;
                       fx=0;
					}
                    // Go to the left
                    else
                    {
                       x-=1;
                       fy+=vy*tx;
                       fx=1;
                    }
                }
                else
                {
                    // qDebug() << "y step";
                    // Go up
                    if(vy>0)
                    {
                       y+=1;
                       fx+=vx*ty;
                       fy=0;
                    }
                    // Go down
                    else
                    {
                       y-=1;
                       fx+=vx*ty;
                       fy=1;
                    }
                }

                // If we are still inside te image.
                if(((int)y < w)&&((int)y > 0)&&((int)x < h)&&((int)x > 0))
                {
                    if(std::find(neighbours.begin(),neighbours.end(),
								 std::pair<double,double>(y,x)) ==  neighbours.end())
                    {
                        // Add it to the list
                       neighbours.push_back(std::pair<double,double>(y,x));
                    }
                    else
                    {
                        // Stop the cicle (we are in a loop)
                        l = k;
                    }
                }
                else
                {
                    // Stop the cicle (out of image)
                    l = k;
                }
            }
            else
            {
                // Stop the cicle (vx and vy = 0 in that point. NO flow.)
                l = k;
            }
        }
    }

    // Reset variables for moving in th other direction
    fx = 0.5;
    fy = 0.5;
    x = j;
    y = i;

    // Add neighbours (negative direction)
    for(int l = 0; l < k; l++)
    {
        // Take the last pixel added, now on the front of the list (current position)
        std::pair<double,double> pi = neighbours.front();

        // Check if we are inside the image bounds still.
        if((pi.first < h)&&(pi.first > 0.0)&&(pi.second < w)&&(pi.second > 0.0))
        {
            // Look for the vector in field(Pi-1)
            Eigen::Vector2d vpi = field[(int)pi.first][(int)pi.second];

            // Normalized
			Eigen::Vector2d vpiNorm = vpi;
			if(vpiNorm.norm() != 0)
				vpiNorm.normalize();

            // Vx and Vy are the directions of the next movement.
            // We stil need to decide if we do a step in the x axis or in the y axis.
            double vx =vpiNorm[0];
            double vy =vpiNorm[1];

            // Tx represents the relationship between the current position inside the pixel (fx)
            // and the magnitud of the deplacement in x (vx). The same for ty.
            double tx = 0;
            double ty = 0;

            // Check if at least one of them is != 0. If one of them is cero, doesn't mather
            // f will be infinite, and we will choose to move in the other direction.
            if((vx!=0)||(vy!=0))
            {
                // Compute fx and fy. It must be always a positive factor.
                if(vx>=0)
                    tx = (1-fx)/vx;
                else
                    tx = -fx/vx;
                if(vy>=0)
                    ty = (1-fy)/vy;
                else
                    ty = -fy/vy;

                // The smaller one is the direction we must follow.
                // Big vx means we want to move in x direction.
                // Big vy means we want to move in y direction.
                // Small (or 0) vx means an infinite tx, we want to move in the y direction.
                // Small (or 0) vy means an infinite ty, we want to move in the x direction.

                if(tx<ty)
                {
                    //qDebug() << "x step";
                     // Go to the left
                    if(vx>0)
                    {
                       x-=1;
                       fy+=vy*tx;
                       fx=0;
                    }
                    else
                    {
                       // Go to the right
                       x+=1;
                       fy+=vy*tx;
                       fx=1;
                    }
                }
                else
                {
                    //qDebug() << "y step";
                    // Go down
                    if(vy>0)
                    {
                       y-=1;
                       fx+=vx*ty;
                       fy=0;
                    }
                    // Go up
                    else
                    {
                       y+=1;
                       fx+=vx*ty;
                       fy=1;
                    }
                }

                // If we are still inside te image.
                if(((int)y < h)&&((int)y > 0)&&((int)x < w)&&((int)x > 0))
                {
					if(std::find(neighbours.begin(),neighbours.end(),
								 std::pair<double,double>(y,x)) ==  neighbours.end())
                    {
                        // Add it to the list
                        neighbours.push_back(std::pair<double,double>(y,x));
                    }
                    else
                    {
                        // Stop the cicle (we are in a loop)
                        l = k;
                    }
                }
                else
                {
                     // Stop the cicle (out of image)
                    l = k;
                }
            }
            else
            {
                // Stop the cicle (vx and vy = 0 in that point. NO flow.)
                l = k;
            }
        }
    }

    return neighbours;
}


int FilterLineIntegralConvolution::computeConvolution(Mat noisyImage, std::vector<std::pair<double,double> > kernel)
{

    // Initial color
    double color = 0;

    // Averaging using the kernel
    for(int i = 0; i < kernel.size(); i++)
    {
        // Get coords as ints
        int indexX = (int)kernel.at(i).first;
        int indexY = (int)kernel.at(i).second;

        // Addition
        color += noisyImage.at<uchar>(indexX,indexY);

    }

    double finalColor = color / kernel.size();

    // return average
    return (int)finalColor;

}

int FilterLineIntegralConvolution::computeConvolutionGrey(Mat image, std::vector<std::pair<double,double> > kernel)
{

    // Initial color
    double color = 0;

    // Averaging using the kernel
    for(int i = 0; i < kernel.size(); i++)
    {
        // Get coords as ints
        int indexX = (int)kernel.at(i).first;
        int indexY = (int)kernel.at(i).second;

        // Addition
        color += image.at<Vec3b>(indexX,indexY)[0];

    }

    double finalColor = color / kernel.size();

    // return average
    return (int)finalColor;

}


Mat FilterLineIntegralConvolution::filter(Eigen::Vector2d ** field, int h, int w, int k)
{
    // The result image
    Mat lic(h,w,CV_8UC1);

    Mat colors(h,w,CV_8UC3);

    // The noisy image
    Mat noisyImage(h,w,CV_8UC1);

    // Create the noise Image
    for(int i = 0; i < h; i++)
    {
        for(int j = 0; j < w; j++)
        {
            noisyImage.at<uchar>(i,j) =  rand() % 256;
        }
    }

    // LIC to the noise
    // For each point in the field
    for(int i = 0; i < h; i++)
    {
        for(int j = 0; j < w; j++)
        {
            // Find its line neighbours
			std::vector<std::pair<double,double> > neighbours = findNeighbour(field,i,j,h,w,k);

            if(neighbours.size() > 1)
            {
                lic.at<uchar>(i,j) = computeConvolution(noisyImage,neighbours);

                // color background

                Eigen::Vector2d v  = field[i][j];
				const int magnitude = int(v.norm() * 255);

				double angle  = FilterLineIntegralConvolution::angle(v);
                angle = fmod(angle,M_PI);
				// if(i<350 && i<380 && j>210 && j<240)
				// 	std::cout<<(angle*180./M_PI)<<"\t";
                colors.at<Vec3b>(i,j)[0] = 255-(int)(angle*180./M_PI);
                colors.at<Vec3b>(i,j)[1] = magnitude;
                colors.at<Vec3b>(i,j)[2] = 255;

            }
            else
            {
                lic.at<uchar>(i,j) = 255;

                // color background

                colors.at<Vec3b>(i,j)[0] = 0;
                colors.at<Vec3b>(i,j)[1] = 0;
                colors.at<Vec3b>(i,j)[2] = 255;
            }
        }
    }

    Mat colorsToSave(h,w,CV_8UC3);
    cv::cvtColor(colors,colorsToSave, cv::COLOR_HSV2BGR);

    //imwrite("output/licTangentsColors.png",colorsToSave);

    // return
    return lic;
}



double FilterLineIntegralConvolution::angle(Eigen::Vector2d const v)
{
	// The angle
	const double vnorm = v.norm();
	if (v[1]>0.0)
		return acos((double)(v[0] / vnorm));
	else
		return (2.0 * M_PI) - acos((double)(v[0] / vnorm));
}

Mat FilterLineIntegralConvolution::filterWithImage(Eigen::Vector2d ** field, int h, int w, int k, Mat image)
{
    // The result image
    Mat lic(h,w,CV_8UC3);

    Mat colors(h,w,CV_8UC3);

    // LIC to the noise
    // For each point in the field
    for(int i = 0; i < h; i++)
    {
        for(int j = 0; j < w; j++)
        {
            // Find its line neighbours
			std::vector<std::pair<double,double> > neighbours = findNeighbour(field,i,j,h,w,k);

            if(neighbours.size() > 1)
            {
                const double greyColor = computeConvolutionGrey(image,neighbours);
                lic.at<Vec3b>(i,j)[0] = (uchar)greyColor;
                lic.at<Vec3b>(i,j)[1] = (uchar)greyColor;
                lic.at<Vec3b>(i,j)[2] = (uchar)greyColor;

                // color background

                Eigen::Vector2d v  = field[i][j];
				int magnitude = int(v.norm() * 255);

				double angle  = FilterLineIntegralConvolution::angle(v);
                angle = fmod(angle,M_PI);

                colors.at<Vec3b>(i,j)[0] = 255-(int)(((angle*180.))/(M_PI));
                colors.at<Vec3b>(i,j)[1] = magnitude;
                colors.at<Vec3b>(i,j)[2] = 255;
            }
            else
            {
                lic.at<Vec3b>(i,j)[0] = 255;
                lic.at<Vec3b>(i,j)[1] = 255;
                lic.at<Vec3b>(i,j)[2] = 255;

                // color background

                colors.at<Vec3b>(i,j)[0] = 0;
                colors.at<Vec3b>(i,j)[1] = 0;
                colors.at<Vec3b>(i,j)[2] = 255;
            }
        }
    }


    Mat colorsToSave(h,w,CV_8UC3);
    cvtColor(colors,colorsToSave, cv::COLOR_HSV2BGR);

   //cv::imwrite("output/licTangentsColors.png",colorsToSave);

    // return
    return lic;
}
