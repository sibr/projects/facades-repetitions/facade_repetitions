/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#ifndef VIEWREPROJECTION_H
#define VIEWREPROJECTION_H

#include "Config.hpp"
#include <core/graphics/Image.hpp>
#include <core/raycaster/Raycaster.hpp>
#include "ReprojectionScene.hpp"

class SIBR_FACADE_REPETITIONS_EXPORT ViewReprojection
{

private:
	
	std::shared_ptr<ReprojectionScene> _scene;
	std::shared_ptr<sibr::Raycaster> _raycaster;
	

public:

	ViewReprojection(const std::string & rootPath, const std::string & imagesPath = "", const std::string & meshPath = "");

	std::shared_ptr<ReprojectionScene> scene() { return _scene; }

	void reproject(const int cameraToUse, const std::vector<int> & imagesToUse, const std::string & outputPath, bool outAlpha = true, bool bilinear = true);

	~ViewReprojection(void);

};

#endif // VIEWREPROJECTION_H