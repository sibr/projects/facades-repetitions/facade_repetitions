/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#ifndef SIBR_STRUCTURETENSOR_H
#define SIBR_STRUCTURETENSOR_H

#include "Config.hpp"

#include "FilterLineIntegralConvolution.h"



#include <Eigen/Core>

#include <vector>

#include <opencv2/core.hpp>
#include <opencv2/core/ocl.hpp>
#include <opencv2/core/base.hpp>
#include <opencv2/core/utility.hpp>
#include <opencv2/core/cvdef.h>
#include <opencv2/core/core_c.h>
#include <opencv2/imgproc.hpp>
#include <opencv2/highgui.hpp>

class SIBR_FACADE_REPETITIONS_EXPORT StructureTensor
{

	Eigen::Vector2d ** tangents;
	double ** magnitude;

    int h,w;

	// return greyscale blurred image
    cv::Mat preprocessForFiltering(cv::Mat);

    cv::Mat findEigenValues(cv::Mat structureTensorM);

    cv::Mat lessChangeEigenvector(cv::Mat st, cv::Mat ev);

public:

	enum COLORS { BLUE = 0, GREEN = 1, RED = 2, NONE };

    StructureTensor();

    ~StructureTensor();

    StructureTensor(cv::Mat, COLORS col = COLORS::NONE);

    Eigen::Vector2d getTangent(int,int);
    double getMagnitude(int,int);

    Eigen::Vector2d ** myStructureTensorXYMap(cv::Mat preprocImage, cv::Mat input);

};

#endif // SIBR_STRUCTURETENSOR_H
