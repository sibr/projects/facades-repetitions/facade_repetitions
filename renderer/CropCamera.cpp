/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#include <core/system/Utils.hpp>
#include "OpenMVGSFM.hpp"
#include "CropCamera.hpp"

std::vector<std::vector<sibr::Vector4f>> CropCameraUtilities::loadCropList(const std::string & path)
{
	std::ifstream cropList(path);
	std::string line2;
	std::vector<std::vector<sibr::Vector4f>> crops;
	if (cropList.is_open()) {
		while (std::getline(cropList, line2)) {
			if (line2.size() == 0) {
				continue;
			}
			if (line2[0] == 'c') {
				std::istringstream lineStream(line2);
				std::string dummy;
				float x, y, w, h;
				lineStream >> dummy >> x >> y >> w >> h;
				sibr::Vector4f crop(x, y, w, h);
				crops.back().push_back(crop);
			}
			else {
				crops.emplace_back();
			}
		}
		cropList.close();
	}
	else {
		SIBR_ERR << "Could'nt load crops listing" << std::endl;
	}
	return crops;

}

std::vector<Trapezoid> CropCameraUtilities::loadTrapezeList(const std::string & path)
{
	std::ifstream trapList(path);
	std::string line2;
	std::vector<Trapezoid> traps;
	if (trapList.is_open()) {
		while (std::getline(trapList, line2)) {
			if (line2.size() == 0) {
				continue;
			}
			if (line2[0] == 't') {
				std::istringstream lineStream(line2);
				std::string dummy;
				sibr::Vector2f p0, p1, p2, p3;
				lineStream >> dummy >> p0[0] >> p0[1] >> p1[0] >> p1[1] >> p2[0] >> p2[1] >> p3[0] >> p3[1];
				Trapezoid trap;
				trap.p0 = sibr::Vector3f(p0[0], p0[1], 1.0f);
				trap.p1 = sibr::Vector3f(p1[0], p1[1], 1.0f);
				trap.p2 = sibr::Vector3f(p2[0], p2[1], 1.0f);
				trap.p3 = sibr::Vector3f(p3[0], p3[1], 1.0f);
				traps.push_back(trap);
			}
			// Ignore image title.
		}
		trapList.close();
	}
	else {
		SIBR_ERR << "Could'nt load trap listing" << std::endl;
	}
	return traps;
}

std::vector<CropCamera> CropCameraUtilities::estimateCameras(const std::vector<std::vector<sibr::Vector4f>> & crops, const std::vector<sibr::InputCamera::Ptr> & cameras)
{
	std::vector<CropCamera> estimatedCameras;

	for (size_t i = 0; i < crops.size(); ++i) {
		for (size_t j = 0; j < crops[i].size(); ++j) {
			auto & inputCam = *cameras[i];

			sibr::Camera tempCam;
			tempCam.fovy(inputCam.fovy());
			tempCam.znear(inputCam.znear());
			tempCam.zfar(inputCam.zfar());
			tempCam.aspect(crops[i][j][2] / crops[i][j][3]);

			// Compute worldspace position of a point on the image plane (or at least on the same dir)
			// new optical center in pixel space, centered.
			sibr::Vector2f pixel = sibr::Vector2f(crops[i][j][0] + 0.5f * crops[i][j][2],
				crops[i][j][1] + 0.5f * crops[i][j][3]);

			float heightWorldSize = 2.f*tanf(inputCam.fovy() / 2.f);
			sibr::Vector2f screenWorldSize = sibr::Vector2f(heightWorldSize * inputCam.aspect(), heightWorldSize);

			sibr::Vector3f right = inputCam.right();
			sibr::Vector3f dir = inputCam.dir();
			sibr::Vector3f up = inputCam.up();

			sibr::Vector3f worldPos = inputCam.position() + inputCam.dir()
				+ screenWorldSize[0] * (pixel.x() / (float)inputCam.w() - 0.5f) * inputCam.right()
				- screenWorldSize[1] * (pixel.y() / (float)inputCam.h() - 0.5f) * inputCam.up();

			tempCam.setLookAt(inputCam.position(), worldPos, inputCam.up());

			float newFov = 2.0f*std::atanf(crops[i][j][3] / float(inputCam.h()) * std::tanf(0.5f*inputCam.fovy()));
			tempCam.fovy(newFov);

			CropCamera camCrop;
			camCrop.camera = tempCam;
			camCrop.crop = crops[i][j];
			camCrop.intersection = sibr::Vector3f(0.0f, 0.0f, 0.0f);
			camCrop.source = i;
			camCrop.id = j;
			estimatedCameras.push_back(camCrop);
		}
	}
	return estimatedCameras;
}

std::vector<std::pair<unsigned int, unsigned int>> CropCameraUtilities::loadWindowsCorrespondences(const std::string & path) {
	unsigned int maxWin, maxView;
	return loadWindowsCorrespondences(path, maxWin, maxView);
}


std::vector<std::pair<unsigned int, unsigned int>> CropCameraUtilities::loadWindowsCorrespondences(const std::string & path, unsigned int & maxWindow, unsigned int & maxView) {
	
	std::vector<std::pair<unsigned int, unsigned int>> windowsIds;

	if (!sibr::fileExists(path)) {
		SIBR_ERR << "Could not load windows correspondences file at path " << path << "." << std::endl;
		return windowsIds;
	}
	maxWindow = 0;
	maxView = 0;
	// Read ids for windows and views.
	std::ifstream fileList;
	fileList.open(path);
	std::string line;
	while (!fileList.eof()) {
		std::getline(fileList, line);
		if (line.size() == 0 || line.at(0) == '#') {
			continue;
		}
		const size_t pos = line.find(' ');
		if (pos == std::string::npos) {
			std::cerr << "Skipping line because missing space." << std::endl;
			continue;
		}
		unsigned int windowId = std::stoi(line.substr(0, pos));
		unsigned int viewId = std::stoi(line.substr(pos + 1));
		maxWindow = std::max(maxWindow, windowId);
		maxView = std::max(maxView, viewId);
		windowsIds.push_back(std::make_pair(windowId, viewId));
	}
	fileList.close();
	return windowsIds;
}

std::vector<sibr::InputCamera::Ptr> CropCameraUtilities::load(const std::string& bundlePath, const std::string& listingPath, std::vector<sibr::Vector3f> & points, float zNear, float zFar)
{
	const std::string bundlerFile = bundlePath;
	const std::string listFile = listingPath;
	SIBR_LOG << "Loading input cameras." << std::endl;
	points.clear();
	struct Z {
		Z() {}
		Z(float f, float n) : far(f), near(n) {}
		float far;
		float near;
	};

	std::vector<Z> nearsFars = { Z(zNear, zFar) };

	
	// check bundler file
	std::ifstream bundle_file(bundlerFile);
	SIBR_ASSERT(bundle_file.is_open());

	//check list of images
	std::ifstream list_images(listFile);
	SIBR_ASSERT(list_images.is_open());

	// read number of images
	std::string line;
	getline(bundle_file, line);	// ignore first line - contains version
	int numImages = 0;
	int numPoints = 0;
	bundle_file >> numImages;	// read first value (number of images)
	bundle_file >> numPoints;
	//getline(bundle_file, line);	// ignore the rest of the line

								// Read all filenames
	struct ImgInfos
	{
		std::string name;
		int id;
		int w, h;
	};
	std::vector<ImgInfos>	imgInfos;
	int falseID = 0;
	{
		ImgInfos				infos;
		while (true)
		{
			list_images >> infos.name;
			if (infos.name.empty()) break;
			list_images >> infos.w >> infos.h;
			infos.name.erase(infos.name.find_last_of("."), std::string::npos);
			infos.id = falseID;// atoi(infos.name.c_str());
			imgInfos.push_back(infos);
			infos.name.clear();
			++falseID;
		}
	}

	std::vector<sibr::InputCamera::Ptr> cameras(numImages);
	//  Parse bundle.out file for camera calibration parameters
	for (int i = 0, infosId = 0; i<numImages && infosId < imgInfos.size(); i++) {
		const ImgInfos& infos = imgInfos[infosId];
		if (i != infos.id)
			continue;

		sibr::Matrix4f m; // bundler params

		bundle_file >> m(0) >> m(1) >> m(2) >> m(3) >> m(4);
		bundle_file >> m(5) >> m(6) >> m(7) >> m(8) >> m(9);
		bundle_file >> m(10) >> m(11) >> m(12) >> m(13) >> m(14);

		//
		cameras[infosId] = std::make_shared<sibr::InputCamera>(infosId, infos.w, infos.h, m, true);
		// get rid of suffix of image file name
		cameras[infosId]->name(infos.name);

		const Z & current_Z = nearsFars[0];
		cameras[infosId]->znear(current_Z.near); cameras[infosId]->zfar(current_Z.far);

		++infosId;
		
	}
	for (int i = 0; i < numPoints; ++i) {
		float x, y, z;
		bundle_file >> x >> y >> z;
		getline(bundle_file, line); //colors
		getline(bundle_file, line); // occurences
		points.emplace_back(x, y, z);
	}
	return cameras;
}

std::map<unsigned int, unsigned int> CropCameraUtilities::checkCorrespondances(const std::string & sfmFile)
{
	sibr::OpenMVGSFM sfm(sfmFile);
	std::map<unsigned int, unsigned int> indices;
	for (auto & corresp : sfm.correspondances()) {
		indices[corresp.initial] = corresp.shifted;
	}
	return indices;
}

