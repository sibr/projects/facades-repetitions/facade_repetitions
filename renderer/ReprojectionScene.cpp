/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#include "ReprojectionScene.hpp"

#include <core/assets/ImageListFile.hpp>
#include <core/assets/ActiveImageFile.hpp>


ReprojectionScene::ReprojectionScene(const std::string & rootPath, const std::string & imagesPath, const std::string & meshPath) : _proxy(false) {
	// Paths
	const std::string ibrPath = rootPath + "/";
	const std::string listImagesPath = ibrPath + "/list_images.txt";
	const std::string activeImagesPath = ibrPath + "/active_images.txt";
	const std::string masksPath = ibrPath + "/masks/";
	const std::string intImagesPath = imagesPath.length() > 0 ? imagesPath : ibrPath;
	const std::string proxyPath = meshPath.length() > 0 ? meshPath : ibrPath + "/pmvs/models/pmvs_recon.ply";

	_path = rootPath;

	sibr::ImageListFile imageListFile;
	sibr::ActiveImageFile activeImageFile;

	if (!imageListFile.load(listImagesPath, false)) {
		SIBR_ERR << "[ReprojectionScene] Cant open " << listImagesPath << std::endl;
	}
	int numImages = (int)imageListFile.infos().size();

	_inputCameras = sibr::InputCamera::load(ibrPath);
	activeImageFile.setNumImages(numImages);
	if (activeImageFile.load(activeImagesPath, false)) {
		for (int i = 0; i<numImages; i++) {
			if (!activeImageFile.active()[i]) {
				_inputCameras[i]->setActive(false);
			}
		}
	}

	// Load images and masks.
	_masks.clear();
	_images.clear();
	for (auto & cam :_inputCameras) {
		//if (cam.isActive()) {
			_masks.emplace_back();
			_masks.back().load(masksPath + cam->name() + "_mask" + ".png", false);
			_images.emplace_back();
			_images.back().load(intImagesPath + cam->name() + ".jpg", false);
		//}
	}
	
	
	if (!_proxy.load(proxyPath)) {
		SIBR_ERR << "proxy model not found at " << proxyPath << std::endl;
	}
	if (_proxy.hasNormals() == false){
		_proxy.generateNormals();
	}


}


ReprojectionScene::~ReprojectionScene(void) {
}