/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#include "OpenMVGSFM.hpp"

#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <map>
#include <utility>


#include <sstream>
#include <iterator>
#include <vector>

namespace sibr {


	std::string OpenMVGSFM::extractSimpleValueForKey(const std::string& key, const std::string& str, size_t & pos) {

		// Find the key.
		pos = str.find("\"" + key + "\":", pos);
		if (pos == std::string::npos) { return ""; }
		size_t start = pos + 1 + key.size() + 2;
		// Find the first comma (as the value is 'simple')
		size_t end = std::min(str.find(",", start), str.find("\n", start));
		// Update pos.
		pos = end;
		if (pos == std::string::npos) { return ""; }
		// Return the value substring.
		return str.substr(start, end - start);

	}


	std::string OpenMVGSFM::extractSimpleArrayForKey(const std::string& key, const std::string& str, size_t & pos) {
		// Find the key.
		size_t start = str.find("\"" + key + "\":", pos);
		if (start == std::string::npos) {
			pos = start;
			return "";
		}
		// Find the next opening bracket.
		start = str.find("[", start);

		// ... and the next closing bracket.
		size_t end = str.find("]", start);
		// Update position.
		pos = start;
		// Return the array substring.
		return str.substr(start, end - start + 1);

	}


	sibr::Vector3f OpenMVGSFM::extractVec3(const std::string & str) {
		std::vector<float> values;

		// Creating a string stream and split it by lines.
		std::istringstream iss(str);
		std::istream_iterator<std::string> begin(iss), end;
		std::vector<std::string> lines(begin, end);

		for (auto& line : lines) {
			// Edge cases.
			if (line.size() == 0 || line[0] == '[' || line[0] == ']') {
				continue;
			}
			// Else, we have a significant line.
			std::string value = line;
			// Handle intermediate commas.
			if (line[line.size() - 1] == ',') {
				value = line.substr(0, line.size() - 1);
			}
			// Convert to float and store
			values.push_back((float)atof(value.c_str()));
		}


		return sibr::Vector3f(values[0], values[1], values[2]);
	}

	OpenMVGSFM::OpenMVGSFM(const std::string & path) {

		// Load the file.
		std::ifstream sfm_file(path);

		SIBR_ASSERT(sfm_file.is_open());

		// Store it completely into a string.
		std::stringstream buffer;
		buffer << sfm_file.rdbuf();
		const std::string sfm_content = buffer.str();
		sfm_file.close();

		// Find the views array.
		size_t dummyPos = 0;
		size_t pos = 0;
		size_t intrinsicCount = 0;
		size_t extrinsicCount = 0;

		const std::string views = extractSimpleArrayForKey("views", sfm_content, dummyPos);

		std::map<size_t, std::pair<size_t, size_t>> ids;
		std::map<size_t, std::string> names;
		// Extract views from the array, as long as we find some.
		while (true) {
			// Extract view id.
			size_t viewID = atoi(extractSimpleValueForKey("key", views, pos).c_str());

			// Exit case: no more ids.
			if (pos == std::string::npos) { break; }

			// We don't assume the ordering of the subkeys, thus we backup the current position.
			size_t posBackup = pos;
			size_t posBackup1 = pos;
			// Extract intrinsic and extrinsic ids.
			size_t intrinsicID = atoi(extractSimpleValueForKey("id_intrinsic", views, pos).c_str());
			size_t extrinsicID = atoi(extractSimpleValueForKey("id_pose", views, posBackup).c_str());
			std::string viewName = extractSimpleValueForKey("filename", views, posBackup1);
			if(!viewName.empty()) {
				viewName = viewName.substr(2, viewName.size() - 3);
			}
			// Udpate current position.
			pos = std::max(std::max(pos, posBackup), posBackup1);
			// Store the highest id encountered for each type, this will allows us to know how many of each we have to parse.
			intrinsicCount = std::max(intrinsicCount, intrinsicID);
			extrinsicCount = std::max(extrinsicCount, extrinsicID);

			// Store ids.
			ids.insert(std::make_pair(viewID, std::make_pair(intrinsicID, extrinsicID)));
			names[viewID] = viewName;
		}

		// Increment to go from highest index to count. We suppose consecutive indices.
		++intrinsicCount;
		++extrinsicCount;

		SIBR_LOG << "Expecting " << intrinsicCount << " intrinsics and " << extrinsicCount << " extrinsics." << std::endl;


		// Extract intrinsics.
		pos = sfm_content.find("\"intrinsics\":");
		std::vector<IntrinsicsParam> intrinsicParams;
		intrinsicParams.resize(intrinsicCount);

		for (size_t i = 0; i < intrinsicCount; ++i) {
			// Extract the id
			size_t intrinsicID = atoi(extractSimpleValueForKey("key", sfm_content, pos).c_str());

			IntrinsicsParam params;
			// Extract 'simple' values: width, height, focal_length. We make no supposion on their ordering, thus we need a backup for the current position.
			size_t posBackup = pos;
			size_t finalPos = pos;

			size_t width = atoi(extractSimpleValueForKey("width", sfm_content, posBackup).c_str());
			finalPos = std::max(finalPos, posBackup);
			params.width = width;

			posBackup = pos;
			size_t height = atoi(extractSimpleValueForKey("height", sfm_content, posBackup).c_str());
			finalPos = std::max(finalPos, posBackup);
			params.height = height;

			posBackup = pos;
			float focalLength = (float)atof(extractSimpleValueForKey("focal_length", sfm_content, posBackup).c_str());
			finalPos = std::max(finalPos, posBackup);
			params.focalLength = focalLength;
			// Update current position.
			pos = finalPos;

			intrinsicParams[intrinsicID] = params;
			//std::cout << "Intrinsic " << intrinsicID << " (" << width << ", " << height << "), f = " << focalLength << std::endl;
		}

		// Extract extrinsincs.
		pos = sfm_content.find("\"extrinsics\":");
		std::vector<ExtrinsicsParam> extrinsicParams;
		extrinsicParams.resize(extrinsicCount);
		//std::vector<int> missingExtrinsincs;

		for (size_t i = 0; i < extrinsicCount; ++i) {
			// Extract the id.
			const std::string vall = extractSimpleValueForKey("key", sfm_content, pos);
			size_t extrinsicID = atoi(vall.c_str());
			// HACK: SR We need to detect missing extrinsics. We assume ids are
			// in order, so if i!=id, we skip it and will never meet it later.
			if (i != extrinsicID) {
				for (size_t k = i; k < extrinsicID; ++k) {
					//missingExtrinsincs.push_back(k);
					extrinsicParams.emplace_back();
					extrinsicParams.back().init = false;
				}
				i = extrinsicID;
			}
			ExtrinsicsParam params;
			params.init = true;
			size_t posBackup = pos;
			//Extract the center, a 3 values array...
			std::string centerString = extractSimpleArrayForKey("center", sfm_content, pos);
			// ... and convert it to a vec3 of floats.
			Eigen::Vector3f center = extractVec3(centerString);
			params.center = center;

			// Find the rotation.
			posBackup = sfm_content.find("\"rotation\":", posBackup) + 11;
			// The rotation is a matrix as nested arrays. We reach the exterior opening bracket...
			posBackup = sfm_content.find("[", posBackup);
			// ...ignore it
			posBackup += 1;
			size_t start = posBackup;
			// ... find the end of the sub-array...
			posBackup = sfm_content.find("]", posBackup) + 1;
			// ... and extract and convert it to a vec3.
			std::string row0String = sfm_content.substr(start, posBackup - start);
			Eigen::Vector3f row0 = extractVec3(row0String);

			// Same for the second column.
			posBackup = sfm_content.find("[", posBackup);
			start = posBackup;
			posBackup = sfm_content.find("]", posBackup) + 1;
			std::string row1String = sfm_content.substr(start, posBackup - start);
			Eigen::Vector3f row1 = extractVec3(row1String);

			// And the last one.
			posBackup = sfm_content.find("[", posBackup);
			start = posBackup;
			posBackup = sfm_content.find("]", posBackup) + 1;
			std::string row2String = sfm_content.substr(start, posBackup - start);
			Eigen::Vector3f row2 = extractVec3(row2String);

			// Multiply by -1 2nd and 3rd rows to be coherent with Bundler.
			Eigen::Matrix3f rotation;
			rotation.row(0) = row0;
			rotation.row(1) = -row1;
			rotation.row(2) = -row2;
			params.rotation = rotation;

			// Update position.
			pos = std::max(pos, posBackup);

			extrinsicParams[extrinsicID] = params;

		}

		_cameras.resize(ids.size());
		unsigned int goodCamerasCount = 0;
		for (auto& camIds : ids) {
			const IntrinsicsParam & iParams = intrinsicParams[camIds.second.first];
			const ExtrinsicsParam & eParams = extrinsicParams[camIds.second.second];

			const std::string camName = names[camIds.first];
			

			if (!eParams.init) {
				_cameras[camIds.first]->name(camName);
				_cameras[camIds.first]->setActive(false);
				continue;
			}
			sibr::Matrix4f m; // bundler params
			m(0) = iParams.focalLength;
			m(1) = 0.0;
			m(2) = 0.0;
			
			m(3) = eParams.rotation(0, 0);
			m(4) = eParams.rotation(0, 1);
			m(5) = eParams.rotation(0, 2);
			m(6) = eParams.rotation(1, 0);
			m(7) = eParams.rotation(1, 1);
			m(8) = eParams.rotation(1, 2);
			m(9) = eParams.rotation(2, 0);
			m(10)  = eParams.rotation(2, 1);
			m(11)  = eParams.rotation(2, 2);

			sibr::Vector3f trans = -eParams.rotation * eParams.center;

			m(12) = trans[0];
			m(13) = trans[1];
			m(14) = trans[2];

			sibr::InputCamera::Ptr cam = std::make_shared<sibr::InputCamera>((int)camIds.first, (int)iParams.width, (int)iParams.height, m, true);
			_cameras[camIds.first] = cam;
			_cameras[camIds.first]->name(camName);
			_corresps.emplace_back();
			_corresps.back().initial = (unsigned int)camIds.first;
			_corresps.back().shifted = goodCamerasCount;
			++goodCamerasCount;
		}

		// Extract the 3D points.
		pos = sfm_content.find("\"structure\":");


		// Iterate while we find new X keys, ie points.
		while (true) {
			std::string point = extractSimpleArrayForKey("X", sfm_content, pos);
			// Stop if no new point found.
			if (pos == std::string::npos) { break; }
			// Move pos to the end of the array.
			pos += point.size();
			// Convert and store the point.
			_points.push_back(extractVec3(point));

		}

		if (_cameras.size() != _corresps.size()) {
			SIBR_WRG << "[OpenMVGSFM] Detected " << (_cameras.size() - _corresps.size()) << " missing cameras extrinsics." << std::endl;
		}

	}

}

