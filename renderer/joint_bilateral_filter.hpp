/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


/*
 *  By downloading, copying, installing or using the software you agree to this license.
 *  If you do not agree to this license, do not download, install,
 *  copy or use the software.
 *
 *
 *  License Agreement
 *  For Open Source Computer Vision Library
 *  (3 - clause BSD License)
 *
 *  Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met :
 *
 *  * Redistributions of source code must retain the above copyright notice,
 *  this list of conditions and the following disclaimer.
 *
 *  * Redistributions in binary form must reproduce the above copyright notice,
 *  this list of conditions and the following disclaimer in the documentation
 *  and / or other materials provided with the distribution.
 *
 *  * Neither the names of the copyright holders nor the names of the contributors
 *  may be used to endorse or promote products derived from this software
 *  without specific prior written permission.
 *
 *  This software is provided by the copyright holders and contributors "as is" and
 *  any express or implied warranties, including, but not limited to, the implied
 *  warranties of merchantability and fitness for a particular purpose are disclaimed.
 *  In no event shall copyright holders or contributors be liable for any direct,
 *  indirect, incidental, special, exemplary, or consequential damages
 *  (including, but not limited to, procurement of substitute goods or services;
 *  loss of use, data, or profits; or business interruption) however caused
 *  and on any theory of liability, whether in contract, strict liability,
 *  or tort(including negligence or otherwise) arising in any way out of
 *  the use of this software, even if advised of the possibility of such damage.
 */

#ifndef __OPENCV_XIMGPROC_HPP__
#define __OPENCV_XIMGPROC_HPP__
#include "Config.hpp"

#include <opencv2/core.hpp>
#include <opencv2/core/ocl.hpp>
#include <opencv2/core/base.hpp>
#include <opencv2/core/utility.hpp>
#include <opencv2/core/cvdef.h>
#include <opencv2/core/core_c.h>
#include <opencv2/imgproc.hpp>
#include <opencv2/core/traits.hpp>

#include <algorithm>
#include <map>
#include <climits>
#include <iostream>

//#ifdef _MSC_VER
//#   pragma warning(disable: 4512)
//#endif

namespace cv
{
	typedef cv::Vec<float, 1> Vec1f;
	typedef Vec<uchar, 1> Vec1b;

	SIBR_FACADE_REPETITIONS_EXPORT void jointBilateralFilter_32f(cv::Mat& joint, Mat& src, Mat& dst, int radius, double sigmaColor, double sigmaSpace, int borderType);

	SIBR_FACADE_REPETITIONS_EXPORT void jointBilateralFilter_8u(Mat& joint, Mat& src, Mat& dst, int radius, double sigmaColor, double sigmaSpace, int borderType);

	template<typename JointVec, typename SrcVec>
	 class JointBilateralFilter_32f : public ParallelLoopBody
	{
		Mat &joint, &src;
		Mat &dst;
		int radius, maxk;
		float scaleIndex;
		int *spaceOfs;
		float *spaceWeights, *expLUT;

	public:

		JointBilateralFilter_32f(Mat& joint_, Mat& src_, Mat& dst_, int radius_,
			int maxk_, float scaleIndex_, int *spaceOfs_, float *spaceWeights_, float *expLUT_)
			:
			joint(joint_), src(src_), dst(dst_), radius(radius_), maxk(maxk_),
			scaleIndex(scaleIndex_), spaceOfs(spaceOfs_), spaceWeights(spaceWeights_), expLUT(expLUT_)
		{
			//CV_DbgAssert(joint.type() == traits::Type<JointVec>::value && src.type() == dst.type() && src.type() == traits::Type<SrcVec>::value);
			CV_DbgAssert(joint.rows == src.rows && src.rows == dst.rows + 2 * radius);
			CV_DbgAssert(joint.cols == src.cols && src.cols == dst.cols + 2 * radius);
		}

		void operator () (const Range& range) const
		{
			for (int i = radius + range.start; i < radius + range.end; i++)
			{
				for (int j = radius; j < src.cols - radius; j++)
				{
					JointVec *jointCenterPixPtr = joint.ptr<JointVec>(i) + j;
					SrcVec *srcCenterPixPtr = src.ptr<SrcVec>(i) + j;

					JointVec jointPix0 = *jointCenterPixPtr;
					SrcVec srcSum = SrcVec::all(0.0f);
					float wSum = 0.0f;

					for (int k = 0; k < maxk; k++)
					{
						float *jointPix = reinterpret_cast<float*>(jointCenterPixPtr + spaceOfs[k]);
						float alpha = 0.0f;

						for (int cn = 0; cn < JointVec::channels; cn++)
							alpha += std::abs(jointPix0[cn] - jointPix[cn]);
						alpha *= scaleIndex;
						int idx = (int)(alpha);
						alpha -= idx;
						float weight = spaceWeights[k] * (expLUT[idx] + alpha*(expLUT[idx + 1] - expLUT[idx]));

						float *srcPix = reinterpret_cast<float*>(srcCenterPixPtr + spaceOfs[k]);
						for (int cn = 0; cn < SrcVec::channels; cn++)
							srcSum[cn] += weight*srcPix[cn];
						wSum += weight;
					}
					
					dst.at<SrcVec>(i - radius, j - radius) = srcSum / wSum;
				}
			}
		}
	};

	

	template<typename JointVec, typename SrcVec>
	 class JointBilateralFilter_8u : public ParallelLoopBody
	{
		Mat &joint, &src;
		Mat &dst;
		int radius, maxk;
		float scaleIndex;
		int *spaceOfs;
		float *spaceWeights, *expLUT;

	public:

		JointBilateralFilter_8u(Mat& joint_, Mat& src_, Mat& dst_, int radius_,
			int maxk_, int *spaceOfs_, float *spaceWeights_, float *expLUT_)
			:
			joint(joint_), src(src_), dst(dst_), radius(radius_), maxk(maxk_),
			spaceOfs(spaceOfs_), spaceWeights(spaceWeights_), expLUT(expLUT_)
		{
			
			//CV_DbgAssert(joint.type() == traits::Type<JointVec>::value && src.type() == dst.type() && src.type() == traits::Type<SrcVec>::value);
			CV_DbgAssert(joint.rows == src.rows && src.rows == dst.rows + 2 * radius);
			CV_DbgAssert(joint.cols == src.cols && src.cols == dst.cols + 2 * radius);
		}

		void operator () (const Range& range) const
		{
			typedef Vec<int, JointVec::channels> JointVeci;
			typedef Vec<float, SrcVec::channels> SrcVecf;

			for (int i = radius + range.start; i < radius + range.end; i++)
			{
				for (int j = radius; j < src.cols - radius; j++)
				{
					JointVec *jointCenterPixPtr = joint.ptr<JointVec>(i) + j;
					SrcVec *srcCenterPixPtr = src.ptr<SrcVec>(i) + j;

					JointVeci jointPix0 = JointVeci(*jointCenterPixPtr);
					SrcVecf srcSum = SrcVecf::all(0.0f);
					float wSum = 0.0f;

					for (int k = 0; k < maxk; k++)
					{
						uchar *jointPix = reinterpret_cast<uchar*>(jointCenterPixPtr + spaceOfs[k]);
						int alpha = 0;
						for (int cn = 0; cn < JointVec::channels; cn++)
							alpha += std::abs(jointPix0[cn] - (int)jointPix[cn]);

						float weight = spaceWeights[k] * expLUT[alpha];

						uchar *srcPix = reinterpret_cast<uchar*>(srcCenterPixPtr + spaceOfs[k]);
						for (int cn = 0; cn < SrcVec::channels; cn++)
							srcSum[cn] += weight*srcPix[cn];
						wSum += weight;
					}

					dst.at<SrcVec>(i - radius, j - radius) = SrcVec(srcSum / wSum);
				}
			}
		}
	};

	

}

#endif // __OPENCV_XIMGPROC_HPP__
