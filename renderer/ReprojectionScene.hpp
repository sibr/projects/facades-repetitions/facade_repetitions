/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#ifndef REPROJECTIONSCENE_H
#define REPROJECTIONSCENE_H

#include "Config.hpp"
#include <core/graphics/Image.hpp>
#include <core/graphics/Mesh.hpp>
#include <core/assets/InputCamera.hpp>

class SIBR_FACADE_REPETITIONS_EXPORT ReprojectionScene
{
	SIBR_DISALLOW_COPY(ReprojectionScene);
		SIBR_CLASS_PTR(ReprojectionScene);
private:
	
	sibr::Mesh _proxy;
	std::vector<sibr::InputCamera::Ptr> _inputCameras;
	std::vector<sibr::ImageRGB> _images;
	std::vector<sibr::ImageL8> _masks;
	std::string _path;


public:

	ReprojectionScene(const std::string & rootPath, const std::string & imagesPath = "", const std::string & meshPath = "");

	sibr::Mesh& proxy() { return _proxy; }
	const std::vector<sibr::InputCamera::Ptr>& inputCameras() const { return _inputCameras;  }
	const std::vector<sibr::ImageRGB>& images() const { return _images; };
	const std::vector<sibr::ImageL8>& masks() const  { return _masks;}
	const std::string path() const { return _path;  }

	~ReprojectionScene(void);

};

#endif // REPROJECTIONSCENE_H