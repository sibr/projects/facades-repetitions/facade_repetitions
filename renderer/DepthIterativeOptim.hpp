/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#ifndef SIBR_REPETITIONS_DEPTH_ITERATIVE_OPTIM_H
#define SIBR_REPETITIONS_DEPTH_ITERATIVE_OPTIM_H
#include "Config.hpp"
#include <core/assets/InputCamera.hpp>


#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/photo/photo.hpp>

class SIBR_FACADE_REPETITIONS_EXPORT DepthMapEnergy {

public:

	typedef Eigen::Vector3d DepthData;

	DepthMapEnergy() { };
	~DepthMapEnergy() { };

	virtual double getCost(int x, int y, const double shift) = 0;
	void registerDepthMap(DepthData ** depthMap) { _depthMap = depthMap; };

protected:
	DepthData ** _depthMap;
};

class SIBR_FACADE_REPETITIONS_EXPORT DepthIterativeOptim
{

public:

	DepthIterativeOptim(const cv::Mat initialDepthMap, DepthMapEnergy * energy, const double minZ, const double maxZ);

	~DepthIterativeOptim(void);

	void solve(const int iteration_count, const int levels);

	DepthMapEnergy::DepthData ** getDepthData() { return _depthMap; };

	cv::Mat getDepthMap();

private:


	void compareCosts(const int x0, const int y0, const double newDepth);


	DepthMapEnergy::DepthData ** _depthMap;
	DepthMapEnergy * _energy;
	cv::Mat _costMap;
	int _w;
	int _h;
	double _minZ, _maxZ;


};

#endif // SIBR_REPETITIONS_PATCHMATCHSTEREO_H
