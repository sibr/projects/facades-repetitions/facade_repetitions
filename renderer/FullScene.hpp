/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#ifndef SIBR_REPETITIONS_FULLSCENE_H
#define SIBR_REPETITIONS_FULLSCENE_H

#include "Config.hpp"
#include "CropCamera.hpp"

#include <core/assets/InputCamera.hpp>
#include "core/graphics/Image.hpp"


struct Plane {
	sibr::Vector3f p0;
	sibr::Vector3f p1;
	sibr::Vector3f p2;
	sibr::Vector3f pNormal;

	Plane() { };

	Plane(const sibr::Vector3f & _p0, const sibr::Vector3f & _p1,
		const sibr::Vector3f & _p2, const sibr::Vector3f & _pNormal)
		: p0(_p0), p1(_p1), p2(_p2), pNormal(_pNormal) {}
};

class SIBR_FACADE_REPETITIONS_EXPORT FullScene {

	SIBR_DISALLOW_COPY(FullScene);

public:

	FullScene(const std::string & rootPath, bool useBundlerFile);

	~FullScene();
	
	std::vector<sibr::InputCamera::Ptr> cameras;
	std::vector<CropCamera> estimatedCameras;
	std::vector<sibr::ImageRGB> images;
	std::vector<sibr::ImageL8> masks;
	std::vector<Trapezoid> trapezoids;
	Plane facadePlane;
	std::vector<std::pair<unsigned int, unsigned int>> windowsIds;
	unsigned int windowsCount;
};



#endif // SIBR_REPETITIONS_SCENE_FULLSCENE_H
