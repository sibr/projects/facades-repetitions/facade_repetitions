/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#ifndef SIBR_REPETITIONS_MULTIVIEW_PMS_ENERGY_H
#define SIBR_REPETITIONS_MULTIVIEW_PMS_ENERGY_H

#include "Config.hpp"
#include "FullScene.hpp"
#include"CropCamera.hpp"
#include"ReprojectionScene.hpp"
#include"DepthIterativeOptim.hpp"
#include"StructureTensor.h"
#include <core/assets/InputCamera.hpp>
#include "core/scene/BasicIBRScene.hpp"


class SIBR_FACADE_REPETITIONS_EXPORT PMSEnergy : public DepthMapEnergy {
public:

	PMSEnergy(sibr::BasicIBRScene::Ptr scene, const std::vector<cv::Mat> & masks, const double gamma,
		const double alpha, const int halfSize, const sibr::InputCamera & cam, 
		cv::Mat colorMap);

	virtual double getCost(int x, int y, const double shift);

private:

	double _gamma;
	double _alpha;
	int _halfSize;
	sibr::BasicIBRScene::Ptr _scene;
	cv::Mat _colorMap;
	cv::Mat _magnitude, _orientation;
	std::vector<cv::Mat> _masks, _inputMagnitudes, _inputOrientations;
	sibr::InputCamera _frontoCam;

};



class SIBR_FACADE_REPETITIONS_EXPORT IterativeEnergy : public DepthMapEnergy {
public:

	IterativeEnergy(sibr::BasicIBRScene::Ptr scene, const std::vector<cv::Mat> & images, const std::vector<cv::Mat> & masks, const double gamma,
		const double alpha, const int halfSize, const sibr::InputCamera & cam, cv::Mat colorMap, std::shared_ptr<StructureTensor> tensor, const sibr::Vector3f & pNormal);

	virtual double getCost(int x, int y, const double shift);

private:

	double _gamma;
	double _alpha;
	int _halfSize;
	sibr::BasicIBRScene::Ptr _scene;
	cv::Mat _colorMap;
	cv::Mat _magnitude, _orientation;
	std::vector<cv::Mat> _images, _masks, _inputMagnitudes, _inputOrientations;
	sibr::InputCamera _frontoCam;
	std::shared_ptr<StructureTensor> _tensor;
	float _depthShiftTan;

};

class SIBR_FACADE_REPETITIONS_EXPORT PhotoconsistencyEnergy : public DepthMapEnergy {
public:

	PhotoconsistencyEnergy(sibr::BasicIBRScene::Ptr scene, const std::vector<cv::Mat> & images,
		const double alpha, const sibr::InputCamera & cam, cv::Mat colorMap);

	double getCostReal(int x, int y, const std::vector<bool> & camIds = {});

	double getCostArbitrary(int x, int y, const double depth, const std::vector<bool> & camIds = {});
	
	double getCost(int x, int y, const double shift, const std::vector<bool> & camIds);
	
	virtual double getCost(int x, int y, const double shift);

private:

	double _alpha;
	std::vector<sibr::InputCamera::Ptr> _inputCams;
	cv::Mat _colorMap;
	cv::Mat _magnitude;
	std::vector<cv::Mat> _images,  _inputMagnitudes;
	sibr::InputCamera _frontoCam;

};

#endif // SIBR_REPETITIONS_MULTIVIEW_PMS_ENERGY_H
