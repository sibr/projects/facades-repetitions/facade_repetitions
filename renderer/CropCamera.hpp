/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#ifndef SIBR_REPETITIONS_CROPCAMERA_H
#define SIBR_REPETITIONS_CROPCAMERA_H
#include <map>
#include <core/graphics/Camera.hpp>
#include <core/assets/InputCamera.hpp>

#include "Config.hpp"

struct CropCamera
{
	sibr::Camera camera;
	sibr::Vector4f crop;
	sibr::Vector3f intersection;
	size_t source;
	size_t id;
};

struct Trapezoid {
	sibr::Vector3f p0;
	sibr::Vector3f p1;
	sibr::Vector3f p2;
	sibr::Vector3f p3;
};


class SIBR_FACADE_REPETITIONS_EXPORT CropCameraUtilities
{
	SIBR_DISALLOW_COPY(CropCameraUtilities);


public:

	static std::vector<std::vector<sibr::Vector4f>> loadCropList(const std::string & path);

	static std::vector<Trapezoid> loadTrapezeList(const std::string & path);


	static std::vector<CropCamera> estimateCameras(const std::vector<std::vector<sibr::Vector4f>> & crops, const std::vector<sibr::InputCamera::Ptr> & cameras);

	/**
	* \brief Load from disk the correspondence information for cropped views (instance, origin view,...).
	* Assumption: returned in the same order as the scene cameras.
	* \param path path to file to load.
	* \return a list of pairs of physical window instance id / initial full scene view id.
	*/
	static std::vector<std::pair<unsigned int, unsigned int>> loadWindowsCorrespondences(const std::string & path);

	/**
	 * \brief Load from disk the correspondence information for cropped views (instance, origin view,...).
	 * Assumption: returned in the same order as the scene cameras.
	 * \param path path to file to load.
	 * \param maxWindow will contain the number of instances.
	 * \param maxView will contain the number of input views.
	 * \return a list of pairs of physical window instance id / initial full scene view id.
	 */
	static std::vector<std::pair<unsigned int, unsigned int>> loadWindowsCorrespondences(const std::string & path, unsigned int & maxWindow, unsigned int & maxView);

	static std::vector<sibr::InputCamera::Ptr> load(const std::string& bundlePath, const std::string& listingPath, std::vector<sibr::Vector3f> & points, float zNear = 0.01f, float zFar = 1000.0f);

	static std::map<unsigned int, unsigned int> checkCorrespondances(const std::string & sfmFile);
};

#endif // SIBR_REPETITIONS_CROPCAMERA_H