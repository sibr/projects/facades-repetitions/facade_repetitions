/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


# include "Config.hpp"
# include <core/assets/Resources.hpp>
# include <map>
# include "ULRRepetitionsRenderer.hpp"
#include "core/system/String.hpp"

namespace sibr
{
	ULRRepetitionsRenderer::ULRRepetitionsRenderer(const BasicIBRScene::Ptr& scene, const unsigned int maxCams,
	                                               const std::string& altShader)
	{
		_numCams = (std::min)(maxCams, (unsigned int)(scene->cameras()->inputCameras().size()));
		// TODO SR: decide if we should keep this safety or ot.
		//_numCams = (std::min)(maxCams, (unsigned int)(scene->inputCameras().size()));

		std::cerr << "[ULRRepetitionsRenderer] Trying to initialize shaders for at most " << _numCams << " cameras." <<
			std::endl;

		

		// Setup depth shader.
		_depthShader.init("ULRV2Depth",
		                  loadFile(sibr::getShadersDirectory("facade_repetitions") + "/ulr_intersect.vert"),
		                  loadFile(sibr::getShadersDirectory("facade_repetitions") + "/ulr_intersect.frag"));
		_proj.init(_depthShader, "proj");

		// Setup ULR shader.
		GLShader::Define::List defines;
		defines.emplace_back("NUM_CAMS", _numCams);
		_ulrShader.init("ULRArray",
		                loadFile(sibr::getShadersDirectory("facade_repetitions") + "/" + altShader + ".vert"),
		                loadFile(sibr::getShadersDirectory("facade_repetitions") + "/" + altShader + ".frag", defines));


		// General settings.
		_occTest.init(_ulrShader, "occ_test");
		_areMasksBinaryGL.init(_ulrShader, "is_binary_mask");
		_doInvertMasksGL.init(_ulrShader, "invert_mask");
		_discardBlackPixelsGL.init(_ulrShader, "discard_black_pixels");
		_doMask.init(_ulrShader, "doMasking");

		// Camera uniforms.
		_ncamPos.init(_ulrShader, "ncam_pos");
		_camCount.init(_ulrShader, "cam_count");
		_lastFullCamId.init(_ulrShader, "last_full_id");
		// Input cameras.
		_icamProj.resize(_numCams);
		// TODO SR: maybe use a Uniform buffer instead as we don't need to update them at each frame.
		_icamPos.resize(_numCams);
		_icamDir.resize(_numCams);
		_icamIds.resize(_numCams); // TODO SR: see if booleans would be better.

		_ulrShader.begin();
		for (uint i = 0; i < (uint)_numCams; i++)
		{
			const auto& cam = scene->cameras()->inputCameras()[i];

			_icamProj[i].init(_ulrShader, sprint("icam_proj[%d]", i));
			//_icamProj[i].set(cam.viewproj());

			_icamPos[i].init(_ulrShader, sprint("icam_pos[%d]", i));
			//_icamPos[i].set(cam.position());

			_icamDir[i].init(_ulrShader, sprint("icam_dir[%d]", i));
			//_icamDir[i].set(cam.dir());

			_icamIds[i].init(_ulrShader, sprint("icam_ids[%d]", i));
		}
		_ulrShader.end();

		// Will be resized as soon as we render.
		_depthRT.reset(new RenderTargetRGBA32F(1280,800));

		_doOccl = true;
		_areMasksBinary = true;
		_doInvertMasks = false;
		_discardBlackPixels = true;
	}


	void
	ULRRepetitionsRenderer::process(std::vector<uint>& imgs_ulr, const int lastFullCamid,
	                                const Camera& eye,
	                                const BasicIBRScene& scene,
	                                Mesh::Ptr altMesh,
	                                const std::shared_ptr<ITexture2DArray>& inputImages,
	                                const std::shared_ptr<ITexture2DArray>& inputDepths,
	                                /*const std::shared_ptr<ITexture2DArray>& inputMasks,*/
	                                IRenderTarget& dst)
	{
		// Resize depth if needed.
		if(dst.w() != _depthRT->w() || dst.h() != _depthRT->h()) {
			_depthRT.reset(new RenderTargetRGBA32F(dst.w(), dst.h()));
		}
		// IMPORTANT: an assumption is made here (SR):
		// Full scene cameras are present first in the imgs_ulr vector.
		std::sort(imgs_ulr.begin(), imgs_ulr.end());

		Camera new_cam = eye;

		// First pass: render current camera positions and depth in rendertarget with full precision.
		glViewport(0, 0, _depthRT->w(), _depthRT->h());
		_depthRT->bind();
		glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
		glClearDepth(1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		_depthShader.begin();
		_proj.set(new_cam.viewproj());
		if (altMesh != nullptr)
		{
			altMesh->render(true, false);
		}
		else
		{
			scene.proxies()->proxy().render(true, false);
		}
		_depthShader.end();
		_depthRT->unbind();

		// Second pass.
		

		dst.clear();
		dst.bind();
		glViewport(0, 0, dst.w(), dst.h());

		_ulrShader.begin();

		// General settings.
		_occTest.set(_doOccl);
		_areMasksBinaryGL.set(_areMasksBinary);
		_doInvertMasksGL.set(_doInvertMasks);
		_discardBlackPixelsGL.set(_discardBlackPixels);
		_doMask.set(useMasks());

		// Current camera.
		_ncamPos.set(eye.position());

		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, _depthRT->texture());
		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_2D_ARRAY, inputImages->handle());
		glActiveTexture(GL_TEXTURE2);
		glBindTexture(GL_TEXTURE_2D_ARRAY, inputDepths->handle());

		_lastFullCamId.set(lastFullCamid);

		// Selected cameras.
		int usedCamerasCount = 0;
		//// First, full cameras.
		//for (int i = 0; i <= lastFullCamid; ++i) {
		//	const sibr::InputCamera & cam = scene.inputCameras()[imgs_ulr[i]];
		//	if (!cam.isActive()) {
		//		continue;
		//	}
		//	
		//	_icamIds[i].set(imgs_ulr[i]);
		//	
		//	_icamDir[i].set(cam.dir());
		//	_icamPos[i].set(cam.position());
		//	_icamProj[i].set(cam.viewproj());

		//	++usedCamerasCount;
		//}

		//_camCount.set(usedCamerasCount);
		//
		//RenderUtility::renderScreenQuad();

		//// Then other cameras.

		//usedCamerasCount = 0;
		// First, full cameras.
		for (int i = 0; i < std::min(imgs_ulr.size(), _numCams); ++i)
		{
			const InputCamera& cam = *scene.cameras()->inputCameras()[imgs_ulr[i]];
			if (!cam.isActive())
			{
				continue;
			}

			const int shiftedIndex = i; // -lastFullCamid - 1;
			_icamIds[shiftedIndex].set(imgs_ulr[i]);

			_icamDir[shiftedIndex].set(cam.dir());
			_icamPos[shiftedIndex].set(cam.position());
			_icamProj[shiftedIndex].set(cam.viewproj());

			++usedCamerasCount;
		}

		_camCount.set(usedCamerasCount);
		RenderUtility::renderScreenQuad();

		//

		/*if (useMasks()) {
			glActiveTexture(GL_TEXTURE3);
			glBindTexture(GL_TEXTURE_2D_ARRAY, inputMasks->handle());
		}*/

		//RenderUtility::renderScreenQuad();
		_ulrShader.end();
		dst.unbind();
	}
} /*namespace sibr*/
