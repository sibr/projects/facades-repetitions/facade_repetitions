/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#include "RepetitionsScene.hpp"

#include <core/scene/ProxyMesh.hpp>
#include <core/scene/InputImages.hpp>
#include <core/scene/CalibratedCameras.hpp>


namespace sibr {

	RepetitionsParseData::RepetitionsParseData(const std::string & basePath)
	{
		_basePathName = basePath;
		_meshPath = basePath + "/pmvs/models/pmvs_recon.ply";

		// Populate images infos.
		{
			const std::string listImages = basePath + "/list_images.txt";
			std::ifstream list_images(listImages);
			if (!list_images.is_open()) {
				SIBR_ERR << "Unable to load list_images file at path \"" << listImages << "\"." << std::endl;
			}
			// Load cameras "the old way", assuming numeric names.
			ImageListFile::Infos infos;
			while (true)
			{
				list_images >> infos.filename;
				if (infos.filename.empty()) break;
				list_images >> infos.width >> infos.height;
				infos.filename.erase(infos.filename.find_last_of("."), std::string::npos);
				infos.camId = atoi(infos.filename.c_str());
				_imgInfos.push_back(infos);
				infos.filename.clear();
			}
		}
		// Load active images
		const size_t currCount = _imgInfos.size();
		_activeImages = std::vector<bool>(currCount, true);
		ActiveImageFile activeImageFile;
		activeImageFile.setNumImages(currCount);
		if (activeImageFile.load(basePath + "/active_images.txt", false)) {
			for (int i = 0; i < currCount; i++) {
				if (!activeImageFile.active()[i]) {
					_activeImages[i] = false;
				}
			}
		}
		// Load excluded images
		ActiveImageFile excludeImageFile;
		excludeImageFile.setNumImages(currCount);
		if (excludeImageFile.load(basePath + "/exclude_images.txt", false)) {
			for (int i = 0; i < currCount; i++) {
				if (excludeImageFile.active()[i]) {
					_activeImages[i] = false;
				}
			}
		}

		// Populate num_cams and the matrices.
		parseBundlerFile(basePath + "/bundle.out");

		// We have to lie here.
		_datasetType = IParseData::Type::SIBR;
	}


	RepetitionsScene::RepetitionsScene(const std::string & basePath){

		_data.reset(new RepetitionsParseData(basePath));

		_proxies.reset(new ProxyMesh());
		_proxies->loadFromData(_data);
		
		// Setup cameras.
		_cams.reset(new CalibratedCameras());
		_cams->setupFromData(_data);
		
		// Recompute the clipping planes.
		std::vector<InputCamera::Ptr> inCams = _cams->inputCameras();
		std::vector<sibr::Vector2f> nearsFars;
		CameraRaycaster::computeClippingPlanes(_proxies->proxy(), inCams, nearsFars);
		_cams->updateNearsFars(nearsFars);

		// We have to setup images by hand because of baked path assumptions in the createFromData methods.
		_imgs.reset(new InputImages());
		std::vector<sibr::ImageRGB::Ptr> images(_data->numCameras());
		#pragma omp parallel for
		for (int cid = 0; cid < _data->numCameras(); ++cid) {
			const auto & cam = *_cams->inputCameras()[cid];
			images[cid] = sibr::ImageRGB::Ptr(new sibr::ImageRGB());
			images[cid]->load(basePath + "/" + cam.name() + ".jpg", false, true);
		}
		_imgs->loadFromExisting(images);
		
		// Setup the rendertargets.
		_renderTargets.reset(new RenderTargetTextures());
		//if (dontInitRts) {
		//	renderTargets()->initRGBandDepthTextureArrays(cameras(), images(), proxies(), SIBR_GPU_LINEAR_SAMPLING | SIBR_FLIP_TEXTURE, false);
		//}
	}



}
