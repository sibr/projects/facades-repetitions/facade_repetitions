/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */



#include "PlatonicMeshRefinement.hpp"
//#include "MultiViewPMSEnergy.hpp"
//#include "MedianBilateralFilter.hpp"

#include "PatchMatchStereo.hpp"
#include "DepthIterativeOptim.hpp"
#include "MultiViewPMSEnergy.hpp"
#include "StructureTensor.h"
#include "joint_bilateral_filter.hpp"
#include "KIPPIGraph.hpp"
#include <projects/facade_repetitions/renderer/RepetitionsScene.hpp>
#include <projects/ulr/renderer/ULRV2Renderer.hpp>
#include <core/graphics/Utils.hpp>
#include <core/graphics/Window.hpp>
#include <core/graphics/Input.hpp>
#include <core/system/Utils.hpp>
#include <core/raycaster/CameraRaycaster.hpp>
#include <omp.h>

#define MAX_DEPTH (10e10)


PlatonicMeshRefinement::PlatonicMeshRefinement(const sibr::Mesh::Ptr & mesh, const std::shared_ptr<FullScene> & fullScene, const std::shared_ptr<ReprojectionScene> & instanceScene)
{
	_mesh = mesh;
	_fullScene = fullScene;
	_instanceScene = instanceScene;
}

PlatonicMeshRefinement::~PlatonicMeshRefinement(void)
{
}

sibr::Mesh::Ptr PlatonicMeshRefinement::cleanMesh(const sibr::Mesh & refMesh, const std::vector<sibr::InputCamera::Ptr> & cameras, const float outborder) {
	sibr::Mesh::Ptr cleanedMesh = std::make_shared<sibr::Mesh>(false);

	std::vector<uint> deleteFacesIds;
	// As soon as it is outside of one camera, we discard it. Maybe too strong.
	for (unsigned int id = 0; id < refMesh.triangles().size(); ++id) {
		const sibr::Vector3u & face = refMesh.triangles()[id];
		bool keep = true;
		sibr::Vector3f v0 = refMesh.vertices()[face[0]];
		sibr::Vector3f v1 = refMesh.vertices()[face[1]];
		sibr::Vector3f v2 = refMesh.vertices()[face[2]];
		for (auto cam : cameras) {
			if (!cam->isActive()) { continue; }
			sibr::Vector3f proj0 = cam->project(v0);
			sibr::Vector3f proj1 = cam->project(v1);
			sibr::Vector3f proj2 = cam->project(v2);
			if ((std::abs(proj0[0]) > outborder || std::abs(proj0[1]) > outborder) ||
				(std::abs(proj1[0]) > outborder || std::abs(proj1[1]) > outborder) ||
				(std::abs(proj2[0]) > outborder || std::abs(proj2[1]) > outborder)) {
				keep = false;
				break;
			}
		}
		if (!keep) {
			deleteFacesIds.push_back(id);
		}
	}

	cleanedMesh->vertices(refMesh.vertices());
	cleanedMesh->normals(refMesh.normals());
	cleanedMesh->colors(refMesh.colors());
	cleanedMesh->triangles(refMesh.triangles());
	cleanedMesh->eraseTriangles(deleteFacesIds);

	return cleanedMesh;
}

void PlatonicMeshRefinement::saveDepthmap(const std::string & path, cv::Mat depthMap, const float scalingZ, const float biasZ) {
	sibr::ImageRGB::Pixel empty(0, 0, 0);
	sibr::ImageRGB finalImg(depthMap.rows, depthMap.cols, empty);
	for (uint py = 0; py < finalImg.h(); ++py) {
		for (uint px = 0; px < finalImg.w(); ++px) {
			const float depth = (float)(depthMap.at<cv::Vec3d>(px, py)[2] * scalingZ + biasZ);
			finalImg.color(px, py, sibr::ColorRGBA(depth, depth, depth, 1.0f));
		}
	}
	finalImg.save(path, false);

}


sibr::InputCamera PlatonicMeshRefinement::estimateFrontoParallelCamera(const std::vector<sibr::InputCamera::Ptr> & inputCameras, 
	const sibr::Vector3f & platonicNormal, const bool useClosestRealCam, int & selectedId, const int downScaling) {

	sibr::InputCamera frontoCam;
	
	if (useClosestRealCam) {
		
		if (selectedId < 0) {
			selectedId = 0;
			float angleCam = std::abs(inputCameras[0]->dir().dot(platonicNormal));
			for (int cami = 1; cami < inputCameras.size(); ++cami) {
				const float newAngle = inputCameras[cami]->dir().dot(platonicNormal);
				if (newAngle < angleCam) {
					selectedId = cami;
					angleCam = newAngle;
				}
			}
		} else {
			// Manual override.
		}
		std::cout << "Selected camera: " << selectedId << std::endl;
		frontoCam = *inputCameras[selectedId];
		frontoCam.size(frontoCam.w() / downScaling, frontoCam.h() / downScaling);
		frontoCam.aspect((float)frontoCam.w() / (float)frontoCam.h());
	} else {
		// Compute mesh centroid.
		sibr::Vector3f centroid(0.0f, 0.0f, 0.0f);
		for (size_t i = 0; i < _mesh->vertices().size(); ++i) {
			centroid += _mesh->vertices()[i];
		}
		centroid /= (float)_mesh->vertices().size();
		// Compute new avg fronto parallel camera.
		sibr::Vector3f avgUp(0.0f, 0.0f, 0.0f);
		float avgFov = 0.0f;
		unsigned int avgWidth = 0;
		unsigned int avgHeight = 0;
		float avgDistance = 0.0f;
		for (const auto & cam : inputCameras) {
			avgUp += cam->up();
			avgFov += cam->fovy();
			avgWidth = (std::max)(cam->w(), avgWidth);
			avgHeight = (std::max)(cam->h(), avgHeight);
			avgDistance += (centroid - cam->position()).norm();
		}
		const float denom = (float)inputCameras.size();
		avgUp /= denom;
		avgFov /= denom;
		avgDistance /= denom;

		frontoCam.setLookAt(centroid + avgDistance * platonicNormal, centroid, avgUp);
		frontoCam.fovy(avgFov);
		frontoCam.size(avgWidth / downScaling, avgHeight / downScaling);
		frontoCam.aspect((float)avgWidth / (float)avgHeight);
	}
	return frontoCam;
}


void PlatonicMeshRefinement::generateFrontoParallelColorMap(const sibr::InputCamera & frontoCam, sibr::ImageRGB & colorMap, const std::string & scenePath) {
	
	// Color map generation.
	sibr::ImageRGB backgroundIm(frontoCam.w(), frontoCam.h());

	sibr::Window window(frontoCam.w(), frontoCam.h(), "One-shot render");

	sibr::RepetitionsArgs args;
	args.dataset_path = scenePath; 
	args.win_width = frontoCam.w();
	args.win_height = frontoCam.h();
	sibr::BasicIBRScene::Ptr scene;
	if(args.newLayout) {
		scene.reset(new sibr::BasicIBRScene(args));
	} else {
		scene.reset(new sibr::RepetitionsScene(args.dataset_path));
	}
	

	// The input RTs will be init when the proxy is loaded, not before (empty proxy).
	//scene->loadProxy(true, true);
	scene->proxies()->replaceProxy(_mesh);
	sibr::ULRV2Renderer::Ptr ulr = sibr::ULRV2Renderer::Ptr(new sibr::ULRV2Renderer(scene->cameras()->inputCameras(), 1280, 720, 90));

	window.makeContextCurrent();
	std::vector<uint> imgs_ulr;
	for (int i = 0; i < scene->cameras()->inputCameras().size(); ++i) {
		imgs_ulr.push_back(i);
	}
	sibr::Mesh::Ptr altMesh = nullptr;

	sibr::Input::poll();

	window.viewport().bind();
	const uint destW = frontoCam.w();
	const uint destH = frontoCam.h();
	sibr::RenderTargetRGB dest(destW, destH);
	glViewport(0, 0, destW, destH);
	
	dest.bind();
	ulr->process( imgs_ulr, frontoCam, scene, altMesh, scene->renderTargets()->inputImagesRT(), dest);
	dest.readBack(backgroundIm);

	window.close();
	colorMap = backgroundIm.clone();
}

void PlatonicMeshRefinement::generateFrontoParallelDepthMap(const sibr::InputCamera & frontoCam, FrontoParallelInfos & infos)
{
	// Generate input fronto parallel Depth map and Color map.
	// SR: why this ? use directly the proxy.
	sibr::Mesh copyProxy;
	copyProxy.vertices(_mesh->vertices());
	copyProxy.normals(_mesh->normals());
	copyProxy.colors(_mesh->colors());
	copyProxy.triangles(_mesh->triangles());

	sibr::Raycaster raycaster;
	raycaster.addMesh(copyProxy);

	sibr::Vector3f dx, dy, upLeftOffset;
	sibr::CameraRaycaster::computePixelDerivatives(frontoCam, dx, dy, upLeftOffset);

	// All depth will be negative because in view space.
	const cv::Vec3d maxDepth(MAX_DEPTH, MAX_DEPTH, MAX_DEPTH);
	infos.depthMap = cv::Mat(frontoCam.w(), frontoCam.h(), CV_64FC3, maxDepth);
	infos.depthMask = cv::Mat(frontoCam.w(), frontoCam.h(), CV_8UC1, cv::Scalar(0));
	// For each pixel of the camera's image
	double minZ = std::numeric_limits<double>::max();
	double maxZ = std::numeric_limits<double>::lowest();

	for (uint py = 0; py < frontoCam.h(); ++py) {
		for (uint px = 0; px < frontoCam.w(); ++px) {
			sibr::Vector3f worldPos = ((float)px + 0.5f)*dx + ((float)py + 0.5f)*dy + upLeftOffset;
			// Cast a ray
			sibr::Vector3f dir = (worldPos - frontoCam.position());
			sibr::RayHit hit = raycaster.intersect(sibr::Ray(frontoCam.position(), dir));

			if (hit.hitSomething()) {
				// Get the intersection point in world space.
				sibr::Vector3f it(frontoCam.position() + hit.dist()*dir.normalized());
				sibr::Vector4f viewSpaceIt = frontoCam.view() * sibr::Vector4f(it[0], it[1], it[2], 1.0f);
				infos.depthMap.at<cv::Vec3d>(px, py) = cv::Vec3d(viewSpaceIt[0], viewSpaceIt[1], viewSpaceIt[2]);
				minZ = std::min(minZ, (double)viewSpaceIt[2]);
				maxZ = std::max(maxZ, (double)viewSpaceIt[2]);
				infos.depthMask.at<uchar>(px, py) = 255;

			} else {
				sibr::Vector3f it(frontoCam.position() + dir.normalized());
				sibr::Vector4f viewSpaceIt = frontoCam.view() * sibr::Vector4f(it[0], it[1], it[2], 1.0f);
				infos.depthMap.at<cv::Vec3d>(px, py) = cv::Vec3d(viewSpaceIt[0], viewSpaceIt[1], MAX_DEPTH);
			}
		}
	}

	infos.scaleZ = 1.0 / (maxZ - minZ);
	infos.biasZ = -minZ / (maxZ - minZ);

	// Compute an estimation of the worldspace step between two pixels in worldspace.
	// TODO (SR): clarify this, can use infos from raycaster instead.
	const sibr::Vector2f p1(infos.depthMap.at<cv::Vec3d>(100, 100)[0], infos.depthMap.at<cv::Vec3d>(100, 100)[1]);
	const sibr::Vector2f p2(infos.depthMap.at<cv::Vec3d>(200, 200)[0], infos.depthMap.at<cv::Vec3d>(200, 200)[1]);
	infos.stepP = (p2 - p1) / 100.0f;
	infos.originP = p1 - 100 * infos.stepP;

	// Depth map generation.
	for (uint py = 0; py < frontoCam.h(); ++py) {
		for (uint px = 0; px < frontoCam.w(); ++px) {
			double depth = infos.depthMap.at<cv::Vec3d>(px, py)[2];
			if (depth == MAX_DEPTH) {
				depth = minZ;
			}
			// All depths in viewspace are negative.
			infos.depthMap.at<cv::Vec3d>(px, py)[2] = depth;
		}
	}

}


void PlatonicMeshRefinement::saveColoredPCMap(const cv::Mat & PCMap, const cv::Mat & mask, const double bias, const double scale, const std::string & outputPath, const bool linearColors) {
	sibr::ImageRGB colorPCMap(PCMap.rows, PCMap.cols);

	for (int x = 0; x < PCMap.rows; ++x) {
		for (int y = 0; y < PCMap.cols; ++y) {
			if (mask.at<uchar>(x, y) > 128) {
				const double proba = (PCMap.at<double>(x, y) - bias) / scale;
				colorPCMap(x, y) = linearColors ? sibr::getLinearColorFromProbaV(proba) : sibr::jetColor<uchar>((float)proba);
			} else {
				colorPCMap(x, y) = sibr::Vector3ub(0, 0, 0);
			}
		}
	}
	colorPCMap.save(outputPath, false);
}

void PlatonicMeshRefinement::computePhotoConsistencyVisualisation(const int steps, const std::string & scenePath, const std::string & masksPath, const std::string & outputPath) {


	// Reference scene.
	sibr::RepetitionsArgs args;
	args.dataset_path = scenePath;
	args.win_width = _infos.frontoCam.w();
	args.win_height = _infos.frontoCam.h();
	sibr::BasicIBRScene::Ptr scene;
	if (args.newLayout) {
		scene.reset(new sibr::BasicIBRScene(args, true));
	}
	else {
		scene.reset(new sibr::RepetitionsScene(args.dataset_path));
	}

	// Initialisation loading all images and masks.
	std::vector<cv::Mat> images(_instanceScene->inputCameras().size());
	std::vector<cv::Mat> masks(_instanceScene->inputCameras().size());

	cv::Mat element = cv::getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(3, 3), cv::Point(1, 1));

	for (int cid = 0; cid < _instanceScene->inputCameras().size(); ++cid) {
		cv::Mat tempColor;
		scene->images()->inputImages()[cid]->toOpenCV().convertTo(tempColor, CV_64FC3, 1.0f / 255.0f);
		cv::transpose(tempColor, images[cid]);

		sibr::ImageL8 reflectionsMask;
		reflectionsMask.load(masksPath + "/" + sibr::imageIdToString(cid) + "_mask.png", false);
		cv::Mat tempMask;
		reflectionsMask.toOpenCV().convertTo(tempMask, CV_32FC1, 1.0f / 255.0f);
		// Reflective layers are white.
		cv::erode(tempMask, tempMask, element);
		cv::GaussianBlur(tempMask, tempMask, cv::Size(3, 3), 0.0, 0.0);
		cv::transpose(tempMask, masks[cid]);
	}

	

	PhotoconsistencyEnergy energy(scene, images, 0.5, _infos.frontoCam, _infos.colorMap);
	DepthMapEnergy::DepthData ** depthMap = new DepthMapEnergy::DepthData*[_infos.depthMap.rows];
	for (int x = 0; x < _infos.depthMap.rows; ++x) {
		depthMap[x] = new DepthMapEnergy::DepthData[_infos.depthMap.cols];
		for (int y = 0; y < _infos.depthMap.cols; ++y) {
			const cv::Vec3d position = _infos.depthMap.at<cv::Vec3d>(x, y);
			depthMap[x][y] = Eigen::Vector3d(position[0], position[1], position[2]);
		}
	}
	energy.registerDepthMap(depthMap);

	// First photoconsistency map, on mesh surface.
	const unsigned int rows = _infos.colorMap.rows;
	const unsigned int cols = _infos.colorMap.cols;
	cv::Mat realPCMap(rows, cols, CV_64FC1);
	for (unsigned int x = 0; x <rows; ++x) {
		for (unsigned int y = 0; y < cols; ++y) {
			realPCMap.at<double>(x, y) = energy.getCostReal(x, y);
		}
	}

	// Compute visualisation scaling. TODO (SR): do it with the loop above.
	double mini = 0.0;
	double maxi = 0.0;
	for (unsigned int x = 0; x <rows; ++x) {
		for (unsigned int y = 0; y < cols; ++y) {
			if (_infos.depthMask.at<uchar>(x, y) > 128) {
				const double newVal = realPCMap.at<double>(x, y);
				mini = std::min(mini, newVal);
				maxi = std::max(maxi, newVal);
			}
		}
	}
	const double probaScale = maxi - mini;

	saveColoredPCMap(realPCMap, _infos.depthMask, mini, probaScale, outputPath + "/real-PC.png", false);
	
	// Compute PC in depth volume.
	const std::string depthVol = outputPath + "/depth-volume/";
	sibr::makeDirectory(depthVol);
	
	for (int i = -10; i <= steps + 10; ++i) {
		const double currentTestDepth = (-_infos.biasZ/ _infos.scaleZ) + i * (1.0 / _infos.scaleZ) / (float)steps;
		cv::Mat PCMap(rows, cols, CV_64FC1);
		for (int x = 0; x < (int)rows; ++x) {
			for (int y = 0; y < (int)cols; ++y) {
				PCMap.at<double>(x, y) = energy.getCostArbitrary(x, y, currentTestDepth);
			}
		}
		saveColoredPCMap(PCMap, _infos.depthMask, mini, probaScale, depthVol + "/arbitrary_" + std::to_string(currentTestDepth) + ".png", true);
	}

	// Compute per-instance PC.
	for (unsigned int wid = 0; wid < _fullScene->windowsCount; ++wid) {
		// Find correct input cameras.
		std::vector<bool> selectedCams(_fullScene->windowsIds.size(), false);
		if (_fullScene->windowsIds.size() != scene->cameras()->inputCameras().size()) {
			SIBR_ERR << "Mismatch in cameras numbers, investigate." << std::endl;
		}
		unsigned int cid = 0;
		for (auto & pair : _fullScene->windowsIds) {
			selectedCams[cid] = (pair.first == wid);
			++cid;
		}

		cv::Mat realSubPCMap(rows, cols, CV_64FC1);
		for (int x = 0; x < (int)rows; ++x) {
			for (int y = 0; y < (int)cols; ++y) {
				realSubPCMap.at<double>(x, y) = energy.getCostReal(x, y, selectedCams);
			}
		}
		saveColoredPCMap(realSubPCMap, _infos.depthMask, mini, probaScale, outputPath + "/real-sub-" + std::to_string(wid) + "PC.png", false);
	}

	// Cleaning depth map.
	for (unsigned int x = 0; x < rows; ++x) {
		delete[] depthMap[x];
		depthMap[x] = NULL;
	}
	depthMap = NULL;
}

void PlatonicMeshRefinement::computeFrontoParallelInfos(const sibr::Matrix4f & estimatedTransformation, const std::string & referenceScenePath, const bool useClosestCam, const int scalingDown, const std::string & outputPath) {

	// Frame for the mesh.
	const sibr::Vector3f normal = _fullScene->facadePlane.pNormal;
	const sibr::Vector3f platonicNormal = (estimatedTransformation.transpose() * sibr::Vector4f(normal[0], normal[1], normal[2], 0.0f)).xyz().normalized();

	// Estimate fronto parallel camera.
	int camId = -1;
	_infos.frontoCam = estimateFrontoParallelCamera(_instanceScene == nullptr ? _fullScene->cameras : _instanceScene->inputCameras(), platonicNormal, useClosestCam, camId, scalingDown);
	_infos.camId = camId;
	// Depth map.
	generateFrontoParallelDepthMap(_infos.frontoCam, _infos);

	// Color map: either input image if closest input cam, or ULR rendering if interpolated.
	sibr::ImageRGB inputColorMap;
	if (useClosestCam) {
		// Avoid loading the complete scene for one input image.
		sibr::ImageRGB tempInputColorMap;
		// If we try superpixel refinement on full scene, use image directly.
		if (_instanceScene == nullptr) {
			tempInputColorMap = _fullScene->images[camId].clone();
		} else {
			const std::string colormapPath = referenceScenePath + "/" + sibr::imageIdToString(camId) + ".jpg";
			tempInputColorMap.load(colormapPath, true, true);
		}

		inputColorMap = tempInputColorMap.resized(_infos.frontoCam.w(), _infos.frontoCam.h());
	} else {
		generateFrontoParallelColorMap(_infos.frontoCam, inputColorMap, referenceScenePath);
	}

	cv::Mat colorMap;
	cv::transpose(inputColorMap.toOpenCV(), colorMap);
	_infos.colorMap = colorMap;

	if (!outputPath.empty()) {
		saveDepthmap(outputPath + "/out_test_depth.png", _infos.depthMap, (float)_infos.scaleZ, (float)_infos.biasZ);
		inputColorMap.save(outputPath + "/out_test_color.png", false);
	}
}

void PlatonicMeshRefinement::refineUsingSuperpixels(const sibr::Matrix4f & estimatedTransfo, const std::string & outputPath) {
	if (_infos.camId < 0) { 
		SIBR_WRG << "Not using an input camera." << std::endl;
		return; 
	}
	std::string baseName;
	if(_instanceScene == nullptr) {
		baseName = "segmentation-full/" + _fullScene->cameras[_infos.camId]->name();
		baseName = baseName.substr(0, baseName.find_last_of("."));
	} else {
		baseName = "segmentation/" + sibr::imageIdToString(_infos.camId);
	}

	const std::string graphPath = outputPath + "/../" + baseName + "_graph.txt";
	  
	//const std::string imgRGBPath = outputPath + "/../IBR_original_OPENMVG-CMPMVS/" + baseName + ".jpg";
	//const std::string imgDepthPath = outputPath + "/../mesh_cleaning/out_test_depth.png";

	// Load image, depthmap.
	cv::Mat tempColorMap;
	cv::transpose(_infos.colorMap, tempColorMap);
	sibr::ImageRGB imgRGB;
	imgRGB.fromOpenCV(tempColorMap);

	// Load segmentation.
	KIPPIGraph graph(graphPath);

	// Compute avg/median infos.
	sibr::ImageRGB imgDebug = imgRGB.clone();
	std::vector<std::vector<float>> candidatesDepths(graph.superPixels().size());

	for (unsigned int sidd = 0; sidd < graph.superPixels().size(); ++sidd) {
		auto & sp = graph.superPixels()[sidd];
		const sibr::Vector2i bmin = sp.bbox.min();
		const sibr::Vector2i bmax = sp.bbox.max();

		
		std::vector<sibr::Vector3f> candidatesColors;
		for ( int y = bmin[1]; y <= bmax[1]; ++y) {
			for ( int x = bmin[0]; x <= bmax[0]; ++x) {
				if (x < 0 || x >= (int)graph.w() || y < 0 || y >= (int)graph.h()) {
					continue;
				}
				if (_infos.depthMask.at<uchar>(x, y) < 128) {
					continue;
				}
				if (graph.isInside(sp, x, y)) {
					// Should we only consider mesh points instead of pixels?
					candidatesDepths[sidd].push_back(float(_infos.depthMap.at<cv::Vec3d>(x, y)[2]));
					const cv::Vec3b col = _infos.colorMap.at<cv::Vec3b>(x, y);
					candidatesColors.emplace_back(float(col[0]) / 255.0f, float(col[1]) / 255.0f, float(col[2]) / 255.0f);
				}
			}
		}
		// Average color.
		sibr::Vector3f avgColor(0.0f, 0.0f, 0.0f);
		for (unsigned int cid = 0; cid < candidatesColors.size(); ++cid) {
			avgColor += candidatesColors[cid];
		}
		if (!candidatesColors.empty()) {
			avgColor /= (float)candidatesColors.size();
		}
		sp.avgColor = avgColor;
		
	}


	for (unsigned int sidd = 0; sidd < graph.superPixels().size(); ++sidd) {
		
		auto & sp = graph.superPixels()[sidd];
		std::vector<float> augmentedCandidateDepths = candidatesDepths[sidd];

		if (!augmentedCandidateDepths.empty()) {

			// Median depth.
			sp.noDepth = false;
			std::sort(augmentedCandidateDepths.begin(), augmentedCandidateDepths.end());
			const float medianDepth = augmentedCandidateDepths[augmentedCandidateDepths.size() / 2];

			// Initial position
			const sibr::Vector2f center2D = _infos.originP + sp.bbox.center().cast<float>().eval().cwiseProduct(_infos.stepP);
			const sibr::Vector4f centerViewSpace(center2D.x(), center2D.y(), medianDepth, 1.0f);
			const sibr::Vector3f centerWorldSpace = (_infos.frontoCam.view().inverse() * centerViewSpace).xyz();
			sp.current3D = centerWorldSpace;
			
		}
		
		
	
	}


	// 3D mesh points covered by each superpixel.
	std::vector<std::vector<sibr::Vector3f>> points3D(graph.superPixels().size());
	for (size_t vid = 0; vid < _mesh->vertices().size(); ++vid) {
		const sibr::Vector3f projv = _infos.frontoCam.projectScreen(_mesh->vertices()[vid]);
		for (unsigned int sid = 0; sid < graph.superPixels().size(); ++sid) {
			if (graph.isInside(sid, (unsigned int)projv[0], (unsigned int)projv[1])) {
				points3D[sid].push_back(_mesh->vertices()[vid]);
				break;
			}
		}
	}
	
	// "Augment" infos for superpixels with low 3D points count (which denote low confidence anyway).
	for (unsigned int sidd = 0; sidd < graph.superPixels().size(); ++sidd) {
		auto & sp = graph.superPixels()[sidd];
		sp.points3D = points3D[sidd];
		if (sp.points3D.size() < 100) {
			for (const unsigned int nid : sp.neighbours) {
				for (const sibr::Vector3f point : points3D[nid]) {
					sp.points3D.push_back(point);
				}
				
			}
		}
	}

	// Basic infos.
	const sibr::Vector3f normal = _fullScene->facadePlane.pNormal;
	const sibr::Vector3f platonicNormal = (estimatedTransfo.transpose() * sibr::Vector4f(normal[0], normal[1], normal[2], 0.0f)).xyz().normalized();
	const double distanceNormalization = 1.0 / _mesh->getBoundingBox().diagonal().norm();
	//std::cout << 1.0 / distanceNormalization << std::endl;

	// Compute 3D centroid related to mesh points.
	for (auto & sp : graph.superPixels()) {
		sibr::Vector3f centroid(0.0f, 0.0f, 0.0f);
		float minZ = 0.0;
		float maxZ = 0.0;
		bool first = true;
		for (const sibr::Vector3f & point3D : sp.points3D) {
			centroid += point3D;

			const float currZ = point3D.dot(platonicNormal);
			if (first) {
				minZ = currZ;
				maxZ = currZ;
				first = false;
			} else {
				minZ = std::min(minZ,currZ);
				maxZ = std::max(maxZ,currZ);
			}
		}
		if (!sp.points3D.empty()) {
			centroid /= (float)sp.points3D.size();
		}
		sp.centroid3D = centroid;
		sp.maxZ = maxZ;
		sp.minZ = minZ;
		

		for (size_t id = 0; id < sp.vertices.size(); ++id) {
			const uint id0 = sp.vertices[id];
			const uint id1 = sp.vertices[(id + 1) % sp.vertices.size()];
			const sibr::Vector2i p0 = graph.vertices()[id0].cast<int>();
			const sibr::Vector2i p1 = graph.vertices()[id1].cast<int>();
			

			sibr::Vector2f deltas((p1 - p0).cast<float>());
			sibr::Vector2f dirs = deltas.unaryExpr([](float f) { return f >= 0 ? 1 : -1; });
			deltas = deltas.cwiseAbs();

			float delta = std::abs(deltas.y() / deltas.x());
			float error = deltas.x() - deltas.y();

			sibr::Vector2i p = p0;

			bool isDone = (p == p1);

			while (true) {
				imgDebug(p) = sibr::Vector3ub(255, 0, 0);
				if (isDone) { break; }

				float error2 = 2.0f*error;
				if (error2 >= -deltas.y()) {
					error -= deltas.y();
					p.x() += int(dirs.x());
				}
				if (error2 < deltas.x()) {
					error += deltas.x();
					p.y() += int(dirs.y());
				}
				isDone = (p == p1);
			}
		}
	}

	// -- Optimization --
	
	const unsigned int iterationCount = 100;
	std::random_device rdDev;
	std::mt19937 rdMT(rdDev());

	const auto energy = [&platonicNormal, &graph, distanceNormalization](const KIPPIGraph::KIPPISuperPixel & superPixel, const sibr::Vector3f & current3D)  {

		const double positionTerm = std::abs((current3D - superPixel.centroid3D).dot(platonicNormal)) * distanceNormalization;

		double neighborhoodTerm = 0.0;
		double neighborhoodDenom = 0.0;
		// Consider neighbours of neighbours? (for glass for instance)
		for (const unsigned int neighbourId : superPixel.neighbours) {
			const auto & neighSuperPixel = graph.superPixels()[neighbourId];
			if (neighSuperPixel.noDepth) {
				continue;
			}
			const double neighbourSubterm = std::abs((current3D - neighSuperPixel.current3D).dot(platonicNormal)) * distanceNormalization;
			const double colorProximity = (superPixel.avgColor - neighSuperPixel.avgColor).norm();
			const double weight = exp(-colorProximity);
			neighborhoodTerm += weight*neighbourSubterm;
			neighborhoodDenom += weight;

			for (const unsigned int neighbourneighbourId : neighSuperPixel.neighbours) {
				if (superPixel.neighbours.count(neighbourneighbourId) > 0) {
					continue;
				}
				const auto & neighneighSuperPixel = graph.superPixels()[neighbourneighbourId];
				if (neighneighSuperPixel.noDepth) {
					continue;
				}
				const double neighneighbourSubterm = std::abs((current3D - neighneighSuperPixel.current3D).dot(platonicNormal)) * distanceNormalization;
				const double neighcolorProximity = (superPixel.avgColor - neighneighSuperPixel.avgColor).norm();
				const double neighweight = exp(-neighcolorProximity);
				neighborhoodTerm += neighweight*neighneighbourSubterm;
				neighborhoodDenom += neighweight;
			}
		}
		if (neighborhoodDenom != 0.0) {
			neighborhoodTerm /= neighborhoodDenom;
		}
		//std::cout << positionTerm << neighborhoodTerm << std::endl;
		return positionTerm + 15.0*neighborhoodTerm;
	};

	for(unsigned int it = 0; it < iterationCount; ++it){
		std::cout << "Iteration " << (it+1) << std::endl;
		
		// Shuffle superpixels via an indices vector.
		// We don't want to modify the graph and superpixels for this.
		std::vector<unsigned int> shuffledIndices(graph.superPixels().size());
		for(unsigned int sid = 0; sid < shuffledIndices.size(); ++sid){
			shuffledIndices[sid] = sid;
		}
		// Fisher - Yates shuffling.
 		for(unsigned int sid = (unsigned int)shuffledIndices.size()-1; sid > 0; --sid){
			std::uniform_int_distribution<unsigned int> rdDist(0, sid);
			const unsigned int sjd = rdDist(rdMT);
			 if(sid == sjd) {
				 continue;
			 }
			std::swap(shuffledIndices[sid], shuffledIndices[sjd]);
		}
		
		// Iterate over shuffled superpixels.
		size_t count = 0;
		for(unsigned int sid = 0; sid < shuffledIndices.size(); ++sid){
			const unsigned int superPixelId = shuffledIndices[sid];
			auto & superPixel = graph.superPixels()[superPixelId];
			if (superPixel.noDepth) {
				continue;
			}
			double currentEnergy = energy(superPixel, superPixel.current3D);
			
			// Try all positions from list.
			bool changed = false;
			sibr::Vector3f current3D = superPixel.current3D;
			for (const sibr::Vector3f position : superPixel.points3D) {
				const sibr::Vector3f candidate3D = superPixel.current3D + (position - superPixel.current3D).dot(platonicNormal) * platonicNormal;
				const double newEnergy = energy(superPixel, candidate3D);
				if (newEnergy < currentEnergy) {
					currentEnergy = newEnergy;
					current3D = candidate3D;
					changed = true;
				}
			}
			// Iterate over depths along the normal axis.
#define SIBR_PLATONIC_REFINEMENT_THREADS 8

			double currentEnergies[SIBR_PLATONIC_REFINEMENT_THREADS];
			sibr::Vector3f current3Ds[SIBR_PLATONIC_REFINEMENT_THREADS];
			bool changeds[SIBR_PLATONIC_REFINEMENT_THREADS];
			for (int i = 0; i < SIBR_PLATONIC_REFINEMENT_THREADS; ++i) {
				currentEnergies[i] = currentEnergy;
				current3Ds[i] = current3D;
				changeds[i] = false;
			}


#pragma omp parallel num_threads(SIBR_PLATONIC_REFINEMENT_THREADS)
			{
				const int tid = omp_get_thread_num();
				const float lowBound = -100.0f + tid*(200.0f) / SIBR_PLATONIC_REFINEMENT_THREADS;
				const float highBound = lowBound + (200.0f) / SIBR_PLATONIC_REFINEMENT_THREADS;
				for (float nz = lowBound; nz <= highBound; nz += 0.01f) {

					const float newDepth = superPixel.minZ + nz * (superPixel.maxZ - superPixel.minZ);
					const sibr::Vector3f candidate3D = superPixel.current3D + (newDepth - superPixel.current3D.dot(platonicNormal)) * platonicNormal;
					const double newEnergy = energy(superPixel, candidate3D);
					if (newEnergy < currentEnergies[tid]) {

						currentEnergies[tid] = newEnergy;
						current3Ds[tid] = candidate3D;
						changeds[tid] = true;
					}
				}
			}
#pragma omp barrier
			for (int i = 0; i < SIBR_PLATONIC_REFINEMENT_THREADS; ++i) {
				if (changeds[i]) {
					changed = true;
					break;
				}
			}

			if (changed) {

				for (int i = 0; i < SIBR_PLATONIC_REFINEMENT_THREADS; ++i) {
					if (changeds[i]) {
						if (currentEnergies[i] < currentEnergy) {
							currentEnergy = currentEnergies[i];
							current3D = current3Ds[i];
						}
					}
				}
				superPixel.current3D = current3D;
				++count;
			}

		}
		if (count == 0) {
			// Early stop.
			std::cout << "Early stop." << std::endl;
			break;
		}
		std::cout << "Moved " << count << " times." << std::endl;
	}
	// -- Export --------

	sibr::Mesh pixGrid;
	sibr::Mesh::Vertices vertices;
	sibr::Mesh::Triangles triangles;

	std::vector<int> shiftIndices;
	for (auto & sg : graph.superPixels()) {
		if (sg.noDepth) {
			shiftIndices.push_back(-1);
			continue;
		}
		// Compute the centroid world space position.

		const unsigned int vi = (unsigned int)vertices.size();

		const Eigen::Hyperplane<float, 3>	superpixelPlane(platonicNormal, sg.current3D);
		// Store the index of the first vertex of the current superpixel, in the final mesh.
		shiftIndices.push_back(vi);
		for (int i = 0; i < sg.vertices.size(); ++i) {
			const unsigned int vid = sg.vertices[i];
			const sibr::Vector2f vert = graph.vertices()[vid];
			// Shhot a ray from the center of the frontoCam, through pixel at coords vert, and intersect plane define by centroid and facade normal.
			const sibr::Vector3f ray = sibr::CameraRaycaster::computeRayDir(_infos.frontoCam, vert);

			const Eigen::ParametrizedLine<float, 3> rayLine(_infos.frontoCam.position(), ray);

			const sibr::Vector3f point = rayLine.intersectionPoint(superpixelPlane);
			vertices.push_back(point);

			if (i > 0 && i < sg.vertices.size() - 1) {
				triangles.emplace_back(vi, vi + i + 1, vi + i);
			}
		}
	}

	{
		//struct VertexInstance {

		//	sibr::Vector3f pos;
		//	unsigned int localId;
		//	unsigned int globalId;

		//	VertexInstance() {
		//		
		//	}

		//	VertexInstance(const sibr::Vector3f & pos_, const unsigned int localId_, const unsigned int globalId_) {
		//		pos = pos_;
		//		localId = localId_;
		//		globalId = globalId_;
		//	}
		//};

		//for (unsigned int vid = 0; vid < graph.vertices().size(); ++vid){
		//	std::map<unsigned int, VertexInstance> instances;
		//	
		//	// Find all instances of this vertex.
		//	for (unsigned int sid = 0; sid < graph.superPixels().size(); ++sid) {
		//		if (shiftIndices[sid] == -1) {
		//			continue;
		//		}
		//		const auto & sp = graph.superPixels()[sid];
		//		for (unsigned int svid = 0; svid < sp.vertices.size(); ++svid) {

		//			if (vid == sp.vertices[svid]) {
		//				const unsigned int globalId = shiftIndices[sid] + svid;
		//				const VertexInstance vertInst(vertices[globalId], svid, globalId);
		//				instances[sid] = vertInst;
		//			}
		//		}
		//	}
		//	std::cout << "Vertex " << vid << ":" << instances.size() << " instances." << std::endl;
		//	

		//	sibr::Vector3f avgPos(0.0f,0.0f,0.f);
		//	for (const auto & nvi : instances) {		
		//		avgPos += nvi.second.pos;		
		//	}
		//	avgPos /= instances.size();
		//	
		//	for (const auto & nvi : instances) {
		//		if ((nvi.second.pos - avgPos).norm() > 1.0/distanceNormalization/300.0) {
		//			continue;
		//		}
		//		vertices[nvi.second.globalId] = avgPos;
		//	}

		//	
		//	
		//}

	}

	for (auto & eg : graph.edges()) {
		const int globalIndexA = shiftIndices[eg.a];
		const int globalIndexB = shiftIndices[eg.b];
		if (globalIndexA == -1 || globalIndexB == -1) {
			continue;
		}

		unsigned int localIndex1A = 0;
		unsigned int localIndex2A = 0;
		const auto & fa = graph.superPixels()[eg.a];
		for (unsigned int ia = 0; ia < fa.vertices.size(); ++ia) {
			if (fa.vertices[ia] == eg.v1) {
				localIndex1A = globalIndexA + ia;
			}
			if (fa.vertices[ia] == eg.v2) {
				localIndex2A = globalIndexA + ia;
			}
			// We could design an early exit, but faces have < 10 vertices most of the time.
		}

		int localIndex1B = 0;
		int localIndex2B = 0;
		const auto & fb = graph.superPixels()[eg.b];
		for (unsigned int ib = 0; ib < fb.vertices.size(); ++ib) {
			if (fb.vertices[ib] == eg.v1) {
				localIndex1B = globalIndexB + ib;
			}
			if (fb.vertices[ib] == eg.v2) {
				localIndex2B = globalIndexB + ib;
			}
		}

		// We build two triangles.
		triangles.emplace_back(localIndex1A, localIndex1B, localIndex2A);
		triangles.emplace_back(localIndex2A, localIndex1B, localIndex2B);
	}

	// We still need to fill holes because one initial vertex can exist in n world positions if it is shared between n faces.
	for (size_t vid = 0; vid < graph.vertices().size(); ++vid) {
		// Find all faces and relative ids related to this vertex.
		std::vector<unsigned int> localIndices;
		for (unsigned int sid = 0; sid < graph.superPixels().size(); ++sid) {
			const auto & vts = graph.superPixels()[sid].vertices;
			for (unsigned int lvid = 0; lvid < vts.size(); ++lvid) {
				if (vts[lvid] == vid && shiftIndices[sid] != -1) {
					localIndices.push_back(shiftIndices[sid] + lvid);
				}
			}
		}
		if (localIndices.size() < 3){
			continue;
		}
		// We need to find a way to sort them? as long as we only have one triangle it's ok, though.
		for (size_t i = 1; i < localIndices.size() - 1; ++i){
			triangles.emplace_back(localIndices[0], localIndices[i+1], localIndices[i]);
		}
	}
	
	pixGrid.vertices(vertices);
	pixGrid.triangles(triangles);
	pixGrid.save(outputPath + "/out_mesh_regions_proj-smooth.ply", true);

	imgDebug.save(outputPath + "/debug-avgcolors.png", false);
}


