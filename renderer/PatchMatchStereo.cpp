/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#include "PatchMatchStereo.hpp"

#include <iostream>


#include <core/raycaster/CameraRaycaster.hpp>


PatchMatchStereo::PatchMatchStereo(const cv::Mat initialDepthMap, DepthMapEnergy * energy, const double minZ, const double maxZ)
{
	_energy = energy;
	_w = initialDepthMap.rows;
	_h = initialDepthMap.cols;
	_deltaZ = std::abs(maxZ - minZ);

	_depthMap = new DepthMapEnergy::DepthData*[_w];
	for (int x = 0; x < _w; ++x) {
		_depthMap[x] = new DepthMapEnergy::DepthData[_h];
		for (int y = 0; y < _h; ++y) {
			const cv::Vec3d position = initialDepthMap.at<cv::Vec3d>(x, y);
			_depthMap[x][y] = Eigen::Vector3d(position[0], position[1], position[2]);
		}
	}
	_energy->registerDepthMap(_depthMap);

	_costMap = cv::Mat(_w, _h, CV_64FC1, std::numeric_limits<double>::max());
	for (int x = 0; x < _w; ++x) {
		for (int y = 0; y < _h; ++y) {
			_costMap.at<double>(x, y) = _energy->getCost(x, y, 0.0);
		}
	}

	std::random_device rd;
	_randomMT = std::mt19937(rd());
	_randomDistri = std::uniform_real_distribution<double>(0, 1);

	
	
}

PatchMatchStereo::~PatchMatchStereo(void)
{
	for (int x = 0; x < _w; ++x) {
		delete[] _depthMap[x];
		_depthMap[x] = NULL;
	}
	_depthMap = NULL;
}



void PatchMatchStereo::solve(const int iteration_count)
{

	for (size_t it = 0; it < iteration_count; ++it) {
		std::cout << "[PMS] Iteration " << (it+1) << "/" << iteration_count << "." << std::endl;
		// Alternate between top-left and bottom-right propagation.
		int x_low = 1; int x_up = _w; int x_incr = 1;
		int y_low = 1; int y_up = _h; int y_incr = 1;
		if (it % 2 == 1) {
			x_low = _w - 2; x_up = -1; x_incr = -1;
			y_low = _h - 2; y_up = -1; y_incr = -1;
		}

		const int y_first_row = y_low - y_incr;
		const int x_first_col = x_low - x_incr;

		std::cout << "[PMS] Propagation." << std::endl;
		// First row.
		for (int x = x_low; x != x_up; x += x_incr) {
			// We only test horizontal neighbour.
			compareCosts(x, y_first_row, x - x_incr, y_first_row);
		}

		// Other rows.
		for (int y = y_low; y != y_up; y += y_incr) {

			// First column.
			// We only test vertical neighbour.
			compareCosts(x_first_col, y, x_first_col, y - y_incr);
			
			// Other columns.
			for (int x = x_low; x != x_up; x += x_incr) {
				
				// Horizontal neighbour.
				compareCosts(x, y, x - x_incr, y);
				// Vertical neighbour.
				compareCosts(x, y, x, y - y_incr);
			}

		}


		std::cout << "[PMS] Refinement." << std::endl;
		// Introduce random modifications with a smaller and smaller delta.
		//planeRandomRefinement(_deltaZ * 0.25, _deltaZ * 0.01);

		
	}
}


void PatchMatchStereo::compareCosts(int x0, int y0, int xneigh, int yneigh)
{
	const double newDepth = _depthMap[xneigh][yneigh][2];
	const double newCost = _energy->getCost(x0, y0, newDepth - _depthMap[x0][y0][2]);
	
	if (newCost < _costMap.at<double>(x0, y0)) {
		_depthMap[x0][y0][2] = newDepth;
		_costMap.at<double>(x0, y0) = newCost;
	} 
 }



void PatchMatchStereo::planeRandomRefinement(const double deltaMax, const double deltaMin)
{
	double deltaCurrent = deltaMax;
	while (deltaCurrent >= deltaMin) {

		for (int x = 0; x < _w; ++x) {
			for (int y = 0; y < _h; ++y) {

				const double depthShift = random(deltaCurrent);
				const double disturbCost = _energy->getCost(x, y, depthShift);
				if (disturbCost < _costMap.at<double>(x, y)) {
					_depthMap[x][y][2] += depthShift;
					_costMap.at<double>(x, y) = disturbCost;
				} 
			}
		}

		deltaCurrent /= 2.0;

	}
}

cv::Mat PatchMatchStereo::getDepthMap() {
	cv::Mat finalDepthMap(_w, _h, CV_64FC3);
	for (int py = 0; py < _h; ++py) {
		for (int px = 0; px < _w; ++px) {
			const sibr::Vector3d pos = _depthMap[px][py];
			finalDepthMap.at<cv::Vec3d>(px, py) = cv::Vec3d(pos[0], pos[1], pos[2]);
		}
	}
	return finalDepthMap;
}

double PatchMatchStereo::random(const double delta)
{
	return _randomDistri(_randomMT)*(2.0*delta) - delta;
}
