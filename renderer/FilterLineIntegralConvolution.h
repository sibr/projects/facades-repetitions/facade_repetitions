/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#ifndef SIBR_FILTERLINEINTEGRALCONVOLUTION_H
#define SIBR_FILTERLINEINTEGRALCONVOLUTION_H
#include "Config.hpp"

#include <Eigen/Core>

#include <vector>
#include <utility>      // std::pair, std::make_pair

#include <opencv2/core.hpp>
#include <opencv2/core/ocl.hpp>
#include <opencv2/core/base.hpp>
#include <opencv2/core/utility.hpp>
#include <opencv2/core/cvdef.h>
#include <opencv2/core/core_c.h>
#include <opencv2/imgproc.hpp>

class SIBR_FACADE_REPETITIONS_EXPORT FilterLineIntegralConvolution
{   
    std::vector<std::pair<double,double> > findNeighbour(Eigen::Vector2d **,int,int, int,int,int);
    int computeConvolution(cv::Mat, std::vector<std::pair<double,double> >);
    int computeConvolutionGrey(cv::Mat, std::vector<std::pair<double,double> >);

public:

    FilterLineIntegralConvolution();
    // Generates a matrix with the LIC representation of the field
    cv::Mat filter(Eigen::Vector2d **, int, int, int);
	static double angle(Eigen::Vector2d const v);
	cv::Mat filterWithImage(Eigen::Vector2d **, int, int, int, cv::Mat);

};

#endif // SIBR_FILTERLINEINTEGRALCONVOLUTION_H
