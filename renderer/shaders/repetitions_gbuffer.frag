/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#version 420

layout(location=0) out vec3 out_pos;
layout(location=1) out vec3 out_normal;

in vec3 vertNormalWorld;
in vec3 vertPositionWorld;


void main(void) {
	out_pos = vertPositionWorld;
	out_normal = vertNormalWorld;
}

/*
void main(void) {
	float masking = texture(mask, gl_FragCoord.xy/textureSize(mask,0).xy).a;
	if(masking < 0.5){
		discard;
	}
	// Computed reflected vector in world space.
	vec3 v = normalize(vertPositionWorld - cameraPositionWorld);
	vec3 n = planeNormal;
	vec3 r = reflect(v, n);
	vec3 reflectionColor = texture(envmap,cartesianToSpherical(normalize(r))).rgb;
	out_color.rgb = reflectionColor;//vec3(1.0,0.0,0.0);

}*/
