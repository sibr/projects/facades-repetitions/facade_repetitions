/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#version 420

#define NUM_CAMS (12)

in vec2 vertex_coord;

layout(binding=0) uniform sampler2D proxy;
layout(binding=1) uniform sampler2DArray inputImages;
layout(binding=2) uniform sampler2DArray inputDepths;
layout(binding=3) uniform sampler2DArray inputMasks;

uniform bool occ_test;
uniform bool invert_mask;
uniform bool is_binary_mask;
uniform bool discard_black_pixels;
uniform bool doMasking;

uniform vec3 ncam_pos;

uniform vec3 icam_pos[NUM_CAMS];
uniform vec3 icam_dir[NUM_CAMS];
uniform mat4 icam_proj[NUM_CAMS];

uniform int cam_count;
uniform int last_full_id;
uniform int icam_ids[NUM_CAMS];

layout(location = 0) out vec4 out_color;


#define INFTY_W 100000.0
#define EPSILON 1e-2
#define BETA 	1e-1  	/* Relative importance of resolution penalty */
#define FOV_BLENDING_BORDER 0.6

float interpolate(float val, float y0, float x0, float y1, float x1) {
		return (val - x0)*(y1 - y0) / (x1 - x0) + y0;
}
	//
float base(float val) {
		if (val <= -0.75) return 0;
		else if (val <= -0.25) return interpolate(val, 0.0, -0.75, 1.0, -0.25);
		else if (val <= 0.25) return 1.0;
		else if (val <= 0.75) return interpolate(val, 1.0, 0.25, 0.0, 0.75);
		else return 0.0;
}
float red(float gray) {
		return base(gray - 0.5);
}
	//
float green(float gray) {
		return base(gray);
}
	//
float blue(float gray) {
		return base(gray + 0.5);
}

vec3 getJetColorFromProba(float proba)
{
		// val must be [-1;+1] for jet color estimation
		float val = 2.0 * proba - 1.0;
		//
		float redColor = red(val);
		float greenColor = green(val);
		float blueColor = blue(val);

		//
		return vec3(redColor, greenColor, blueColor);
}


vec3 project(vec3 point, mat4 proj) {
  vec4 p1 = proj * vec4(point, 1.0);
  vec3 p2 = (p1.xyz/p1.w);
  return (p2.xyz*0.5 + 0.5);
}

bool frustumTest(vec3 p, vec2 ndc, int cam_id) {
  vec3 d1 = icam_dir[cam_id];
  vec3 d2 = p - icam_pos[cam_id];
  return !any(greaterThan(ndc, vec2(1.0))) && dot(d1,d2)>0.0;
}


void main(void){
  		
  vec4 point = texture(proxy, vertex_coord);
  // discard if there was no intersection with the proxy
  if ( point.w >= 1.0) {
	discard;
  }

  vec4  color0 = vec4(0.0);
  vec4  color1 = vec4(0.0);
  vec4  color2 = vec4(0.0);
  vec4  color3 = vec4(0.0);

  vec4 weights = vec4(INFTY_W);
  
	 // We need to keep the uvs of the selected colors for the fov blending.
  vec4 uvs01 = vec4(0.0,0.0,0.0,0.0);
  vec4 uvs23 = vec4(0.0,0.0,0.0,0.0);

  bool onlyFull = true;
  for(int cid = 0; cid < NUM_CAMS; cid++){
	if(cid >= cam_count){
	  break;
    }

	int cam_id = icam_ids[cid];

	vec3 uvd = project(point.xyz, icam_proj[cid]);
	vec2 ndc = abs(2.0*uvd.xy-1.0);

	if (frustumTest(point.xyz, ndc, cid))
	{
		// IMPORTANT: an assumption is made here (SR):
		// Full scene cameras are present first in the imgs_ulr vector.
		if(onlyFull && cam_id > last_full_id){
			// We are in the frustum of one of the instance cameras.
			// We want to ignore the colors from full input images.
			// We know that ids are in increasing order and such images are first in list.
			// We won't meet any other full scene camera after here.
			// And we are currently meeting our first instance camera.
			// So we can safely reset colors and weights.
			weights = vec4(INFTY_W);
			color0 = vec4(0.0);
			color1 = vec4(0.0);
			color2 = vec4(0.0);
			color3 = vec4(0.0);
			onlyFull = false;
		}
		vec3 uv_array = vec3(uvd.xy, cam_id);
        vec4 color = texture(inputImages, uv_array);
		

		float masked = 1.0;
		if(doMasking){
            
			masked = texture(inputMasks, uv_array).r;
             
            if( invert_mask ){
                masked = 1.0 - masked;
            }
			
		}
		

		// Separate uniform and per-pixel branching. TODO SR: test impact.
		if (discard_black_pixels){
			if(all(lessThan(color.xyz, vec3(0.05)))){
				//continue;
			}
		}
 		
		if (occ_test){
			float inputDepth = texture(inputDepths, uv_array).r;
			if(abs(uvd.z-inputDepth) >= EPSILON) {	  
				continue;
			}
		} 

		vec3 v1 = (point.xyz - icam_pos[cid]);
		vec3 v2 = (point.xyz - ncam_pos);
		float dist_i2p 	= length(v1);
		float dist_n2p 	= length(v2);

		float penalty_ang = float(occ_test) * max(0.0001, acos(dot(v1,v2)/(dist_i2p*dist_n2p)));

		float penalty_res = max(0.0001, (dist_i2p - dist_n2p)/dist_i2p );
		 
		float currentW = penalty_ang + BETA*penalty_res;
		  
		// compare with best four candiates and insert at the
		// appropriate rank
		if (currentW<weights[3]) {    // better than fourth best candidate
			
			if (currentW<weights[2]) {    // better than third best candidate
				color3 = color2;
				weights[3] = weights[2];
				uvs23.zw = uvs23.xy;

				if (currentW<weights[1]) {    // better than second best candidate
					color2 = color1;
					weights[2] = weights[1];
					uvs23.xy = uvs01.zw;

					if (currentW<weights[0]) {    // better than best candidate
						color1 = color0;
						weights[1] = weights[0];
						uvs01.zw = uvs01.xy;

						color0 = color;
						weights[0] = currentW;
						uvs01.xy = ndc;

					} else {
						color1 = color;
						weights[1] = currentW;
						uvs01.zw = ndc;
					}

				} else {
					color2 = color;
					weights[2] = currentW;
					uvs23.xy = ndc;
				}

			} else {
				color3 = color;
				weights[3] = currentW;
				uvs23.zw = ndc;
			}
		}
	 }  
   }
   
	float thresh = 1.0000001 * weights[3];
	weights = max(vec4(0.0), 1.0 - weights/thresh);
    

    // ignore any candidate which is uninit
	weights = mix(weights, vec4(0.0), equal(weights, vec4(INFTY_W)));
	//if (weights[0] == INFTY_W) weights[0] = 0.0;
    // if (weights[1] == INFTY_W) weights[1] = 0.0;
    // if (weights[2] == INFTY_W) weights[2] = 0.0;
    //if (color3.w == INFTY_W) color3.w = 0; uneeded, color3.w = 1.0 - 1.0/1.0000001


	
	// Compute the attenuation factors.
	vec4 fcs01 = 1.0 - smoothstep(vec4(FOV_BLENDING_BORDER), vec4(1.0), uvs01);
	vec4 fcs23 = 1.0 - smoothstep(vec4(FOV_BLENDING_BORDER), vec4(1.0), uvs23);
	fcs01.xz *= fcs01.yw;
	fcs23.xz *= fcs23.yw;

	weights[0] *= fcs01.x;
	weights[1] *= fcs01.z;
	weights[2] *= fcs23.x;
	weights[3] *= fcs23.z;
	

    // blending
	weights /=  (weights[0] + weights[1] + weights[2] + weights[3]);

	

    out_color = weights[0] * color0 + weights[1] * color1 + weights[2] * color2 + weights[3] * color3;
    
	/*if(all(lessThan(out_color.rgb, vec3(0.05)))){
		discard;
	}  */
	
    //gl_FragDepth = point.w;
	
}
