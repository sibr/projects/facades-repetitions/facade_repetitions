/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#version 420

layout(binding = 0) uniform sampler2D envmap;
layout(binding = 1) uniform sampler2D mask;
layout(binding = 2) uniform sampler2D posmap;
layout(binding = 3) uniform sampler2D normap;

uniform vec3 cameraPositionWorld;

uniform mat4 envmapMat = mat4(1.0);
uniform vec3 planeNormal;
uniform float intens;

out vec4 out_color;

in vec2 vertex_coord;


#define M_PI 3.14159265359

vec2 cartesianToSpherical(vec3 r){
	vec3 p = normalize((envmapMat * vec4(r,0.0)).xyz);

	float phi = acos(p.z);
	float theta = M_PI*0.5;
	if(p.x != 0.0){
		theta = atan(p.y,p.x);
	} else if (p.y < 0.0){
		theta *= -1.0;
	}
	theta += M_PI*0.75; //0.5 on paris1, 0.75 on paris7 // 0.75 or 0.0 on ucl5 prob, same on ucl3 prob.
	theta = mod(theta - 0.5*M_PI, 2.0*M_PI);

	float u = theta / (2.0*M_PI);
	float v = phi / M_PI;
	vec2 adjustedUV = vec2(u,v);
	return adjustedUV;
}


void main(void) {
	out_color = vec4(0.0);
	vec2 uv = vertex_coord;
	if(texture(mask, uv).a < 0.5){
		discard;
	}

	vec3 vertNormalWorld = texture(normap, uv).rgb;
	vec3 vertPositionWorld = texture(posmap, uv).rgb;

	// Computed reflected vector in world space.
	vec3 v = normalize(vertPositionWorld - cameraPositionWorld);
	vec3 n = normalize(planeNormal);
	vec3 r = reflect(v, n);

	vec3 reflectionColor = texture(envmap, cartesianToSpherical(normalize(r))).rgb ;
	

	float VdotH = max(0.0, abs(dot(v,normalize(vertNormalWorld))));
	const float F0 = 0.3;
	float fresnelFactor = F0+(1.0-F0)*pow(1.0 - VdotH, 5);
	
	
	out_color.rgb = reflectionColor;
	out_color.a = intens*fresnelFactor;
	
}

/*
void main(void) {
	float masking = texture(mask, gl_FragCoord.xy/textureSize(mask,0).xy).a;
	if(masking < 0.5){
		discard;
	}
	// Computed reflected vector in world space.
	vec3 v = normalize(vertPositionWorld - cameraPositionWorld);
	vec3 n = planeNormal;
	vec3 r = reflect(v, n);
	vec3 reflectionColor = texture(envmap,cartesianToSpherical(normalize(r))).rgb;
	out_color.rgb = reflectionColor;//vec3(1.0,0.0,0.0);

}*/
