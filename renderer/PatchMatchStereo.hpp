/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#ifndef SIBR_REPETITIONS_PATCHMATCHSTEREO_H
#define SIBR_REPETITIONS_PATCHMATCHSTEREO_H
#include "Config.hpp"
#include <core/assets/InputCamera.hpp>
#include <projects/facade_repetitions/renderer/CropCamera.hpp>
#include <projects/facade_repetitions/renderer/ReprojectionScene.hpp>
#include <projects/facade_repetitions/renderer/DepthIterativeOptim.hpp>

#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/photo/photo.hpp>
#include <random>



class SIBR_FACADE_REPETITIONS_EXPORT PatchMatchStereo
{

public:

	PatchMatchStereo(const cv::Mat initialDepthMap, DepthMapEnergy * energy, const double minZ, const double maxZ);

	~PatchMatchStereo(void);

	void solve(const int iteration_count);

	DepthMapEnergy::DepthData ** getDepthData() { return _depthMap; };

	cv::Mat getDepthMap();

private:


	void compareCosts(int x0, int y0, int xneigh, int yneigh);

	void planeRandomRefinement(const double deltaMax, const double deltaMin);

	double random(const double delta);

	DepthMapEnergy::DepthData ** _depthMap;
	DepthMapEnergy * _energy;
	cv::Mat _costMap;
	int _w;
	int _h;
	double _deltaZ;

	std::mt19937 _randomMT;
	std::uniform_real_distribution<double> _randomDistri;

};

#endif // SIBR_REPETITIONS_PATCHMATCHSTEREO_H
