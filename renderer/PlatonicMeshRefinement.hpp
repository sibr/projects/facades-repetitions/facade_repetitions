/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#ifndef SIBR_REPETITIONS_PLATONICMESHREFINEMENT_H
#define SIBR_REPETITIONS_PLATONICMESHREFINEMENT_H

#include "Config.hpp"

#include "FullScene.hpp"
#include "ReprojectionScene.hpp"

#include <core/assets/InputCamera.hpp>


class SIBR_FACADE_REPETITIONS_EXPORT PlatonicMeshRefinement
{

public:

	PlatonicMeshRefinement(const sibr::Mesh::Ptr & mesh, const std::shared_ptr<FullScene> & fullScene, const std::shared_ptr<ReprojectionScene> & instanceScene);

	~PlatonicMeshRefinement(void);

	static sibr::Mesh::Ptr cleanMesh(const sibr::Mesh & refMesh, const std::vector<sibr::InputCamera::Ptr> & cameras, const float outborder);
	
	void computeFrontoParallelInfos(const sibr::Matrix4f & estimatedTransformation, const std::string & referenceScenePath, const bool useClosestCam, const int scalingDown = 1, const std::string & outputPath = "");

	void computePhotoConsistencyVisualisation(const int steps, const std::string & scenePath, const std::string & masksPath, const std::string & outputPath);

	void refineUsingSuperpixels(const sibr::Matrix4f& estimatedTransfo, const std::string& outputPath);

	sibr::InputCamera estimateFrontoParallelCamera(const std::vector<sibr::InputCamera::Ptr>& inputCameras, const sibr::Vector3f & platonicNormal,
		const bool useClosestRealCam, int & selectedId, const int downScaling);

private:

	struct FrontoParallelInfos {
		cv::Mat depthMap;
		cv::Mat depthMask;
		cv::Mat colorMap;
		sibr::InputCamera frontoCam;
		sibr::Vector2f originP;
		sibr::Vector2f stepP;
		double biasZ, scaleZ;
		int camId;
	};

	static void saveDepthmap(const std::string & path, cv::Mat depthMap, const float scalingZ, const float biasZ);

	

	void generateFrontoParallelColorMap(const sibr::InputCamera & frontoCam, sibr::ImageRGB & colorMap, const std::string & scenePath);

	void generateFrontoParallelDepthMap(const sibr::InputCamera & frontoCam, FrontoParallelInfos & infos);

	static void saveColoredPCMap(const cv::Mat & PCMap, const cv::Mat & mask, const double bias, const double scale, const std::string & outputPath, const bool linearColors);

	sibr::Mesh::Ptr _mesh;
	std::shared_ptr<FullScene> _fullScene;
	std::shared_ptr<ReprojectionScene> _instanceScene;
	FrontoParallelInfos _infos;
};

#endif // SIBR_REPETITIONS_PLATONICMESHREFINEMENT_H
