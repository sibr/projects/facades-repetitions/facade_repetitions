/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#include "RepetitionsReflectionRenderer.hpp"

namespace sibr
{
	RepetitionsReflectionRenderer::RepetitionsReflectionRenderer(uint w, uint h)
	{
		_shader.init("ReflectingMesh",
		             loadFile(sibr::getShadersDirectory("facade_repetitions") + "/repetitions_reflecting_mesh.vert"),
		             loadFile(sibr::getShadersDirectory("facade_repetitions") + "/repetitions_reflecting_mesh.frag"));
		_shaderGeom.init("GBuffer",
		                 loadFile(sibr::getShadersDirectory("facade_repetitions") + "/repetitions_gbuffer.vert"),
		                 loadFile(sibr::getShadersDirectory("facade_repetitions") + "/repetitions_gbuffer.frag"));
		_paramMVP.init(_shaderGeom, "MVP");
		_paramCamPosWorld.init(_shader, "cameraPositionWorld");
		_paramEnvmapMat.init(_shader, "envmapMat");
		_paramNormal.init(_shader, "planeNormal");
		/*_paramScale.init(_shader, "scale");
		_paramMini.init(_shader, "mini");
		_paramShift.init(_shader, "shift");*/
		_paramFresnel.init(_shader, "intens");
		_paramFresnel = 0.8f;
		_gbuffer = std::make_shared<RenderTargetRGB32F>(w, h, SIBR_CLAMP_UVS, 2);
	}

	void RepetitionsReflectionRenderer::process(const GLuint mask, const Camera& eye, const Mesh& mesh,
	                                            const GLuint envmap, const Vector3f& normal, const Matrix4f& envmapMat,
	                                            IRenderTarget& dst)
	{
		// No clear;
		_gbuffer->clear();
		_gbuffer->bind();
		_shaderGeom.begin();
		_paramMVP.set(eye.viewproj());
		mesh.render(true, false);
		_shaderGeom.end();
		_gbuffer->unbind();

		dst.bind();
		glEnable(GL_BLEND);
		_shader.begin();
		_paramCamPosWorld.set(eye.position());
		_paramNormal.set(normal);
		_paramEnvmapMat.set(envmapMat);
		_paramFresnel.send();
		//_paramScale.set(scale);
		//_paramMini.set(mini);
		//_paramShift.set(shift);
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, envmap);
		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_2D, mask);
		glActiveTexture(GL_TEXTURE2);
		glBindTexture(GL_TEXTURE_2D, _gbuffer->texture(0));
		glActiveTexture(GL_TEXTURE3);
		glBindTexture(GL_TEXTURE_2D, _gbuffer->texture(1));
		RenderUtility::renderScreenQuad();

		_shader.end();
		dst.unbind();

		glDisable(GL_BLEND);
	}
} /*namespace sibr*/
