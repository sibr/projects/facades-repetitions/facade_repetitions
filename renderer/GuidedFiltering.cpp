/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#include "GuidedFiltering.hpp"


GuidedFilter::GuidedFilter(const cv::Mat &guide, int halfRadius, double eps) {
	cv::Mat I;
	if (guide.depth() == CV_32F) {
		I = guide.clone();
	} else {
		guide.convertTo(I, CV_32F, (guide.depth() == CV_8U) ? 1.0f/255.0f : 1.0f);
	}

	_r = 2 * halfRadius + 1;
	_eps = eps;

	cv::split(I, Ichannels);

	_mean_I_r = boxfilter(Ichannels[0], _r);
	_mean_I_g = boxfilter(Ichannels[1], _r);
	_mean_I_b = boxfilter(Ichannels[2], _r);

	// variance of I in each local patch: the matrix Sigma in Eqn (14).
	// Note the variance in each local patch is a 3x3 symmetric matrix:
	//           rr, rg, rb
	//   Sigma = rg, gg, gb
	//           rb, gb, bb
	cv::Mat var_I_rr = boxfilter(Ichannels[0].mul(Ichannels[0]), _r) - _mean_I_r.mul(_mean_I_r) + _eps;
	cv::Mat var_I_rg = boxfilter(Ichannels[0].mul(Ichannels[1]), _r) - _mean_I_r.mul(_mean_I_g);
	cv::Mat var_I_rb = boxfilter(Ichannels[0].mul(Ichannels[2]), _r) - _mean_I_r.mul(_mean_I_b);
	cv::Mat var_I_gg = boxfilter(Ichannels[1].mul(Ichannels[1]), _r) - _mean_I_g.mul(_mean_I_g) + _eps;
	cv::Mat var_I_gb = boxfilter(Ichannels[1].mul(Ichannels[2]), _r) - _mean_I_g.mul(_mean_I_b);
	cv::Mat var_I_bb = boxfilter(Ichannels[2].mul(Ichannels[2]), _r) - _mean_I_b.mul(_mean_I_b) + _eps;

	// Inverse of Sigma + eps * I
	_invrr = var_I_gg.mul(var_I_bb) - var_I_gb.mul(var_I_gb);
	_invrg = var_I_gb.mul(var_I_rb) - var_I_rg.mul(var_I_bb);
	_invrb = var_I_rg.mul(var_I_gb) - var_I_gg.mul(var_I_rb);
	_invgg = var_I_rr.mul(var_I_bb) - var_I_rb.mul(var_I_rb);
	_invgb = var_I_rb.mul(var_I_rg) - var_I_rr.mul(var_I_gb);
	_invbb = var_I_rr.mul(var_I_gg) - var_I_rg.mul(var_I_rg);

	cv::Mat covDet = _invrr.mul(var_I_rr) + _invrg.mul(var_I_rg) + _invrb.mul(var_I_rb);

	_invrr /= covDet;
	_invrg /= covDet;
	_invrb /= covDet;
	_invgg /= covDet;
	_invgb /= covDet;
	_invbb /= covDet;
}

cv::Mat GuidedFilter::filter(const cv::Mat &p) {
	cv::Mat p2;
	p.convertTo(p2, CV_32F, (p.depth() == CV_8U) ? 1.0f/255.0f : 1.0f);

	cv::Mat result;
	if (p.channels() == 1)
	{
		result = filterSingleChannel(p2);
	} else
	{
		std::vector<cv::Mat> pc;
		cv::split(p2, pc);

		for (std::size_t i = 0; i < pc.size(); ++i)
			pc[i] = filterSingleChannel(pc[i]);

		cv::merge(pc, result);
	}
	cv::Mat finalRes;
	result.convertTo(finalRes, p.depth(), (p.depth() == CV_8U) ? 255 : 1);
	return finalRes;
}

cv::Mat GuidedFilter::boxfilter(const cv::Mat &I, int r)
{
	cv::Mat result;
	cv::blur(I, result, cv::Size(r, r));
	return result;
}


cv::Mat GuidedFilter::filterSingleChannel(const cv::Mat &p) const
{
	cv::Mat mean_p = boxfilter(p, _r);

	cv::Mat mean_Ip_r = boxfilter(Ichannels[0].mul(p), _r);
	cv::Mat mean_Ip_g = boxfilter(Ichannels[1].mul(p), _r);
	cv::Mat mean_Ip_b = boxfilter(Ichannels[2].mul(p), _r);

	// covariance of (I, p) in each local patch.
	cv::Mat cov_Ip_r = mean_Ip_r - _mean_I_r.mul(mean_p);
	cv::Mat cov_Ip_g = mean_Ip_g - _mean_I_g.mul(mean_p);
	cv::Mat cov_Ip_b = mean_Ip_b - _mean_I_b.mul(mean_p);

	cv::Mat a_r = _invrr.mul(cov_Ip_r) + _invrg.mul(cov_Ip_g) + _invrb.mul(cov_Ip_b);
	cv::Mat a_g = _invrg.mul(cov_Ip_r) + _invgg.mul(cov_Ip_g) + _invgb.mul(cov_Ip_b);
	cv::Mat a_b = _invrb.mul(cov_Ip_r) + _invgb.mul(cov_Ip_g) + _invbb.mul(cov_Ip_b);

	cv::Mat b = mean_p - a_r.mul(_mean_I_r) - a_g.mul(_mean_I_g) - a_b.mul(_mean_I_b); 

	return (boxfilter(a_r, _r).mul(Ichannels[0])
		+ boxfilter(a_g, _r).mul(Ichannels[1])
		+ boxfilter(a_b, _r).mul(Ichannels[2])
		+ boxfilter(b, _r));  
}

GuidedFilter::~GuidedFilter(void) {
}