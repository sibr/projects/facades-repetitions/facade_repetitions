/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#include "StructureTensor.h"

StructureTensor::StructureTensor(){}

StructureTensor::~StructureTensor()
{
    for(int i = 0; i < this->h; i++)
    {
        delete tangents[i];
    }

    delete tangents;
}

/**
Expects (input row, input column)
*/
Eigen::Vector2d StructureTensor::getTangent(int i, int j)
{
    return this->tangents[i][j];
}

/**
Expects (input row, input column)
*/
double StructureTensor::getMagnitude(int i, int j)
{
    return this->magnitude[i][j];
}

cv::Mat StructureTensor::preprocessForFiltering(cv::Mat input)
{
	cv::Mat grayscaleImage, blurred;

    // Greyscale version of it
	cv::cvtColor(input, grayscaleImage, cv::COLOR_BGR2GRAY);

	cv::GaussianBlur(grayscaleImage,blurred, cv::Size(3,3),2,2);

    // imwrite("output/Image.png",input);
    // imwrite("output/blurImage.png",blurred);

    return blurred;
}


cv::Mat StructureTensor::findEigenValues(cv::Mat st)
{

    // Eigenvalues matrix (to return)
	cv::Mat eigenValues = cv::Mat::zeros(st.rows,st.cols, CV_64FC2);


    // Compute eigenvalues
    for(int i = 0; i < st.rows; i++)
    {
        for(int j = 0; j < st.cols; j++)
        {
			
            double E = st.at<cv::Vec4d>(i,j)[0];
            double F = st.at<cv::Vec4d>(i,j)[1];
            double G = st.at<cv::Vec4d>(i,j)[3];

            // λ_1 = ( E + G + sqrt((E-G)^2 + 4*F^2) ) / 2
            double lambda_1 = (double)((E + G + sqrt( pow((E-G),2.0) + 4.0*pow(F,2.0) )) / 2.0);

            // λ_2 = ( E + G - sqrt((E-G)^2 + 4*F^2) ) / 2
            double lambda_2 = (double)((E + G - sqrt( pow((E-G),2.0) + 4.0*pow(F,2.0) )) / 2.0);

            // Copy into mat
            eigenValues.at<cv::Vec2d>(i,j)[0] =  lambda_1;
            eigenValues.at<cv::Vec2d>(i,j)[1] =  lambda_2;
			
		}
    }
    return eigenValues;
}

cv::Mat StructureTensor::lessChangeEigenvector(cv::Mat st, cv::Mat ev)
{
    // Eigenvalues matrix (to return)
	cv::Mat eigenVectors_less = cv::Mat::zeros(st.rows,st.cols, CV_64FC2);

    // Compute eigenvectors
    for(int i = 0; i < st.rows; i++)
    {
        for(int j = 0; j < st.cols; j++)
        {
            // Two different ways of computing it (λ_1-E, -F) or (-F, λ_1-G)            
            double E = st.at<cv::Vec4d>(i,j)[0];
			double F = st.at<cv::Vec4d>(i,j)[1];
            double G = st.at<cv::Vec4d>(i,j)[3];
            double lambda_1 = ev.at<cv::Vec2d>(i,j)[0];
			// Copy into mat
			if ( fabs(lambda_1 - E)<1e-5 && E != 0)
			{
				eigenVectors_less.at<cv::Vec2d>(i,j)[0] =  (-F);
				eigenVectors_less.at<cv::Vec2d>(i,j)[1] =  (lambda_1 - G);
			}
			else
			{
				eigenVectors_less.at<cv::Vec2d>(i,j)[0] =  (lambda_1 - E);
				eigenVectors_less.at<cv::Vec2d>(i,j)[1] =  (-F);
			}
			// double lambda_2 = ev.at<Vec2d>(i,j)[1];
			// // Copy into mat
			// if (E != 0)
			// {
			// 	eigenVectors_less.at<Vec2d>(i,j)[0] =  (F);
			// 	eigenVectors_less.at<Vec2d>(i,j)[1] =  (lambda_2 - E);
			// }
			// else
			// {
			// 	eigenVectors_less.at<Vec2d>(i,j)[0] =  (lambda_2 - G);
			// 	eigenVectors_less.at<Vec2d>(i,j)[1] =  (F);
			// }
        }
    }
    return eigenVectors_less;
}


// Builds the angent XY map with gaussian struture tensor
Eigen::Vector2d ** StructureTensor::myStructureTensorXYMap(cv::Mat preprocImage, cv::Mat input)
{
    // Partian derivatives with sobel
	using namespace std;
    // Generate sobel X
	cv::Mat gradientMapX, X2, diff;

    // Generate sobel Y
	cv::Mat gradientMapY, Y2;

    // Gradient map (blurImage, gradientX, 1(x), 0(y), kernelSize(3), scale 1 (nonscale), delta 0 (nondelta)
    Sobel(preprocImage,gradientMapX,CV_64FC1,1,0,3,-1,0);

    // Gradient map (blurImage, gradientY, 0(x), 1(y), kernelSize(3), scale 1 (nonscale), delta 0 (nondelta)
    Sobel(preprocImage,gradientMapY,CV_64FC1,0,1,3,1,0);

    // imwrite("output/sobelX.png",gradientMapX);
    // imwrite("output/sobelY.png",gradientMapY);

    // Normalize
    normalize(gradientMapX, gradientMapX, -1, 1, cv::NORM_INF , CV_64FC1);
    normalize(gradientMapY, gradientMapY, -1, 1, cv::NORM_INF, CV_64FC1);

    // Compute structure tensor. Each component in an image channel
	cv::Mat structureTensorM = cv::Mat::zeros( preprocImage.size(), CV_64FC4 );
    for(int i = 0; i < preprocImage.rows; i++)
    {
        for(int j = 0; j < preprocImage.cols; j++)
        {
            // Compute struture tensor for each pixel
            // df/dx * df/dx
            structureTensorM.at<cv::Vec4d>(i,j)[0] = gradientMapX.at<double>(i,j) * gradientMapX.at<double>(i,j);
            // df/dx * df/dy
            structureTensorM.at<cv::Vec4d>(i,j)[1] = gradientMapX.at<double>(i,j) * gradientMapY.at<double>(i,j);
            // df/dx * df/dy
            structureTensorM.at<cv::Vec4d>(i,j)[2] = gradientMapX.at<double>(i,j) * gradientMapY.at<double>(i,j);
            // df/dy * df/dy
            structureTensorM.at<cv::Vec4d>(i,j)[3] = gradientMapY.at<double>(i,j) * gradientMapY.at<double>(i,j);
        }

    }

    //Smoothing
	cv::Mat smoothTensor = cv::Mat::zeros( structureTensorM.size(), CV_64FC4 );
	GaussianBlur(structureTensorM, smoothTensor, cv::Size(0,0), 1.0,1.0);
	// Find Eigenvalues
	cv::Mat eigenValues = this->findEigenValues(smoothTensor);

    // Find tangent eigenvector
	cv::Mat lessChangeVector = this->lessChangeEigenvector(smoothTensor,eigenValues);

    // Declare the matrix
    this->tangents = new Eigen::Vector2d*[this->h];
	this->magnitude = new double*[this->h];
	cv::Mat visu(h,w,CV_8UC3);
	
    // Fill it
    for(int i = 0; i < this->h; i++)
    {
        tangents[i] = new Eigen::Vector2d[this->w];
        magnitude[i] = new double[this->w];

        // Fill it
        for(int j = 0; j < this->w; j++)
        {
            double x_st = lessChangeVector.at<cv::Vec2d>(i,j)[0];
            double y_st = lessChangeVector.at<cv::Vec2d>(i,j)[1];

            // The gradient vector
            Eigen::Vector2d stVector = Eigen::Vector2d(x_st,y_st);
            // The ortogonal
			if (stVector.norm() > 1e-15)
			{
				tangents[i][j] = stVector.normalized();
				visu.at<cv::Vec3b>(i,j)[0] = 0;
                visu.at<cv::Vec3b>(i,j)[1] = 0;
                visu.at<cv::Vec3b>(i,j)[2] = 0;
			}
			else
			{
				tangents[i][j] = stVector;
				visu.at<cv::Vec3b>(i,j)[0] = 255;
                visu.at<cv::Vec3b>(i,j)[1] = 255;
                visu.at<cv::Vec3b>(i,j)[2] = 255;
			}
			magnitude[i][j] = eigenValues.at<cv::Vec2d>(i,j)[0];
			
        }
    }



    // Save LineIntegralCovolution of tangent flow
    FilterLineIntegralConvolution lic;
	//cv::Mat licTangent = lic.filter(tangents,this->h,this->w,7);

    //cv::imwrite("output/structureTensor.png",visu);

    return tangents;
}

/**
	Expect a CV_8C3 matrix as input.
*/
StructureTensor::StructureTensor(cv::Mat input, COLORS col)
{   
    // Dimensions
    this->w = input.cols;
    this->h = input.rows;

	cv::Mat filtered = input.clone();
	
	if (col != COLORS::NONE)
	{
		for(int i = 0; i < h; i++)
		{
			for(int j = 0; j < w; j++)
			{
				cv::Vec3b pix = filtered.at<cv::Vec3b>(i,j);
				if((pix[0] + pix[1] + pix[2] > 255) || (pix[col] == 0 && pix != cv::Vec3b(0,0,0)))
					filtered.at<cv::Vec3b>(i,j) = cv::Vec3b(255,255,255);
				// else
				// 	filtered.at<Vec3b>(i,j) = Vec3b(0,0,0);
			}
		}
	}
	
    // Blur + Grayscale
	cv::Mat preprocImage = this->preprocessForFiltering(filtered);

    this->myStructureTensorXYMap(preprocImage,filtered);

}


