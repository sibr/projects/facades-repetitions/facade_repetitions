/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#include "FullScene.hpp"
#include <core/graphics/Image.hpp>

#include <iostream>
#include <projects/facade_repetitions/renderer/OpenMVGSFM.hpp>
#include <core/raycaster/CameraRaycaster.hpp>


FullScene::FullScene(const std::string & rootPath, bool useBundlerFile)
{

	const std::string approxBundlerFile = rootPath + "/aligned_cams.txt";
	const std::string cropsPath = rootPath + "/crop_list.txt";
	const std::string trapezePath = rootPath + "/trapezoid_list.txt";
	const std::string listingPath = rootPath + "/initial_cameras.txt";
	const std::string omvgSfmPath = rootPath + "/full/reconstruction_sequential/sfm_data.json";
	const std::string osfmPath = rootPath + "/sfm/reconstruction_sequential/sfm_data.json";

	const std::map<unsigned int, unsigned int> camerasShift = CropCameraUtilities::checkCorrespondances(osfmPath);

	// ---- Plane
	// Load plane points in bundle file.
	std::ifstream approx_bundle_file(approxBundlerFile);
	SIBR_ASSERT(approx_bundle_file.is_open());
	std::string line;
	getline(approx_bundle_file, line);	// ignore first line - contains version
	float x, y, z;
	approx_bundle_file >> x >> y >> z;
	sibr::Vector3f p0(x, y, z);
	approx_bundle_file >> x >> y >> z;
	sibr::Vector3f p1(x, y, z);
	approx_bundle_file >> x >> y >> z;
	sibr::Vector3f p2(x, y, z);
	getline(approx_bundle_file, line);
	// Plane normal and point.
	sibr::Vector3f pNormal = (p1 - p0).normalized().cross((p2 - p0).normalized()).normalized();
	this->facadePlane = Plane(p0, p1, p2, pNormal);


	// ---- Cameras
	auto crops = CropCameraUtilities::loadCropList(cropsPath);
	// Create corresponding crops cameras.
	const std::string bundleExt = omvgSfmPath.substr(omvgSfmPath.size() - 3, 3);
	if (useBundlerFile || bundleExt == "out") {
		const std::string listPath = omvgSfmPath + "/../list_images.txt";
		const std::string bundlePath = omvgSfmPath + "/../bundle.out";
		std::vector<sibr::Vector3f> points;
		this->cameras = CropCameraUtilities::load(bundlePath, listPath, points);
	} else {
		sibr::OpenMVGSFM sfmData(omvgSfmPath);
		this->cameras = sfmData.cameras();
	}

	// Load images/windows associations.
	unsigned int maxView = 0;
	unsigned int maxWindow = 0;
	auto tempWindowsIds = CropCameraUtilities::loadWindowsCorrespondences(listingPath, maxWindow, maxView);
	windowsCount = maxWindow + 1;
	// Estimated cameras
	auto tempEstimatedCameras = CropCameraUtilities::estimateCameras(crops, this->cameras);
	auto tempTrapezoids = CropCameraUtilities::loadTrapezeList(trapezePath);


	this->windowsIds.clear();
	this->estimatedCameras.clear();
	this->trapezoids.clear();

	for (size_t k = 0; k < tempEstimatedCameras.size(); ++k) {
		if (camerasShift.count((unsigned int)k) > 0) {
			this->estimatedCameras.push_back(tempEstimatedCameras[k]);
			this->trapezoids.push_back(tempTrapezoids[k]);
			this->windowsIds.push_back(tempWindowsIds[k]);
		}
	}

	// ---- Intersections
	for (auto & camCrop : this->estimatedCameras) {
		sibr::Vector3f c0 = camCrop.camera.position();
		sibr::Vector3f cDir = camCrop.camera.dir().normalized();

		float orientation = cDir.dot(pNormal);
		// Check for parallelism
		if (orientation == 0.0) {
			SIBR_WRG << "Ignoring a camera (from image " << camCrop.source << ") as parallel to the facade plane.\n";
			continue;
		}
		// Intersect camera center ray and plane.
		float abscisse = (p0 - c0).dot(pNormal) / orientation;
		sibr::Vector3f intersectionPoint = c0 + abscisse * cDir;
		camCrop.intersection = intersectionPoint;
	}

	// ---- Images
	this->images.clear();
	const std::string imagesPath = rootPath + "/images/";
	boost::filesystem::directory_iterator  endDir;
	
	for (boost::filesystem::directory_iterator dir(imagesPath); dir != endDir; ++dir) {
		if (boost::filesystem::is_regular_file(dir->status())) {
			this->images.emplace_back();
			const std::string imagePath = imagesPath + dir->path().stem().string() + ".jpg";
			this->images.back().load(imagePath, false);	
		}
	}

	// ---- Masks
	/*fullScene->masks.clear();
	const std::string masksPath = rootPath + "/images-masks-complete/";
	boost::filesystem::directory_iterator  endDir1;

	for (boost::filesystem::directory_iterator dir1(masksPath); dir1 != endDir1; ++dir1) {
	if (boost::filesystem::is_regular_file(dir1->status())) {
	fullScene->masks.emplace_back();
	const std::string imageMaskPath = masksPath + dir1->path().stem().string() + ".png";
	fullScene->masks.back().load(imageMaskPath, false);
	}
	}*/
}

FullScene::~FullScene() {

}
