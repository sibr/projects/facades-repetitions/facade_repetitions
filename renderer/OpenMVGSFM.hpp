/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#ifndef __SIBR_REPETITIONS_OPENMVGSFM_HPP__
# define __SIBR_REPETITIONS_OPENMVGSFM_HPP__

# include "Config.hpp"
# include <core/graphics/Window.hpp>
# include <core/assets/InputCamera.hpp>


namespace sibr {

	/**
	Load and parse an openMVG sfm.json file (equivalent to a Bundler .out file).
	*/
	class SIBR_FACADE_REPETITIONS_EXPORT OpenMVGSFM {

	public:

		struct CamerasCorrespondance {
			unsigned int initial;
			unsigned int shifted;
		};

		/**
		Load and parse a sfm JSON file output by OpenMVG utilities.
		\param path the path to the JSON file.
		*/
		OpenMVGSFM(const std::string & path);

		/**
		\return a vector of InputCamera objects extracted from the OpenMVG file.
		*/
		const std::vector<sibr::InputCamera::Ptr> cameras() { return _cameras; }

		/**
		\return a vector of 3D points extracted from the OpenMVG file.
		*/
		const std::vector<sibr::Vector3f> points() { return _points; }

		/**
		\return a vector of cameras indices correspondances (for the case where some cameras where not estimated by openMVG).
		*/
		const std::vector<CamerasCorrespondance> correspondances() { return _corresps; }

	private:

		struct IntrinsicsParam {
			size_t width;
			size_t height;
			float focalLength;
		};

		struct ExtrinsicsParam {
			Eigen::Matrix3f rotation;
			Eigen::Vector3f center;
			bool init;
		};

		/**
		Extract a simple, one-line value, given a key, a string, and a current position in the string.
		The position is then moved at the end of the value substring.
		\return the string representation of the value
		*/
		static std::string extractSimpleValueForKey(const std::string& key, const std::string& str, size_t & pos);

		/**
		Extract a simple, non-nested, brackets-delimited array, given a key, a string, and a current position in the string.
		The position is updated to the beginning of the array substring.
		\return the string representation of the value
		*/
		static std::string extractSimpleArrayForKey(const std::string& key, const std::string& str, size_t & pos);

		/** Convert a non-nested bracketed array string into a vec3 with float components.
		\return the parsed Vector3f
		*/
		static sibr::Vector3f extractVec3(const std::string & str);


		std::vector<sibr::InputCamera::Ptr> _cameras; ///< The parsed cameras.
		std::vector<sibr::Vector3f> _points; ///< The parsed 3D points.
		std::vector<CamerasCorrespondance> _corresps; ///< The correspondances between initial cameras ids and the ones in the cameras array. It can be ignored for general use.
	};

}
#endif // __SIBR_REPETITIONS_OPENMVGSFM_HPP__
