/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#ifndef SIBR_GUIDED_FILTERING_H
#define SIBR_GUIDED_FILTERING_H

#include "Config.hpp"
#include <core/graphics/Image.hpp>

/**
	Guided filtering algorithm implementation.
	Approach similar to joint bilateral filtering but using another image "edges" as a guide.
	The Guided filter kernel is designed to preserve step edges present in the guide, 
	ie preserving strong gradients.

	He, Kaiming, Jian Sun, and Xiaoou Tang. "Guided image filtering." 
	European conference on computer vision. Springer, Berlin, Heidelberg, 2010.

	Code adapted from https://github.com/atilimcetin/guided-filter

	
*/
class SIBR_FACADE_REPETITIONS_EXPORT GuidedFilter
{
	SIBR_DISALLOW_COPY(GuidedFilter)

public:

	/** Build a Guided filter with a given guide and paremeters
		\param guide the RGB guide. Will be converted to CV_32F and scaled if CV_8U.
		\param halfRadius the half size of the kernel.
		\param eps epsilon paramter value for variance (see paper).
	*/
	GuidedFilter(const cv::Mat &guide, int halfRadius, double eps);

	/** Apply guided filtering to the image passed as argument.
		\param p the image to process. Will be converted to CV_32F and scaled if CV_8U.
		\return the result image, with the same depth as p.
	*/
	cv::Mat filter(const cv::Mat &p);

	~GuidedFilter(void);

private:
	
	static cv::Mat boxfilter(const cv::Mat &I, int r);

	cv::Mat filterSingleChannel(const cv::Mat &p) const;

	std::vector<cv::Mat> Ichannels;
	double _eps;
	int _r;
	cv::Mat _mean_I_r, _mean_I_g, _mean_I_b;
	cv::Mat _invrr, _invrg, _invrb, _invgg, _invgb, _invbb;
};

#endif // SIBR_GUIDED_FILTERING_H