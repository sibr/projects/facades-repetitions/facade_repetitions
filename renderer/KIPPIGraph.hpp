/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#ifndef SIBR_REPETITIONS_KIPPIGRAPH_H
#define SIBR_REPETITIONS_KIPPIGRAPH_H

#include "Config.hpp"
#include <core/system/Vector.hpp>
#include <Eigen/Core>
#include <vector>
#include <set>

class SIBR_FACADE_REPETITIONS_EXPORT KIPPIGraph
{
	SIBR_DISALLOW_COPY(KIPPIGraph);


public:

	/**
	Represent a polygonal superpixel, as a list of vertices indices, a bounding box and a precise centroid.
	*/
	struct KIPPISuperPixel {
		std::vector<unsigned int> vertices; ///< successives vertices indices.
		std::vector<sibr::Vector3f> points3D; ///< mesh points falling inside this superpixel
		std::set<unsigned int> neighbours;
		sibr::Vector3f avgColor;
		Eigen::AlignedBox2i bbox;
		//sibr::Vector2f centroid2D;
		sibr::Vector3f centroid3D;
		sibr::Vector3f current3D;
		float minZ, maxZ;
		bool noDepth;
		/*float mdnDepth;
		float avgDepth;
		float curDepth;*/
	};

	/**
	Represent an edge of the graph, between superpixels at indices a and b, using the v1 and v2.
	Will evolve (SR).
	*/
	struct KIPPIEdge {
		unsigned int a;
		unsigned int b;
		unsigned int v1;
		unsigned int v2;
	};

	/** Load a polygonal superpixel graph produce by the KIPPI utility.
		\param path the path to the text file containing the graph to load.
	*/
	KIPPIGraph(const std::string & path);

	/**
	Check if a pixel is inside a given superpixel by triangulating the region and testing the point against each triangle.
	\param spid index of the superpixel
	\param x x-coordinate of the pixel
	\param y y-coordinate of the pixel
	\return a boolean denoting if the pixel falls inside the superpixel.
	*/
	bool isInside(const unsigned int spid, const unsigned int x, const unsigned int y);
	/**
	Check if a pixel is inside a given superpixel by triangulating the region and testing the point against each triangle.
	\param sp the superpixel
	\param x x-coordinate of the pixel
	\param y y-coordinate of the pixel
	\return a boolean denoting if the pixel falls inside the superpixel.
	*/
	bool isInside(const KIPPISuperPixel & sp, const unsigned int x, const unsigned int y);

	std::vector<sibr::Vector2f> & vertices() {
		return _vertices;
	}

	std::vector<KIPPISuperPixel> & superPixels() {
		return _superPixels;
	}

	std::vector<KIPPIEdge> & edges() {
		return _graphEdges;
	}

	unsigned int w() { return _w; }
	unsigned int h() { return _h; }

private:
	std::vector<sibr::Vector2f> _vertices;
	std::vector<KIPPISuperPixel> _superPixels;
	std::vector<KIPPIEdge> _graphEdges;

	unsigned int _w;
	unsigned int _h;

private:
};
#endif // SIBR_REPETITIONS_KIPPIGRAPH_H
