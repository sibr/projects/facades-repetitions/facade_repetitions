/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */



#include <iostream>
#include "ViewReprojection.hpp"
#include <core/raycaster/CameraRaycaster.hpp>


ViewReprojection::ViewReprojection(const std::string & rootPath, const std::string & imagesPath, const std::string & meshPath)
{
	// Load an IBR scene.
	_scene = std::make_shared<ReprojectionScene>(rootPath, imagesPath, meshPath);


	// Setup raycaster.
	_raycaster = std::make_shared<sibr::Raycaster>();
	if (!_raycaster->init()) {
		SIBR_ERR << " failed raycaster init in constructor " << std::endl;
		return;
	}
	sibr::Mesh::Ptr doubleMesh = _scene->proxy().clone();
	doubleMesh->merge(_scene->proxy().invertedFacesMesh());
	_raycaster->addMesh(*doubleMesh);
}



void ViewReprojection::reproject(const int cameraToUse, const std::vector<int> & imagesToUse, const std::string & outputPath, bool outAlpha, bool bilinear) {
	
	sibr::makeDirectory(outputPath);
	const sibr::ImageRGBA::Pixel empty(0, 0, 0, outAlpha ? 0 : 255);
	
	const sibr::InputCamera camera = *_scene->inputCameras()[cameraToUse];
	sibr::Vector3f dx, dy, upLeftOffset;
	sibr::CameraRaycaster::computePixelDerivatives(camera, dx, dy, upLeftOffset);
	
	#pragma omp parallel for
	for (int ii = 0; ii < imagesToUse.size(); ++ii ) {

		const int id = imagesToUse[ii];
		if (id >= _scene->inputCameras().size()) {
			std::cout << "id " << id << " out of bounds." << std::endl;
			continue;
		}
		
		sibr::ImageRGBA img(camera.w(), camera.h(), empty);
		sibr::ImageL8 mask(camera.w(), camera.h(), 0);
		sibr::InputCamera projCam(*_scene->inputCameras()[id]);

		// For each pixel of the camera's image
		for (uint py = 0; py < camera.h(); ++py) {
			for (uint px = 0; px < camera.w(); ++px) {
				
				if (id == cameraToUse) {
					// Skip raycasting.
					mask(px, py) = _scene->masks()[id](px, py);
					sibr::ColorRGBA color = _scene->images()[id].color(px, py);
					sibr::ColorRGBA finalColor(color.x(),color.y(),color.z(), 1.0f);
					img.color(px, py, finalColor);
					continue;
				}

				sibr::Vector3f worldPos = ((float)px+0.5f)*dx + ((float)py+0.5f)*dy + upLeftOffset;
				// Cast a ray
				sibr::Vector3f dir = worldPos - camera.position();
				sibr::RayHit hit = _raycaster->intersect(sibr::Ray(camera.position(), dir));
				if (hit.hitSomething()) {
					// Get the intersection point in world space.
					sibr::Vector3f intersection(camera.position() + hit.dist()*dir.normalized());
					// Check visibility from input camera.
					sibr::Vector3f dir = projCam.position() - intersection;
					sibr::RayHit hitViz = _raycaster->intersect(sibr::Ray(intersection, dir), 0.01f);
					// If we hit the mesh again, the intersection is occluded for the input camera.
					if (hitViz.hitSomething()) {
						continue;
					}
					// Reproject in current processed camera screen space.
					sibr::Vector3f projInter = projCam.projectScreen(intersection);
					// Check that we are inside projectedCam frustum.
					if (projInter.x() < 0 || projInter.x() >= projCam.w() ||
						projInter.y() < 0 || projInter.y() >= projCam.h()) {
						continue;
					}

					// Read corresponding color in input image.
					uint baseX = (uint)std::floor(projInter.x());
					uint baseY = (uint)std::floor(projInter.y());
					float fractX = projInter.x() - baseX;
					float fractY = projInter.y() - baseY;

					// For mask, use nearest neighbour for now.
					uint maskX = baseX;
					uint maskY = baseY;
					if (fractX > 0.5) {
						maskX++;
					}
					if (fractY > 0.5) {
						maskY++;
					}
					maskX = std::min(maskX, projCam.w() - 1);
					maskY = std::min(maskY, projCam.h() - 1);

					sibr::ImageL8::Pixel finalMask = _scene->masks()[id](maskX, maskY); 
					mask(px, py) = finalMask;


					if (bilinear) {
						
						const uint baseX1 = std::min(baseX + 1, projCam.w() - 1);
						const uint baseY1 = std::min(baseY + 1, projCam.h() - 1);
					
						const sibr::ColorRGBA color00 = _scene->images()[id].color(baseX, baseY);
						const sibr::ColorRGBA color01 = _scene->images()[id].color(baseX, baseY1);
						const sibr::ColorRGBA color10 = _scene->images()[id].color(baseX1, baseY);
						const sibr::ColorRGBA color11 = _scene->images()[id].color(baseX1, baseY1);
						 
						const sibr::ColorRGBA mixColor = (1.0f - fractX) * ((1.0f - fractY) * color00 + fractY * color01)
						 	+ fractX * ((1.0f - fractY) * color10 + fractY * color11);
						
						const sibr::ColorRGBA finalColor(mixColor.x(), mixColor.y(), mixColor.z(), 1.0f);
						img.color(px, py, finalColor);
					}
					else {
						const sibr::ColorRGBA finalColor = _scene->images()[id].color(maskX, maskY);
						img.color(px, py, finalColor);
					}

					
					
				}
			}
		}
		img.save(outputPath + "d" + (id < 10 ? "0" : "") + std::to_string(id) + ".png", false);
		mask.save(outputPath + "m" + (id < 10 ? "0" : "") + std::to_string(id) + ".png", false);
	}
}


ViewReprojection::~ViewReprojection(void)
{
}
