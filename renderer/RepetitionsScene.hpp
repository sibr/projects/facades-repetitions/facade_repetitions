/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#ifndef REPETITIONS_SCENE_COMPAT_HH
#define REPETITIONS_SCENE_COMPAT_HH

#include <projects/facade_repetitions/renderer/Config.hpp>
#include <core/scene/BasicIBRScene.hpp>
#include <core/scene/ParseData.hpp>


namespace sibr {
	
	struct SIBR_FACADE_REPETITIONS_EXPORT RepetitionsArgs : virtual BasicIBRAppArgs {
		Arg<bool> newLayout = { "new", "load a new SIBR dataset" };
	};

	class SIBR_FACADE_REPETITIONS_EXPORT RepetitionsParseData : public virtual ParseData {
	public:

		RepetitionsParseData(const std::string & basePath);
	};


	class SIBR_FACADE_REPETITIONS_EXPORT RepetitionsScene : public virtual BasicIBRScene {

		SIBR_DISALLOW_COPY(RepetitionsScene);
		SIBR_CLASS_PTR(RepetitionsScene);

	public:

		RepetitionsScene(const std::string & basePath);

	private:


	};

}

#endif
