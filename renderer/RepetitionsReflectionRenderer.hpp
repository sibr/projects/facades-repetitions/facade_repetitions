/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#ifndef __SIBR_REPETITIONS_REFLECTIONRENDERER_HPP__
# define __SIBR_REPETITIONS_REFLECTIONRENDERER_HPP__

# include <core/graphics/Shader.hpp>
# include <core/graphics/Mesh.hpp>
# include <core/graphics/Texture.hpp>
# include <core/graphics/Camera.hpp>

# include "Config.hpp"

namespace sibr
{

	/**
	 * Render a planar reflection on a mesh using an environment map and a mask.
	 */
	class SIBR_FACADE_REPETITIONS_EXPORT RepetitionsReflectionRenderer
	{
	public:
	SIBR_CLASS_PTR(RepetitionsReflectionRenderer);

		/**
		 * Renderer constructor.
		 * \param w Width of the internal rendering resolution.
		 * \param h Height of the internal rendering resolution.
		 */
		RepetitionsReflectionRenderer(uint w, uint h);

		/**
		 * Perform rendering.
		 * \param mask A binary mask OpenGL texture, in the white areas the reflection will be applied.
		 * \param eye The current camera
		 * \param mesh The mesh onto which the reflections should be applied.
		 * \param envmap An OpenGL texture containing an equirectangular environment map.
		 * \param normal The normal to the plane of reflection.
		 * \param envmapMat A transformation to apply to the environment map.
		 * \param dst The destination render target.
		 */
		void process(GLuint mask, const Camera& eye, const Mesh& mesh, GLuint envmap, const Vector3f& normal,
		             const Matrix4f& envmapMat, IRenderTarget& dst);

		/**
		 * Update the value of the Fresnel grazing angle coefficient.
		 * \param fresn The new coefficient value.
		 */
		void updateFresnel(const float fresn) { _paramFresnel += fresn; }

		/**
		 * Obtain the value of the Fresnel grazing angle coefficient.
		 * \return The coefficient value.
		 */
		float getFresnel() { return _paramFresnel; }

	private:
		
		GLShader _shader;
		GLShader _shaderGeom;
		GLParameter _paramMVP;
		GLParameter _paramEnvmapMat;
		GLParameter _paramShift;
		GLParameter _paramCamPosWorld;
		GLParameter _paramScale;
		GLParameter _paramMini;
		GLParameter _paramNormal;
		GLuniform<float> _paramFresnel;

		RenderTargetRGB32F::Ptr _gbuffer;
	};
} /*namespace sibr*/

#endif // __SIBR_EXP_REPETITIONS_REFLECTIONRENDERER_HPP__
