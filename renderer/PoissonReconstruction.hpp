/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#ifndef POISSONRECONSTRUCTION_H
#define POISSONRECONSTRUCTION_H

#include "Config.hpp"
#include <core/graphics/Image.hpp>

class SIBR_FACADE_REPETITIONS_EXPORT PoissonReconstruction
{

private:
	cv::Mat _img_target;
	cv::Mat _gradientsX; 
	cv::Mat _gradientsY; 
	cv::Mat _mask;

	std::vector<sibr::Vector2i> _pixels;
	std::vector<sibr::Vector2i> _boundaryPixels;
	std::vector<int > _pixelsId;
	std::vector<std::vector<int> > _neighborMap;

	/* parse the mask and the additional label condition into a list of pixels to modified and boundaries conditions */
	void parseMask( void );

	/* make sure that every modified pixel is connected to some boundary condition, all non connected pixels are discarded */
	void checkConnectivity( void ) ;

	/* HACK to fill isolated black pixels */
	void postProcessing(void);

	/**
	 * Mask: 0 -> in reconstruction, 1 -> keep fixed., -1 -> ignore completely
	 */
	bool isInMask( sibr::Vector2i & pos);

	bool isIgnored(sibr::Vector2i & pos);

public:
	PoissonReconstruction(void);

	/**
		labels assigns for each pixel a label, which has a double purpose.
		first the solver will force a zero gradient between pixels with different labels ( l>=0 )
		secondly, pixels will negative labels will be ignored by the solver
		 that means their color value will not be modified and they will have no impact on edited pixels 
	**/

	PoissonReconstruction( 
		const cv::Mat & gradientsX, 
		const cv::Mat & gradientsY, 
		const cv::Mat & mask,
		cv::Mat & img_target
		);

	void solve( void );

	cv::Mat result(){ return _img_target; }

	static std::vector< sibr::Vector2i > getNeighbors( sibr::Vector2i pos, int cols, int rows );

	~PoissonReconstruction(void);
};

#endif // POISSONRECONSTRUCTION_H