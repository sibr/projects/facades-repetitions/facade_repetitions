/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#include "KIPPIGraph.hpp"
#include <core/system/Utils.hpp>
#include <core/system/Vector.hpp>
#include <core/raycaster/Intersector2D.h>
#include <fstream>


KIPPIGraph::KIPPIGraph(const std::string & path)
{

	std::ifstream graphFile(path);
	if (!graphFile.is_open()) {
		SIBR_WRG << "Unable to load KIPPI graph at path " << path << std::endl;
		return;
	}
	graphFile >> _h >> _w; // Height and Width are inverted.
	size_t numVertices, numEdges, numFaces;
	graphFile >> numVertices >> numEdges >> numFaces;

	std::string dummyString;
	unsigned int dummyInt;

	// Load vertices.
	_vertices.resize(numVertices);
	graphFile >> dummyString;
	for (size_t vid = 0; vid < numVertices; ++vid) {
		graphFile >> dummyInt >> _vertices[vid][0] >> _vertices[vid][1];
		// We might have to flip the Y axis.
		_vertices[vid][1] = _h - _vertices[vid][1];
	}

	// Load edges (we'll need them temporarily).
	std::vector<sibr::Vector2u> edges(numEdges);
	graphFile >> dummyString;
	for (size_t eid = 0; eid < numEdges; ++eid) {
		graphFile >> dummyInt >> edges[eid][0] >> edges[eid][1];
	}

	// Load faces expressed as lists of oriented edges (we'll need them temporarily).
	struct OrientedEdge {
		unsigned int edgeId;
		unsigned int vertId;
	};
	unsigned int edgeId; unsigned int vertId;

	std::vector<std::vector<OrientedEdge>> faces(numFaces);
	graphFile >> dummyString;
	for (size_t fid = 0; fid < numFaces; ++fid) {
		unsigned int fCount;
		graphFile >> dummyInt >> fCount;
		faces[fid].resize(fCount);
		for (size_t vid = 0; vid < fCount; ++vid) {
			// The second number denotes which vertex of the edge to use.
			graphFile >> edgeId >> vertId;
			faces[fid][vid] = { edgeId, vertId };
		}
	}


	// We can now build superpixels.
	_superPixels.resize(numFaces);
	for (size_t sid = 0; sid < numFaces; ++sid) {
		std::vector<unsigned int> verticesIds(faces[sid].size());
		sibr::Vector2f centroid(0.0f, 0.0f);
		Eigen::AlignedBox2i bbox;
		for (size_t vid = 0; vid < verticesIds.size(); ++vid) {
			const auto vef = faces[sid][vid];
			verticesIds[vid] = edges[vef.edgeId][vef.vertId];
			// Centroid & BBox
			const sibr::Vector2f currVert = _vertices[verticesIds[vid]];
			centroid += currVert;
			bbox.extend(currVert.cast<int>());
		}
		// Dilate the bbox slightly.
		sibr::Vector2i mini = (bbox.min() - sibr::Vector2i(1, 1)).cwiseMax(sibr::Vector2i(0,0));
		sibr::Vector2i maxi = (bbox.max() + sibr::Vector2i(1, 1)).cwiseMin(sibr::Vector2i(_w-1, _h-1));
		bbox.extend(mini); bbox.extend(maxi);

		_superPixels[sid].vertices = verticesIds;
		//_superPixels[sid].centroid = centroid / (float)verticesIds.size();
		_superPixels[sid].bbox = bbox;
		_superPixels[sid].noDepth = true;
		//_superPixels[sid].neighbours = std::set<unsigned int>();
		//_superPixels[sid].avgColor = sibr::Vector3f(0.0f,0.0f,0.0f);
		//_superPixels[sid].current3D = ;
	}

	// Compute adjacency.
	// bruteforce.
	_graphEdges.clear();
	for (unsigned int fid0 = 0; fid0 < numFaces; ++fid0) {
		const auto f0 = faces[fid0];


		for (unsigned int eid0 = 0; eid0 < f0.size(); ++eid0) {
			const auto e0 = f0[eid0];
			bool found = false;

			for (unsigned int fid1 = fid0 + 1; fid1 < numFaces; ++fid1) {
				const auto f1 = faces[fid1];

				for (unsigned int eid1 = 0; eid1 < f1.size(); ++eid1) {
					const auto e1 = f1[eid1];
					if (e0.edgeId == e1.edgeId) {
						// Register edge (mainly for hole/seam filling when generating the mesh).
						_graphEdges.push_back({ fid0, fid1, edges[e0.edgeId][e0.vertId], edges[e1.edgeId][e1.vertId] });
						// Register respetive neighbours.
						_superPixels[fid0].neighbours.insert(fid1);
						_superPixels[fid1].neighbours.insert(fid0);
						found = true;
						break;
					}
				}

				// TODO: make sure we can assume 1-1 mapping on edges.
				if (found) {
					break;
				}
			}
		}
	}

}

bool KIPPIGraph::isInside(const unsigned int spid, const unsigned int x, const unsigned int y)
{
	return isInside(_superPixels[spid], x, y);
}

bool KIPPIGraph::isInside(const KIPPISuperPixel & sp, const unsigned int x, const unsigned int y)
{
	// Test all triangles (we know the polygons are convex).
	// If the region is delimited by n vertices, we can build a fan of n-2 triangles and test them.
	for (size_t tid = 0; tid < sp.vertices.size() - 2; ++tid){
		const sibr::Vector2f v1 = _vertices[sp.vertices[0]];
		const sibr::Vector2f v2 = _vertices[sp.vertices[tid+1]];
		const sibr::Vector2f v3 = _vertices[sp.vertices[tid+2]];

		if (sibr::Intersector2D::PointInTriangle(sibr::Vector2f(x, y), v1, v2, v3)) {
			return true;
		}
	}
	return false;
}
